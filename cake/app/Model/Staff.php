<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class Staff extends AppModel {

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'ユーザーIDは必ず入力してください'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'すでに使用されているユーザーIDのため、使用できません'
            ),
            'alphanumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'ユーザーIDは半角英数字で指定してください'
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'パスワードは必ず入力してください'
            ),
            'alphanumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'パスワードは半角英数字で指定してください'
            ),
        ),
        'showname' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => '表示名は必ず入力してください'
            ),
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array(0, 1)),
                'message' => '役割を選択してください',
                'allowEmpty' => false
            )
        )
    );


}
