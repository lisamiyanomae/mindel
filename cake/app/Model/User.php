<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel
{
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
    /*
    public $field_jps = array(
        'username'               =>  'ログインID',
        //'password'               =>  'パスワード',
        //'status'                 =>  'ステータス'            ,
        'company_name'           =>  '会社名または屋号'      ,
        'company_kana'           =>  '会社名または屋号カナ'  ,
        'responsible_name'       =>  '代表者/担当者名'       ,
        'responsible_kana'       =>  '代表者/担当者名カナ'   ,
        'zip'                    =>  '郵便番号'              ,
        'prefecture'             =>  '都道府県'              ,
        'address'                =>  '住所'                  ,
        //'building'               =>  '建物'                  ,
        'email'                  =>  'メールアドレス'        ,
        'shop_name'              =>  '店舗名'                ,
        'purpose'                =>  '利用目的'              ,
        'user_telno'             =>  'ご連絡先Tel'           ,
        'confirmation_filename'  =>  '本人確認書類写し(画像)',
    );
    */
    public $validate_basic = array(
       'password' => array(             
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'パスワードは必ず入力してください'
            ),
            'alphanumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'パスワードは半角英数字で指定してください'
            ),
        ),

        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'ログインIDは必ず入力してください'
            ),
            
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'このログインIDはすでに使用されています、別のIDを指定してください'
            ),
            
            'alphanumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'ログインIDは半角英数字で指定してください'
            ),
        ),
        /*
        'company_name' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('maxLength',50),
                'message' => '会社名・契約社名は必ず入力して下さい。',
        ),
        'company_kana' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('maxLength',100),
                'message' => 'カイシャメイ・ケイヤクシャメイは必ず入力して下さい。',
        ),
        */
        'responsible_name' => array(
            
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('maxLength',123),
                'message' => '担当者名は必ず入力して下さい。',
            
        ),
        
        'responsible_kana' => array(
            
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('maxLength',123),
                'message' => '担当者名カナは必ず入力して下さい。',
            
        ),

    
        
        'email' => array(
            
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'メールアドレスは必ず入力してください'
            ),
            
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'このメールアドレスはすでに登録されています。'
            ),
            
            'mail' => array(
                'required' => false,
                'allowEmpty' => true,
                'rule' => array('email',true),
                'message' => '正しいメールアドレスを入力して下さい。',
            ),
        ),
        'shop_name' => array(
            
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('maxLength',123),
                'message' => '店舗名は125文字以内で必ず入力して下さい。',
            ),
            
        ),
       

        
    );
    
    
    // 更新
    public $validate = array(

        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'ログインIDは必ず入力してください'
            ),
            
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'このログインIDはすでに使用されています、別のIDを指定してください'
            ),
            
            'alphanumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'ログインIDは半角英数字で指定してください'
            ),
        ),
        
        
        
        'responsible_name' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('maxLength',123),
                'message' => '担当者名は125文字以内で必ず入力して下さい。',
            ),
        ),
        
        'responsible_kana' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('maxLength',123),
                'message' => '担当者名カナは125文字以内で必ず入力して下さい。',
            ),
        ),
        'zip' => array(
            'required' => false,
            'allowEmpty' => true,
            'rule' => '/^\d{3}\-\d{4}$/i',
            'message' => '郵便番号は半角数字とハイフンで正しく入力して下さい。',
        
        ),
        
        'prefecture' => array(
            'default' => array(
                'rule' => 'notBlank',
                'required' => true,
                'message'  => '住所(都道府県)は必ず選択してください',
            ),
            
        ),
        
        'address' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('maxLength',100),
                'message' => '住所は100文字以内で必ず入力して下さい。',
            ),
            
        ),
   
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'メールアドレスは必ず入力してください'
            ),
            
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'このメールアドレスはすでに登録されています。'
            ),

            'mail' => array(
                'required' => false,
                'allowEmpty' => true,
                'rule' => array('email',true),
                'message' => '正しいメールアドレスを入力して下さい。',
            ),
        ),
        'shop_name' => array(
            
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => array('maxLength',123),
                'message' => '店舗名は125文字以内で必ず入力して下さい。',
            ),
            
        ),
        
        
        'user_telno' => array(
            
            'default' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^[0-9-]{7,14}$/i',
                'message' => 'ご連絡先電話番号は半角数字で必ず入力して下さい。',
            ),
            
        ),
        
        'purpose' => array(
            'default' => array(
                'rule' => 'notBlank',
                'message' => '利用目的は必ず入力して下さい。',
            ),
            
        ),
        'image_name' => array(
            'upload-file' => array( 
                'rule' => array( 'uploadError'),
                'message' => array( 'Error uploading file')
            ),
            
            'extension' => array(
                'rule' => array('extension',array(
                                                'gif', 
                                                'jpg', 
                                                'jpeg', 
                                                'png', 
                                                'bmp',
                                                'pdf',
                                                )),
                'message' => array( '指定のファイル形式ではありません(gif, jpg, jpeg, png, bmp, pdf)'),
            ),
            'mimetype'    => array( 
                'rule'    => array( 'mimeType', array( 
                                            'image/gif',
                                            'image/jpeg',
                                            'image/png',
                                            'image/bmp',
                                            'application/pdf',
                                            ) // MIMEタイプを配列で定義
                ),
                'message' => array( 'MIME type エラー')
            ),
            'size' => array(
                'maxFileSize' => array( 
                    'rule' => array( 'fileSize', '<=', '6MB'),  // 6M以下
                    'message' => array( 'ファイルサイズがオーバーしています')
                ),
                'minFileSize' => array( 
                    'rule' => array( 'fileSize', '>',  0),    // 0バイトより大
                    'message' => array( 'file size error')
                ),
            ),
            
        ),
        'image_name2' => array(
            'upload-file' => array( 
                'rule' => array( 'uploadError'),
                'message' => array( 'Error uploading file')
            ),
            
            'extension' => array(
                'rule' => array('extension',array(
                                                'gif', 
                                                'jpg', 
                                                'jpeg', 
                                                'png', 
                                                'bmp',
                                                'pdf',
                                                )),
                'message' => array( '指定のファイル形式ではありません(gif, jpg, jpeg, png, bmp, pdf)'),
            ),
            'mimetype'    => array( 
                'rule'    => array( 'mimeType', array( 
                                            'image/gif',
                                            'image/jpeg',
                                            'image/png',
                                            'image/bmp',
                                            'application/pdf',
                                            ) // MIMEタイプを配列で定義
                ),
                'message' => array( 'MIME type エラー')
            ),
            'size' => array(
                'maxFileSize' => array( 
                    'rule' => array( 'fileSize', '<=', '6MB'),  // 6M以下
                    'message' => array( 'ファイルサイズがオーバーしています')
                ),
                'minFileSize' => array( 
                    'rule' => array( 'fileSize', '>',  0),    // 0バイトより大
                    'message' => array( 'file size error')
                ),
            ),
            
        ),
    );
    
    public $validate_tropo = array(
    /*    'tropo_apikey' => array(
            'default' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'alphaNumeric',
                'message' => 'TROPOAPIキーは半角英数字で必ず入力して下さい。',
            ),
            
        ),
        'tropo_telno' => array(
            'default' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => array( 'custom', '/^0\d{1,4}-\d{1,4}-\d{4}$/'),
                'message' => '発行番号は半角数字で必ず入力して下さい。',
            ),
            
        ),
    */
    );
    
    /* 自動応答設定 */
    public $validate_answer = array(
    /*
        'message_from' => array(
            'default' => array(
                'required' => true,
                'allowEmpty' => false,
                'message' => '応答メッセージ開始時刻は必ず入力して下さい。',
            )
        ),
        'message_to' => array(
            'default' => array(
                'required' => true,
                'allowEmpty' => false,
                'message' => '応答メッセージ終了時刻は必ず入力して下さい。',
            )
        ),
        'message' => array(
            'default' => array(
                'required' => true,
                'allowEmpty' => false,
                'message' => '応答メッセージは必ず入力して下さい。',
            )
        ),
        */
    );
    
    /**
     * アクティベーション用ハッシュを生成する
     *
     * @return string
     */
    function createActivationHash($length=16)
    {
        if (!isset($this->id)) return false;
        return substr(Security::hash(Configure::read('Security.salt') . $this->field('created') . date('Ymd')), 0, $length);
    }

    /**
     * パスワードを再発行する
     *
     * @return string
     */
    function publishPassword($id="")
    {
        $password = StringUtility::createRandomText(10);
        if ($id) $this->save(array('id'=>$id, 'password'=>Security::hash(Configure::read('Security.salt') . $password)),false);
        return $password;

    }

    /**
     * パスワードリセットコードを発行する
     * @param integer $id
     * @return string
     */
    function publishPasswordResetCode($id)
    {
        $code = Security::hash(Configure::read('Security.salt') . StringUtility::createRandomText(20));
        $this->save(array('id'=>$id, 'password_reset_code'=>$code),false);
        return $code;
    }

    /**
     * パスワードをアップデートする
     * @param integer $id
     * @param string $password
     * @param boolean $isClearResetCode
     * @return boolean
     */
    function updatePassword($id, $password, $isClearResetCode=true)
    {
        $params = array('id'=>$id, 'password'=>$password);
        if ($isClearResetCode) $params['password_reset_code'] = null;
        return $this->save($params,false);
    }

    /**
     * パスワードバリデーション
     *
     * @param string $password
     * @param boolean $isNotEmpty
     * @return boolean
     */
    function isPassword($password="", $isNotEmpty=true)
    {
        if (!$password) {
            if ($isNotEmpty) {
                $this->invalidate('plain_password', "パスワードを入力してください");
                return false;
            } else {
                return false;
            }
            return true;
        }
        if (!stringUtility::isRangeLengthIn($password, 4, 20)) {
            $this->invalidate('plain_password', "パスワードは4文字以上20文字以内で入力してください");
            return false;
        }
        return true;
    }


}