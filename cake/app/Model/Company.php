<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class Company extends AppModel {

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'ユーザーIDは必ず入力してください'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'このユーザーIDはすでに使用されています、別のIDを指定してください'
            ),
            'alphanumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'ユーザーIDは半角英数字で指定してください'
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'パスワードは必ず入力してください'
            ),
            'alphanumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'パスワードは半角英数字で指定してください'
            ),
        ),
        'showname' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => '表示名は必ず入力してください'
            ),
        ),
        'email' => array(
            
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'メールアドレスは必ず入力してください'
            ),
            
            'mail' => array(
                'required' => false,
                'allowEmpty' => true,
                'rule' => array('email',true),
                'message' => '正しいメールアドレスを入力して下さい。',
            ),
        ),
    );


}
