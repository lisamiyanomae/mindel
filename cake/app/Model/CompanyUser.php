<?php
App::uses('AppModel', 'Model');
class CompanyUser extends AppModel {

    public $validate = array(
        'user_id' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'ユーザーIDは必ず指定してください'
            ),
        ),
    );


}
