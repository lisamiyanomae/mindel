<?php
App::uses('Controller', 'Controller');

// 6か月以前のデータを削除する
class DeleteLogShell extends AppShell
{
    public $uses = array('Telephones');
    
    function startup()
    {
        parent::startup();

    }

    public function main()
    {
        $sql = "DELETE FROM telephones WHERE (created < DATE_SUB(CURDATE(), INTERVAL 6 MONTH));";
        if($this->Telephones->query($sql)===false){
            $this->out('fail');
        }else{
            $this->out('success');
        }
    }
    
}