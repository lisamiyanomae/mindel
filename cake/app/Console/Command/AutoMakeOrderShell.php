<?php
App::uses('Controller', 'Controller');
App::uses('OrdersController', 'Controller');
App::uses('ComponentCollection', 'Controller');
App::uses('CpOrderComponent', 'Controller/Component'); 

class AutoMakeOrderShell extends AppShell
{
    
    public $uses = array('Order','OrderErr');
    
    //public $components = ['CpOrder'];

    function startup()
    {
        // コンポーネント読み込み
        $collection = new ComponentCollection(); 
        
        // コントローラー設定
        $this->OrdersController = new OrdersController();
        $this->CpOrder = new CpOrderComponent($collection); 
        
        
        parent::startup();

    }

    // 週次処理
    public function main()
    {
        $update_data = array();
        
        // 請求データ作成
        $datas = $this->OrdersController->set_order_weekly();        
        
        
        if(!empty($datas)){
            
            // 対象期間を取得
            $start_day = $datas[0]['Order']['start_date'];
            $end_day   = $datas[0]['Order']['end_date'];
            
            
            // 請求データ登録
            $pay_users = $this->OrdersController->invoicing_weekly_make($datas,$start_day);
            
            
            // 定期課金(月次・週次)決済処理
            $update_data = $this->CpOrder->periodic_claim($pay_users);
            
            
            // 決済結果をDB登録
            if(array_key_exists("Order",$update_data)){
                
                // タイムアウトしないよう処理を変更 201812
                $tmp = array();
                foreach($update_data['Order'] AS $update){
                    $tmp[] = $update['id'];
                }
                $this->Order->Chdb('bluepond79_wp1'); // 再接続 201812
                
                $sql = "UPDATE orders  SET pay_date= NOW() ,modified = NOW() WHERE id IN( ". implode($tmp , ",") .")";
                $ret = $this->Order->query($sql);
                //$this->log($sql, LOG_DEVELOP);
                
                if($ret === false){
                //if (!$this->Order->saveall($update_data['Order'])) {
                    $this->log('check month update pay_date ng', LOG_DEVELOP);
                    $this->log($order, LOG_DEVELOP);
                }
            }
            if(array_key_exists("OrderErr",$update_data)){
                if (!$this->OrderErr->saveall($update_data['OrderErr'])) {
                    $this->log('check month  ng', LOG_DEVELOP);
                    $this->log($order, LOG_DEVELOP);
                }
            }
            
            // メール送信
            $ret = $this->OrdersController->invoicing_weekly_mail($start_day);
            
            
        }
        
        $this->out('success');

    }
    
    // 月次処理
    public function monthly()
    {
        $update_data = array();
        
        // 請求データ作成
        $datas = $this->OrdersController->set_order_monthly();
        
        if(!empty($datas)){
            
            // 対象期間を取得
            $start_day = $datas[0]['Order']['start_date'];
            $end_day   = $datas[0]['Order']['end_date'];
            
            
            // 請求データ登録
            $pay_users = $this->OrdersController->invoicing_monthly_make($datas,$start_day);
            
            
            // 定期課金(月次・週次)決済処理
            $update_data = $this->CpOrder->periodic_claim($pay_users);
            
            
            // 決済結果をDB登録
            if(array_key_exists("Order",$update_data)){
                
                // タイムアウトしないよう処理を変更 201812
                $tmp = array();
                foreach($update_data['Order'] AS $update){
                    $tmp[] = $update['id'];
                }
                $this->Order->Chdb('bluepond79_wp1'); // 再接続 201812

                $sql = "UPDATE orders  SET pay_date= NOW() ,modified = NOW() WHERE id IN( ". implode($tmp , ",") .")";
                $ret = $this->Order->query($sql);
                $this->log($sql, LOG_DEVELOP);
                
                if($ret === false){
                //if (!$this->Order->saveall($update_data['Order'])) {

                    $this->log('check month update pay_date ng', LOG_DEVELOP);
                    $this->log($order, LOG_DEVELOP);
                }
            }
            if(array_key_exists("OrderErr",$update_data)){
                if (!$this->OrderErr->saveall($update_data['OrderErr'])) {
                    $this->log('check month  ng', LOG_DEVELOP);
                    $this->log($order, LOG_DEVELOP);
                }
            }
            
            // メール送信
            $ret = $this->OrdersController->invoicing_monthly_mail($start_day);
            
            
        }
        
        $this->out('success');

    }

}