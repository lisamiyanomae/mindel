<?php
// Twilio
App::uses('AppController', 'Controller');
App::import('Vendor', '/Twilio/Twilio/autoload');
App::import('Vendor', 'Twiml',array('file'=>'Twilio/Twilio/Twiml.php') );
App::uses('CakeEmail', 'Network/Email');

class TwilioController extends AppController {
    
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();                
        $this->Auth->allow('index','digits','test','complete_to_customer','complete_to_shop','complete_to_shop_action','timeout_recording_save','timeout_recording_end');
    }
    

    
    // 通話終了のコールバック
    public function complete_to_customer($id = null,$telephone_id = null) {
        
        $this->autoRender = false;
        
        //$this->log($_POST, LOG_DEVELOP);
        
        // 課金種別 応答メッセージ0 IP電話1 携帯電話2 一般電話3
        $kind = 8;
        if($_POST['CallStatus'] =='completed'){
            if(substr($_POST['Called'],0,5) == '+8150'){
                $kind = 5;
            }elseif(substr($_POST['Called'],0,5) == '+8170'||substr($_POST['Called'],0,5) == '+8180' ||substr($_POST['Called'],0,5) == '+8190'){
                 $kind = 6;
            }else{
                $kind = 7;
            }
        }
        
        // 切断を登録
        $this->loadModel('Telephone');
        $data = array(
            "id"                => $telephone_id , 
            "user_id"           => $id , 
            //"teltransferno"     => $_POST['Called'],
            "kind"              => $kind,
            "end_time"          => date('Y-m-d H:i:s', strtotime($_POST['Timestamp'])) ,
        );
        if (!$this->Telephone->save($data)) {
            $this->log('teltransfer complete ng: id = '.$id.' :telephone_id = '. $telephone_id, LOG_DEVELOP);
        }
        
        //$this->log($data, LOG_DEVELOP);
    
    }
    public function complete_to_shop($id = null,$telephone_id = null) {
        
        $this->autoRender = false;
        
        //$this->log($_POST, LOG_DEVELOP);
        
        // 課金種別 応答メッセージ0 IP電話1 携帯電話2 一般電話3
        $kind = 4;
        if($_POST['CallStatus'] =='completed'){
            if(substr($_POST['Called'],0,5) == '+8150'){
                $kind = 1;
            }elseif(substr($_POST['Called'],0,5) == '+8170'||substr($_POST['Called'],0,5) == '+8180' ||substr($_POST['Called'],0,5) == '+8190'){
                $kind = 2;
            }else{
                $kind = 3;
            }


            // 切断を登録
            $this->loadModel('Telephone');
            $data = array(
                "id"                => $telephone_id , 
                "user_id"           => $id , 
                "teltransferno"     => $_POST['Called'],
                "kind"              => $kind,
                "end_time"          => date('Y-m-d H:i:s', strtotime($_POST['Timestamp'])) ,
            );
            if (!$this->Telephone->save($data)) {
                $this->log('teltransfer complete ng: id = '.$id.' :telephone_id = '. $telephone_id, LOG_DEVELOP);
            }
        }
        //$this->log($data, LOG_DEVELOP);
    
    }
    // タイムアウト処理
    public function complete_to_shop_action($id = null,$telephone_id = null) {
        
        $this->autoRender = false;
        
        $response = new Twiml();
        
        //if( $_POST['CallStatus'] == 'in-progress' && $_POST['DialCallStatus'] == 'no-answer' ){
         
        if(($_POST['CallStatus'] == 'in-progress' && $_POST['DialCallStatus']   == 'no-answer' ) ||
           ($_POST['CallStatus'] == 'in-progress' && $_POST['DialCallStatus']   == 'busy'      ) ||
           ($_POST['CallStatus'] == 'in-progress' && $_POST['DialCallStatus']   == 'failed'    )){
        
            $this->loadModel('Voicemail');
            $message = $this->Voicemail->find('first', array(
                        'conditions' => array('user_id' => $id),
                        'fields' => array('timeout_message','timeout_seconds','record_use','autovoice_mail_use2','autovoice_mail'),
               ));
            if($message['Voicemail']['timeout_seconds'] > 0){
                
                $response->say($message['Voicemail']['timeout_message'], array('voice' => 'Polly.Mizuki','language' => 'ja-jp')); 

                // 自動応答を登録
                $this->loadModel('Telephone');
                $data = array(
                    "id"                => $telephone_id , 
                    "kind"              => 0,
//                    "end_time"          =>date('Y-m-d H:i:s'),  // action からの処理にはtimestampが入らないのでサーバー時間をセット
                );
                if (!$this->Telephone->save($data)) {
                    $this->log('teltransfer complete ng: id = '.$id.' :telephone_id = '. $telephone_id, LOG_DEVELOP);
                }
                
                
                // 留守電
                if($message['Voicemail']['record_use'] == 1){
                    $response->record(array('action'=>'https://minderu.com/mypage/twilio/timeout_recording_end','recordingStatusCallback' => "https://minderu.com/mypage/twilio/timeout_recording_save/$id/$telephone_id", 'finishOnKey' => '*'));
                }

                // 不在着信をメール送信
               if($message['Voicemail']['autovoice_mail_use2'] == 1){

                    $this->loadModel('User');
                    $user = $this->User->find('first', array(
                            'conditions' => array('id' => $id),
                            'fields' => array('email','shop_name','responsible_name','username'),
                   ));
                    $to = $user['User']['email'];
                    if($message['Voicemail']['autovoice_mail']){
                        $to = $message['Voicemail']['autovoice_mail'];
                    }

                    $email = new CakeEmail('smtp');
                    
                    $email->to($to);
                    $userLoginUrl = Router::url('/telephones/', true);
                    $email->emailFormat('text');
                    $email->template('twilio_no_responce');
                    $email->viewVars(array(
                      
                      'shop_name'              => $user['User']['shop_name'],
                      'responsible_name'       => $user['User']['responsible_name'],
                      'user_login_url'         => $userLoginUrl,
                      'loginid'                => $user['User']['username'],
                      'email'                  => $user['User']['email']
                      
                    ));
                    $email->subject('【'.SITE_NAME.'】不在着信がありました');
                    $email->send();
                
                }
            }

        }
        
        $this->response->type('text/xml');
        $this->response->body($response);
        return $this->response;

    }

    // 留守電
    public function timeout_recording_save($id = null , $telephone_id = null) {
        
        $this->autoRender = false;
    
        // 留守電音声URLを登録
        $this->loadModel('Telephone');
        $data = array(
            "id"                => $telephone_id , 
            "recording_url"     => $_POST['RecordingUrl'] , 
            "end_time"          =>date('Y-m-d H:i:s'),  
        );
        if (!$this->Telephone->save($data)) {
            $this->log('timeout_recording_save ng: id = '.$id.' :telephone_id = '. $telephone_id, LOG_DEVELOP);
        }
    
    }
    public function timeout_recording_end() {
    
        $this->autoRender = false;
        $response = new Twiml();
        
        $response->hangup();
        
        $this->response->type('text/xml');
        $this->response->body($response);
        return $this->response;
        
    
    }
    
    public function index($id = null) {
    
        $this->autoRender = false;
        $this->loadModel('User');
        $this->loadModel('Telephone');
        
        // Twilioクラスの初期化
        $response = new Twiml();
        if (!$id) {
            throw new NotFoundException(__('Invalid Twilio'));
        }
        
         // 初回開通確認
        $ret = $this->User->find('all', array('conditions' => array('id' => $id),'fields' => array('status_5_date','shop_name','status'),));
        if(!$ret[0]['User']['status_5_date']){
            $data = array(
                "id"                => $id , 
                "status_5_date"     => date('Y-m-d H:i:s') ,
            );
            if (!$this->User->save($data , false)) {
                $this->log('status_5_date update ng: id = '.$id, LOG_DEVELOP);
            }
            $response->say($ret[0]['User']['shop_name']."様の転送番号設定を、完了します。",['voice' => 'Polly.Mizuki', 'language' => 'ja-jp']); 
            $response->say("みんなにでんわ転送運営センターにて「開通テスト終了」ボタン を、押してください。",['voice' => 'Polly.Mizuki', 'language' => 'ja-jp']); 

        
        // 停止(ステータス8)の場合
        }elseif($ret[0]['User']['status'] == 8){
            $response->say("この電話番号は現在、お客様の都合で通話ができません",['voice' => 'Polly.Mizuki', 'language' => 'ja-jp']); 
        
        // サービス提供中(ステータス5)以外
        }elseif($ret[0]['User']['status'] != 5){
            $response->say("この電話番号は現在、ご利用できません",['voice' => 'Polly.Mizuki', 'language' => 'ja-jp']); 
        
        }else{
            
            // 登録された番号からの受電によりトーンナンバーが押された場合の処理
            if (array_key_exists('Digits', $_POST)) {
            
                $tel_no = $_POST['Digits'];
                
                //$this->log($_POST, LOG_DEVELOP);
                
                // 発信を登録
                $data = array(
                    "user_id"      => $id , 
                    "start_time"   => date("Y-m-d H:i:s"),
                    "telno"        => $_POST['From'],
                    "kind"         => 8,
                    "teltransferno"=> $tel_no,
                    
                );
                if (!$this->Telephone->save($data)) {
                    $this->log('teltransfer ng:'.$_POST['Called'].' to '. $tel_no, LOG_DEVELOP);
                }
                
                
                if($tel_no == "123"){
                    $response->say("Never give up.");
                    $response->say("You can do it!");
                    $response->say("アリスです。今は皆さんのお手伝いをしています。",['language' => 'ja-jp']); 
                    $response->say("私はみずきです、よろしくお願いします。開発確認のお手伝いをしています。",['voice' => 'Polly.Mizuki', 'language' => 'ja-jp']); 
                    $response->say("僕はたくみです。二人はポリーの友達です。僕が一番日本語がうまいんじゃないかと思いますが、いかがですか？",['voice' => 'Polly.Takumi', 'language' => 'ja-jp']); 
                
                // 登録番号からトーンした番号へ発信
                }else{
                    
                    
                    $response->say( CONNECT_SAY ); 
                    
                    //$response->say("おつなぎします。",['language' => 'ja-jp']); 
                
                    if(substr($tel_no,0,1) == '0'){
                        $tel_no = '81'. ltrim($tel_no, '0'); 
                    }
                    $dial = $response->dial(array('callerId' => $_POST['Called']));

                    $dial->number($tel_no , array('statusCallback'=>'https://minderu.com/mypage/twilio/complete_to_customer/'.$id.'/'.$this->Telephone->id ,'statusCallbackMethod'=>'POST',"statusCallbackEvent"=>"answered completed"));
            
                }
            
            }else{
            
                // 着信番号の取得
                $caller_id = $_POST['From'];//+81457892243

                // 転送先番号の取得
                $this->loadModel('Teltransfer');
                $tel_list = $this->Teltransfer->find('list', array(
                        'conditions' => array('user_id' => $id,'active'=>1),
                        'fields' => array('telno'),
                    ));
                
                // 発信者番号の取得(activeに関せず全設定番号)
                $tel_list_all = $this->Teltransfer->find('list', array(
                        'conditions' => array('user_id' => $id),
                        'fields' => array('telno'),
                    ));
                    
                    
                // 番号内のハイフンを削除
                $tel_list     = str_replace('-','',$tel_list);
                $tel_list_all = str_replace('-','',$tel_list_all);
                
                // 着信番号を +8122223333 から 022223333 の形式に変換
                $caller_id = "0". substr( $caller_id , 3 , strlen($caller_id) - 3 );
                
                // 着信が設定された転送先なら、発信処理
                if(in_array($caller_id,$tel_list_all)){
                
                    $gather = $response->gather(array('numDigits' => 11, 'timeout' => '60'));
                    $gather->say("発信番号を入力して、sharp、を押してください", array('voice' => 'Polly.Mizuki','language' => 'ja-jp'));

                }else{
                
                
                    
                    // 現在時刻の取得と自動応答時間の比較
                    $strTime = strtotime(date('H:i:s'));
                    $this->loadModel('Voicemail');
                    $datetime = new DateTime();
                    $w = (int)$datetime->format('w');
                    $ret = $this->Voicemail->find('all', array(
                                'conditions' => array('user_id'=> $id ),
                                'fields'     => array('teltransfer_day_0'.$w ,'teltransfer_from_0'.$w,'teltransfer_to_0'.$w,'message','switch','timeout_seconds','record_use','autovoice_mail_use1','autovoice_mail') 
                            )
                        );
                    
                    $message = null;
                    
                    if($ret){
                        if($ret[0]['Voicemail']['switch'] == 0){ 
                            if( 
                                  ($ret[0]['Voicemail']['teltransfer_day_0'.$w] == 1 && (strtotime($ret[0]['Voicemail']['teltransfer_from_0'.$w]) > $strTime || strtotime($ret[0]['Voicemail']['teltransfer_to_0'.$w]) < $strTime))
                                || $ret[0]['Voicemail']['teltransfer_day_0'.$w] == 0
                                || empty($tel_list)
                                
                            ){
                                $message = $ret[0]['Voicemail']['message'];
                            }
                        }else{ 
                            if( 
                                  ($ret[0]['Voicemail']['teltransfer_day_0'.$w] == 1 && (strtotime($ret[0]['Voicemail']['teltransfer_from_0'.$w]) < $strTime && strtotime($ret[0]['Voicemail']['teltransfer_to_0'.$w]) > $strTime))
                            ){
                                $message = $ret[0]['Voicemail']['message'];
                            }
                        } 
                    }
                    
                    //自動応答時間の場合
                    if($message){
                    
                         // 発信を登録
                        $data = array(
                            "user_id"      => $id , 
                            "telno"        => $_POST['From'],
                            "start_time"   => date("Y-m-d H:i:s"),
                            
                        );
                        if (!$this->Telephone->save($data)) {
                            $this->log('twilio auto message ng: id= '.$id, LOG_DEVELOP);
                        }
                        
                        if($ret[0]['Voicemail']['message'] == ""){ $ret[0]['Voicemail']['message'] = DEFAULT_MESSAGE;}
                        $response->say($ret[0]['Voicemail']['message'], array('voice' => 'Polly.Mizuki','language' => 'ja-jp'));
                        
                         // 不在着信をメール送信
                        if($ret[0]['Voicemail']['autovoice_mail_use1'] == 1){
                            
                            $this->loadModel('User');
                            $user = $this->User->find('first', array(
                                    'conditions' => array('id' => $id),
                                    'fields' => array('email','shop_name','responsible_name','username'),
                            ));
                            
                            $to = $user['User']['email'];
                            if($ret[0]['Voicemail']['autovoice_mail']){
                                $to = $ret[0]['Voicemail']['autovoice_mail'];
                            }
                        
                            $email = new CakeEmail('smtp');
                            
                            $email->to($to);
                            $email->cc(null);
                            $userLoginUrl = Router::url('/telephones/', true);
                            $email->emailFormat('text');
                            $email->template('twilio_outof_time');
                            $email->viewVars(array(
                                
                                'shop_name'              => $user['User']['shop_name'],
                                'responsible_name'       => $user['User']['responsible_name'],
                                'user_login_url'         => $userLoginUrl,
                                'loginid'                => $user['User']['username'],
                                'email'                  => $to,
                                //'RecordingUrl'           => $RecordingUrl
                                
                                
                            ));
                            $email->subject('【'.SITE_NAME.'】時間外応答がありました');
                            $email->send();
                        }

                        // 留守電
                        if($ret[0]['Voicemail']['record_use'] == 1){
                            $telephone_id = $this->Telephone->getLastInsertID();
                            $response->record(array('action'=>'https://minderu.com/mypage/twilio/timeout_recording_end','recordingStatusCallback' => "https://minderu.com/mypage/twilio/timeout_recording_save/$id/$telephone_id", 'finishOnKey' => '*'));
                        }
                        
                        
                    // 転送時間の場合
                    }else{
                    
                        // 通話開始時間を登録
                        $data = array(
                            "user_id"      => $id , 
                            "start_time"   => date("Y-m-d H:i:s"),
                            "telno"        => $_POST['From'],
                            "teltransferno"=> '',
                            "kind"         => '10', // ワン切り対策
                            
                        );
                        if (!$this->Telephone->save($data)) {
                            $this->log('twilio log update to 10 ng:'.$caller_id, LOG_DEVELOP);
                        }
                        // タイムアウト追加
                        $timeout_seconds = $ret[0]['Voicemail']['timeout_seconds'];
                        if($timeout_seconds == 0){
                            $timeout_seconds = 30;
                        }
                        //$dial = $response->dial(array('callerId' => $_POST['Called'],"ringTone"=>"tw"));
                        $dial = $response->dial(array('callerId' => $_POST['Called'],"ringTone"=>"tw" , "timeout" => $timeout_seconds ,"action"=>"https://minderu.com/mypage/twilio/complete_to_shop_action/".$id."/".$this->Telephone->id)); 
                        foreach($tel_list AS $tel_no){
                            if(substr($tel_no,0,1) == '0'){
                                $tel_no = '+81'. ltrim($tel_no, '0'); 
                            }
                            //$dial->number($tel_no);
                            $dial->number($tel_no , array('statusCallback'=>'https://minderu.com/mypage/twilio/complete_to_shop/'.$id.'/'.$this->Telephone->id ,'statusCallbackMethod'=>'POST',"statusCallbackEvent"=>"answered completed"));
                            
                        }
                        // ステータスを転送中に
                        $data = array(
                            "id"           => $this->Telephone->id , 
                            "kind"         => '4', 
                        );
                        if (!$this->Telephone->save($data)) {
                            $this->log('twilio log update to 4 ng:'.$caller_id, LOG_DEVELOP);
                        }
                    }
                }
            }
        }
        
        
        $this->response->type('text/xml');
        $this->response->body($response);
        return $this->response;
    }

}