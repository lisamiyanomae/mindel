<?php
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

    public $uses = array( 'User');
    
    
    public function beforeFilter() {
        parent::beforeFilter();
        Security::setHash('blowfish');
        
        $this->Auth->allow('add','login','logout','thanks');
    }


    public function login($id = null) {
                
        $this->layout = 'user_add';
        
        // 案内メールからのログインの場合
        if($id){
            if ($user = $this->controller->User->findById($id)) {
                $this->request->data['User']['username'] = $user['User']['username'];
            }
        }

        if ($this->request->is('post')) {
        
            if ($this->Auth->login()) {
                $this->redirect($this->Auth->redirect());
            } else {
                $this->Flash->error('ログインIDまたはパスワードが誤っています。もう一度正しく入力してください。');
            }
        
        }
    }
    

    public function admin_index($status = 0,$flg = 0) {
    
        $teltransfer_cnts  = array();
        $conditions = array('User.delflg' => 0);
        
        // 選択リストをセット
        $this->set('user_status',Configure::read("user_status"));
        
        if ($this->request->is('post') || $status > 0) {

            $this->__sanitize();
            
            // リセットボタン押下
            if(isset($this->request->data['reset'])) {
                $this->request->data['User'] = array();
            }
            
            // ページ数リセット
            $this->request->params['named']['page'] = 1;
            
            
            if(array_key_exists('User',$this->request->data)){ 
            
                // ステータス
                if(array_key_exists('status',$this->request->data['User'])){ 
                    
                    
                    /* 複数選択できる場合
                    foreach($this->request->data['User']['status'] AS $this->user){
                        $innner_user_conditions[] = array('User.status' => $this->user);
                    }
                    $conditions = array_merge(array('OR'=>$innner_user_conditions));
                    */
                    if($this->request->data['User']['status']!=""){
                        $conditions = array_merge($conditions,array('User.status' => $this->request->data['User']['status']));
                    }
                }
                            
                // 期間from>期間to の場合は値をひっくり返す
                if(array_key_exists('start_date',$this->request->data['User'])){
                    if($this->request->data['User']['start_date'] && $this->request->data['User']['end_date']){
                        if($this->request->data['User']['start_date'] > $this->request->data['User']['end_date']){
                            $tmp_date = $this->request->data['User']['start_date'];
                            $this->request->data['User']['start_date'] = $this->request->data['User']['end_date'];
                            $this->request->data['User']['end_date']   = $tmp_date;
                        }
                    }
                }
                // 期間from
                if(array_key_exists('start_date',$this->request->data['User'])){
                    if($this->request->data['User']['start_date']){
                        $conditions = array_merge($conditions,array('User.created >=' => $this->request->data['User']['start_date'].' 00:00:00'));
                    }
                }
                // 期間to
                if(array_key_exists('end_date',$this->request->data['User'])){
                    if($this->request->data['User']['end_date']){
                        $conditions = array_merge($conditions,array('User.created <=' => $this->request->data['User']['end_date'].' 23:59:59'));
                    }
                
                }
                // 印刷
                if(array_key_exists('didprint',$this->request->data['User'])){
                    if($this->request->data['User']['didprint'] == 1){
                        $conditions = array_merge($conditions,array('User.didprint' => 0));
                    }
                 }

                // 請求書払い add
                if(array_key_exists('hand_payment',$this->request->data['User'])){
                    if($this->request->data['User']['hand_payment'] == 1){
                        $conditions = array_merge($conditions,array('User.hand_payment' => 1));
                    }
                }
                // 非課金ユーザーを除く add
                if(array_key_exists('no_payment',$this->request->data['User'])){
                    if($this->request->data['User']['no_payment'] == 1){
                        $conditions = array_merge($conditions,array('User.no_payment' => 0));
                    }
                }               
                //フリーワード
                if(!empty($this->request->data['User']['search_text'])|| array_key_exists('search_text',$_POST)){
                    $conditions = array_merge($conditions,array('or' => array(
                        array('User.username                         like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.company_name                     like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.company_kana                     like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.responsible_name                 like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.responsible_kana                 like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.zip                              like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.address                          like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.building                         like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.email                            like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.shop_name                        like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.purpose                          like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.user_telno                       like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.tropo_telno                      like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.memo                             like' => "%{$this->request->data['User']['search_text']}%"),
                        array('User.message                          like' => "%{$this->request->data['User']['search_text']}%"),
                                            )
                    ));
                    $this->Session->write('search_text', $this->request->data['User']['search_text']);
                }else{
                    $this->Session->write('search_text', '');
                }
            
            // パラメータの特定ステータスのみ絞り込み
            }else{
            
                $conditions = array_merge($conditions,array('User.status' => $status));
                
                //$innner_user_conditions[] = array('User.status' => $status);
                //$conditions = array_merge(array('OR'=>$innner_user_conditions));
                
            }
            
        }else{
            if(array_key_exists('page',$this->request->params['named'])){
                if($this->Session->check('conditions')) {
                    $conditions = $this->Session->read('conditions');
                    //if(array_key_exists('OR',$conditions)){
                    //    foreach($conditions['OR'] AS $user ){
                    //        if(array_key_exists('User.status',$user)){
                    //            $this->request->data['User']['status'][] = $user['User.status'];
                    //        }
                    //    }
                    //}
                    if(array_key_exists('User.status'    ,$conditions)){$this->request->data['User']['status']     = $conditions['User.status'];}
                    if(array_key_exists('User.created >=',$conditions)){$this->request->data['User']['start_date'] = date('Y-m-d', strtotime($conditions['User.created >=']));}
                    if(array_key_exists('User.created <=',$conditions)){$this->request->data['User']['end_date']   = date('Y-m-d', strtotime($conditions['User.created <=']));}
                    if(array_key_exists('User.didprint'  ,$conditions)){$this->request->data['User']['didprint']   = 1;}
                    if(array_key_exists('User.hand_payment',$conditions)){$this->request->data['User']['hand_payment']   = 1;}
                    if(array_key_exists('User.no_payment'  ,$conditions)){$this->request->data['User']['no_payment']     = 0;}
                }
                if($this->Session->check('search_text')) {
                    $this->request->data['User']['search_text'] = $this->Session->read('search_text');
                }
            }    
            
        }
        //print_r($conditions);
        $this->Session->write('conditions', h($conditions));
        $this->Paginator->settings = array(
            'fields' => array('User.*'),
            'conditions' => $conditions,
            'limit' => 100,
            'order' => array('User.created' => 'desc'),
        );
        
        $members = $this->Paginator->paginate('User');
        $this->set('members', $members);
        
        //
        foreach($members AS $member){
            $teltransfer_cnts[$member['User']['id']] = 0;
        }
        $this->loadModel('Teltransfer');
        $data = $this->Teltransfer->find('all',array('group'=>'user_id', 'fields'=>'user_id, count(id) as "tcnt"'));
        foreach($data AS $teltransfer_cnt){
            $teltransfer_cnts[$teltransfer_cnt['Teltransfer']['user_id']] = $teltransfer_cnt[0]['tcnt'];
        }
        $this->set('teltransfer_cnts', $teltransfer_cnts);
        
        $tstr = '変更・返金・解約・印刷';
        $icono = 4;
        $title = 'アカウント';
        if($status == 1 && $flg == 1){
            $title =  '審　査';
            $tstr =  '審査待ちが _CNT_ 件あります';
            $icono = 1;
        }elseif($status == 4 && $flg == 1){
            $title =  '番号発行';
            $tstr =  '発行待ちが _CNT_ 件あります';
            $icono = 3;
        }
        $this->set('tstr',$tstr);
        $this->set('title',$title);
        $this->set('icono',$icono);
        
        
        $this->set('status',$status);
        $this->set('flg',$flg);
      
        // 選択リストをセット
        $this->set('user_status',Configure::read("user_status"));
        $this->set('pref_list',Configure::read("pref_list"));
    
    }

    public function index() {

        if($this->auth['status'] < 4){
            $this->edit();
            $this->render('edit');
        }else{
            $this->view();
            $this->render('view');
        }

    }

    public function logout() {
        $this->redirect($this->Auth->logout());
    }
    
    // 新規登録
    public function add() {

        $this->layout = 'user_add';

        // 選択リストをセット
        $this->set('pref_list',Configure::read("pref_list"));
        
        if ($this->request->is('post') || $this->request->is('put')) {
            
            
            // ユーザー情報用バリデーションをセット
            $this->User->validate = $this->User->validate_basic;
        
            if ($this->User->save($this->request->data)) {
                
                $userLoginUrl = Router::url('/users/edit/', true);
                $userAddUrl = Router::url('/users/add/', true);
                $RepublishPasswordUrl = Router::url('/forgot_password/', true);
                
                //読み込む設定ファイルの変数名を指定
                $email = new CakeEmail('smtp');
                $email->to($this->request->data['User']['email']);

                //HTMLorテキストメール
                $email->emailFormat('text');
                $email->template('entry_user');
                $email->viewVars(array(
                  'user_login_url'         => $userLoginUrl,
                  'user_add_url'           => $userAddUrl,
                  'republish_password_url' => $RepublishPasswordUrl,
                  'loginid'                => $this->request->data['User']['username'],
                  'shop_name'              => $this->request->data['User']['shop_name'],
                  'responsible_name'       => $this->request->data['User']['responsible_name'],
                  'email'                  => $this->request->data['User']['email']
                ));
                $email->subject('【'.SITE_NAME.'】次のステップへお進みください');
                $email->send();
            
                return $this->redirect(array('action' => 'thanks/'.$this->request->data['User']['email']));
            }
            
            //print_r($this->User->validationErrors);
            
            $this->Flash->error('アカウントを作成できませんでした。エラー内容をご確認ください。');
            
        } else {
        
        }
    }
    public function thanks($email = null) {
    
        $this->layout = 'user_add';
        $this->set('email',$email);
    }



    public function delete($id = null) {
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');
        $this->autoRender = false;
        
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Flash->success('ユーザーが削除されました');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error('ユーザー削除に失敗しました');
        return $this->redirect(array('action' => 'index'));
    }
    
    public function thanks_1() {
        
        $this->layout = 'user_add';
        $this->set('email', $this->auth['email']);
        
    }
    
    public function view() {
    
        $user = $this->User->findById($this->auth['id']);
        unset($user['User']['password']);
        $this->set('user',$user['User']);
        
    }
    
    
    public function edit($basic = 0) {

        // 選択リストをセット
        $this->set('pref_list',Configure::read("pref_list"));
        $this->set('user_status',Configure::read("user_status"));
        
        // 処理のリダイレクト先
        $redirect = 'view';
        
        // 編集モードを渡す
        $this->set('basic',$basic);
        
        // 初期値をセット
        if ($this->request->is('post') || $this->request->is('put')) {
            $user = $this->request->data['User'];
            
        }else{
            $user = $this->auth;
            $this->request->data['User'] = $user;
        }
            
        if($user['status'] == 0){
        
            $this->set('bstring', 'サービスに申し込む');
            $redirect = "thanks_1";

        }elseif($user['status'] == 2){
        
            $this->set('bstring', '訂正した内容を再送信');
            $redirect = "thanks_1";
        
        }else{
            $this->set('bstring', '更新');
        }
        
        if($user['status'] < 5){
            $this->layout = 'user_add';
        }
        

         // ステータスでviewを切り替え(審査中 決済待ち TROPO設定待ち) 参照のみ
        if($user['status'] == 1 || $user['status'] == 3 || $user['status'] == 4 ){ 
            return $this->render('view');
        }
        
        // ボタン押下
        if ($this->request->is('post') || $this->request->is('put')) {
            
            $this->User->set($this->request->data);
            
           
            $file_validates = true;
        
             // ファイルのバリデート
            if(array_key_exists('image_name1',$this->request->data['User'])){
            
                if($this->request->data['User']['image_name1']){ 
                    if($this->request->data['User']['image_name1']['name'] != '' ){
                        // ファイルのバリデートのみ先に行う
                        if (!$this->User->validates(array('fieldList' => array('image_name1')))) {
                            $file_validates = false;
                        }
                    }
                }
            }
            if(array_key_exists('image_name2',$this->request->data['User'])){
            
                if($this->request->data['User']['image_name2']){ 
                    if($this->request->data['User']['image_name2']['name'] != '' ){
                        // ファイルのバリデートのみ先に行う
                        if (!$this->User->validates(array('fieldList' => array('image_name2')))) {
                            $file_validates = false;
                        }
                    }
                }
            }
            if($file_validates){

                if(array_key_exists('image_name1',$this->request->data['User'])){
                    if($this->request->data['User']['image_name1']){
                        if($this->request->data['User']['image_name1']['name'] != ''){
                        
                            // ファイル情報をセット   
                            $fileName1 = $this->request->data['User']['image_name1'];
                            
                            //保存する画像ファイル名を作成
                            $ext = substr($fileName1['name'], strrpos($fileName1['name'], '.') + 1);
                            $save_name1 = sprintf('%06d', $user['id']) . '_'.date("Y-m-d H:i:s") .'_1.'.$ext;
                            $this->request->data['User']['confirmation_filename_1'] = $save_name1;
                        
                        }
                    }
                }
                
                if(array_key_exists('image_name2',$this->request->data['User'])){
                    if($this->request->data['User']['image_name2']){
                        if($this->request->data['User']['image_name2']['name'] != ''){
                        
                            // ファイル情報をセット   
                            $fileName2 = $this->request->data['User']['image_name2'];
                            
                            //保存する画像ファイル名を作成
                            $ext = substr($fileName2['name'], strrpos($fileName2['name'], '.') + 1);
                            $save_name2 = sprintf('%06d', $user['id']) . '_'.date("Y-m-d H:i:s") .'_2.'.$ext;
                            $this->request->data['User']['confirmation_filename_2'] = $save_name2;
                        
                        }
                    }
                }
                
                unset($this->User->validate['image_name1']); // 保存時はファイル情報のバリデートを行わない
                unset($this->User->validate['image_name2']);
                
                // 保存
                $this->User->begin();
                try{
                    
                    if($this->User->validates()){
                        
                        // 印刷済フラグを初期化
                        $this->request->data['User']['didprint'] = 0;
                        
                        if($user['status'] == 0 || $user['status'] == 2 ){
                        
                            if($this->request->data['User']['image_name1']['name'] == '' ){
                                $this->User->invalidate("image_name1", "本人確認書類を登録してください。");
                                throw new InternalErrorException(); 
                            }

                            //ステータスを承認待ちに
                            $this->request->data['User']['status'] = 1;
                            
                            $user['status'] = 1;
                            
                            
                            // Auth情報の書き換え
                           /*
                            
                            $this->Session->write('Auth', $user);
                            $this->Auth->logout();
                            $this->Auth->login($user);
*/
                            
                        }
                        
                        if ($this->User->save($this->request->data,false)) {                        

                            // ファイルを移動
                            if(array_key_exists('image_name1',$this->data['User'])){
                                if($this->request->data['User']['image_name1']['name'] != '' ){
                                
                                     if(!move_uploaded_file($fileName1['tmp_name'],CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$save_name1)){
                                    //if(!move_uploaded_file($fileName1['tmp_name'],WWW_ROOT.'images'.DIRECTORY_SEPARATOR.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$save_name1)){
                                        $this->User->rollback();
                                        $this->Flash->error('更新に失敗しました。');
                                        return $this->redirect(array('action' => 'index'));
                                    }
                                }
                            }
                            if(array_key_exists('image_name2',$this->data['User'])){
                                if($this->request->data['User']['image_name2']['name'] != '' ){
                                    if(!move_uploaded_file($fileName2['tmp_name'],CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$save_name2)){
                                        $this->User->rollback();
                                        $this->Flash->error('更新に失敗しました。');
                                        return $this->redirect(array('action' => 'index'));
                                    }
                                }
                            }
                            $this->User->commit();
                            
                            // Auth情報の書き換え
                            $this->Session->write('Auth', $user);
                            $this->Auth->logout();
                            $this->Auth->login($user);
                            
                            //$this->Flash->success('ユーザー情報が更新されました');
                            
                            
                            // 管理者にメール送信
                            if($this->request->data['User']['status'] == 1 || $this->request->data['User']['status'] == 5 || $this->request->data['User']['status'] == 8){

                                    $email = new CakeEmail('smtp');
                                    $email->to(ADMIN_MAIL);

                                    //HTMLorテキストメール
                                    $email->emailFormat('text');

                                if($this->request->data['User']['status'] == 1){
                                
                                    //テンプレート
                                    $email->template('admin_status_request');
                                    $email->subject('【みんでる運営センター】'.$this->request->data['User']['shop_name'].' の承認');
                                    
                                }else{
                                    
                                    //テンプレート
                                    $email->template('admin_status_change');
                                    $email->subject('【みんでる運営センター】'.$this->request->data['User']['shop_name'].' のアカウント変更');
                                    $this->Flash->success('ユーザー情報が更新されました');
                                    
                                }
                                    
                                //テンプレートへ渡す変数。[変数名=>値]
                                $email->viewVars(array(
                                    'admin_url'         => Router::url('/admin/', true),
                                    'id'                => USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->auth['id']),
                                    'shop_name'         => $this->request->data['User']['shop_name'],
                                    'responsible_name'  => $this->request->data['User']['responsible_name'],
                                    'address'           => $this->request->data['User']['address'],
                                ));
                                
                                $email->send();
                                
                                
                            }
                            
                            //$this->Flash->success('ユーザー情報が更新されました');
                            return $this->redirect(array('action' => $redirect));
                            
                        }else{
                            //print_r($this->User->validationErrors);
                        
                        }
                    }else{
                       // print_r($this->User->validationErrors);                        
                    }
                } catch(Exception $e) {
                    //$error = $e->getError();
                    //print($error);
                
                }
            }
            
            $this->User->rollback();
            $this->Flash->error('ユーザー情報の更新に失敗しました。エラー内容をご確認ください。');
            
        }

   }

   public function admin_edit($id = null) {
       
        // 表示文言の設定
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('tstring', '詳細');
        $this->set('bstring', '更新');
        
        // 選択リストをセット
        $this->set('pref_list',Configure::read("pref_list"));
        $this->set('user_status',Configure::read("user_status"));
        
        if ($this->request->is('post') || $this->request->is('put')) {
            
            $this->User->validate = $this->User->validate_basic;
            if ($this->User->save($this->request->data)) {
                
                // Auth情報の書き換え
                $auth = $this->Auth->user();
                if($id == $auth['id']){
                    $auth['username'] = $this->data['User']['username'];
                    if(array_key_exists('role',$this->data['User'])){
                        $auth['role'] = $this->data['User']['role'];
                    }
                    $this->Session->write('Auth', $auth);
                    $this->Auth->logout();
                    $this->Auth->login($auth);
                }
                
                $this->Flash->success('ユーザー情報が更新されました');
                return $this->redirect(array('action' => 'edit',$id));
            }
            $this->Flash->error('ユーザー情報の更新に失敗しました。エラー内容をご確認ください。');
            
        } else {
            
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }
    public function admin_view($id = null) {
       
        // 表示文言の設定
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        // 選択リストをセット
        $this->set('pref_list',Configure::read("pref_list"));
        
        $user = $this->User->findById($id);
        $ret = Configure::read("user_status");
        
        //if($user['User']['status'] != 8 && $user['User']['status'] != 9){
            //$user_status = array($user['User']['status'] => $ret[$user['User']['status']], 8 => $ret[8], 9 => $ret[9]);
            
        //}else{
            //$user_status = array(5 => $ret[5], 8 => $ret[8], 9 => $ret[9]);
        //}
        $user_status = $ret;
        
        $this->set('user_status',$user_status);
        
        $this->loadModel('Administrator');
        $admin_list = $this->Administrator->find('list',array('fields'=>'id,showname','conditions' => array('active' => 1),));
        $this->set('admin_list', $admin_list);
        
        // 転送設定データ取得
        $this->loadModel('Teltransfer');
        $teltransfers = $this->Teltransfer->find('all',array('conditions' => array('user_id'=>$id),));
        $this->set('teltransfers', $teltransfers);

        // 自動応答データ取得
        $this->loadModel('Voicemail');
        $voicemail = $this->Voicemail->find('all',array('fields'=>array('Voicemail.*'),'conditions' => array('user_id'=>$id),));
        if($voicemail){
            $this->set('voicemail', $voicemail[0]);
        }else{
            $this->set('voicemail', array());
        }
        
        // データ更新
        if ($this->request->is('post') || $this->request->is('put')) {
            
            $update_date = array();
            $update_date['User']['id'] = $id;
            
            // 更新ボタン押下
            if(isset($this->request->data['button_update'])) {
                $update_date['User']['memo']     = $this->request->data['User']['memo'];
                $update_date['User']['admin_id'] = $this->request->data['User']['admin_id'];
                $update_date['User']['status']   = $this->request->data['User']['status'];
                $update_date['User']['didprint']   = $this->request->data['User']['didprint'];
                $update_date['User']['company_name']   = $this->request->data['User']['company_name'];
                $update_date['User']['company_kana']   = $this->request->data['User']['company_kana'];
                $update_date['User']['formally_start_date']   = $this->request->data['User']['formally_start_date'];
                if($this->request->data['User']['didprint'] == 1){
                    $update_date['User']['print_datetime'] = date('Y-m-d H:i:s');
                }
                if($this->request->data['User']['status'] > 7){
                    $update_date['User']['status_'.$this->request->data['User']['status'].'_date'] = date('Y-m-d H:i:s');
                    $update_date['User']['status_'.$this->request->data['User']['status'].'_admin_id']   =  $this->request->data['User']['admin_id'];;
                }
            
            }
            
            // 無課金セット・ユーザー削除
            if(isset($this->request->data['button_no_payment'])) {
                $update_date['User']['no_payment']    = $this->request->data['User']['no_payment'];
                $update_date['User']['delflg']        = $this->request->data['User']['delflg'];
                $update_date['User']['hand_payment']  = $this->request->data['User']['hand_payment'];
            }
            
            $this->User->begin();
            try{
                if ($this->User->save($update_date,false)) {
                    $this->User->commit();
                    $this->Flash->success('更新しました。');
                    
                    $this->request->data = $this->User->findById($id);
                    unset($this->request->data['User']['password']);
                    
                    return ;
                }
            } catch(Exception $e) {
            }
            $this->User->rollback();
            $this->Flash->error('更新に失敗しました。エラー内容をご確認ください。');
            
        }else{
        
            $user  = $this->User->find('first',array(
                "fields"     => array("User.*,Parent.shop_name"),
                "conditions" => array("User.id" => $id),
                'joins'      => array(
                        array (
                            'type' => 'LEFT',
                            'table' => 'users',
                            'alias' => 'Parent',
                            'conditions' => 'Parent.id = User.parent_id'
                            ),
                 )
            ));
            
            $this->request->data = $user;
            //$this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
        
        // ポイント子ユーザー取得
        $children = $this->User->find("list",array(
            "fields" =>"id,shop_name",
            'conditions' => array('parent_id'=>$id),
        ));
        $this->set('children',$children);

    }
    public function admin_examination($id = null) {
        
        // 表示文言の設定
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        // 選択リストをセット
        $this->set('pref_list',Configure::read("pref_list"));
        $this->loadModel('Administrator');
        $admin_list = $this->Administrator->find('list',array('fields'=>'id,showname','conditions' => array('active' => 1/*,'role' => 0*/),));
        $this->set('admin_list', $admin_list);
        $this->set('status_2_reason_list',Configure::read("status_2_reasons"));
        
        
        $user_status_all = Configure::read("user_status");
        $user_status = array(1=>$user_status_all[1],2=>$user_status_all[2],3=>$user_status_all[3],7=>$user_status_all[7]);
        
        $this->set('user_status',$user_status);
        
        if ($this->request->is('post') || $this->request->is('put')) {
            
            //メール送信準備
            $email = new CakeEmail('smtp');
            $email->to($this->request->data['User']['email']);

            //HTMLorテキストメール
            $email->emailFormat('text');
            
            $update_date = array();
            $update_date['User']['id'] = $id;
            
            if(isset($this->request->data['button_memo_update'])) {
                $update_date['User']['memo'] = $this->request->data['User']['memo'];

            }elseif(isset($this->request->data['button_status_to_2'])) {
            
                if(!$this->request->data['User']['status_2_reason']){
                    $this->Flash->error('再審査理由が選択されていません。');
                     $this->request->data = $this->User->findById($id);
                    unset($this->request->data['User']['password']);
                    return;
                }
                
                if(!$this->request->data['User']['status_2_admin_id']){
                    $this->Flash->error('再審査担当者が選択されていません。');
                     $this->request->data = $this->User->findById($id);
                    unset($this->request->data['User']['password']);
                    return;
                }
                
                
                if($this->request->data['User']['status_2_reason'] == "その他" && !$this->request->data['User']['status_2_mailadd']){
                    $this->Flash->error('再審査理由にその他を選択する場合はコメントを入力してください。');
                    
                    $status_2_reason = $this->request->data['User']['status_2_reason'];
                    $this->request->data = $this->User->findById($id);
                    unset($this->request->data['User']['password']);
                    $this->request->data['User']['status_2_reason'] = $status_2_reason;
                    return;
                }
                
                
                
                $email->template('update_status_2');
                $update_date['User']['status'] = 2;
                
                $update_date['User']['status_2_date'] = date('Y-m-d H:i:s');
                $update_date['User']['status_2_admin_id'] = $this->request->data['User']['status_2_admin_id'];
                $update_date['User']['status_2_mailadd']  = $this->request->data['User']['status_2_mailadd'];
                $update_date['User']['status_2_reason']   = $this->request->data['User']['status_2_reason'] ;

                //再審査の場合は本人確認書類をもう一度登録できるようにする
                $update_date['User']['confirmation_filename_first_1'] = $this->request->data['User']['confirmation_filename_1'];
                $update_date['User']['confirmation_filename_1']   = '';
                $update_date['User']['confirmation_filename_first_2'] = $this->request->data['User']['confirmation_filename_2'];
                $update_date['User']['confirmation_filename_2']   = '';
                
                $email->subject('【'.SITE_NAME.'】確認書類の再提出のお願い');
                
            }elseif(isset($this->request->data['button_status_to_3'])) {
                if(!$this->request->data['User']['status_3_admin_id']){
                    $this->Flash->error('承認担当者が選択されていません。');
                     $this->request->data = $this->User->findById($id);
                    unset($this->request->data['User']['password']);
                    return;
                }
                $email->template('update_status_3');
                $update_date['User']['status'] = 3;
                $update_date['User']['status_3_date'] = date('Y-m-d H:i:s');
                $update_date['User']['status_3_admin_id'] = $this->request->data['User']['status_3_admin_id'];
                $email->subject('【'.SITE_NAME.'】転送内容の設定にお進みください');
                
            
            }elseif(isset($this->request->data['button_status_to_7'])) {                
                if(!$this->request->data['User']['status_7_admin_id']){
                    $this->Flash->error('却下担当者が選択されていません。');
                    $this->request->data = $this->User->findById($id);
                    unset($this->request->data['User']['password']);
                    return;
                }
                $email->template('update_status_7');
                $update_date['User']['status'] = 7;
            
                $update_date['User']['status_7_date'] = date('Y-m-d H:i:s');
                $update_date['User']['status_7_admin_id'] = $this->request->data['User']['status_7_admin_id'];
                $update_date['User']['status_7_reason']   = $this->request->data['User']['status_7_reason'] ;
                
                $email->subject('【'.SITE_NAME.'】審査結果のお知らせ');
                
            // 無課金セット
            }elseif(isset($this->request->data['button_no_payment'])) {
                $update_date['User']['no_payment']  = $this->request->data['User']['no_payment'];
            }
            
            
            $ret = $email->template();
                
            $this->User->begin();
            try{
            
                if ($this->User->save($update_date,false)) {
                    
                    if($ret['template']){
                    
                        $tgt_user = $this->User->findById($id);
                        unset($tgt_user['User']['password']);

                    
                        //テンプレートへ渡す変数。[変数名=>値]
                        $email->viewVars(array(
                            'login_url'         => Router::url(array('controller'=>'users', 'action'=>'login','admin'=>false),true),
                            //'shop_name'         => $this->request->data['User']['shop_name'],
                            
                            'shop_name'               => $tgt_user['User']['shop_name'               ],
                            'company_name'            => $tgt_user['User']['company_name'            ],
                            'company_kana'            => $tgt_user['User']['company_kana'            ],
                            'responsible_name'        => $tgt_user['User']['responsible_name'        ],
                            'responsible_kana'        => $tgt_user['User']['responsible_kana'        ],
                            'email'                   => $tgt_user['User']['email'                   ],
                            'loginid'                 => $tgt_user['User']['username'                ],
                            'zip'                     => $tgt_user['User']['zip'                     ],
                            'address'                 => $tgt_user['User']['address'                 ],
                            'building'                => $tgt_user['User']['building'                ],
                            'telno'                   => $tgt_user['User']['user_telno'              ],

                            'republish_password_url'  => Router::url('/forgot_password/', true),
                            
                            //'request_url'             => Router::url(array('controller'=>'teltransfers', 'action'=>'new','admin'=>false),true),   
                            //'responsible_name'  => $this->request->data['User']['responsible_name'],
                            //'loginid'           => $this->request->data['User']['username'],
                            
                            'status_2_reason'  => $this->request->data['User']['status_2_reason'],
                            'status_2_mailadd' => $this->request->data['User']['status_2_mailadd'],

                        ));
                        
//                        $email->subject('サービス利用審査結果のご案内');
                        $email->send();
                                
                        $this->User->commit();
                        
                        $this->Flash->success('審査結果を通知しました。');
                        //return ;
                        return $this->redirect(array('action' => 'index/1/1'));
                        //return $this->redirect(array('action' => 'thanks'));
                        
                        
                    }else{
                        
                        $this->User->commit();
                        $this->Flash->success('顧客情報を更新しました。');
                        
                        $this->request->data = $this->User->findById($id);
                        unset($this->request->data['User']['password']);
                        return ;
                    }
                }
            } catch(Exception $e) {
            }
            
            $this->User->rollback();
            $this->Flash->error('審査結果の更新に失敗しました。エラー内容をご確認ください。');
    
        
        }else{

        }
        $this->request->data = $this->User->findById($id);
        unset($this->request->data['User']['password']);

    }
    // 運営センター アカウント印刷
    public function admin_print($id = null) {
       
        // 表示文言の設定
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        // 選択リストをセット
        //$this->set('pref_list',Configure::read("pref_list"));
        
        $user = $this->User->findById($id);
        $ret = Configure::read("user_status");
        
        $user_status = $ret;
        
        $this->set('user_status',$user_status);
        
        $this->loadModel('Administrator');
        $admin_list = $this->Administrator->find('list',array('fields'=>'id,showname','conditions' => array('active' => 1),));
        $this->set('admin_list', $admin_list);
        
        // 転送設定データ取得
        $this->loadModel('Teltransfer');
        $teltransfers = $this->Teltransfer->find('all',array('conditions' => array('user_id'=>$id),));
        $this->set('teltransfers', $teltransfers);

        // 自動応答データ取得
        $this->loadModel('Voicemail');
        $voicemail = $this->Voicemail->find('all',array('conditions' => array('user_id'=>$id),));
        if($voicemail){
            $this->set('voicemail', $voicemail[0]);
        }else{
            $this->set('voicemail', array());
        }
        
        $this->request->data = $this->User->findById($id);
        unset($this->request->data['User']['password']);
        
        $this->set('pref_list',Configure::read("pref_list"));
    }

    public function admin_thanks() {
    }

    public function admin_tropo($id = null) {
       
        // 表示文言の設定
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        // 選択リストをセット
        $this->set('pref_list',Configure::read("pref_list"));
        $user_status_all = Configure::read("user_status");
        $user_status = array(4=>$user_status_all[4],5=>$user_status_all[5]);
        $this->set('user_status',$user_status);
        $this->loadModel('Administrator');
        $admin_list = $this->Administrator->find('list',array('fields'=>'id,showname','conditions' => array('active' => 1,/*'role' => 0*/),));
        $this->set('admin_list', $admin_list);
        
        // 転送設定データ取得
        $this->loadModel('Teltransfer');
        $teltransfers = $this->Teltransfer->find('all',array('conditions' => array('user_id'=>$id),));
        $this->set('teltransfers', $teltransfers);

        // 自動応答データ取得
        $this->loadModel('Voicemail');
        $voicemail = $this->Voicemail->find('all',array('conditions' => array('user_id'=>$id),));
        if($voicemail){
            $this->set('voicemail', $voicemail[0]);
        }else{
            $this->set('voicemail', array());
        }
        

        if ($this->request->is('post') || $this->request->is('put')) {

                // メール送信準備
                $email = new CakeEmail('smtp');
                $email->emailFormat('text');

                // 登録データの作成
                $update_date = array();
                $update_date['User']['id'] = $id;
               
                // コネクトボタン押下
                if(isset($this->request->data['button_set_tropo_telno'])) {
                    $update_date['User']['tropo_telno'] = $this->request->data['User']['tropo_telno'];
                    
                // 開通テスト終了ボタン押下
                }elseif(isset($this->request->data['button_status_to_5'])) {
                
                    $ret = $this->User->find('all', array('conditions' => array('id' => $id),'fields' => array('status_5_date','tropo_telno'),));
                    if(!$ret[0]['User']['tropo_telno']){
                        $this->Flash->error("発行番号を入力してコネクトボタンを押してください。");
                        
                        $tropo_telno = $this->request->data['User']['tropo_telno'];
                        $this->request->data = $this->User->findById($id);
                        unset($this->request->data['User']['password']);
                        $this->request->data['User']['tropo_telno'] = $tropo_telno;
                        return;
                    }
                    
                    if(!$ret[0]['User']['status_5_date']){
                        $this->Flash->error("開通確認が行われていません。 ".$ret[0]['User']['tropo_telno']." に電話をかけて開通確認をしてください。");
                        $this->request->data = $this->User->findById($id);
                        unset($this->request->data['User']['password']);
                        return;

                    }
                    $email->template('update_status_5');
                    $update_date['User']['status'] = 5;
                    $update_date['User']['status_5_admin_id'] = $this->request->data['User']['status_5_admin_id'];
                }

                $this->User->validate = $this->User->validate_tropo;
                $this->User->begin();
                try{
                    if ($this->User->save($update_date)) {
                        
                        //メール送信
                        $ret = $email->template();
                        if($ret['template']){
                            
                            // ユーザー情報の取得
                            $tgt_user = $this->User->findById($id);
                            unset($tgt_user['User']['password']);
                            
                            // 自動転送設定の取得
                            $this->loadModel('Voicemail');
                            $voicemail = $this->Voicemail->find('all',array('conditions' => array('user_id'=>$id),));
                            $voicemail_str  = "";
                            $voicemail_str .="月　" ;if($voicemail[0]['Voicemail']['teltransfer_day_01'] == 1){$voicemail_str .= date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_from_01'])).'～'.date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_to_01']))."\n";}else{$voicemail_str .= "―\n";} 
                            $voicemail_str .="火　" ;if($voicemail[0]['Voicemail']['teltransfer_day_02'] == 1){$voicemail_str .= date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_from_02'])).'～'.date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_to_02']))."\n";}else{$voicemail_str .= "―\n";} 
                            $voicemail_str .="水　" ;if($voicemail[0]['Voicemail']['teltransfer_day_03'] == 1){$voicemail_str .= date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_from_03'])).'～'.date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_to_03']))."\n";}else{$voicemail_str .= "―\n";} 
                            $voicemail_str .="木　" ;if($voicemail[0]['Voicemail']['teltransfer_day_04'] == 1){$voicemail_str .= date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_from_04'])).'～'.date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_to_04']))."\n";}else{$voicemail_str .= "―\n";} 
                            $voicemail_str .="金　" ;if($voicemail[0]['Voicemail']['teltransfer_day_05'] == 1){$voicemail_str .= date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_from_05'])).'～'.date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_to_05']))."\n";}else{$voicemail_str .= "―\n";} 
                            $voicemail_str .="土　" ;if($voicemail[0]['Voicemail']['teltransfer_day_06'] == 1){$voicemail_str .= date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_from_06'])).'～'.date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_to_06']))."\n";}else{$voicemail_str .= "―\n";} 
                            $voicemail_str .="日　" ;if($voicemail[0]['Voicemail']['teltransfer_day_00'] == 1){$voicemail_str .= date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_from_00'])).'～'.date('H:i',strtotime($voicemail[0]['Voicemail']['teltransfer_to_00']))."\n";}else{$voicemail_str .= "―\n";} 
                                                                                                                                                                                           
                            // 転送先設定の取得
                            $this->loadModel('Teltransfer');
                            $teltransfers = $this->Teltransfer->find('all',array('conditions' => array('user_id'=>$id),));
                             $teltransfer_str = "";
                            foreach($teltransfers AS $teltransfer){ 
                                 $teltransfer_str .=  "転送先" .$teltransfer['Teltransfer']['num']."　" . $teltransfer['Teltransfer']['telno'] . "　" . $teltransfer['Teltransfer']['memo']."\n";
                            } 
                            

                            $email->to($tgt_user['User']['email']);
                            $email->viewVars(array(
                                'login_url'         => Router::url(array('controller'=>'users', 'action'=>'login','admin'=>false),true),
                                
                                'id'                => USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$tgt_user['User']['id']),
                                
                                'user'              => $tgt_user['User'],
                                'voicemail'         => $voicemail_str,
                                'message'           => $voicemail[0]['Voicemail']['message'],
                                'teltransfers'      => $teltransfer_str,
                                
                                
                                //'tropo_telno'       => $tgt_user['User']['tropo_telno'],
                                //'shop_name'         => $tgt_user['User']['shop_name'],
                                //'loginid'           => $tgt_user['User']['username'],
                                //'responsible_name'  => $tgt_user['User']['responsible_name']
                                  
                                  
                              
                            ));
                            
                            $email->subject('【'.SITE_NAME.'】サービス開始のお知らせ');
                            $email->send();
                            

                            /* ご利用ガイドを別メールに 201806追加 */
                            $email->template('update_status_5_1');
                            $email->subject('【'.SITE_NAME.'】サービスご利用ガイド');
                            $email->send();
                            /* ご利用ガイドを別メールに ここまで */
                            
                            
                            $this->User->commit();
                            
                            
                            $this->Flash->success('利用開始を通知しました。');
                            
                            
                            //return $this->redirect(array('action' => 'thanks'));
                            return $this->redirect(array('action' => '/index/4/1'));
                            
                            
                        
                        }else{

                            $this->User->commit();

                            $this->Flash->success('発行番号を登録しました。');
                            
                            $this->request->data = $this->User->findById($id);
                            unset($this->request->data['User']['password']);
                            return;
                        }

                    }
                    
                } catch(Exception $e) {
                }
                
                $this->User->rollback();
                $this->Flash->error('利用開始処理に失敗しました。エラー内容をご確認ください。');

        }else{
            
        }
        $this->request->data = $this->User->findById($id);
        unset($this->request->data['User']['password']);
        
        
    }
    
    // 解約
    public function leave() {
        
        if ($this->request->is('post') || $this->request->is('put')) {
            
            // ステータスの変更
            $update_date['User']['id'] =$this->auth['id'] ;
            $update_date['User']['status'] = 8;
            $update_date['User']['status_8_date'] = date('Y-m-d H:i:s');
            if ($this->User->save($update_date,false)) {
                
                // メール送信
                $email = new CakeEmail('smtp');
                $email->emailFormat('text');
                
                // ユーザーへ
                $email->to($this->auth['email']);
                $email->template('update_status_8');
                $email->viewVars(array(
//                    'user_id'                => $this->auth['id'],
                    'responsible_name'       => $this->auth['responsible_name'],
                    'shop_name'              => $this->auth['shop_name'],
//                    'company_name'           => $this->auth['company_name'],
//                    'tropo_telno'            => $this->auth['tropo_telno'],
//                    'content'                => $this->request->data['User']['content'],
                ));
                $email->subject('【'.SITE_NAME.'】'.'解約を承りました');
                $email->send();
            
                
                // 管理者へ
                $email->to(ADMIN_MAIL);
                $email->template('admin_leave');
                $email->viewVars(array(
                    
                    'admin_url'              => Router::url('/admin/', true),
                    'id'                     => USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->auth['id']),
                    'responsible_name'       => $this->auth['responsible_name'],
                    'shop_name'              => $this->auth['shop_name'],
                    
                    'address'           => $this->auth['address'],
                    //'tropo_telno'            => $this->auth['tropo_telno'],
                    
                    'content'                => $this->request->data['User']['content'],
                ));
                $email->subject('【みんでる運営センター】'.$this->auth['shop_name'] .' の解約');
                $email->send();
            
                return $this->redirect(array('action' => 'leave_end'));
            
            }else{
                $this->Flash->error('解約処理に失敗しました。エラー内容をご確認ください。');

            }
        }
        
    }
    public function leave_end() {
    }
    
    public function admin_pdfdisp($id = null,$item_name = null) {
        
        $this->autoRender = false;
        $user = $this->User->find('first',array('fields'=>$item_name,'conditions'=>array('id'=>$id)));
        
        if(isset($user)){  
            $pdf = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$user['User'][$item_name]);  
            header("Content-Type: application/pdf");  
            echo $pdf;
            exit();
        }
    }

    public function admin_imgdisp($id = null,$item_name = null) {
        
        $this->autoRender = false;
        $user = $this->User->find('first',array('fields'=>$item_name,'conditions'=>array('id'=>$id)));
        
        if(isset($user)){  
            $img = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$user['User'][$item_name]);  
            $ext = substr($item_name, strrpos($item_name, '.') + 1) ;
            echo "<img src='data:image/jpg;base64,".base64_encode($img) . "' style='width:100%'>";
            exit();  
        }
    }
    
    // 親ユーザー設定
    public function admin_parent($user_id = null) {
        
        $user = $this->User->findById($user_id);
        unset($user['password']);
        $this->set('user', $user);
        
        // 既ユーザー一覧
        $this->LoadModel("Point");
        $add_data = array();
        $tmps = $this->Point->find("list",array(
            "fields" =>"User.id,User.shop_name",
            "groups"=>array("user_id"),
            "order"=>array("user_id"),
            "joins" => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'users', 
                            'alias' => 'User',  
                            'conditions' => 'Point.user_id = User.id'
                            ),
            )
        ));

        $error = 0;
        
        // 親に出来るアカウントを取得
        $reg_users = array();
         foreach($tmps AS $key => $tmp){
             if($key == $user_id){
                 $error = 1;
                 $this->Flash->error('このユーザーはユーザー自身のポイントデータが既に登録されているため親アカウントを指定できません');
             }
            $reg_users[] = array(
                'value'        => $key,
                'name'         => $tmp ." (". USER_ID_HEDDER.sprintf('%06d',$key).")",
                );
        }

        $this->set('reg_users', $reg_users);

        // 誰かの親ではないか?
        $children = $this->User->find("list",array(
            "fields" =>"User.id,User.shop_name",
            'conditions' => array('parent_id'=>$user_id)
            
        ));
        if(count($children) > 0){
            $error = 1;
            $this->Flash->error('このユーザーをポイント払い(親)ユーザーに設定しているアカウントが '.count($children).' つあるため、設定できません');
        }
        if ($this->request->is('post')) {
            
            if($error == 0){
                if ($this->User->save($this->request->data,false)) {
                    $this->Flash->success('親アカウントを設定しました');
                    
                }else{
                    $this->Flash->error('親アカウントの設定に失敗しました。');
                }
            }

        
        }
    
    }
    
}