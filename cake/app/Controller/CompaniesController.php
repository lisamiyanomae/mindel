<?php
class CompaniesController extends AppController {
        
    public $uses = array('Company');
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        Security::setHash('blowfish');
        
        //$this->Auth->allow('add', 'logout');
    }

    

    public function admin_index() {
    
        $this->Paginator->settings = array(
            'fields' => 'Company.*,COUNT(CompanyUser.id) AS CNT',
            'joins' => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'company_users', 
                            'alias' => 'CompanyUser',  
                            'conditions' => 'CompanyUser.company_id = Company.id'
                            ),
            ),
            'group' => array('Company.id'),
            'limit' => 30,
            'order' => array('Company.created' => 'desc')
        );

        $datas = $this->Paginator->paginate('Company');

        $this->set('datas', $datas);
        

    
    }

    public function admin_view($id = null) {
        $this->Company->id = $id;
        if (!$this->Company->exists()) {
            throw new NotFoundException(__('Invalid administrator'));
        }
        $this->set('datas', $this->Company->findById($id));
    }

    public function admin_add() {

        if ($this->request->is('post')) {
            $this->Company->create();
            if ($this->Company->save($this->request->data)) {
                $this->Flash->success('マルチアカウントが追加されました');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error('マルチアカウントの追加に失敗しました。');
        }
    }

    public function admin_delete($id = null) {
        
        // Prior to 2.5 use
        $this->request->onlyAllow('post');
        $this->autoRender = false;
        
        $this->Company->id = $id;
        if (!$this->Company->exists()) {
            throw new NotFoundException(__('Invalid company'));
        }
        if ($this->Company->delete()) {
            $this->Flash->success('マルチアカウントが削除されました');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error('マルチアカウント削除に失敗しました');
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_edit($id = null) {
        $this->Company->id = $id;
        if (!$this->Company->exists()) {
            throw new NotFoundException(__('Invalid administrator'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
        
            if ($this->Company->save($this->request->data)) {
                
               
                $this->Flash->success('マルチアカウント情報が更新されました');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error('マルチアカウント情報の更新に失敗しました。');
        } else {
            $this->request->data = $this->Company->findById($id);
            unset($this->request->data['Company']['password']);
        }
        
        
    }
   


}