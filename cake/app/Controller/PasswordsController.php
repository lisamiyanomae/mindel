<?php
/**
 * PasswordsController
 *
 */
class PasswordsController extends AppController
{
    var $name = 'Passwords';
    var $components = array('CpPassword');
    var $uses = array('User');

    /**
     * 実行前処理
     *
     */
    function beforeFilter()
    {
        $this->Auth->allow('create', 'add', 'edit', 'update','thanks','err');
        return parent::beforeFilter();
    }

    /**
     * GET /passwords/new
     *
     */
    function create()
    {
    }


    /**
     * GET /passwords/{id}/{hash}
     *
     */
    function edit($id, $hash)
    {
        $this->layout = 'user_add';
        if (!$id || !$hash || !$this->CpPassword->isCorrectResetRequest($id, $hash)) {
            //$this->Flash->error('このURLは利用できません。');
            $this->redirect(array('controller'=>'passwords', 'action'=>'err'));
        }
        $this->CpPassword->setRepublishKeys($id, $hash);
        
        if ($this->data) {
                $this->_update();

        }
    }
    
    function thanks(){
        $this->layout = 'user_add';
    }
    function err(){
        $this->layout = 'user_add';
    }
    
    /**
     * POST /passwords
     *
     */
    function add()
    {
        $this->layout = 'user_add';
    
        $this->pageTitle = __("パスワード再発行", true);
        if ($this->data) {
            if ($this->CpPassword->republishPassword($this->data['User']['email'])) {
                $this->redirect(array('controller'=>'passwords', 'action'=>'thanks'));
            }
            $this->Flash->error('このメールアドレスは登録されていません。');
        }
        $this->render('create');
    }

    /**
     * PUT /passwords
     *
     */
    function _update()
    {
        
        list($id, $resetCode) = $this->CpPassword->getRepublishKeys();
        if (!$id || !$resetCode || !$this->CpPassword->isCorrectResetRequest($id, $resetCode)) {
            $this->Flash->error('パスワードを更新できませんでした。');
            $this->redirect(array('controller'=>'passwords', 'action'=>'err'));
        }
        $plainPassword = $this->data['User']['plain_password'];
        if ($this->User->isPassword($plainPassword)) {
            $password = $plainPassword;
            if ($this->User->updatePassword($id, $password)) {
                $this->CpPassword->clearRepublishKeys();
                $this->Auth->logout();
                $this->Flash->set('変更されたパスワードでログインしてください。');
                //$this->redirect(array('controller'=>'pages', 'action'=>'home'));
                $this->redirect(array('controller'=>'telephones', 'action'=>'index'));
                
            }
        }
        $this->render('edit');
    }
}