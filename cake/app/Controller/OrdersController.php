<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class OrdersController extends AppController {
 
    public $components = ['CpOrder'];

    public function beforeFilter() {
        parent::beforeFilter();

    }
    // 決済エラー時個別決済
    public function pay($id = null) {
    
        $this->first($id);
        $this->render('first');
    
    }
    // 請求書払い初回登録
    public function start($id = null) {
        
        $this->layout = 'user_add';
        
        $this->set('tstring', 'サービス開始お手続き');
        $this->set('bstring', 'サービス開始を申し込む');
        $this->set('tstring2', $this->auth['shop_name'].' '.$this->auth['responsible_name'].' 様　決済内容を確認してサービス申し込みにお進みください。');
        $this->set('back_btn', array('controller'=>'voicemails','action'=>'index'));

        // サービス開始お申込み
        if ($this->request->is('post') || $this->request->is('put')) {
                
                if($this->action == 'start' && $this->auth['status'] == 3){
                    
                    //初回請求データの作成
                    $update_data = $this->CpOrder->save_start($this->request->data,$this->auth);
                    if (is_array($update_data)) {
                        $this->loadModel('OrderErr');
                        if(array_key_exists("OrderErr",$update_data)){
                            if (!$this->OrderErr->saveall($update_data['OrderErr'])) {
                                $this->log('check month  ng', LOG_DEVELOP);
                                $this->log($update_data, LOG_DEVELOP);
                            }
                            $err_cnt = count($update_data["OrderErr"]);
                        }
                        // ユーザーステータスの変更
                        $this->loadModel('User');
                        
                        $data['User']['id'] = $this->auth['id'];
                        $data['User']['status'] = 4;
                        $data['User']['hand_payment'] = 1;
                        
                        if ($this->User->save($data,false)) {

                            $this->auth['status'] = 4;
                            $this->Session->write('Auth', $this->auth);
                            $this->Auth->logout();
                            $this->Auth->login($this->auth);

                            // 管理者にメール送信
                            $email = new CakeEmail('smtp');
                            $email->to(ADMIN_MAIL);

                            //HTMLorテキストメール
                            $email->emailFormat('text');
                            
                            //テンプレート
                            $email->template('admin_twilio_request');

                            //テンプレートへ渡す変数。[変数名=>値]
                            $email->viewVars(array(
                                'admin_url'         => Router::url('/admin/', true),
                                'shop_name'         => $this->auth['shop_name'],
                                'id'                => USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->auth['id']) ,
                                'responsible_name'  => $this->auth['responsible_name'],
                                'address'           => $this->auth['address'],
                                
                            ));
                            $email->subject('【みんでる運営センター】'. $this->auth['shop_name'].' の番号発行（振込払いユーザー）');
                            $email->send();
                            
                            // ユーザーへメール送信
                            $email->to($this->auth['email']);
                            $email->template('update_status_4');
                            $email->viewVars(array(
                                'shop_name'         => $this->auth['shop_name'],
                                'responsible_name'  => $this->auth['responsible_name'],
                            ));
                            $email->subject('【'.SITE_NAME.'】お振込みのご案内');
                            $email->send();

                            return $this->redirect(array('action' => 'thanks'));
                            
                        }else{
                            $this->Flash->error('処理に失敗しました。ステイタスを変更できませんでした。');
                        }
                    }else{
                        $this->Flash->error('処理に失敗しました。');

                    }

                }
        }
    
    }
    
    // 初回支払い
    public function first($id = null) {
           
        // 新規
        if($this->action == 'first' && $this->auth['status'] == 3){
        
            $this->layout = 'user_add';
        
            $this->set('tstring', 'サービス開始お手続き(3/3)');
            $this->set('bstring', 'お支払い処理');
            $this->set('tstring2', $this->auth['shop_name'].' '.$this->auth['responsible_name'].' 様　カード情報を入力して決済を行ってください。');
            $this->set('back_btn', array('controller'=>'voicemails','action'=>'index'));
            $this->set('amount', FIRST_COST * (1 + TAX) );
        
        
        // 個別決済
        }elseif($this->action == 'pay' && $id){
            
            $pay = $this->Order->findById($id);
            if($pay['Order']['pay_date']||$pay['Order']['user_id'] != $this->auth['id']){
                return $this->redirect(array('action' => 'forbidden'));
            }
            
            $order_kind = Configure::read("order_kind");
            $this->set('tstring', 'お支払い');
            $this->set('bstring', 'お支払い実行');
            $this->set('tstring2', 'ご登録のクレジットカードで '.$pay['Order']['start_date'].'～'.$pay['Order']['end_date'].' のご利用料金( '.$order_kind[$pay['Order']['kind']].' ) が決済できませんでした。<br>利用可能なクレジットカードに変更し '.$pay['Order']['limit_date'].' までにお支払いください。');
            $this->set('back_btn', array('controller'=>'telephones','action'=>'index'));

            $this->set('amount', $pay['Order']['amount']);
            
            // 登録されているカード情報
            $ret = $this->CpOrder->getCardInfo($this->auth['id'],3);
            if(!empty($ret)){
                $this->set('card_info',array_values($ret)[0]);
            }else{
                $this->set('card_info','');
            }
        
        // カード情報変更
        }elseif($this->auth['status'] == 5){
        
        
            $this->set('tstring', 'カード情報');
            $this->set('bstring', '変更');
            $this->set('tstring2', 'お客様のカード情報を変更します。');
            $this->set('back_btn', array('controller'=>'pages','action'=>'home'));
            
            $this->set('amount', 0);
            
            // 登録されているカード情報
            $ret = $this->CpOrder->getCardInfo($this->auth['id'],3);
            if(!empty($ret)){
                $this->set('card_info',array_values($ret)[0]);
            }else{
                $this->set('card_info','');
            }
        }else{
            return $this->redirect(array('action' => 'forbidden'));
            
        }
        
        // 有効期限年月のリスト
        $this->set('list_month',array('01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12'));
        $this->set('list_year' ,array(date('y') => date('y') , date('y') + 1 => date('y') + 1, date('y') + 2  => date('y') + 2, date('y') + 3  => date('y') + 3, date('y') + 4  => date('y') + 4,date('y') + 5 => date('y') + 5,date('y') + 6 => date('y') + 6,date('y') + 7 => date('y') + 7,date('y') + 8 => date('y') + 8,date('y') + 9 => date('y') + 9,date('y') + 10 => date('y') + 10));
       
        // 決済実行
        if ($this->request->is('post') || $this->request->is('put')) {
                
                if($this->action == 'first' && $this->auth['status'] == 3){
                    
                    if ($this->CpOrder->save_first($this->request->data,$this->auth)) {
                    
                        // ユーザーステータスの変更
                        $this->loadModel('User');
                        
                        $data['User']['id'] = $this->auth['id'];
                        $data['User']['status'] = 4;
                        
                        if ($this->User->save($data,false)) {

                            $this->auth['status'] = 4;
                            $this->Session->write('Auth', $this->auth);
                            $this->Auth->logout();
                            $this->Auth->login($this->auth);

                    
                            // 管理者にメール送信
                            $email = new CakeEmail('smtp');
                            $email->to(ADMIN_MAIL);

                            //HTMLorテキストメール
                            $email->emailFormat('text');
                            //テンプレート
                            $email->template('admin_twilio_request');

                            //テンプレートへ渡す変数。[変数名=>値]
                            $email->viewVars(array(
                                'admin_url'         => Router::url('/admin/', true),
                                'shop_name'         => $this->auth['shop_name'],
                                'id'                => USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->auth['id']) ,
                                'responsible_name'  => $this->auth['responsible_name'],
                                'address'           => $this->auth['address'],
                                
                            ));
                            $email->subject('【みんでる運営センター】'. $this->auth['shop_name'].' の番号発行');
                            $email->send();
                            
                            //$this->Flash->success('サービス開始のお手続きを承りました。<br>受付した内容で電話番号の発行と転送設定をいたします。<br>設定完了はメールにて'. $this->auth['email'] .'へご連絡いたします。<br>それまで少しお待ちください。');
                            
                            // 完了画面表示
                            //return $this->redirect(array('controller'=>'users','action' => 'logout'));
                        
                            return $this->redirect(array('action' => 'thanks'));
                            
                            
                        }else{
                            $this->Flash->error('決済に失敗しました。ステイタスを変更できませんでした。');
                        }

                    }else{
                        $this->Flash->error('決済に失敗しました。');

                    }
                
                }elseif($this->action == 'pay'){
                
                    $this->loadModel('OrderErr');
                    $this->request->data['Order']['id'] = $id;
                    if ($this->CpOrder->pay($this->request->data,$this->auth)) {
                        //$this->Flash->success('お支払い処理が完了しました。');
                        return $this->redirect(array('action' => 'thanks_pay'));
                        
                        
                    }else{
                        $this->Flash->error('クレジットカード決済に失敗しました。');
                
                    }
                
                }else{

                    if ($this->CpOrder->change_card($this->request->data,$this->auth)) {
                        $this->Flash->success('クレジットカードを変更しました。');
                        return $this->redirect(array('action' => 'change'));
                    }else{
                        $this->Flash->error('クレジットカード情報変更に失敗しました。');
                    }

                }
            
        }
        
    }
    // カード情報変更
    public function change() {
        $this->first();
        $this->render('first');
    }
    
    public function thanks() {
        $this->layout = 'user_add';
        $this->auth = $this->Auth->user();
        $this->set('email', $this->auth['email']);
    }
    public function thanks_pay() {
    }
    public function forbidden() {
    }

    // 登録カード上の参照
    public function view() {
    
        $this->autoRender = false;

        //$this->auth = $this->Auth->user();
        $ret = $this->CpOrder->getCardInfo($this->auth['id'],3);
        //print_r($ret);

    }
    
    // 運営センター売り上げ管理(エラー詳細)
    public function admin_err($id = null) {
    
        if ($this->request->is('post') ) {
            $this->request->data['Order']['id'] = $id;
            $this->request->data['OrderErr']['order_id'] = $id;
            $this->loadModel('OrderErr');
            $this->Order->begin();
            $this->OrderErr->begin();
            try{
                if ($this->Order->save($this->request->data,false)) {
                    if ($this->OrderErr->save($this->request->data,false)) {
                        $this->Flash->success(ORDER_ID_HEDDER .sprintf('%0'.ORDER_ID_DIGIT.'d',$id).'の入金日を登録しました。');
                        $this->Order->commit();
                        $this->OrderErr->commit();
                        return $this->redirect(array('action' => 'errs'));
                    }
                }
            } catch(Exception $e) {
            }
            $this->Order->rollback();
            $this->OrderErr->rollback();
            $this->Flash->error('入金日登録に失敗しました。');
            
            return $this->redirect(array('action' => 'errs'));
        }

        $data = $this->Order->find('all',array(
            'fields' => 'Order.*,User.id,User.shop_name,User.tropo_telno,User.email,User.status,OrderErr.id,OrderErr.note,OrderErr.err_date',
            'joins' => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'users', 
                            'alias' => 'User',  
                            'conditions' => 'Order.user_id = User.id'
                            ),
                        array (
                            'type' => 'LEFT',    
                            'table' => 'order_errs', 
                            'alias' => 'OrderErr',  
                            'conditions' => 'OrderErr.order_id = Order.id'
                            ),
                        ),
            'conditions' => array('Order.id' => $id),
            'order' => array('OrderErr.created' => 'desc'),
            
        ));

        $this->set('order', $data[0]);
        $this->set('user_status',Configure::read("user_status"));
    
    }
    
    // 運営センター売り上げ管理(エラー)
    public function admin_errs() {
        
        $this->Paginator->settings = array(
            'fields' => 'Order.*,User.id,User.shop_name,User.tropo_telno,OrderErr.note,OrderErr.err_date,OrderErr.story',
            'joins' => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'users', 
                            'alias' => 'User',  
                            'conditions' => 'Order.user_id = User.id'
                            ),
                        array (
                            'type' => 'INNER',    
                            'table' => 'order_errs', 
                            'alias' => 'OrderErr',  
                            'conditions' => 'OrderErr.order_id = Order.id'
                            ),
                        ),
            'conditions' => array('Order.pay_date' => NULL),
            'order' => array('Order.id' => 'desc'),
            'limit' => 30,
        );
        $data = $this->Paginator->paginate('Order');
        $this->set('orders', $data);
        
        
        $ret = $this->Order->find('all',array(
            'fields' => 'id',
            'conditions' => array('Order.pay_date' => NULL),            
        ));
        $this->set('err_cnt', count($ret));
        
    
    }
    
    // 運営センター売り上げ管理
    public function admin_index($id = null) {

        $conditions = array();
        $this->loadModel('User');
        $this->loadModel('Telephones');
        $this->loadModel('OrderErr');
        
        $cnt = 0;
        $err_cnt = 0;
        
        if ($this->request->is('post') ) {

            $this->__sanitize();
            
            // 週次請求
            if(isset($this->request->data['do_week_data'])) {
                
                // 請求データ作成
                $datas = $this->set_order_weekly();
                if(empty($datas)){
                    $this->Flash->success('週次請求データはすでに作成されています。');
                    
                }else{
                    
                    $this->loadModel('OrderErr');
                    
                    // 対象期間を取得
                    $start_day = $datas[0]['Order']['start_date'];
                    $end_day   = $datas[0]['Order']['end_date'];
                    
                    // 請求データ登録
                    $pay_users = $this->invoicing_weekly_make($datas,$start_day);
                    
                    // 決済
                    $update_data = $this->CpOrder->periodic_claim($pay_users);
                    
                    // 決済結果をDB登録
                    if(array_key_exists("Order",$update_data)){
                        if (!$this->Order->saveall($update_data['Order'])) {
                            $this->log('check month update pay_date ng', LOG_DEVELOP);
                            $this->log($update_data, LOG_DEVELOP);
                        }
                        $cnt = count($update_data["Order"]);
                    }
                    if(array_key_exists("OrderErr",$update_data)){
                        if (!$this->OrderErr->saveall($update_data['OrderErr'])) {
                            $this->log('check month  ng', LOG_DEVELOP);
                            $this->log($update_data, LOG_DEVELOP);
                        }
                        $err_cnt = count($update_data["OrderErr"]);
                    }
                    // メール送信
                    $ret       = $this->invoicing_weekly_mail($start_day);
                    
                    if(!empty($update_data)){
                        $this->Flash->success('週次請求を'. count($pay_users).'件作成し '. $cnt.'件請求しました。[エラー'.$err_cnt.'件]');  
                    }else{
                        $this->Flash->error('週次請求作成に失敗しました。');
                    }
                }
            }
            
            
            // 月次請求
            if(isset($this->request->data['do_month_data'])) {
                
                // 請求データ作成
                $datas = $this->set_order_monthly();
                if(empty($datas)){
                    $this->Flash->success('月次請求データはすでに作成されています。');
                    
                }else{
                	
                	// 対象期間を取得
                    $start_day = $datas[0]['Order']['start_date'];
                    $end_day   = $datas[0]['Order']['end_date'];
                    
                    
                    // 請求データ登録
                    $pay_users = $this->invoicing_monthly_make($datas,$start_day);
                    
                    // 決済
                    $update_data = $this->CpOrder->periodic_claim($pay_users);
                    
                    
                    //print_r($update_data);
                    
                    // 決済結果をDB登録
                    if(array_key_exists("Order",$update_data)){
                        if (!$this->Order->saveall($update_data['Order'])) {
                            $this->log('check month update pay_date ng', LOG_DEVELOP);
                            $this->log($update_data, LOG_DEVELOP);
                        }
                        $cnt = count($update_data["Order"]);
                        
                    }
                    if(array_key_exists("OrderErr",$update_data)){
                    
                        if (!$this->OrderErr->saveall($update_data['OrderErr'])) {
                            $this->log('check month  ng', LOG_DEVELOP);
                            $this->log($update_data, LOG_DEVELOP);
                        }
                        $err_cnt = count($update_data["OrderErr"]);
                        
                    }
                    // メール送信
                    $ret       = $this->invoicing_monthly_mail($start_day);
                    
                    
                    if(!empty($update_data)){
                        $this->Flash->success('月次請求を'. $cnt.'件作成し '. ($cnt - $err_cnt) .'件請求しました。');  
                    }else{
                       $this->Flash->error('月次請求作成に失敗しました。');
                    }
                    
                }
            }
            
            // リセットボタン押下
            if(isset($this->request->data['reset'])) {
                $this->request->data['Order'] = array();
            }
            
            // ページ数リセット
            $this->request->params['named']['page'] = 1;

            
            if(array_key_exists('Order',$this->request->data)){ 
                
                // 未入金
                if(isset($this->request->data['no_pay'])) {
                    $conditions = array_merge($conditions,array('Order.pay_date' => NULL));
                }
                
                // 請求No
                if(array_key_exists('serch_id',$this->request->data['Order'])){
                    if($this->request->data['Order']['serch_id'] ){
                        $serch_id = (int)str_replace(ORDER_ID_HEDDER,"",$this->request->data['Order']['serch_id']);
                        $conditions = array_merge($conditions,array('Order.id' => $serch_id));
                    }
                }
                
                // 期間from>期間to の場合は値をひっくり返す
                if(array_key_exists('serch_start_date',$this->request->data['Order'])){
                    if($this->request->data['Order']['serch_start_date'] && $this->request->data['Order']['serch_end_date']){
                        if($this->request->data['Order']['serch_start_date'] > $this->request->data['Order']['serch_end_date']){
                            $tmp_date = $this->request->data['Order']['serch_start_date'];
                            $this->request->data['Order']['serch_start_date'] = $this->request->data['Order']['serch_end_date'];
                            $this->request->data['Order']['serch_end_date']   = $tmp_date;
                        }
                    }
                }
                // 期間from
                if(array_key_exists('serch_start_date',$this->request->data['Order'])){
                    if($this->request->data['Order']['serch_start_date']){
                        $conditions = array_merge($conditions,array('Order.invoice_date >=' => $this->request->data['Order']['serch_start_date'].' 00:00:00'));
                    }
                }
                // 期間to
                if(array_key_exists('serch_end_date',$this->request->data['Order'])){
                    if($this->request->data['Order']['serch_end_date']){
                        $conditions = array_merge($conditions,array('Order.invoice_date <=' => $this->request->data['Order']['serch_end_date'].' 23:59:59'));
                    }
                }
                
                
                // 支払い日　期間from>期間to の場合は値をひっくり返す
                if(array_key_exists('serch_pay_start_date',$this->request->data['Order'])){
                    if($this->request->data['Order']['serch_pay_start_date'] && $this->request->data['Order']['serch_pay_end_date']){
                        if($this->request->data['Order']['serch_pay_start_date'] > $this->request->data['Order']['serch_pay_end_date']){
                            $tmp_date = $this->request->data['Order']['serch_pay_start_date'];
                            $this->request->data['Order']['serch_pay_start_date'] = $this->request->data['Order']['serch_pay_end_date'];
                            $this->request->data['Order']['serch_pay_end_date']   = $tmp_date;
                        }
                    }
                }
                // 期間from
                if(array_key_exists('serch_pay_start_date',$this->request->data['Order'])){
                    if($this->request->data['Order']['serch_pay_start_date']){
                        $conditions = array_merge($conditions,array('Order.pay_date >=' => $this->request->data['Order']['serch_pay_start_date'].' 00:00:00'));
                    }
                }
                // 期間to
                if(array_key_exists('serch_pay_end_date',$this->request->data['Order'])){
                    if($this->request->data['Order']['serch_pay_end_date']){
                        $conditions = array_merge($conditions,array('Order.pay_date <=' => $this->request->data['Order']['serch_pay_end_date'].' 23:59:59'));
                    }
                }
                
                
                //ユーザー名
                if(!empty($this->request->data['Order']['search_text'])|| array_key_exists('search_text',$_POST)){
                    $conditions = array_merge($conditions,array('or' => array(
                                                                                array('Order.shop_name                        like' => "%{$this->request->data['Order']['search_text']}%"),
                                            )
                    ));
                    $this->Session->write('search_text', $this->request->data['Order']['search_text']);
                }else{
                    $this->Session->write('search_text', '');
                }
            }
            
        }else{
            
            if(array_key_exists('page',$this->request->params['named'])){
                if($this->Session->check('conditions')) {
                    $conditions = $this->Session->read('conditions');
                    if(array_key_exists('OR',$conditions)){
                        foreach($conditions['OR'] AS $user ){
                        }
                    }
                    if(array_key_exists('Order.invoice_date >=',$conditions)){$this->request->data['Order']['serch_start_date'] = date('Y-m-d', strtotime($conditions['Order.invoice_date >=']));}
                    if(array_key_exists('Order.invoice_date <=',$conditions)){$this->request->data['Order']['serch_end_date']   = date('Y-m-d', strtotime($conditions['Order.invoice_date <=']));}

                    if(array_key_exists('Order.pay_date >=',$conditions)){$this->request->data['Order']['serch_pay_start_date'] = date('Y-m-d', strtotime($conditions['Order.pay_date >=']));}
                    if(array_key_exists('Order.pay_date <=',$conditions)){$this->request->data['Order']['serch_pay_end_date']   = date('Y-m-d', strtotime($conditions['Order.pay_date <=']));}

                }
                if($this->Session->check('search_text')) {
                    $this->request->data['Order']['search_text'] = $this->Session->read('search_text');
                }
            }

        }
        
        $this->Session->write('conditions', h($conditions));
        
        // アカウント明細からユーザー指定で遷移してきた場合
        if($id){
            $conditions = array_merge($conditions,array('Order.user_id' => $id));
            $this->set('back_controller' , 'users');
            $this->set('back_action' , 'index');
            $this->set('user_status',Configure::read("user_status"));
            $this->set('user' , $this->User->findById($id));
            
        }else{
            $this->set('back_controller' , 'pages');
            $this->set('back_action' , 'home');

        }
        
        
        //print_r($conditions);
        $this->Paginator->settings = array(
            'fields' => 'Order.*,OrderErr.story',
            'conditions' => $conditions,
            'limit' => 100,
            'order' => array('Order.id' => 'desc'),
            'joins' => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'order_errs', 
                            'alias' => 'OrderErr',  
                            'conditions' => 'OrderErr.order_id = Order.id'
                            ),
                        ),
        );
        
        $data = $this->Paginator->paginate('Order');
        $this->set('orders', $data);
        
        
        $this->set('id', $id);
        
        // csv出力
        if(isset($this->request->data['outputcsv'])) {

            $data = $this->Order->find('all',array('fields'=>'Order.*','conditions'=>$conditions));
            
            $fp = fopen('php://temp/maxmemory:'.(5*1024*1024),'r+');
                
            foreach($data as $line){
                fputs($fp, implode(",",$line['Order'])."\n");
            }
            
            //ファイルポインタを先頭へ
            rewind($fp);
            
            //リソースを読み込み文字列取得
            $csv = stream_get_contents($fp);

            $filename = 'mindel_order_'. date('YmdHis').'.csv';

            header("Content-Type: application/octet-stream; charset=Shift_JIS");
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            
            //CSVをエクセルで開くことを想定して文字コードをSJIS-winSJISへ
            $csv = mb_convert_encoding($csv,"Shift-JIS", "UTF-8");
            
            print $csv;
            fclose($fp);
            
            die;
            exit;
        }

    
    }
    public function set_order_monthly(){
    
        $this->loadModel('User');
        
        $start_day = date('Y-m-01');
        $end_day   = date('Y-m-t');
        
        //すでに請求済みデータを取得
        $pay_users = $this->Order->find('list',array(
                'fields' => 'user_id',
                'conditions' => array(
                    'kind'         => 1,
                    'start_date'   => $start_day,
                    //'limit_date'   => NULL,
                    
                ),
        ));
        
        if(empty($pay_users)){
            $pay_users[] = 0;
        }
        
         $users = $this->User->find('all',array(
            'fields' => 'id,shop_name,responsible_name,email,hand_payment',
            'conditions' => array(
                'status' => 5,
                'id NOT IN' => $pay_users,
                'no_payment' => 0
                )
            ));
        $datas = array();
        
        // 請求データ作成
        foreach($users AS $user){
            
            $data['Order']['user_id']          = $user['User']['id'];
            
            $data['Order']['shop_name']        = $user['User']['shop_name'];
            $data['Order']['responsible_name'] = $user['User']['responsible_name'];
            $data['Order']['email']            = $user['User']['email'];
            $data['Order']['hand_payment']     = $user['User']['hand_payment'];
            $data['Order']['kind']         = 1;
            
            $data['Order']['invoice_date'] = date('Y-m-d');
            $data['Order']['limit_date']   = date('Y-m-d',strtotime('+ 7day'));
            $data['Order']['start_date']   = $start_day;
            $data['Order']['end_date']     = $end_day;

            $data['Order']['sum_price']    = FIRST_COST;
            $data['Order']['tax']          = FIRST_COST * TAX;
            $data['Order']['amount']       = FIRST_COST * (1 + TAX);
            
            $datas[] = $data;
            
        }
        return $datas;
        
    }
    
    public function set_order_weekly(){

        $this->loadModel('User');
        $this->loadModel('Telephones');

        $tgt_date = strtotime(date('Y/m/d'));
        //$tgt_date = strtotime('2018/2/25');
    
        //日曜日に請求
        //先週利用分のうちすでに請求済みデータを取得
        $weekNo       = date('w', $tgt_date);
        $weekFirstDay = date('Ymd', strtotime("-{$weekNo} day", $tgt_date));
        $start_day    = date('Y-m-d',strtotime($weekFirstDay . ' -1 week'));
        $end_day      = date('Y-m-d',strtotime($weekFirstDay . ' -1 day'));
        
        // すでに請求されたユーザーを調査
        $pay_users = $this->Order->find('list',array(
                'fields' => 'user_id',
                'conditions' => array(
                    'kind'         => 2,
                    'start_date'   => $start_day,
                    //'NOT' => array('Order.pay_date' => NULL),
                ),
        ));
        
        if(empty($pay_users)){
            $pay_users[] = 0;
        }
        
        $users = $this->User->find('all',array(
            'fields' => 'id,shop_name,responsible_name,email,hand_payment',
            'conditions' => array(
                'status' => 5,
                'id NOT IN' => $pay_users,
                'no_payment' => 0
                )
            ));
        $datas = array();
        
        // 請求データ作成
        foreach($users AS $user){
            
            // 通話代金
            
            $tels = $this->Telephones->find('all',array(
                //'fields' => 'SUM(time_to_sec(TIMEDIFF(Telephones.end_time,Telephones.start_time))) as timediff,(Telephones.kind % 2) as oddnum,',
                'fields' => 'SUM( CEIL(time_to_sec(TIMEDIFF(Telephones.end_time,Telephones.start_time)) / 60 )) as timediff,(Telephones.kind % 2) as oddnum,',
                'conditions' => array(
                    'Telephones.user_id     ' => $user['User']['id'],
//rec                    'Telephones.kind      > ' => '0',
                    'Telephones.kind NOT IN ' => array(4,8,10), //電話に出なかった場合は課金対象外
                    'Telephones.created between ? and ?' => array($start_day.' 00:00:00', $end_day.' 23:59:59'),
                    
                    ),
                'order' => array('Telephones.created'=>'desc' ,'(Telephones.kind % 2)' => 'asc'),
                'group'=> ' (Telephones.kind % 2)'
            ));
            $call_fees = Configure::read("call_fee");
            $tel_fee = 0;
            foreach($tels as $tel){
                //$tel_fee = $tel_fee  +  round($tel[0]['timediff']  * $call_fees[$tel[0]['oddnum']] / 60,0) ;
                $tel_fee = $tel_fee  +  $tel[0]['timediff']  * $call_fees[$tel[0]['oddnum']] ;
                
            }

            // 自動応答の回数を取得
            $autos = $this->Telephones->find('all',array(
                'fields' => 'COUNT(id) as cnt',
                'conditions' => array(
                    'user_id'=> $user['User']['id'],
                    'Telephones.created between ? and ?' => array($start_day.' 00:00:00', $end_day.' 23:59:59'),
                    'kind'   => '0',
                ),
            ));
            
            $auto_fee = 0;
            
            // 消費税増税対応
            $old_tax_rate = Configure::read("old_tax_rate");
            $tax_rate = TAX;
            foreach($old_tax_rate AS $limit => $old_rate){
                if( strtotime($start_day) <= strtotime($limit) ){
                    $tax_rate = $old_rate;
                    break;
                }
            }
            
            foreach($autos as $auto){
                $auto_fee = $auto[0]['cnt'] * AUTO_RESPONCE_FEE;
            }
            if(($tel_fee + $auto_fee) > 0){
                $data['Order']['user_id']           = $user['User']['id'];
                
                $data['Order']['shop_name']         = $user['User']['shop_name'];
                $data['Order']['responsible_name']  = $user['User']['responsible_name'];
                $data['Order']['email']             = $user['User']['email'];
                $data['Order']['hand_payment']      = $user['User']['hand_payment'];
                $data['Order']['kind']              = 2;
                
                $data['Order']['invoice_date']      = date('Y-m-d');
                $data['Order']['limit_date']        = date('Y-m-d',strtotime('+ 7day'));
                $data['Order']['start_date']        = $start_day;
                $data['Order']['end_date']          = date('Y-m-d',strtotime($start_day.' +6 day'));

                $data['Order']['callomg_price']     = round($tel_fee ,0);
                $data['Order']['message_price']     = round($auto_fee,0);

                $data['Order']['sum_price']         = round(( $tel_fee + $auto_fee)             ,0);
                $data['Order']['tax']               = round((($tel_fee + $auto_fee) * $tax_rate      ),0);
                $data['Order']['amount']            = round((($tel_fee + $auto_fee) * (1 + $tax_rate)),0);
                
                $datas[] = $data;
            }
            
        }
        
        return $datas;
    
    }
    
    public function invoicing_monthly_make($datas = array(),$start_day=null){
        
        $this->Order->begin;
        try {
            //請求データ作成
            if (!$this->Order->saveall($datas)) {
                throw new InternalErrorException(); 
            }
            $this->Order->commit();
        
        } catch (Exception $e) {
            $this->Order->rollback();
            return false;
            //$this->Flash->error('月次請求作成に失敗しました。');
        }
        
        // 決済
        //決済対象データを取得
        $pay_users = $this->Order->find('all',array(
                // ng 'fields' => 'id,user_id',
                'fields' => 'id,user_id,kind,amount,hand_payment',//add
                'conditions' => array(
                    'kind' => 1,
                    'start_date'   => $start_day,
                    'pay_date' => NULL,
                ),
        ));
        
        return $pay_users;
    }
    
    public function invoicing_monthly_mail($start_day = null){
   
        // 決済状況を取得
        $email_users = $this->Order->find('all',array(
                'fields' => '*',
                'conditions' => array(
                    'kind' => 1,
                    'start_date'   =>  $start_day,
                ),
        ));
        
        // メール送信
        $email = new CakeEmail('smtp');
        $email->emailFormat('text');
        foreach($email_users AS $email_user){
            
                $email->to($email_user['Order']['email']);
                $email->viewVars(array(
                //'pay_url'           => Router::url('/orders/pay/'.$email_user['Order']['id'], true),
                'pay_url'           => 'https://minderu.com/mypage/orders/pay/'. $email_user['Order']['id'],
                'shop_name'         => $email_user['Order']['shop_name'],
                'responsible_name'  => $email_user['Order']['responsible_name'],
                'data'              => $email_user['Order'],
                
            ));
            
            if($email_user['Order']['pay_date']){
                //$email->subject($start_day.'～'.$end_day.'のご利用代金を受領しました');
                //$email->template('invoicing');
            }else{
                if($email_user['Order']['hand_payment'] == 0){
                    $email->subject('【'.SITE_NAME.'】※重要なお知らせ※決済ができませんでした。');
                    $email->cc(ADMIN_MAIL);
                    $email->template('invoicing_err');
                    $email->send();
                }
            }
            
        }
        
        return true;
    }
    
    public function invoicing_weekly_make($datas = array(),$start_day = null){
    
        $this->Order->begin;
        try {
            // 請求データ作成
            if (!$this->Order->saveall($datas)) {
                return false;
                //throw new InternalErrorException(); 
            }
            
            // 登録確定
            $this->Order->commit();
        
        } catch (Exception $e) {
            $this->Order->rollback();
            return false;
            //$this->Flash->error('週次請求作成に失敗しました。');
        }

        // 決済対象データを取得
        $pay_users = $this->Order->find('all',array(
                //ng 'fields' => 'id,user_id',
                'fields' => 'id,user_id,kind,amount,hand_payment',//add
                'conditions' => array(
                    'kind' => 2,
                    'start_date'   =>  $start_day,
                    'pay_date' => NULL,
                ),
        ));

        return $pay_users;
    }
 
    public function invoicing_weekly_mail($start_day = null){
        
        // 決済状況を取得
        $email_users = $this->Order->find('all',array(
                'fields' => '*',
                'conditions' => array(
                    'kind' => 2,
                    'start_date'   =>  $start_day,
                ),
        ));
        
        // メール送信
        $email = new CakeEmail('smtp');
        $email->emailFormat('text');
        foreach($email_users AS $email_user){
            
                $email->to($email_user['Order']['email']);
                $email->viewVars(array(
                //'pay_url'           => Router::url('/orders/pay/'.$email_user['Order']['id'], true),
                'pay_url'           => 'https://minderu.com/mypage/orders/pay/'. $email_user['Order']['id'],
                'shop_name'         => $email_user['Order']['shop_name'],
                'responsible_name'  => $email_user['Order']['responsible_name'],
                'data'              => $email_user['Order'],
                
            ));
            
            if($email_user['Order']['pay_date']){
                //$email->subject($start_day.'～'.$end_day.'のご利用代金を受領しました');
                //$email->template('invoicing');
            }else{
                if($email_user['Order']['hand_payment'] == 0){
                    $email->subject('【'.SITE_NAME.'】※重要なお知らせ※決済ができませんでした。');
                    $email->cc(ADMIN_MAIL);
                    $email->template('invoicing_err');
                    $email->send();
                }
            }
            
        }
        
        return true;
    
    }
    
    // 決済エラーメールを個別手動で送る
    public function admin_send_err_mail($order_id = null){
        
        $this->autoRender = false;
        
        // ユーザーを取得
        $email_users = $this->Order->find('all',array(
                'fields' => '*',
                'conditions' => array(
                    'id' => $order_id,
                ),
        ));
        
        // メール送信
        $email = new CakeEmail('smtp');
        $email->emailFormat('text');
        foreach($email_users AS $email_user){
            
                $email->to($email_user['Order']['email']);
                $email->viewVars(array(
                //'pay_url'           => Router::url('/orders/pay/'.$email_user['Order']['id'], true),
                'pay_url'           => 'https://minderu.com/mypage/orders/pay/'. $email_user['Order']['id'],
                'shop_name'         => $email_user['Order']['shop_name'],
                'responsible_name'  => $email_user['Order']['responsible_name'],
                'data'              => $email_user['Order'],
                
            ));
            
            if($email_user['Order']['hand_payment'] == 0){
                $email->subject('【'.SITE_NAME.'】※重要なお知らせ※決済ができませんでした。');
                $email->cc(ADMIN_MAIL);
                $email->template('invoicing_err');
                $email->send();
            }
            
        }
        
        $this->Flash->success($email_user['Order']['email'].'宛にメール送信しました');
        return $this->redirect(array('action' => 'errs'));
    }
}