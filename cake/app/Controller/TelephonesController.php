<?php
class TelephonesController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    // 通話明細
    public function detail($yyyymm = null) {
        
        $this->loadModel('Orders'); 

        // csv出力
        if(isset($this->request->data['outputcsv'])) {
        
            $this->autoRender = false;
            $data = $this->Orders->find('all',array(
                'fields' => 'kind,start_date,end_date,invoice_date,limit_date,pay_date,callomg_price,message_price,sum_price,tax,amount',
                'conditions' => array(
                    'Orders.user_id     ' => $this->auth['id'],
                    'DATE_FORMAT(Orders.created, "%Y")'  => $this->request->data['Telephone']['yyyy'],
                    ),
            ));
            
            $fp = fopen('php://temp/maxmemory:'.(5*1024*1024),'r+');
            $title = "請求種類[1:基本使用料(月)、2:従量課金(週)],課金集計開始日,課金集計終了日,請求日,支払い期限,入金日,通話代金,自動応答使用代金,小計,税額,合計";
            //fputs($fp , mb_convert_encoding($title,"Shift-JIS","auto")."\r\n");
            fputs($fp , $title."\r\n");
            

            foreach($data as $line){
                fputs($fp, implode(",",$line['Orders'])."\r\n");
            }
            
            //ファイルポインタを先頭へ
            rewind($fp);
            
            //リソースを読み込み文字列取得
            $csv = stream_get_contents($fp);

            $filename = 'mindel_order_'. $this->request->data['Telephone']['yyyy'].'.csv';

            header("Content-Type: application/octet-stream; charset=Shift_JIS");
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            
            //CSVをエクセルで開くことを想定して文字コードをSJIS-winSJISへ
            $csv = mb_convert_encoding($csv,"Shift-JIS", "UTF-8");
            
            print $csv;
            fclose($fp);
            
            die;
            exit;
            
        }

        $yyyymm_list = array();
        
        // 年月切替
        if ($this->request->is('post') || $this->request->is('put'))  {
            $yyyymm = $this->request->data['Telephone']['yyyymm'];
        }
        if(!$yyyymm){
            $yyyymm = date('Ym');
        }
        $this->set('yyyymm', $yyyymm);
        
        
        // 該当月の日曜日を取得
        $tgt_w = array();
        for($i = 5; $i > 0;$i--){
            $tgt_w[] = date('Y-m-d',strtotime($this->_getNthWeekday($yyyymm, $i, 0)));
        }
        // 翌月初週が入ってしまった場合は削除
        if(date('Ym',strtotime($tgt_w[0])) > $yyyymm){
            unset($tgt_w[0]);
        }
        $this->set('tgt_w', $tgt_w);
        
        
        // 通話データの取得
        $this->loadModel('Telephones'); 
        $datas = $this->Telephones->find('all',array(
            //'fields' => 'date(created + interval 1 - DAYOFWEEK(created) day) as week ,count(id) as cnt,SUM(time_to_sec(TIMEDIFF(Telephones.end_time,Telephones.start_time))) as timediff,(Telephones.kind % 2) as oddnum,',
            'fields' => 'date(created + interval 1 - DAYOFWEEK(created) day) as week ,count(id) as cnt,SUM(  CEIL( time_to_sec(TIMEDIFF(Telephones.end_time,Telephones.start_time)) / 60)  ) as timediff,(Telephones.kind % 2) as oddnum,',
            'conditions' => array(
                'Telephones.user_id     ' => $this->auth['id'],
//rec                'Telephones.kind      > ' => '0',
                'Telephones.kind NOT IN ' => array(4,8,10), //電話に出なかった場合は課金対象外
//ng                'DATE_FORMAT(Telephones.created, "%Y%m")' => $yyyymm,
                ),
                
            'order' => array('Telephones.created'=>'desc' ,'(Telephones.kind % 2)' => 'asc'),
            'group'=> 'date(created + interval 1 - DAYOFWEEK(created) day), (Telephones.kind % 2)'
        ));

        $call_fees = Configure::read("call_fee");
        $data = array();
        foreach($datas as $ret){

            if(date('Ym',$ret[0]['week'] == $yyyymm)){  // add
                if(array_key_exists($ret[0]['week'],$data)){
                    //$data[$ret[0]['week']] = round($data[$ret[0]['week']]  +  $ret[0]['timediff']  * $call_fees[$ret[0]['oddnum']] / 60 );
                    $data[$ret[0]['week']] = $data[$ret[0]['week']] +  $ret[0]['timediff']      * $call_fees[$ret[0]['oddnum']];
                    
                }else{
                    //$data[$ret[0]['week']] = round($ret[0]['timediff']  * $call_fees[$ret[0]['oddnum']] / 60 );
                    $data[$ret[0]['week']] = $ret[0]['timediff']   * $call_fees[$ret[0]['oddnum']];
                }
            }
        }
        
        $this->set('telephones', $data);
        
        
        // 該当月の請求データを取得
        $datas = $this->Orders->find('list',array(
            'fields' => 'start_date,id',
            'conditions' => array(
                'Orders.user_id     ' => $this->auth['id'],
                'Orders.kind        ' => 2,
                'DATE_FORMAT(Orders.start_date, "%Y%m")' => $yyyymm,
                ),
        ));
        $this->set('orders', $datas);
        
        
        // 年月リストの取得 
        $yyyymm_list1 = array(); 
        $yyyymm_list2 = array(); 
        $rets = $this->Telephones->find('all', array(
            'fields' => array('DATE_FORMAT(date(Telephones.created + interval 1 - DAYOFWEEK(Telephones.created) day), "%Y%m") as ym','DATE_FORMAT(date(Telephones.created + interval 1 - DAYOFWEEK(Telephones.created) day), "%Y/%m") as val'),
            'group'  => array('DATE_FORMAT(date(Telephones.created + interval 1 - DAYOFWEEK(Telephones.created) day), "%Y%m")'),
            'order' => array('Telephones.created' => 'desc'),
            'conditions' => array(
                'Telephones.user_id     ' => $this->auth['id'],
                ),
            ));
        foreach($rets AS $ret){
            $yyyymm_list1[$ret[0]['ym']] = $ret[0]['val'];
        }
        $rets = $this->Orders->find('all', array(
            'fields' => array('DATE_FORMAT(Orders.created, "%Y%m") as ym','DATE_FORMAT(Orders.created, "%Y/%m") as val'),
            'group'  => array('DATE_FORMAT(Orders.created, "%Y%m")'),
            'order' => array('Orders.created' => 'desc'),
            'conditions' => array(
                'Orders.user_id     ' => $this->auth['id'],
                ),
            ));
        foreach($rets AS $ret){
            $yyyymm_list2[$ret[0]['ym']] = $ret[0]['val'];
        }
        
        $yyyymm_list = array(date('Ym')=>date('Y/m'));
        $yyyymm_list = $yyyymm_list + $yyyymm_list1 + $yyyymm_list2;
        
        arsort($yyyymm_list);
        
        $this->set('yyyymm_list', $yyyymm_list);
        
        // 年リストの作成
        $yyyy_list = array();
        $rets = $this->Orders->find('all', array(
            'fields' => array('DATE_FORMAT(Orders.created, "%Y") as y','DATE_FORMAT(Orders.created, "%Y") as v'),
            'group'  => array('DATE_FORMAT(Orders.created, "%Y")'),
            'order' => array('Orders.created' => 'desc'),
            'conditions' => array(
                'Orders.user_id     ' => $this->auth['id'],
                ),
            ));
        foreach($rets AS $ret){
            $yyyy_list[$ret[0]['y']] = $ret[0]['v'];
        }
        $this->set('yyyy_list', $yyyy_list);
        $this->set('yyyy', date('Y',strtotime($yyyymm.'01')));
        
        
        
        
        // 自動応答の回数を取得
        $rets = $this->Telephones->find('all',array(
            'fields' => 'date(created + interval 1 - DAYOFWEEK(created) day) as week,COUNT(id) as cnt',
            'conditions' => array(
                'user_id'=> $this->auth['id'],
//ng                'DATE_FORMAT(Telephones.created, "%Y%m")' => $yyyymm,
                'kind'   => '0',
            ),
            'group'=> 'date(created + interval 1 - DAYOFWEEK(created) day)'
            
        ));
        $auto_responces = array();
        foreach($rets as $ret){
            if(date('Ym',$ret[0]['week'] == $yyyymm)){  // add
                $auto_responces[$ret[0]['week']] = $ret[0]['cnt'];
            }
        }
        $this->set('auto_responces',$auto_responces);
        
        
        // 月額基本料
        $datas = $this->Orders->find('all',array(
            'fields' => 'id,start_date,end_date,sum_price,tax,amount,invoice_date,limit_date',
            'conditions' => array(
                'Orders.user_id     ' => $this->auth['id'],
                'Orders.kind        ' => 1,
                'Orders.start_date ' => date('Y-m-d',strtotime($yyyymm.'01')),
                ),
        ));
        if(empty($datas)){
            $this->set('order_monthly', null);
        }else{
            $this->set('order_monthly',$datas[0]['Orders']);
        }
    
        // 消費税変更対応
        $old_tax_rate = Configure::read("old_tax_rate");
        $this->set('old_tax_rate', $old_tax_rate);
    
    
    }
    // 着信履歴
    public function index() {
    
        if($this->auth['status'] == 0){
            return $this->redirect(array('controller'=>'users','action' => 'edit'));
        }elseif($this->auth['status'] == 1){
            return $this->redirect(array('controller'=>'telephones','action' => 'status_1'));
        }elseif($this->auth['status'] == 2){
            return $this->redirect(array('controller'=>'users','action' => 'edit'));
        }elseif($this->auth['status'] == 3){
            return $this->redirect(array('controller'=>'teltransfers','action' => 'new'));
        }elseif($this->auth['status'] == 4){
            return $this->redirect(array('controller'=>'telephones','action' => 'status_4'));
        }
        
        // 本運用年月日を加味
        $conditions = array('Telephones.user_id' => $this->auth['id']);
        if($this->auth['formally_start_date']){
            $conditions = array_merge($conditions,array('Telephones.created >=' => $this->auth['formally_start_date'].' 00:00:00'));
        }
        
        $this->loadModel('Telephones'); 
        $this->Paginator->settings = array(
            'fields' => 'Telephones.id,Telephones.recording_url,Telephones.telno,Telephones.teltransferno,Telephones.start_time,Telephones.end_time,Telephones.kind,Telephones.memo,Teltransfer.memo,Teltransfer1.memo',
            //'conditions' => array('Telephones.user_id' => $this->auth['id'],'Telephones.created >= ' => date('Y-m-d H:i:s',strtotime("-3 month"))),
            'conditions' => $conditions,
            'order' => array('Telephones.created' => 'desc'),
            'limit' => 30, 
            'joins' => array(
                        array (
                        'type' => 'LEFT',    
                        'table' => 'teltransfers', 
                        'alias' => 'Teltransfer',  
                        'conditions' => "replace(Teltransfer.telno,'-','') = replace(Telephones.teltransferno,'+81','0') AND Teltransfer.user_id =".$this->auth['id'],
                        ),
                        array (
                        'type' => 'LEFT',    
                        'table' => 'teltransfers', 
                        'alias' => 'Teltransfer1',  
                        'conditions' => "replace(Teltransfer1.telno,'-','') = replace(Telephones.telno,'+81','0') AND Teltransfer1.user_id =".$this->auth['id'],
                        ),
                   ),
        );
        $data = $this->Paginator->paginate('Telephones');
        $this->set('telephones', $data);
        
        // メモ更新
        if ($this->request->is('post') || $this->request->is('put')) {
            if($this->request->data['Telephones']['id'] > 0){
              if ($this->Telephones->save($this->request->data)) {
                  //$this->Flash->success('メモを更新しました');
              }
            }
            return $this->redirect(array('action' => 'index/page:'.$this->request->params['named']['page']));
            //$this->Flash->error('メモの更新に失敗しました。');
        }
        
    }
    
    public function status_1() {
        $this->layout = 'user_add';
    }
    public function status_4() {
        $this->layout = 'user_add';
    }
    
    function _getNthWeekday($month, $n, $w){
        $date = $this->_getFirstWeekday($month, $w);
        return date('Ymd', strtotime($date . ' +' . ($n-1) . ' week'));
    }
    
    function _getFirstWeekday($month, $w){
        $date = date('Ymd', strtotime($month . '01 this week '.($w-1).' day'));
        if(substr($date, 0, 6) !== $month){
            $date = date('Ymd', strtotime($date . ' 1 week'));
        }
        return $date;
    }
    
    // 通話明細 ( 管理者用 )
    public function admin_detail($user_id = null,$yyyymm = null) {
        
        
        $this->loadModel('Orders'); 
        $yyyymm_list = array();
        
        // 年月切替
        if ($this->request->is('post') || $this->request->is('put'))  {
            $yyyymm = $this->request->data['Telephone']['yyyymm'];
        }
        if(!$yyyymm){
            $yyyymm = date('Ym');
        }
        $this->set('yyyymm', $yyyymm);
        
        
        // 該当月の日曜日を取得
        $tgt_w = array();
        for($i = 5; $i > 0;$i--){
            $tgt_w[] = date('Y-m-d',strtotime($this->_getNthWeekday($yyyymm, $i, 0)));
        }
        // 翌月初週が入ってしまった場合は削除
        if(date('Ym',strtotime($tgt_w[0])) > $yyyymm){
            unset($tgt_w[0]);
        }
        $this->set('tgt_w', $tgt_w);
        
        // 通話データの取得
        $this->loadModel('Telephones'); 
        $datas = $this->Telephones->find('all',array(
            //'fields' => 'date(created + interval 1 - DAYOFWEEK(created) day) as week ,count(id) as cnt,SUM(time_to_sec(TIMEDIFF(Telephones.end_time,Telephones.start_time))) as timediff,(Telephones.kind % 2) as oddnum,',
            'fields' => 'date(created + interval 1 - DAYOFWEEK(created) day) as week ,count(id) as cnt,SUM(  CEIL( time_to_sec(TIMEDIFF(Telephones.end_time,Telephones.start_time)) / 60)  ) as timediff,(Telephones.kind % 2) as oddnum,',
            'conditions' => array(
                'Telephones.user_id     ' => $user_id,
                'Telephones.kind      > ' => '0',
                'Telephones.kind NOT IN ' => array(4,8,10), //電話に出なかった場合は課金対象外
//ng                'DATE_FORMAT(Telephones.created, "%Y%m")' => $yyyymm,
                ),
                
            'order' => array('Telephones.created'=>'desc' ,'(Telephones.kind % 2)' => 'asc'),
            'group'=> 'date(created + interval 1 - DAYOFWEEK(created) day), (Telephones.kind % 2)'
        ));

        $call_fees = Configure::read("call_fee");
        $data = array();
        foreach($datas as $ret){

            if(date('Ym',$ret[0]['week'] == $yyyymm)){  // add
                if(array_key_exists($ret[0]['week'],$data)){
                    //$data[$ret[0]['week']] = round($data[$ret[0]['week']]  +  $ret[0]['timediff']  * $call_fees[$ret[0]['oddnum']] / 60 );
                    $data[$ret[0]['week']] = $data[$ret[0]['week']] +  $ret[0]['timediff']      * $call_fees[$ret[0]['oddnum']];
                    
                }else{
                    //$data[$ret[0]['week']] = round($ret[0]['timediff']  * $call_fees[$ret[0]['oddnum']] / 60 );
                    $data[$ret[0]['week']] = $ret[0]['timediff']   * $call_fees[$ret[0]['oddnum']];
                }
            }
        }
        $this->set('telephones', $data);
        
        
        // 該当月の請求データを取得
        $datas = $this->Orders->find('list',array(
            'fields' => 'start_date,id',
            'conditions' => array(
                'Orders.user_id     ' => $user_id,
                'Orders.kind        ' => 2,
                'DATE_FORMAT(Orders.start_date, "%Y%m")' => $yyyymm,
                ),
        ));
        $this->set('orders', $datas);
        
        
        // 年月リストの取得 
        $yyyymm_list1 = array(); 
        $yyyymm_list2 = array(); 
        $rets = $this->Telephones->find('all', array(
            //'fields' => array('DATE_FORMAT(Telephones.created, "%Y%m") as ym','DATE_FORMAT(Telephones.created, "%Y/%m") as val'),
            'fields' => array('DATE_FORMAT(date(Telephones.created + interval 1 - DAYOFWEEK(Telephones.created) day), "%Y%m") as ym','DATE_FORMAT(date(Telephones.created + interval 1 - DAYOFWEEK(Telephones.created) day), "%Y/%m") as val'),
            //'group'  => array('DATE_FORMAT(Telephones.created, "%Y%m")'),
            'group'  => array('DATE_FORMAT(date(created + interval 1 - DAYOFWEEK(Telephones.created) day), "%Y%m")'),
            'order' => array('Telephones.created' => 'desc'),
            'conditions' => array(
                'Telephones.user_id     ' => $user_id,
                ),
            ));
        foreach($rets AS $ret){
            $yyyymm_list1[$ret[0]['ym']] = $ret[0]['val'];
        }

        $rets = $this->Orders->find('all', array(
            'fields' => array('DATE_FORMAT(Orders.created, "%Y%m") as ym','DATE_FORMAT(Orders.created, "%Y/%m") as val'),
            'group'  => array('DATE_FORMAT(Orders.created, "%Y%m")'),
            'order' => array('Orders.created' => 'desc'),
            'conditions' => array(
                'Orders.user_id     ' => $user_id,
                ),
            ));
        foreach($rets AS $ret){
            $yyyymm_list2[$ret[0]['ym']] = $ret[0]['val'];
        }

        $yyyymm_list = array(date('Ym')=>date('Y/m'));
        //$yyyymm_list = $yyyymm_list + $yyyymm_list + $yyyymm_list1;
        $yyyymm_list = $yyyymm_list + $yyyymm_list1 + $yyyymm_list2;
        
        arsort($yyyymm_list);
        
        $this->set('yyyymm_list', $yyyymm_list);
        
        
        // 自動応答の回数を取得
        $rets = $this->Telephones->find('all',array(
            'fields' => 'date(created + interval 1 - DAYOFWEEK(created) day) as week,COUNT(id) as cnt',
            'conditions' => array(
                'user_id'=> $user_id,
//ng                'DATE_FORMAT(Telephones.created, "%Y%m")' => $yyyymm,
                'kind'   => '0',
            ),
            'group'=> 'date(created + interval 1 - DAYOFWEEK(created) day)'
            
        ));
        $auto_responces = array();
        foreach($rets as $ret){
            if(date('Ym',$ret[0]['week'] == $yyyymm)){  // add
                $auto_responces[$ret[0]['week']] = $ret[0]['cnt'];
            }
        }
        $this->set('auto_responces',$auto_responces);
        
        
        // 月額基本料
        $datas = $this->Orders->find('all',array(
            'fields' => 'id,start_date,end_date,sum_price,tax,amount,invoice_date,limit_date',
            'conditions' => array(
                'Orders.user_id     ' => $user_id,
                'Orders.kind        ' => 1,
                'Orders.start_date ' => date('Y-m-d',strtotime($yyyymm.'01')),
                ),
        ));
        if(empty($datas)){
            $this->set('order_monthly', null);
        }else{
            $this->set('order_monthly',$datas[0]['Orders']);
        }
        
        // ショップ名表示
        $this->loadModel('Users');
        $user_data = $this->Users->findById($user_id);
        unset($user_data['Users']['password']);
        $this->set('shop_name',$user_data['Users']['shop_name']);
    
    
        // 消費税変更対応
        $old_tax_rate = Configure::read("old_tax_rate");
        $this->set('old_tax_rate', $old_tax_rate);

    
    }
    
    // 着信履歴 ( 管理者用 )
    public function admin_index($user_id = null) {
    
         // ショップ名表示
        $this->loadModel('Users');
        $user_data = $this->Users->findById($user_id);
        unset($user_data['Users']['password']);
        $this->set('shop_name',$user_data['Users']['shop_name']);
        
        $this->set('user',$user_data['Users']);
        
        // 本運用年月日を加味
        $conditions = array('Telephones.user_id' => $user_id);
        if($user_data['Users']['formally_start_date']){
            $conditions = array_merge($conditions,array('Telephones.created >=' => $user_data['Users']['formally_start_date'].' 00:00:00'));
        }

        $this->loadModel('Telephones'); 
        $this->Paginator->settings = array(
            'fields' => 'Telephones.recording_url,Telephones.telno,Telephones.teltransferno,Telephones.start_time,Telephones.end_time,Telephones.kind,Teltransfer.memo,Teltransfer1.memo,Telephones.memo',
            //'conditions' => array('Telephones.user_id' => $user_id,'Telephones.created >= ' => date('Y-m-d H:i:s',strtotime("-12 month"))),
            'conditions' => $conditions,
            'order' => array('Telephones.created' => 'desc'),
            'limit' => 30, 
            'joins' => array(
                        array (
                        'type' => 'LEFT',    
                        'table' => 'teltransfers', 
                        'alias' => 'Teltransfer',  
                        'conditions' => "replace(Teltransfer.telno,'-','') = replace(Telephones.teltransferno,'+81','0') AND Teltransfer.user_id =".$user_id,
                        ),
                        array (
                        'type' => 'LEFT',    
                        'table' => 'teltransfers', 
                        'alias' => 'Teltransfer1',  
                        'conditions' => "replace(Teltransfer1.telno,'-','') = replace(Telephones.telno,'+81','0') AND Teltransfer1.user_id =".$user_id,
                        ),
                   ),
        );
        $data = $this->Paginator->paginate('Telephones');
        $this->set('telephones', $data);
        
        // 料金計算用
        $call_kind = Configure::read("call_kind");
        $call_fees = Configure::read("call_fee");
        $this->set('call_fees', $call_fees);
        $this->set('call_kind', $call_kind);
        
        
        // csv出力
        if(isset($this->request->data['outputcsv'])) {

            $data = $this->Telephones->find('all',array(
            'fields' => 'Telephones.recording_url,Telephones.telno,Telephones.teltransferno,Telephones.start_time,Telephones.end_time,Telephones.kind,Teltransfer.memo,Teltransfer1.memo,Telephones.memo',
            'conditions' => $conditions,
            'order' => array('Telephones.created' => 'desc'),
            
            'joins' => array(
                        array (
                        'type' => 'LEFT',    
                        'table' => 'teltransfers', 
                        'alias' => 'Teltransfer',  
                        'conditions' => "replace(Teltransfer.telno,'-','') = replace(Telephones.teltransferno,'+81','0') AND Teltransfer.user_id =".$user_id,
                        ),
                        array (
                        'type' => 'LEFT',    
                        'table' => 'teltransfers', 
                        'alias' => 'Teltransfer1',  
                        'conditions' => "replace(Teltransfer1.telno,'-','') = replace(Telephones.telno,'+81','0') AND Teltransfer1.user_id =".$user_id,
                        ),
                   ),
            ));
            
            
            
            
            
            
            $fp = fopen('php://temp/maxmemory:'.(5*1024*1024),'r+');

            $header = array();
            $header[] = "通話種類";
            $header[] = "発信元";
            $header[] = "発信元名称";
            $header[] = "着信先";
            $header[] = "着信先名称/録音URL";
            $header[] = "通話開始";
            $header[] = "通話終了";
            $header[] = "通話時間";
            $header[] = "料金(概算)";
            $header[] = "メモ";
            fputs($fp, implode(",",$header)."\n");
            
            foreach($data as $line){
                $body = array();
                $body[] = $call_kind[$line['Telephones']['kind']];                  // 通話種類
                $body[] = str_replace("+81","0", $line['Telephones']['telno']);     // 発信元
                $body[] = $line['Teltransfer1']['memo'];                            // 発信元名称
                if($line['Telephones']['kind'] == 0){                               
                    $body[] = "[自動応答]";                                         // 着信先
                    $body[] = $line['Telephones']['recording_url'];                 //着信先名称/録音URL
                }elseif($line['Telephones']['kind'] == 4 || $line['Telephones']['kind'] == 8 ){
                    $body[] = "[対応中/未応答]";
                    $body[] = "";
                }elseif($line['Telephones']['kind'] == 10 ){
                    $body[] = "（着信前離脱）";
                    $body[] = "";
                }else{
                    $body[] = str_replace("+81","0", $line['Telephones']['teltransferno']);
                    $body[] = $line['Teltransfer']['memo'];   
                }
                
                $body[] = $line['Telephones']['start_time'];                                                                                                                                    // 通話開始
                if($line['Telephones']['end_time']){$body[] = date("H:i:s",strtotime($line['Telephones']['end_time']));}else{$body[] = "";}                                                     // 通話終了
                if($line['Telephones']['end_time']){$body[] = gmdate("H:i:s", (strtotime($line['Telephones']['end_time']) - strtotime($line['Telephones']['start_time'])));}else{$body[] = "";} // 通話時間
              
                // 料金(概算)
                if(in_array($line['Telephones']['kind'],array(1,2,3,5,6,7))){$body[] = round(((strtotime($line['Telephones']['end_time']) - strtotime($line['Telephones']['start_time'])) / 60 * $call_fees[$line['Telephones']['kind'] % 2 ]),2)."円";} 
                elseif($line['Telephones']['kind'] == 0)                    {$body[] = AUTO_RESPONCE_FEE."円";}  
                else{$body[] = "";}

                $body[] = $line['Telephones']['memo']; // メモ
                                
                fputs($fp, rtrim(implode(",",$body),',')."\n");
            }
            
            //ファイルポインタを先頭へ
            rewind($fp);
            
            //リソースを読み込み文字列取得
            $csv = stream_get_contents($fp);

            $filename = 'minderu_telephones_'.$user_id.'_'. date('YmdHis').'.csv';

            header("Content-Type: application/octet-stream; charset=Shift_JIS");
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            
            //CSVをエクセルで開くことを想定して文字コードをSJIS-winSJISへ
            $csv = mb_convert_encoding($csv,"Shift-JIS", "UTF-8");
            
            print $csv;
            fclose($fp);
            
            die;
            exit;
        }

    }
    
}