<?php
class StaffsController extends AppController {
    
    public $uses = array('Staff');
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        Security::setHash('blowfish');
        
    }

    public function index() {
    
         if ($this->request->is('post') ) {

            $this->__sanitize();
             
            // 一括登録ボタン押下
            if(isset($this->request->data['bulk_reg'])) {
                
                $this->LoadModel('Teltransfer');
                $teltransfers = $this->Teltransfer->find('all',array('conditions' => array('user_id' => $this->auth['id']),'order' => array('num' => 'asc')));

                $save_datas = array();
                foreach($teltransfers AS $teltransfer){

                    $save_data['user_id'] = $this->auth['id'];
                    $save_data['username'] = $this->auth['id'].'A'.str_replace('-','',$teltransfer['Teltransfer']['telno']);
                    $save_data['showname'] = $teltransfer['Teltransfer']['memo'];
                    $save_data['password'] = "9999";
                    $save_data['role'] = 0;
                    $save_data['active'] = 1;
                    
                    $save_datas[] = $save_data;    
                
                }
                
                if(!empty($save_datas)){
                    $ret = $this->Staff->saveall($save_datas);

                    if(!empty($this->Staff->validationErrors)){
                        //$this->log($this->Staff->validationErrors, LOG_DEVELOP);
                        $this->Flash->error('一括登録できませんでした。（転送先がすでに登録されているかもしれません。)');
                    }else{
                        $this->Flash->success('一括登録しました。');
                    }

                }
            }
         }

        $this->Paginator->settings = array(
            'limit' => 30,
            'conditions' => array('user_id' => $this->auth['id']),
            'order' => array('Staff.id' => 'asc')
        );

        $datas = $this->Paginator->paginate('Staff');
        $this->set('datas', $datas);
    
    }

    
    public function view($id = null) {
        $this->Staff->id = $id;
        if (!$this->Staff->exists()) {
            throw new NotFoundException(__('Invalid staff'));
        }
        $this->set('staff', $this->Staff->findById($id));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Staff->create();
            if ($this->Staff->save($this->request->data)) {
                $this->Flash->success('スタッフが追加されました');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error('スタッフの追加に失敗しました。');
        }
    }

    public function delete($id = null) {
                
        // Prior to 2.5 use
        $this->request->onlyAllow('post');
        $this->autoRender = false;
        
        $this->Staff->id = $id;
        if (!$this->Staff->exists()) {
            throw new NotFoundException(__('Invalid staff'));
        }
        if ($this->Staff->delete()) {
            $this->Flash->success('スタッフが削除されました');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error('スタッフ削除に失敗しました');
        return $this->redirect(array('action' => 'index'));
    }

    public function edit($id = null) {
        $this->Staff->id = $id;
        if (!$this->Staff->exists()) {
            throw new NotFoundException(__('Invalid staff'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
        
            if ($this->Staff->save($this->request->data)) {
                
                
                $this->Flash->success('スタッフ情報が更新されました');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error('スタッフ情報の更新に失敗しました。');
        } else {
            $this->request->data = $this->Staff->findById($id);
            unset($this->request->data['Staff']['password']);
        }
        
        
        
    }
   


}