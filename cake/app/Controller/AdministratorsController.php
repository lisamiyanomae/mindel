<?php
class AdministratorsController extends AppController {
    
    
    public $uses = array('Administrator');
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        Security::setHash('blowfish');
        
        //$this->Auth->allow('add', 'logout');
    }

    public function admin_login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $auth = $this->Auth->user();
                $this->log("ログイン:".$auth['username'],'app');
                $this->redirect($this->Auth->redirect());
            } else {
                $this->Flash->error('ユーザーIDとパスワードを正しく入力してください。');
            }
        }
    }
    

    public function admin_index() {
    
        $this->Paginator->settings = array(
            'limit' => 30,
            'order' => array('Administrator.created' => 'desc')
        );

        $data = $this->Paginator->paginate('Administrator');
        $this->set('admininfo', $data);

        $this->set('role_name', Configure::read("role_name"));

    
    }

    public function admin_logout() {
        $this->redirect($this->Auth->logout());
    }
    
    public function admin_view($id = null) {
        $this->Administrator->id = $id;
        if (!$this->Administrator->exists()) {
            throw new NotFoundException(__('Invalid administrator'));
        }
        $this->set('administrator', $this->Administrator->findById($id));
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Administrator->create();
            if ($this->Administrator->save($this->request->data)) {
                $this->Flash->success('運営ユーザーが追加されました');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error('運営ユーザーの追加に失敗しました。時間を空けて再度お試しください。');
        }
    }

    public function admin_delete($id = null) {
        
        // Prior to 2.5 use
        $this->request->onlyAllow('post');
        $this->autoRender = false;
        
        $this->Administrator->id = $id;
        if (!$this->Administrator->exists()) {
            throw new NotFoundException(__('Invalid administrator'));
        }
        if ($this->Administrator->delete()) {
            $this->Flash->success('運営ユーザーが削除されました');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error('運営ユーザー削除に失敗しました');
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_edit($id = null) {
        $this->Administrator->id = $id;
        if (!$this->Administrator->exists()) {
            throw new NotFoundException(__('Invalid administrator'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
        
            if ($this->Administrator->save($this->request->data)) {
                
                // Auth情報の書き換え
                $auth = $this->Auth->user();
                if($id == $auth['id']){
                    $auth['username'] = $this->data['Administrator']['username'];
                    $auth['showname'] = $this->data['Administrator']['showname'];
                    if(array_key_exists('role',$this->data['Administrator'])){
                        $auth['role'] = $this->data['Administrator']['role'];
                    }
                    $this->Session->write('Auth', $auth);
                    $this->Auth->logout();
                    $this->Auth->login($auth);
                }
                
                $this->Flash->success('運営ユーザー情報が更新されました');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error('運営ユーザー情報の更新に失敗しました。');
        } else {
            $this->request->data = $this->Administrator->findById($id);
            unset($this->request->data['Administrator']['password']);
        }
        
        $this->set('role_name', Configure::read("role_name"));
        
    }
   


}