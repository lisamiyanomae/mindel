<?php
class CompanyUsersController extends AppController {
    
    
    //public $uses = array('CompanyUser');
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        Security::setHash('blowfish');
        
    }

    public function admin_index($id = null) {
    
        $this->LoadModel('Company');
        $company = $this->Company->findById($id);
        unset($company['password']);
        $this->set('company', $company['Company']);

        $this->Paginator->settings = array(
            'fields' => 'CompanyUser.*,User.shop_name,User.tropo_telno,User.email',
            'limit' => 30,
            'order' => array('CompanyUser.user_id' => 'desc'),
            'joins' => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'users', 
                            'alias' => 'User',  
                            'conditions' => 'CompanyUser.user_id = User.id'
                            ),
            ),
            'conditions' => array('CompanyUser.company_id' => $id),
            
        );

        $datas = $this->Paginator->paginate('CompanyUser');
        $this->set('datas', $datas);
        
        
        // ユーザーをカンマ区切りで取得
        $usrlist = array();
        foreach($datas AS $data){
            $usrlist[] = $data['CompanyUser']['user_id'];
        }
        
        $this->LoadModel('User');
        $rets = $this->User->Find('list',array(
            'fields' => 'User.id,User.shop_name,User.prefecture',
            'order' => array('User.prefecture' => 'asc','User.id' => 'asc'),
            'conditions' => array('User.status <=' => 5, 'User.status >' => 0,'NOT' => array('User.id' => $usrlist)),
        ));
        $users = array();
        $pref_list = Configure::read("pref_list");
        foreach($rets AS $key=>$ret){
            $users[$pref_list[$key]][] = $ret;
        }
        $this->set('users', $users);
        
        // 追加
        if ($this->request->is('post')) {
            
            if ($this->CompanyUser->save($this->request->data)) {
                $this->Flash->success('アカウントが追加されました');
                
            }else{
                $this->Flash->error('アカウントの追加に失敗しました。');
                $this->CompanyUser->invalidate('user_id', 'ユーザーを指定してください。');

            }
            return $this->redirect(array('action' => 'index/'.$id));
        
        }
    
    }


    public function admin_delete($id = null,$company_id = null) {
        
        // Prior to 2.5 use
        $this->request->onlyAllow('post');
        $this->autoRender = false;
        
        $this->CompanyUser->id = $id;
        if (!$this->CompanyUser->exists()) {
            throw new NotFoundException(__('Invalid company'));
        }
        if ($this->CompanyUser->delete()) {
            $this->Flash->success('アカウントが削除されました');
            return $this->redirect(array('action' => 'index/'.$company_id));
        }
        $this->Flash->error('アカウント削除に失敗しました');
        return $this->redirect(array('action' => 'index/'.$company_id));
    }



}