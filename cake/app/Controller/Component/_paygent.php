<?php
include_once("jp/co/ks/merchanttool/connectmodule/entity/ResponseDataFactory.php");
include_once("jp/co/ks/merchanttool/connectmodule/system/PaygentB2BModule.php");
include_once("jp/co/ks/merchanttool/connectmodule/exception/PaygentB2BModuleConnectException.php");
include_once("jp/co/ks/merchanttool/connectmodule/exception/PaygentB2BModuleException.php");
/**
 * paygentお支払い用コンポーネントクラス
 *
 */
class Paygent extends Object
{
    /*
     * 電文種別ID
     */
    const TELEGRAM_KIND = "telegram_kind";
    /*
     * 電文ハ繧凵[シ繧刄㏍灯ﾔ号
     */
    const TELEGRAM_VERSION = "telegram_version";
    /*
     * マーチャントID
     */
    const MERCHANT_ID = "merchant_id";
    /*
     * 取引ID
     */
    const TRADING_ID = "trading_id";
    /*
     * 3Dセキュア不要区分
     */
    const THREED_SECURE_RYAKU = "3dsecure_ryaku";
    /*
     * 決済金額
     */
    const PAYMENT_AMOUNT = "payment_amount";
    /*
     * カート繧剩ﾔ号
     */
    const CARD_NUMBER = "card_number";
    /*
     * カート繧剽L効期限
     */
    const CARD_VALID_TERM = "card_valid_term";
    /*
     * カート繧刳m認番号
     */
    const CARD_CONF_NUMBER = "card_conf_number";
    /*
     * カードトークン
     */
    const CARD_TOKEN = "card_token";
    /*
     * 支払区分
     */
    const PAYMENT_CLASS = "payment_class";
    /*
     * 分割払い回数
     */
    const SPLIT_COUNT = "split_count";
    /*
     * コンビニ利用者姓
     */
    const CUSTOMER_FAMILY_NAME = "customer_family_name";
    /*
     * コンビニ利用者名
     */
    const CUSTOMER_NAME = "customer_name";
    /*
     * コンビニ利用者電話番号
     */
    const CUSTOMER_TEL = "customer_tel";
    /*
     * コンビニID
     */
    const CVS_COMPANY_ID = "cvs_company_id";
    /*
     * コンビニ支払期限日
     */
    const PAYMENT_LIMIT_DATE = "payment_limit_date";
    /*
     * コンビニ支払種別
     */
    const SALES_TYPE = "sales_type";
    /*
     * 電文種別カード払い
     */
    const TELEGRAM_KIND_CARD = 'card';
    /*
     * 電文種別コンビニ払い
     */
    const TELEGRAM_KIND_CSTORE = 'cstore';
    
    /*
     * 電文種別カード情報照会 10pg
     */
    const TELEGRAM_KIND_CARD_INFO = 'cardinfo';
    /*
     * 電文種別カード情報登録 10pg
     */
    const TELEGRAM_KIND_CARD_INSERT = 'cardinsert';
    /*
     * 電文種別カード情報削除 10pg
     */
    const TELEGRAM_KIND_CARD_DELETE = 'carddelete';
    
    /*
     * カード情報お預りモード 10pg
     */
    const STOCK_CARD_MODE = 'stock_card_mode';
    /*
     * 顧客ID 10pg カード情報を預ける場合に値をセット
     */
     const CUSTOMER_ID = 'customer_id';
    /*
     * 顧客カードID 10pg 預けたカード情報のID これの指定のみで決済できる
     */
     const CUSTOMER_CARD_ID = 'customer_card_id';

    /*
     * 対応電文種別IDリスト
     */
    private $_telegramKinds = array(self::TELEGRAM_KIND_CARD=>'020', self::TELEGRAM_KIND_CSTORE=>'030',self::TELEGRAM_KIND_CARD_INSERT=>'025',self::TELEGRAM_KIND_CARD_INFO=>'027',self::TELEGRAM_KIND_CARD_DELETE=>'026');
    /*
     * 対応コンビニ店一覧（コンヒ繧刄j接続タイフ繧哂）
     */
    private $_cstoreList = array('00C016'=>'セイコーマート',
                                 '00C002'=>'ローソン',
                                 '00C004'=>'ミニストップ',
                                 '00C006'=>'サンクス',
                                 '00C007'=>'サークルK',
                                 '00C014'=>'デイリーヤマザキ',
                                 '00C001'=>'セブンイレブン',
                                 '00C005'=>'ファミリーマート',
                                );
    /*
     * マーチャントID
     */
    private $_merchantId = '37778';//10pg 
    /*
     * 電文バージョン
     */
    private $_telegramVersion = '1.0';
    /*
     * コンビニ支払期限日デフォルト
     * 1縲・0日で設定する
     */
    private $_defaultCstorePaymentLimitDate = 7;
    /*
     * コンビニ支払種別デフォルト
     * 1:前払い、3:後払い
     */
    private $_defaultCstoreSalesType = 3;
    /*
     * module instance
     */
    private $_paymentModule = null;
    /*
     * カレントの電文種別
     */
    private $_telegramKind = null;
    /*
     * 送信に必要な各パラーメータを格納
     */
    private $_settings = array();
    /*
     * paygentからのresponse
     */
    private $_response = null;
    /*
     * エラー
     */
    private $_errors = array();

    /*
     * カード情報リスト 10pg
     */
    private $_card_info = array();

    /**
     *
     * @param $controller
     * @return void
     */
    public function startup(&$controller)
    {
        $this->controller = $controller;
    }

    /**
     * 支払い処理実行
     * @param string $telegramKind
     * @param array $params
     * @return boolean
     */
    public function execute($telegramKind, $params=array())
    {
    
        if (!$this->_isTelegramKind($telegramKind)) return false;
       
        $this->_telegramKind = $telegramKind;
        $this->_settings[$telegramKind]  = am(array(
            self::TELEGRAM_KIND => $this->_telegramKinds[$telegramKind],
            self::TELEGRAM_VERSION => $this->_telegramVersion,
            self::MERCHANT_ID => $this->_merchantId,
            self::THREED_SECURE_RYAKU => 1),
        $params);
        
        
        return $this->_post();
        
    }
    /**
     * paygentからのresponseをそのまま返却する
     * @return array
     */
    public function getResponse()
    {
        return $this->_response;
    }
    /**
     * responst データ部(カード情報)を返却 10pg
     * 
     */
    public function getCardInfo()
    {
        return $this->_card_info;
    }
    /**
     * エラーあり？
     * @return boolean
     */
    public function isError()
    {
        return (count($this->_errors));
    }
    /**
     * エラー情報を返却する
     * @return boolean
     */
    public function getErrors()
    {
        return $this->_errors;
    }
    /**
     * コンビニリストを返却すいる
     * @return array
     */
    public function getCstoreList()
    {
        return $this->_cstoreList;
    }
    /**
     * 正しい電文種別が指定されているか
     * @param string $telegramKind
     * @return boolean
     */
    private function _isTelegramKind($telegramKind="")
    {
        if (empty($telegramKind) || !isset($this->_telegramKinds[$telegramKind])) {
            return false;
        }
        return true;
    }
    /**
     * paygentにpost実行
     * @return boolean
     */
    private function _post()
    {
        
        extract($this->_settings[$this->_telegramKind]);
        
        $paymentMethod = $this->_createPaymentModule();
        
        
        
        $paymentMethod->reqPut(self::TELEGRAM_KIND, ${self::TELEGRAM_KIND});
        $paymentMethod->reqPut(self::TELEGRAM_VERSION, ${self::TELEGRAM_VERSION});
        $paymentMethod->reqPut(self::MERCHANT_ID, ${self::MERCHANT_ID});

        if ($this->_isCard()) {
            
            $paymentMethod->reqPut(self::TRADING_ID, ${self::TRADING_ID});
            $paymentMethod->reqPut(self::PAYMENT_CLASS, ${self::PAYMENT_CLASS});
            $paymentMethod->reqPut(self::SPLIT_COUNT, ${self::SPLIT_COUNT});
            $paymentMethod->reqPut(self::THREED_SECURE_RYAKU, $this->_settings[$this->_telegramKind][self::THREED_SECURE_RYAKU]);
            
             if(${self::CUSTOMER_CARD_ID}){
                 $paymentMethod->reqPut(self::CUSTOMER_ID, ${self::CUSTOMER_ID});           //10pg
                 $paymentMethod->reqPut(self::CUSTOMER_CARD_ID, ${self::CUSTOMER_CARD_ID}); //10pg
                 $paymentMethod->reqPut(self::STOCK_CARD_MODE, ${self::STOCK_CARD_MODE});   //10pg
             }else{
                $paymentMethod->reqPut('security_code_token',0);                           //カード情報非保持化
                $paymentMethod->reqPut(self::CARD_TOKEN, ${self::CARD_TOKEN});             //カード情報非保持化
            }
            $paymentMethod->reqPut(self::PAYMENT_AMOUNT, ${self::PAYMENT_AMOUNT});         //10pg
        
        } else if ($this->_isCardInfo()) { //10pg 
            $paymentMethod->reqPut(self::CUSTOMER_ID, ${self::CUSTOMER_ID});           
            $paymentMethod->reqPut(self::CUSTOMER_CARD_ID, ${self::CUSTOMER_CARD_ID}); 
        
        } else if ($this->_isCardInsert()) { //10pg 
            $paymentMethod->reqPut(self::CUSTOMER_ID, ${self::CUSTOMER_ID});          
            $paymentMethod->reqPut(self::CARD_TOKEN, ${self::CARD_TOKEN});             //カード情報非保持化
            //$paymentMethod->reqPut(self::TRADING_ID, ${self::TRADING_ID});

        } else if ($this->_isCardDelete()) { //10pg 
            $paymentMethod->reqPut(self::CUSTOMER_ID, ${self::CUSTOMER_ID});           
            $paymentMethod->reqPut(self::CUSTOMER_CARD_ID, ${self::CUSTOMER_CARD_ID});
        }
        
        $result = $paymentMethod->post();

        $this->log($result);
        if (!$result === true) {
            $this->_errors[] = "支払い処理に失敗しました";
            return false;
        }
        $resultStatus = $paymentMethod->getResultStatus();
        $this->log($resultStatus);

        if ($resultStatus == 1) {
            $this->_errors[] = sprintf('支払い処理に失敗しました(%s)', $paymentMethod->getResponseCode());
            $this->log($this->_errors);
            return false;
        }
        
        if ($paymentMethod->hasResNext()) {
           
            $this->_response = $paymentMethod->resNext();
            $this->log($this->_response);
            
            // データ部をセット 10pg
            $this->_card_info[] = $this->_response;
            while ($paymentMethod->hasResNext()) {
                $this->_card_info[] = $paymentMethod->resNext();
            }
        }
        
        return true;
    }

    /**
     * カード決済か？
     * @return boolean
     */
    private function _isCard()
    {
        return (isset($this->_telegramKind) && $this->_telegramKind === self::TELEGRAM_KIND_CARD);
    }

    /**
     * コンビニ決済か？
     * @return boolean
     */
    private function _isCStore()
    {
        return (isset($this->_telegramKind) && $this->_telegramKind === self::TELEGRAM_KIND_CSTORE);
    }
    /**
     * カード情報取得か？//10pg 
     * @return boolean
     */
    private function _isCardInfo()
    {
        return (isset($this->_telegramKind) && $this->_telegramKind === self::TELEGRAM_KIND_CARD_INFO);
    }
    /**
     * カード情報削除か？//10pg 
     * @return boolean
     */
    private function _isCardDelete()
    {
        return (isset($this->_telegramKind) && $this->_telegramKind === self::TELEGRAM_KIND_CARD_DELETE);
    }
    /**
     * カード情報登録か？//10pg 
     * @return boolean
     */
    private function _isCardInsert()
    {
        return (isset($this->_telegramKind) && $this->_telegramKind === self::TELEGRAM_KIND_CARD_INSERT);
    }
    /**
     * paygent先生のありがたいmoduleインスタンスを生成する。初期化もする。
     *
     * @return PaygentB2BModule
     */
    private function _createPaymentModule()
    {
    
        if (!$this->_paymentModule) {
             $this->_paymentModule = new PaygentB2BModule();
        }
        $this->_paymentModule->init();
        
        
        return $this->_paymentModule;
    }
}