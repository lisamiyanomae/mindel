<?php
App::uses('Component', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * CpPasswordComponent
 *
 */
class CpPasswordComponent extends Component 
{
    var $_RESET_ID_SESSION = "Password.reset_id";
    var $_RESET_CODE_SESSION = "Password.reset_code";

    /**
     * Startup component
     *
     * @param object $controller Instantiating controller
     */
    function startup(Controller $controller)
    {
        $this->controller = $controller;
    }
    
    /**
     * (non-PHPdoc)
     * @see app/controllers/components/CpComponent#getModelName()
     */
    function getModelName()
    {
        return $this->controller->User->name;
    }

    /**
     * パスワード再発行処理を行う
     *
     * @param string $email
     * @return boolean
     */
    function republishPassword($email=null)
    {
        if (!$email) return false;
        if ($user = $this->controller->User->findByEmail($email)) {
            if($user['User']['status'] > 5 ){
                return false;
            }
            $resetCode = $this->controller->User->publishPasswordResetCode($user['User']['id']);
            $this->sendRepublishEmail($user, $resetCode);
            return true;
        }
        return false;
    }

    /**
     * パスワード再発行メールを送信する
     *
     * @param integer $userId
     * @param string $passwordResetCode
     * @return boolean
     */
    function sendRepublishEmail($user, $passwordResetCode)
    {
        $this->controller->User->id = $user['User']['id'];
        if (!$this->controller->User->exists()) return false;

        $republishUrl = Router::url(sprintf('/forgot_password/%s/%s', $user['User']['id'], $passwordResetCode), true);

        //読み込む設定ファイルの変数名を指定
        $email = new CakeEmail('default');
        $email->to($user['User']['email']);

        //HTMLorテキストメール
        $email->emailFormat('text');
        //テンプレート
        $email->template('republish_password');
        //テンプレートへ渡す変数。[変数名=>値]
        $email->viewVars(array(
          'republish_url'   => $republishUrl,
          'responsible_name'=> $user['User']['responsible_name'],
          'email'           => $user['User']['email'],
          'shop_name'       => $user['User']['shop_name']
        ));
        $email->subject('【'.SITE_NAME.'】パスワード変更手順のお知らせ');
        $email->send();
        
    }

    /**
     * 再発行キーをセットする
     * @param integer $userId
     * @param string $passwordResetCode
     * @return void
     */
    function setRepublishKeys($userId, $passwordResetCode)
    {
        $this->controller->Session->write($this->_RESET_ID_SESSION, $userId);
        $this->controller->Session->write($this->_RESET_CODE_SESSION, $passwordResetCode);
    }

    /**
     * 再発行キーを返却する
     *
     * @return array
     */
    function getRepublishKeys()
    {
        if ($this->controller->Session->check($this->_RESET_ID_SESSION) &&
            $this->controller->Session->check($this->_RESET_CODE_SESSION)) {

            return array($this->controller->Session->read($this->_RESET_ID_SESSION),
                         $this->controller->Session->read($this->_RESET_CODE_SESSION));
        }
        return array();
    }

    /**
     * 再発行キーを削除する
     *
     * @return void
     */
    function clearRepublishKeys()
    {
        $this->controller->Session->delete($this->_RESET_ID_SESSION);
        $this->controller->Session->delete($this->_RESET_CODE_SESSION);
    }

    /**
     * パスワード再発行リクエストは正しいですか？
     * @param integer $id
     * @param string $passwordResetCode
     * @return boolean
     */
    function isCorrectResetRequest($id, $passwordResetCode)
    {
        return (bool) $this->controller->User->findByIdAndPasswordResetCode($id, $passwordResetCode);
    }
}
?>