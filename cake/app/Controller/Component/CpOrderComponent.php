<?php
//App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');

/**
 * CpOrderComponent
 *
 */
 
include_once("paygent.php");

 
class CpOrderComponent extends Component
{

    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }
    
    private $reg_customer_card_id;
    
    /**
     * バリデーション
     *
     * @param array $params
     * @return boolean
     */
    function validate($params)
    {
        $isError = false;
        
        //order
        $this->controller->Order->data = $params;
        if (!$this->controller->Order->validates()) {
            $isError = true;
        }

        return !$isError;
    }

    /**
     * 注文処理実行
     *
     * @param array $params
     * @param array $carts
     * @param array $paymentOption
     * @return boolean
     */
    //function save(&$params, $carts, $paymentOption, $user, &$orderId)
    function save_first(&$params,$user)
    {
        if (!$this->validate($params)) return false;
        
        $this->controller->Order->begin;
        try {
            $this->controller->Order->data = $params;
            
            //請求データ登録
            $params['Order']['user_id']                  = $user['id'];
            
            $params['Order']['shop_name']                = $user['shop_name'];
            $params['Order']['responsible_name']         = $user['responsible_name'];
            
            $params['Order']['hand_payment']             = 0;
            $params['Order']['kind']         = 1;
            
            $params['Order']['invoice_date'] = date('Y-m-d');
            //$params['Order']['limit_date']   = date('Y-m-t');
            $params['Order']['start_date']   = date('Y-m-01');
            $params['Order']['end_date']     = date('Y-m-t');

            $params['Order']['sum_price']    = FIRST_COST;
            $params['Order']['tax']          = FIRST_COST * TAX;
            $params['Order']['amount']       = FIRST_COST * (1 + TAX);
            $params['Order']['pay_date']     = date('Y-m-d H:i:s');
        
            if (!$this->controller->Order->save($params)) {
                $this->controller->Order->rollback();
                return false;
            }
            $orderId = $this->controller->Order->getLastInsertId();


            // カード登録処理
            if($params['Order']['save_card_info'] == 1){   // カード情報を保存する 10pg
                
                if(!$this->_insertCardInfo($params)){
                    $this->controller->Order->rollback();
                    $this->controller->Order->invalidate("credit_card_no", "カード情報を保存できませんでした。");
                    return false;
                    
                }else{
                    $params['Order']['customer_card_id'] = $this->reg_customer_card_id;
                }
            }
            
            // 決済本体
            if($params['Order']['received_amount'] > 0){
                if(!$this->_checkCard($params,$orderId)){
                    $this->controller->Order->rollback();
                    $this->controller->Order->invalidate("credit_card_no", "カード決済エラーです。正しい情報を入れなおしてください。");
                    return false;
                }
            }
            
        } catch (Exception $e) {
            $this->controller->Order->rollback();
            return false;
        }
        
        $this->controller->Order->commit();

        return true;
    }
    
     /**
     * ポイント払いユーザーの初期請求作成
     *
     * @param array $params
     * @param array $carts
     * @param array $paymentOption
     * @return boolean
     */
    //function save(&$params, $carts, $paymentOption, $user, &$orderId)
    function save_start(&$params,$user)
    {
        if (!$this->validate($params)) return false;
        
        $data = array();
        $this->controller->Order->begin;
        try {
            $this->controller->Order->data = $params;
            
            //請求データ登録
            $params['Order']['user_id']                  = $user['id'];
            
            $params['Order']['shop_name']                = $user['shop_name'];
            $params['Order']['responsible_name']         = $user['responsible_name'];
            
            $params['Order']['hand_payment']             = 1;
            $params['Order']['kind']         = 1;
            
            $params['Order']['invoice_date'] = date('Y-m-d');
            //$params['Order']['limit_date']   = date('Y-m-t');
            $params['Order']['start_date']   = date('Y-m-01');
            $params['Order']['end_date']     = date('Y-m-t');

            $params['Order']['sum_price']    = FIRST_COST;
            $params['Order']['tax']          = FIRST_COST * TAX;
            $params['Order']['amount']       = FIRST_COST * (1 + TAX);
            
            if (!$this->controller->Order->save($params)) {
                $this->controller->Order->rollback();
                return false;
            }
            
            
            // 決済エラーを作る
            $orderId = $this->controller->Order->getLastInsertId();
            $data['user_id']   = $params['Order']['user_id'];
            $data['order_id']  = $orderId;
            $data['err_date']  = date('Y-m-d H:i:s');
            $data['note']  = '振込払いユーザー初回月額費用';
            
            $datas['OrderErr'][] = $data;
            
        } catch (Exception $e) {
            $this->controller->Order->rollback();
            return false;
        }
        
        $this->controller->Order->commit();

        return $datas;
    }
    
    
    /* 決済エラー後再請求書処理 */
    function pay(&$params,$user)
    {
        if (!$this->validate($params)) return false;

        $this->controller->Order->begin;
        try {

            $this->controller->Order->data = $params;
            
            //請求データ登録
            $params['Order']['pay_date'] = date('Y-m-d H:i:s');

            // カード登録処理
            if($params['Order']['save_card_info'] == 1){   // カード情報を保存する 10pg
                
                if(!$this->_insertCardInfo($params)){
                    $this->controller->Order->rollback();
                    $this->controller->Order->invalidate("credit_card_no", "カード情報を保存できませんでした。");
                    return false;
                    
                }else{
                    $params['Order']['customer_card_id'] = $this->reg_customer_card_id;
                }
            }
            // 決済本体
            if($params['Order']['received_amount'] > 0){
                if($this->_checkCard($params,$params['Order']['id'])){
                    
                    // 決済okなら注文エラー情報に顛末をセット
                    $ret = $this->controller->OrderErr->findByOrderId($params['Order']['id']);
                    $this->controller->OrderErr->id = $ret['OrderErr']['id'];
                    $err['OrderErr']['story']  = '別カード再決済';
                    if (!$this->controller->OrderErr->save($err,false)) {
                        $this->log('OrderErr Update Err', LOG_DEVELOP);
                        $this->log($params, LOG_DEVELOP);
                    }
                }else{
                    $this->controller->Order->rollback();
                    $this->controller->Order->invalidate("credit_card_no", "カード決済エラーです。正しい情報を入れなおしてください。");
                    return false;
                }
            }
            
            
            if (!$this->controller->Order->save($params)) {
                $this->controller->Order->rollback();
                return false;
            }
            
            
            
        } catch (Exception $e) {
            $this->controller->Order->rollback();
            return false;
        }
        
        $this->controller->Order->commit();

        return true;
    }
    
    /* カード情報変更 */
    function change_card(&$params,$user){
        
        // カード登録処理
        if($params['Order']['save_card_info'] == 1){   // カード情報を保存する 10pg
            
            if(!$this->_insertCardInfo($params)){
                $this->controller->Order->rollback();
                $this->controller->Order->invalidate("credit_card_no", "カード情報を保存できませんでした。");
                return false;
                
            }else{
                $params['Order']['customer_card_id'] = $this->reg_customer_card_id;
                return true;
            }
        }
    }
    
    
    // 定期課金(月次・週次)
    function periodic_claim(&$orders)
    {
    
        $ret_cnt = 0;
        $datas = array();
        
        foreach($orders as $order){
                    
            $data = array();
            
            $card = $this->getCardInfo($order['Order']['user_id'],3);
            
            
            if($card && $order['Order']['hand_payment'] == 0){
            
                $order['Order']['customer_card_id'] = key($card);
                if($order['Order']['kind'] == 1){  //add
                	$order['Order']['received_amount']  = FIRST_COST * (1 + TAX);   //ng
                }else{
                	$order['Order']['received_amount']  =  CEIL($order['Order']['amount']);
                }
                
                if($this->_checkCard($order,$order['Order']['id'])){
                
                    $data['id']             = $order['Order']['id'] ;
                    $data['pay_date']       = date('Y-m-d H:i:s');
					
					$datas['Order'][] = $data;
					
					
                }else{
                
                    $data['user_id']   = $order['Order']['user_id'];
		            $data['order_id']  = $order['Order']['id'];
		            $data['err_date']  = date('Y-m-d H:i:s');
                    $data['note']  = '決済エラー';
					
					$datas['OrderErr'][] = $data;
					
                }
                
            }else{


	            $data['user_id']   = $order['Order']['user_id'];
	            $data['order_id']  = $order['Order']['id'];
	            $data['err_date']  = date('Y-m-d H:i:s');
            
                if($order['Order']['hand_payment'] == 0){
                    $data['note']  = 'カード情報未登録';
                }else{
                	$data['note']  = '振込払い';
                }
				
				
				$datas['OrderErr'][] = $data;
            }
            
            
        }
        
        return $datas;
        


    }

    /**
     * データコンバート
     *
     * @param array $params
     * @return array
     */
    function convertSaveParams($params)
    {
        if (!isset($params['Order']['deliver_name'])) return $params;
        $params = $this->_convertSaveParams($params);
        

        
        $params['Order']["user_fax"] = mb_convert_kana($params['Order']["user_fax"], 'n', 'UTF-8');
        $params['Order']['cvs_cusutomer_tel'] = str_replace("-", "", $params['Order']['user_tel']);
//        $params['Order']['cvs_cusutomer_tel'] = $params['Order']['user_tel'];
        $params = $this->_convertSaveParams($params, "deliver");
        return $params;
    }



    /**
     * サニタイズ
     *
     * @param array $params
     * @return array
     */
    function sanitize($params)
    {
        $params = $this->_sanitize($params);
        $params['Order']["user_fax"] = Sanitize::html($params['Order']['user_fax'], true);
        $params = $this->_sanitize($params, "deliver");
        return $params;
    }



    /**
     * 保存されているカード情報を取得 10pg
     * $flg 削除判定用フラグ
     *  初期値：null
     *  1:顧客番号と有効期限だけ返す
     *  2:有効期限切れのものだけ返す
     */
    function getCardInfo($user_id = null,$flg = null) 
    {
        //App::import('Component', 'Paygent');
        
        $paygent = new Paygent();
        
        $paygentParams = array(
                        Paygent::CUSTOMER_CARD_ID => NULL,
                        Paygent::CUSTOMER_ID => $user_id,
                        
                        );
        
        $paygent->execute(Paygent::TELEGRAM_KIND_CARD_INFO, $paygentParams);
        
        if ($paygent->isError()) 
        {
            return false;
        }
        
        $card_infos = $paygent->getCardInfo();
        
        $return = array();
        foreach($card_infos AS $card_info){
        
            $card_limit = (int)substr($card_info['card_valid_term'],2,2).substr($card_info['card_valid_term'],0,2);
            $today_ym   = (int)(date('y').date('m'));
        
            //顧客番号と有効期限だけ返す
            if($flg == 1){
                $return = $return + array($card_info['customer_card_id'] => $card_info['card_last_use_date']);
                
            //有効期限切れのものだけ返す
            }elseif($flg == 2){
                if($card_limit < $today_ym  ){
                    $return = $return + array($card_info['customer_card_id'] => $card_info['card_brand'].' '.$card_info['card_number'].' 有効期限 '.substr($card_info['card_valid_term'],0,2).'月/'.substr($card_info['card_valid_term'],2,2).'年 ');
                }
            //全て
            }elseif($flg == 3){
            	if($card_limit >= $today_ym  ){
                	$return = $return + array($card_info['customer_card_id'] => $card_info['card_brand'].' '.$card_info['card_number'].' 有効期限 '.substr($card_info['card_valid_term'],0,2).'月/'.substr($card_info['card_valid_term'],2,2).'年 ');
                }else{
                	$return = $return + array($card_info['customer_card_id'] => $card_info['card_brand'].' '.$card_info['card_number'].' 有効期限 <span class="tyuui">'.substr($card_info['card_valid_term'],0,2).'月/'.substr($card_info['card_valid_term'],2,2).'年(有効期限が切れています)</span> ');
                }
                
            //期限が切れていないものだけ
            }else{
                if($card_limit >= $today_ym  ){
                    $return = $return + array($card_info['customer_card_id'] => $card_info['card_brand'].' '.$card_info['card_number'].' 有効期限 '.substr($card_info['card_valid_term'],0,2).'月/'.substr($card_info['card_valid_term'],2,2).'年 ');
                }
            }
        
        }

        return $return;
    
    }
    /**
     * カード情報を登録 10pg
     *
     */
    function _insertCardInfo($params , $max = 0)
    {
        App::import('Component', 'Paygent');
            
        $paygent = new Paygent();
        
        //カード登録数を確認 
        $card_infos = $this->getCardInfo($params['Order']['user_id'],1);
        
        // 登録されているカード情報を削除してから登録する(1枚だけ登録)
        if(is_array($card_infos)){
            if($max == 0){
                //foreach($card_infos AS $key => $val){
                //    $this->_deleteCardInfo($params['Order']['user_id'],$key);
                //}
            //指定枚数以上登録されている場合は、利用日の一番古いものを削除
            }else{
                if($max > 10){$max = 10;}
                if(count($card_infos) >= $max){
                    $older_nos = array_keys($card_infos);
                    $this->_deleteCardInfo($params['Order']['user_id'],$older_nos[0]);
                }
            }
        }
        
        // 登録
        $paygentParams = array(
                        Paygent::CUSTOMER_ID => $params['Order']['user_id'],
                        Paygent::CARD_TOKEN => $params['Order']['token'], //カード情報非保持化 
                        );
        
        
        $paygent->execute(Paygent::TELEGRAM_KIND_CARD_INSERT, $paygentParams);
        
        if ($paygent->isError()) 
        {
            return false;
        }
        
        
        // 新しいカードが登録出来たら以前のカードを削除
        if(is_array($card_infos) && $max == 0){
            $older_nos = array_keys($card_infos);
            if(count($older_nos) > 0){
            	$this->_deleteCardInfo($params['Order']['user_id'],$older_nos[0]);
            }
        }
        
        $response = $paygent->getResponse();
        
        $this->reg_customer_card_id = $response['customer_card_id'];

        unset($response['']);
        return true;
    
    }
    /**
     * カード情報を削除 10pg
     *
     */
    function _deleteCardInfo($user_id,$customer_card_id = '')
    {
    App::import('Component', 'CpPaygent');
        
        $paygent = new Paygent();
        
        $paygentParams = array(
                        
                        Paygent::CUSTOMER_ID => $user_id,
                        Paygent::CUSTOMER_CARD_ID =>  $customer_card_id,
                        );
        $paygent->execute(Paygent::TELEGRAM_KIND_CARD_DELETE, $paygentParams);
        
        
        if ($paygent->isError()) 
        {
            return false;
        }
        $response = $paygent->getResponse();
        
        unset($response['']);
        
        return true;
    
    }
    /**
     * カード決済処理
     *
     */
    function _checkCard($params, $orderId)
    {
        $paygent = new Paygent();
        
        $paygentParams = array($paygent::PAYMENT_AMOUNT => $params['Order']['received_amount'] ,
                               $paygent::PAYMENT_CLASS  => '10',
                               $paygent::SPLIT_COUNT    => 1,
                               $paygent::TRADING_ID     => $orderId
                       );
        
        //保存されたカード情報を使う場合 10pg
        if($params['Order']['customer_card_id'] > 0){
            $this->reg_customer_card_id = $params['Order']['customer_card_id'];
        }
        
        if($this->reg_customer_card_id > 0){
            $paygentParams = array_merge($paygentParams,array(Paygent::CUSTOMER_ID      => $params['Order']['user_id']));
            $paygentParams = array_merge($paygentParams,array(Paygent::CUSTOMER_CARD_ID => $this->reg_customer_card_id));
            $paygentParams = array_merge($paygentParams,array(Paygent::STOCK_CARD_MODE  => 1));
            
        }else{
            $paygentParams = array_merge($paygentParams,array(Paygent::CARD_TOKEN => $params['Order']['token']));
        }
        
        // トークンを渡す      
        $paygent->execute($paygent::TELEGRAM_KIND_CARD, $paygentParams);

        
        
        if ($paygent->isError()) return false;
        
        $response = $paygent->getResponse();
        unset($response['']);
        
        $this->reg_customer_card_id = null;
        
        $response['order_id'] = $orderId;

        return true;
    }




}
