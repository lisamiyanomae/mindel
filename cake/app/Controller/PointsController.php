<?php
class PointsController extends AppController {
    
    public $uses = array('Point');
    
    public function beforeFilter() {
        parent::beforeFilter();
    
    }

    public function admin_index() {

        // データが作成されていない振り込み払いユーザーがいれば対応を即す
        $reg_ids = $this->Point->find("list",array(
            "fields" => "Point.user_id",
            "groups" => array("user_id"),
        ));
        
        if(empty($reg_ids)){
            $reg_ids[] = 0;
        }
        
        $this->LoadModel("User");
        $add_user_ids = $this->User->find("all",array("fields" =>"id,shop_name","conditions"=>array("hand_payment" => 1,"id NOT IN" => $reg_ids , "parent_id" => NULL)));
        $this->set('add_user_ids', $add_user_ids);
        
        
        $conditions = array();
        
        if ($this->request->is('post') ) {

            $this->__sanitize();
            
            // リセットボタン押下
            if(isset($this->request->data['reset'])) {
                $this->request->data['Point'] = array();
            }
            
            // ページ数リセット
            $this->request->params['named']['page'] = 1;
            
            if(array_key_exists('Point',$this->request->data)){ 
                
                // ユーザーID
                if(array_key_exists('serch_id',$this->request->data['Point'])){
                    if($this->request->data['Point']['serch_id'] ){
                        $serch_id = (int)str_replace(USER_ID_HEDDER,"",$this->request->data['Point']['serch_id']);
                        $conditions = array_merge($conditions,array('Point.user_id' => $serch_id));
                    }
                }
                
                //店舗名または屋号
                if(!empty($this->request->data['Point']['search_text'])|| array_key_exists('search_text',$_POST)){
                    $conditions = array_merge($conditions,array('or' => array(
                                                                                array('User.shop_name                        like' => "%{$this->request->data['Point']['search_text']}%"),
                                            )
                    ));
                    $this->Session->write('search_text', $this->request->data['Point']['search_text']);
                }else{
                    $this->Session->write('search_text', '');
                }
                
                // 残高
                if(array_key_exists('search_balance',$this->request->data['Point'])){
                    if($this->request->data['Point']['search_balance']){
                        $conditions = array_merge($conditions,array('Point.balance <=' => $this->request->data['Point']['search_balance']));
                    }
                }
            }
            
        }else{
        
            if(array_key_exists('page',$this->request->params['named'])){
                if($this->Session->check('conditions')) {
                    $conditions = $this->Session->read('conditions');
                    //print_r($conditions);
                    if(array_key_exists('Point.id'        ,$conditions)){$this->request->data['Point']['serch_id']         = $conditions['Point.user_id'];}
                    //if(array_key_exists('Point.id'        ,$conditions)){$this->request->data['Point']['search_text']      = $conditions['Point.user_id'];}
                    if(array_key_exists('Point.balance <=',$conditions)){$this->request->data['Point']['search_balance']   = $conditions['Point.balance'];}
                    
                }
                if($this->Session->check('search_text')) {
                    $this->request->data['Point']['search_text'] = $this->Session->read('search_text');
                }
            }
        }
        
        $conditions = array_merge($conditions,array('Point.created = (SELECT MAX(created) FROM points AS Balance WHERE Point.user_id = Balance.user_id)'));
        
        $this->Paginator->settings = array(
            'fields' => 'Point.balance,Point.created,User.shop_name,User.id,User.shop_name',
            'order' => 'User.id DESC',
            'limit' => 30,
            'conditions' => $conditions,
            'joins' => array(
                        array (
                            'type' => 'LEFT',
                            'table' => 'users',
                            'alias' => 'User',
                            'conditions' => 'Point.user_id = User.id'
                            ),
            )
        );

        $datas = $this->Paginator->paginate('Point');
        $this->set('datas', $datas);
        
        
        $tmp = $this->User->find('all',array(
            "fields" => "parent_id,id,shop_name",
            "conditions" => array('parent_id IN' => $reg_ids),
            "order" => "parent_id ASC"
            ));
        $children = array();
        foreach($tmp AS $val){
            $children[$val['User']['parent_id']][$val['User']['id']] = $val['User']['shop_name'];
        }

        $this->set('children', $children);

    }
    
    public function admin_detail($user_id = null) {
        
        if(!$user_id){
            return $this->redirect(array('action' => 'index'));
        }
        
        $conditions = array();
        
        // ユーザーID指定
        if($user_id){
            $conditions = array_merge($conditions,array('Point.user_id' => $user_id));
        }

        if(array_key_exists('page',$this->request->params['named'])){
            if($this->Session->check('conditions')) {
                $conditions = $this->Session->read('conditions');
            }
            if($this->Session->check('search_text')) {
                $this->request->data['Point']['search_text'] = $this->Session->read('search_text');
            }
        }

        $this->Paginator->settings = array(
            'fields' => 'Point.*,User.shop_name,User.id,User.shop_name',
            'limit' => 30,
            'conditions' => $conditions,
            'order' => array('Point.id' => 'desc'),
            'joins' => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'users', 
                            'alias' => 'User',  
                            'conditions' => 'Point.user_id = User.id'
                            ),
            )
        );

        $datas = $this->Paginator->paginate('Point');
        $this->set('datas', $datas);
        
        $user_cnt = $this->Point->find('count',array(
            'fields' => 'Point.user_id',
            'group' => array('Point.user_id'),
            'conditions' => $conditions,
            'joins' => array(
                        array (
                            'type'  => 'LEFT',    
                            'table' => 'users', 
                            'alias' => 'User',  
                            'conditions' => 'Point.user_id = User.id'
                            ),
            )
        ));

    }
    public function admin_new($add_user_id = null) {
        
        $add_data = array();
        $this->Point->create();
        $add_data["Point"]["user_id"] = $add_user_id;
        $add_data["Point"]["memo"] = "ポイントデータ新規登録";
        if($this->Point->save($add_data,false)){
            $this->Flash->success('ポイントデータが新規作成されました。');
        }else{
            $this->Flash->error('ポイントデータの新規作成に失敗しました。');
        }
        return $this->redirect(array('action' => 'index'));
        
    }

    // "p" plus  "m" minus
    public function admin_add($user_id = null,$flg = "p") {
        
        if(!$user_id || $flg == ""){
            return $this->redirect(array('action' => 'index'));
        }

        $this->set('flg', $flg);
        
        $user = $this->Point->find('first',array(
            'fields' => 'Point.*,User.shop_name,User.id,User.shop_name',
            'conditions'=>array('user_id'=>$user_id ),
            'order' => 'created DESC',
            'joins' => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'users', 
                            'alias' => 'User',  
                            'conditions' => 'Point.user_id = User.id'
                            ),

            )
        ));
        
        $this->set('user', $user);
        
        // 子ユーザーリストを取得
        $children = array();
        $this->LoadModel("User");
        $tmps = $this->User->find('list',array(
            'fields' => 'User.id,User.shop_name',
            'conditions'=>array('parent_id' => $user_id ),
            'order' => 'id DESC',
        ));
         
         $children[] = array(
                'value'        => $user['User']['shop_name']." (".USER_ID_HEDDER.sprintf('%06d',$user['User']['id']).")",
                'name'         => $user['User']['shop_name']." (".USER_ID_HEDDER.sprintf('%06d',$user['User']['id']).")",
                );                                                                                                 
         foreach($tmps AS $key => $tmp){
            $children[] = array(
                'value'        => $tmp ." (". USER_ID_HEDDER.sprintf('%06d',$key).")",
                'name'         => $tmp ." (". USER_ID_HEDDER.sprintf('%06d',$key).")",
                );
        }

        $this->set('children', $children);
        
        if ($this->request->is('post')) {
            
            // 残高の計算
            if(array_key_exists('add_points',$this->request->data['Point'])){
                $this->request->data['Point']['balance'] = $user['Point']['balance'] + $this->request->data['Point']['add_points'] ;
            }
            if(array_key_exists('def_points',$this->request->data['Point'])){
                if($user['Point']['balance'] < $this->request->data['Point']['def_points']){
                    $this->Flash->error('減算ポイントが残高を超えています。');
                }
                $this->request->data['Point']['balance'] = $user['Point']['balance'] - $this->request->data['Point']['def_points'];
            }
            if(array_key_exists('do_user_id',$this->request->data['Point'])){
                $this->request->data['Point']['memo'] = $this->request->data['Point']['do_user_id']." | ". $this->request->data['Point']['memo'];
            }
            
            
            $this->Point->create();
            if ($this->Point->save($this->request->data)) {
                $this->Flash->success('ポイントデータが追加されました');

            }else{
                $this->Flash->error('ポイントデータの追加に失敗しました。時間を空けて再度お試しください。');
            }
            return $this->redirect(array('action' => 'detail/'.$user_id));
        }
    }

}