<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller','Telephone');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
 App::uses('Sanitize', 'Utility');
class AppController extends Controller {

    public $helpers = array('Html','Form');
    public $components = array(
        'Paginator',
        'Session',
        'Flash',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
        )
    );
    
    
    public $auth;
    public $auth_admin;
    public $notice_flash;
    
    public function beforeFilter() {
        
        
        
        // prefixが"admin"であれば
        if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {

           $this->layout = 'admin';

           $this->Auth->authenticate = array(
            'Form' => array(
                    'passwordHasher' => 'Blowfish',
                     'userModel' => 'Administrator'
                    //'fields' => array('username' => 'username','password'=>'password')
                )
           );
           
           $this->Auth->loginAction    = array('controller' => 'administrators', 'action' => 'login',   'admin'=>true);
           $this->Auth->loginRedirect  = array('controller' => 'pages',          'action' => 'home',    'admin'=>true);
           $this->Auth->logoutRedirect = array('controller' => 'administrators', 'action' => 'login',   'admin'=>true);

           Router::connect('/', array('controller' => 'pages', 'action' => 'home', 'admin' => true)); 
           
           AuthComponent::$sessionKey  = "Auth.Administrator";

            $this->auth_admin = $this->Auth->user();
            $this->set('admin', $this->auth_admin);
            
        // マイページ
        }else{
           
           parent :: beforeFilter();
            
           $acc_stop_msg = "";
            
           $this->Auth->authenticate = array(
            'Form' => array(
                    'passwordHasher' => 'Blowfish',
                    'scope' => array( 'User.status IN' => array(0,1,2,3,4,5,8))
                )
           );
           $this->Auth->loginAction    = array('controller' => 'users',          'action' => 'login',   'admin'=>false);
           $this->Auth->loginRedirect  = array('controller' => 'telephones',     'action' => 'index',   'admin'=>false);
           $this->Auth->logoutRedirect = array('controller' => 'users',          'action' => 'login',   'admin'=>false);

           AuthComponent::$sessionKey = "Auth.User";
           
           $this->auth = $this->Auth->user();

           if($this->auth){if(!array_key_exists('tropo_telno',$this->auth)){$this->auth['tropo_telno'] ='';}}
           
           $this->set('user', $this->auth);   

            // お知らせ注目数カウント
            $notice_cnt = -1;
            if($this->auth['id'] ){
                
                $this->loadModel('Notice');
                if($this->Notice->find('count') > 0){
                
                    $conditions  = " created > '".date('Y-m-d H:i:s',strtotime('-6 month')) ."'" ;
                    $conditions .= " AND ( ";
                    $conditions .= "  (start_date <= '" .date('Y-m-d'). "' AND end_date >='" .date('Y-m-d'). "') OR ";
                    $conditions .= "  (start_date <= '" .date('Y-m-d'). "' AND end_date IS NULL) OR ";
                    $conditions .= "  (start_date IS NULL AND end_date >='" .date('Y-m-d'). "')";
                    $conditions .= "  ) ";
                    $conditions .= " AND ((user_id = ".$this->auth['id'] .") OR (user_id IS NULL))";

                    $ret = $this->Notice->find('list',array(
                        'fields' => 'id',
                        'conditions' => $conditions
                      
                    ));
                    $this->notice_flash = $ret;
                    $notice_cnt = count($ret);
                    //$this->set('notice_cnt', count($ret));
                
                }else{
                    //$this->set('notice_cnt', -1);
                
                }
                
                
                if($this->auth['status'] == 8){ //アカウント停止中
                    $acc_stop_msg = "お客様のアカウントは現在停止中です。カスタマーセンターにご連絡ください。";
                }
                
            }
            $this->set('notice_cnt', $notice_cnt);
            $this->set('acc_stop_msg', $acc_stop_msg);
        
        }
        
    }

    public function __sanitize() {
        $this->data = Sanitize::clean($this->data, array('remove_html' => true, 'encode' => true, 'escape' => false));
    }




}
