<?php
class VoicemailsController extends AppController {
 
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function index() {
     
        
        // 表示文言の設定
        if($this->action == 'edit' || $this->auth['status'] > 4){
            $this->set('tstring', 'スケジュール・自動応答');
            $this->set('bstring', '変更');
            $this->set('back_btn', array('controller'=>'telephones','action'=>'index'));
            
        }else{
        
            $this->layout = 'user_add';
            
            $this->set('tstring', 'サービス開始お手続き(2/3)');
            
            $this->set('back_btn', array('controller'=>'teltransfers','action'=>'new'));

            if($this->auth['no_payment'] == 0 ){
                $this->set('bstring', '転送設定を登録して次に進む');
            }else{
                $this->set('bstring', '決済せず無料ユーザーとして登録(社内用)');
                
            
            }
        }
        
        $this->loadModel('Voicemails'); 
        $data = $this->Voicemails->findByUserId($this->auth['id'],'*');

        // データがないときは作成する
        if(!$data){
            $this->Voicemails->user_id = $this->auth['id'];
            if ($this->Voicemails->save($this->Voicemails)) {
                $data = $this->Voicemails->findByUserId($this->auth['id']);
            }
        }
        
        if ($this->request->is('post') || $this->request->is('put')) {
        
            if ($this->Voicemails->save($this->request->data)) {
                
                
                if($this->action == 'edit' || $this->auth['status'] > 4){
                    $this->Flash->success('スケジュール・自動応答が更新されました。');
                    return true;
                    //return $this->redirect(array('controller'=>'voicemails','action' => 'edit'));
                }else{
                    if($this->auth['no_payment'] == 0 ){
                        
                        // 支払い方法判定 10pg add
                        
                        $this->loadModel('User');
                        $data['User']['id']           = $this->auth['id'];
                        $data['User']['hand_payment'] = $this->request->data['User']['hand_payment'];
                        if ($this->User->save($data,false)) {
                            if($data['User']['hand_payment']  == 0){
                                return $this->redirect(array('controller'=>'orders','action' => 'first'));
                            }else{
                                return $this->redirect(array('controller'=>'orders','action' => 'start'));
                            }
                        }
                        
                        
                    }else{
                    
                        // ユーザーステータスの変更
                        $this->loadModel('User');
                        $data['User']['id'] = $this->auth['id'];
                        $data['User']['status'] = 4;
                        
                        if ($this->User->save($data,false)) {
                            return $this->redirect(array('action' => 'end'));

                        }
                    }
                }
            }
            $this->Flash->error('スケジュール・自動応答の更新に失敗しました。');
            //return $this->redirect(array('action' => $this->action));
            
        } else{
            $this->request->data = $data;
        
        }
    }
    
    public function end() {
    }
    
    public function edit() {

        $this->index();
        $this->render('index');
        
    }

}