<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
 
 
App::uses('CakeEmail', 'Network/Email');

 
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
    public $uses = array();



/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *    or MissingViewException in debug mode.
 */
    public function display() {
    
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    
    }
    
    public function admin_home() {
    
        // R¸Ò¿/ÏÏ(TROPOÝèÒ¿)ðæ¾
        $this->loadModel('User');
        $this->loadModel('Order');
        $status1_cnt = $this->User->find('count', array(
            'conditions' => array('User.status' => 1)
        ));
        $status4_cnt = $this->User->find('count', array(
            'conditions' => array('User.status' => 4)
        ));
        $order_err_cnt = $this->Order->find('count', array(
           'conditions' => array('Order.pay_date' => NULL)
        ));

        $this->set('status1_cnt', $status1_cnt);
        $this->set('status4_cnt', $status4_cnt);
        $this->set('order_err_cnt', $order_err_cnt);
        
    }
    
    public function admin_mails() {
        
    }



}
