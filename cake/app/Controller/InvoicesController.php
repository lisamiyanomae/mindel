<?php
App::uses('CakeEmail', 'Network/Email');

class InvoicesController extends AppController {
 
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    
    public function admin_index() {
    
    	$conditions = array();
        
    	if ($this->request->is('post') ) {

            $this->__sanitize();
            
            // リセットボタン押下
            if(isset($this->request->data['reset'])) {
                $this->request->data['User'] = array();
            }
            
            // ページ数リセット
            $this->request->params['named']['page'] = 1;
    
	    	if(array_key_exists('Invoice',$this->request->data)){ 
	            
                // 期間from>期間to の場合は値をひっくり返す
                if(array_key_exists('serch_start_date',$this->request->data['Invoice'])){
                    if($this->request->data['Invoice']['serch_start_date'] && $this->request->data['Invoice']['serch_end_date']){
                        if($this->request->data['Invoice']['serch_start_date'] > $this->request->data['Invoice']['serch_end_date']){
                            $tmp_date = $this->request->data['Invoice']['serch_start_date'];
                            $this->request->data['Invoice']['serch_start_date'] = $this->request->data['Invoice']['serch_end_date'];
                            $this->request->data['Invoice']['serch_end_date']   = $tmp_date;
                        }
                    }
                }
                // 期間from
                if(array_key_exists('serch_start_date',$this->request->data['Invoice'])){
                    if($this->request->data['Invoice']['serch_start_date']){
                        $conditions = array_merge($conditions,array('Invoice.invoice_date >=' => $this->request->data['Invoice']['serch_start_date'].' 00:00:00'));
                    }
                }
                // 期間to
                if(array_key_exists('serch_end_date',$this->request->data['Invoice'])){
                    if($this->request->data['Invoice']['serch_end_date']){
                        $conditions = array_merge($conditions,array('Invoice.invoice_date <=' => $this->request->data['Invoice']['serch_end_date'].' 23:59:59'));
                    }
                
                }
                
                //ユーザー名
                if(!empty($this->request->data['Invoice']['search_text'])|| array_key_exists('search_text',$_POST)){
                    $conditions = array_merge($conditions,array('or' => array(
                                                                                array('User.user_name                        like' => "%{$this->request->data['Invoice']['search_text']}%"),
                                            )
                    ));
                    $this->Session->write('search_text', $this->request->data['Invoice']['search_text']);
                }else{
                    $this->Session->write('search_text', '');
                }
            
                
            }
            
        }else{
            if(array_key_exists('page',$this->request->params['named'])){
                if($this->Session->check('conditions')) {
                    $conditions = $this->Session->read('conditions');
                    if(array_key_exists('OR',$conditions)){
                        foreach($conditions['OR'] AS $user ){
                        }
                    }
                    if(array_key_exists('Invoice.created >=',$conditions)){$this->request->data['Invoice']['serch_start_date'] = date('Y-m-d', strtotime($conditions['Invoice.created >=']));}
                    if(array_key_exists('Invoice.created <=',$conditions)){$this->request->data['Invoice']['serch_end_date'] = date('Y-m-d', strtotime($conditions['Invoice.created <=']));}
                        
                        
                }
                if($this->Session->check('search_text')) {
                    $this->request->data['Invoice']['search_text'] = $this->Session->read('search_text');
                }
            }    
            
        }
        
        
        $this->Session->write('conditions', h($conditions));
        
        
        $this->Paginator->settings = array(
            'fields' => array('Invoice.*'),
            'conditions' => $conditions,
            'limit' => 20,
            'order' => array('Invoice.created' => 'desc'),
        );
        
        
    
        
        $data = $this->Paginator->paginate('Invoice');
        $this->set('invoices', $data);
    
    

   
    
    }
}