<?php
class StringUtility
{
    /**
     * $valueの文字数が$lengthを越えていないかチェックする
     *
     * @param string $value
     * @param integer $length
     * @return boolean
     */
    public static function isMaxLength($value, $length)
    {
        $value = str_replace("\r\n", "\n", $value);
        $value = str_replace("\n", "", $value);
        return (mb_strlen($value, mb_detect_encoding($value)) <= $length);
    }
    
    /**
     * $valueの文字数が$minと$max数の範囲内に収まるかチェックする
     *
     * @param string $value
     * @param integer $min
     * @param integer $max
     * @return boolean
     */
    public static function isRangeLengthIn($value, $min, $max)
    {
        $valueString = (string) $value;
        $length = mb_strlen($valueString);
        
        if ($length < $min || $max < $length) {
            return false;
        }
        return true;
    }
    
    /**
     * ランダムテキストを生成する
     *
     * @param integer $length
     * @return string
     */
    public static function createRandomText($length)
    {
        //App::import('Vendor', 'pear_init');
        //App::import('Vendor', 'Text/Password', array('file' => 'Text' . DS . 'Password.php'));
        //return Text_Password::create($length, "unpronounceable");
        
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, $length);
    }
}
?>