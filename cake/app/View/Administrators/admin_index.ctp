<?php
$this->assign('title', '運営アカウント管理');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
?>
<div id="main">
    
<div class="section inner">

    <h1>運営アカウント管理</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6">運営アカウント一覧</div>
    
    <?php //echo $this->Form->create('Order',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    <div><?php print($this->Html->link('<i class="fas fa-user-plus fa-2x"></i>',array( 'action'=>'add'),array('escape' => false))); ?></div>
    <div id="claimList" class="table">
        <table>
            <thead>
            <tr>
                <th>表示名</th>
                <th>運営アカウントID</th>
                <th>権限</th>
                <th>登録日時</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            
            <tbody>
            <?php foreach ($admininfo as $member): ?>
            <tr>
                <td><?php echo $member['Administrator']['showname']; ?></td>
                <td><?php echo $member['Administrator']['username']; ?></td>
                <td><?php echo $role_name[$member['Administrator']['role']]; ?></td>
                
                <td class="mobile_del"><?php echo $member['Administrator']['created']; ?></td>
                <td><?php print($this->Html->link('<i class="fas fa-edit"></i>',array( 'action'=>'edit',$member['Administrator']['id']),array('escape' => false))); ?></td>
                <td><?php if($member['Administrator']['id'] != $admin['id']){ print($this->Form->postLink('<i class="fas fa-trash-alt"></i>',array('action'=>'delete',$member['Administrator']['id']),array('escape' => false),'本当に削除しますか?')); } ?></td>
                </tr>
            <?php endforeach; ?>
            <?php unset($member); ?>
            </tbody>
        </table>
    </div>

    <div class="pagenate">
        <?php echo $this->Paginator->prev(  '< 前のページ',  null,  null,  array('class' => 'disabled')); ?>
        <?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(  '次のページ >',  null,  null,  array('class' => 'disabled'));?>
        <?php echo $this->Paginator->counter(); ?>
    </div>

    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'pages', 'action' => 'home','admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->



