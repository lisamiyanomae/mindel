<?php 
$this->assign('title', 'ログイン');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));
?>

<div id="main">
    
<div class="section inner">
    
        
        <div id="login" class="w400">
            <h2>ログイン</h2>
            <p class="mb10">運営アカウントIDとパスワードを入力してください</p>
            <div class="flash mb20"><?php echo $this->Flash->render(); ?></div>
            <?php echo $this->Form->create('Administrator',array('id'=>'varidate_form','type'=>'file','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
            <div class="w300 mb30">
                <p>運営アカウントID</p>
                <?php echo $this->Form->input('username',array('class' => 'validate[required]')); ?>
                <p>パスワード</p>
                <?php echo $this->Form->input('password',array('class' => 'validate[required]')); ?>
                
            </div>
            <div class="registration"><?php echo $this->Form->end('ログイン'); ?></div>
        </div>
    
</div>

</div><!--/End main-->




<script>
jQuery(function($){
    
    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
    
});
</script>

