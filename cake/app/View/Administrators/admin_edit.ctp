<?php
$this->assign('title', '運営アカウント管理');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
?>
<div id="main">
    
<div class="section inner">
    <h1>運営アカウント管理</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6">運営アカウント更新</div>
    
    <p>更新にはパスワードの入力が必要です。</p>
    
    <?php echo $this->Form->create('Administrator',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    <div id="claimList" class="table">
        <table>
            <thead>
            <tr>
                <th>表示名</th>
                <th>運営アカウントID</th>
                <th>パスワード</th>
                <th>権限</th>
            </tr>
            </thead>
            <tbody>
             <tr>
            <td><?php echo $this->Form->input('showname',array('label' => false)); ?></td>
            <td><?php echo $this->Form->input('username',array('label' => false)); ?></td>
            <td><?php echo $this->Form->input('password',array('label' => false)); ?></td>
            <td><?php echo $this->Form->input('role',array('label' => false,'type' => 'select','options' => $role_name)); ?></td>
            </tr>
            </tbody>
        </table>
        
    </div>
    <?php echo $this->Form->end('更新'); ?>
    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'administrators', 'action' => 'index','admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->
