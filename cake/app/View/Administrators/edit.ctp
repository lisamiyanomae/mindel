<?php 
if($admininfo['role'] == 'admin'){
	$this->Html->addCrumb('担当者管理', '/administrators');
}else{
	$this->Html->addCrumb('ログイン情報', '/administrators');
}
$this->Html->addCrumb('編集', array('controller' => 'administrators', 'action' => 'edit'));
?>
<div class="users form">
<?php echo $this->Form->create('Administrator'); ?>
    <fieldset>
        <?php 
        echo $this->Form->input('showname',array('label' => '表示名'));
        echo $this->Form->input('username',array('label' => 'ログインID'));
        echo $this->Form->input('password',array('label' => 'パスワード'));
        
        if($admininfo['role'] == 'admin'){ 
            echo $this->Form->input('role', array(
                'options' => $role_name,
                'label' => '権限'
            ));
        }
    ?>
    </fieldset>
<?php echo $this->Form->end('更新'); ?>
<span class="button"><?php print($this->Html->link('戻る', 'index')); ?></span>
</div>