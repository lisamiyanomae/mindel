<?php 
$this->Html->addCrumb('担当者管理', '/administrators');
$this->Html->addCrumb('新規追加', array('controller' => 'administrators', 'action' => 'add'));
?>
<div class="users form">
<?php echo $this->Form->create('Administrator'); ?>
    <fieldset>
       
        <?php 
        echo $this->Form->input('showname',array('label' => '表示名'));
        echo $this->Form->input('username',array('label' => 'ログインID'));
        echo $this->Form->input('password',array('label' => 'パスワード'));
				echo $this->Form->input('role', array(
                'options' => $role_name,
                'label' => '権限'
            ));    ?>
    </fieldset>
<?php echo $this->Form->end('追加'); ?>
<span class="button"><?php print($this->Html->link('戻る', 'index')); ?></span>
</div>