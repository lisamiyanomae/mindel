<?php echo $user['shop_name'] ?> <?php echo $user['responsible_name'] ?> 様

みんなにでんわ転送のサービスが開始されました。
お客様の発行電話番号は　<?php echo $user['tropo_telno']; ?>　です。

本メールは当サービスをご利用いただくにあたり
非常に重要なお知らせでございます。

お取り扱いには十分にご注意の上、
プリントアウトをするなどして、大切に保管してくださいますようお願い申し上げます。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
　登録情報
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
お客様NO： <?php echo $id ?>

店舗名または屋号： <?php echo $user['shop_name'] ?>

会社名（契約者名）： <?php echo $user['company_name'] ?>

カイシャメイ（ケイヤクシャメイ）： <?php echo $user['company_kana'] ?>

ご担当者名： <?php echo $user['responsible_name'] ?>

ゴタントウシャメイ： <?php echo $user['responsible_kana'] ?>

メールアドレス： <?php echo $user['email'] ?>

ログインID： <?php echo $user['username'] ?>

パスワード： [ご指定のパスワード]
郵便番号： <?php echo $user['zip'] ?>

都道府県： <?php if($this->data['User']['prefecture']){echo $pref_list[$this->data['User']['prefecture']];} ?>

住所1： <?php echo $user['address'] ?>

住所2： <?php echo $user['building'] ?>

ご連絡先電話番号： <?php echo $user['user_telno'] ?>


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
　サービス内容
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
【発行電話番号】<?php echo $user['tropo_telno']; ?>


【 転送先 】
<?php echo $teltransfers; ?>

【 転送スケジュール 】
<?php echo $voicemail; ?>

【 時間外応答メッセージ 】
<?php echo $message; ?>


【 確認事項 】
契約約款/免責事項
個人情報保護方針

に同意しました。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
 マイページのご案内
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
マイページでは、着信履歴の確認や、登録情報の変更などが行えます。
以下のURLからアクセスしてください

<?php echo $login_url ?>　

ログインID : <?php echo $user['username'] ?>

パスワード : [ご指定のパスワード]


