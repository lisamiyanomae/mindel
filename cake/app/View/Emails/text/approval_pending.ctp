<?php echo $company_name ?> <?php $responsible_name ?> 様 

パスワードをリセットするには、
以下の「パスワード変更フォーム」のURLをクリックしてください。

<?php echo $republish_url ?>


※URLをクリックしてもページが表示されない場合は
ご利用のブラウザの「アドレス」欄にURLを貼り付けてアクセスしてください。
