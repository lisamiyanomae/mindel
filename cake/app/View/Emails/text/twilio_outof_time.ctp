<?php echo $shop_name; ?> <?php echo $responsible_name; ?> 様

時間外応答がありました。
マイページにログインして内容をご確認ください。

<?php echo $user_login_url; ?>

ログインID : <?php echo $loginid; ?> 
パスワード : [ご指定のパスワード]

-----
<?php echo $shop_name; ?> <?php echo $responsible_name; ?> 様が
ご登録したメールアドレス：<?php echo $email; ?> 宛にメールをお送りしています。
 
 
※このメールは配信専用です。
※URLをクリックしてもページが表示されない場合は
ご利用のブラウザの「アドレス」欄にURLを貼り付けてアクセスしてください。

