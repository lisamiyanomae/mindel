<?php echo $shop_name ?> <?php echo $responsible_name ?> 様


確認書類の提出をありがとうございました。
以下のURLからログインして、転送の設定と初月基本料のお支払いにお進みください。

<?php echo $login_url ?>

ログインID : <?php echo $loginid ?>

パスワード : [ご指定のパスワード]


決済から3営業日以内にサービス開始のご連絡をいたします。

※転送先や転送時間は、電話番号発行後マイページからいつでも変更できますので、初期値のまま登録を進めることもできます。


-----------------------------------------------------

※パスワードを忘れた方はこちらから再発行のお手続きをお願いします。
 <?php echo $republish_password_url ?> 

-----------------------------------------------------

登録内容は以下の通りです。
===============================

店舗名または屋号：　<?php echo $shop_name ?>

会社名（契約者名）：　<?php echo $company_name ?>

カイシャメイ（ケイヤクシャメイ）：　<?php echo $company_kana ?>

ご担当者名：　<?php echo $responsible_name ?>

ゴタントウシャメイ：　<?php echo $responsible_kana ?>

メールアドレス：　<?php echo $email ?>

ログインID：　<?php echo $loginid ?>

パスワード：　[ご指定のパスワード]

郵便番号：　<?php echo $zip ?>

住所1：　<?php echo $address ?>

住所2：　<?php echo $building ?>

ご連絡先電話番号：　<?php echo $telno ?>

===============================


ご不明な点、お気づきの点などがありましたら、当サポートまで
お気軽にお問い合わせください。




