<?php echo $shop_name; ?> <?php echo $responsible_name; ?> 様

みんなにでんわ転送のアカウント作成をありがとうございました。

以下のURLをクリックして詳細入力にお進みください。

<?php echo $user_login_url; ?>

ログインID : <?php echo $loginid; ?>

パスワード : [ご指定のパスワード]

-----
<?php echo $shop_name; ?> <?php echo $responsible_name; ?> 様が
<?php echo $user_add_url; ?> で
入力したメールアドレス：<?php echo $email; ?> 宛にメールをお送りしています。


※パスワードを忘れた方はこちらから再発行のお手続きをお願いします。
<?php echo $republish_password_url; ?> 
 
 
※このメールは配信専用です。
※URLをクリックしてもページが表示されない場合は
ご利用のブラウザの「アドレス」欄にURLを貼り付けてアクセスしてください。

