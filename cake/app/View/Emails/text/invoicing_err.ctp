<?php echo $shop_name ?> <?php echo $responsible_name ?> 様

みんなにでんわ転送をご利用いただき、誠にありがとうございます。
以下の請求内容について、正常に決済が行われませんでした。

ご利用期間
<?php echo $data['start_date']; ?> ～ <?php echo $data['end_date']; ?>

請求NO.： <?php echo ORDER_ID_HEDDER .sprintf('%0'.ORDER_ID_DIGIT.'d',$data['id']); ?>

請求日： <?php echo $data['invoice_date']; ?>

支払期限： <?php echo $data['limit_date']; ?>

<?php if($data['kind'] == 1){ ?>
月額基本料： <?php echo $this->Number->currency($data['sum_price'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?> 円
<?php }else{ ?>
通信料： <?php echo $this->Number->currency(($data['message_price'] + $data['callomg_price']),'JPY',array('wholeSymbol'=>"",'places'=>0)); ?> 円
<?php } ?>

小計： <?php echo $this->Number->currency($data['sum_price'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?> 円

税額： <?php echo $this->Number->currency($data['tax'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?> 円

合計： <?php echo $this->Number->currency($data['amount'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?> 円


以下のURLより、早急に決済を行ってください。
決済が行われないと、サービスは順次休止されます。
一時的に振込払いご希望の方は、カスタマーサポートまでご連絡ください。

<?php echo $pay_url; ?>