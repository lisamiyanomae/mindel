<?php echo $shop_name ?> <?php echo $responsible_name ?> 様

みんなにでんわ転送のサービス申込みをありがとうございます。
いただいた書類に以下の不備がありました。


【<?php echo $status_2_reason; ?>】

<?php 
switch ($status_2_reason){
	case "画像不明瞭":
		?>
		いただいた証明書類の画像では確認できませんでした。
		再度画像をアップロードしてください。

		確認書類については、
		http://minderu.com/confirmation/
		をご確認ください。
		<?php
		break;
	case "氏名不一致":
		?>
		いただいた証明書類とお申込者の名前が一致していませんでした。
		お申込み者のお名前の証明書類の画像を再度アップロードしてください。

		確認書類については、
		http://minderu.com/confirmation/
		をご確認ください。
		<?php
		break;
	case "補助書類必要":
		?>
		いただいた証明書類の住所が登録のご住所と異なっています。
		現在の住所がわかる補助書類の画像を再度アップロードしてください。

		補助書類については
		http://minderu.com/confirmation/#auxiliary
		をご確認ください
		<?php
		break;
	case "有効期限切れ":
		?>
		いただいた証明書類の有効期限が切れています
		有効期限内の書類の画像を再度アップロードしてください。

		確認書類については
		http://minderu.com/confirmation/
		をご確認ください。
		<?php
		break;
	case "その他":
		echo $status_2_mailadd;
		break;
}

?>

以下のURLをクリックして再登録をお願いします。

<?php echo $login_url; ?>

ログインID : <?php echo $loginid; ?> 
パスワード : [ご指定のパスワード]


再登録がうまく行かない場合は、このメールの返信に画像を添付して送っていただいても結構です。


-----------------------------------------------------

ご不明な点、お気づきの点などがありましたら、当サポートまで
お気軽にお問い合わせください。

