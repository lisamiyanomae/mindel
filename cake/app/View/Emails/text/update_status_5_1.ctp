<?php echo $user['shop_name'] ?> <?php echo $user['responsible_name'] ?> 様

みんなにでんわ転送の使い方をご説明します。

ユーザーサポート
https://minderu.com/usersupport/
もご覧ください。

＜ステップ１＞
お客様の発行電話番号　<?php echo $user['tropo_telno']; ?>　を
転送先電話の電話帳に新規登録します。
登録名にお店の屋号を入れるとわかりやすいです。

＜ステップ２＞
発行番号　<?php echo $user['tropo_telno']; ?>　に登録していない電話で電話をかけてみます。
ステップ1で電話帳に登録した番号から着信することを確認してください。

＜ステップ３＞
今あるお店の番号から転送をかける場合は、ボイスワープ等の設定で
<?php echo $user['tropo_telno']; ?>　に転送を設定してください。

※NTTボイスワープ
https://www.ntt-west.co.jp/denwa/service/voicewp/gaiyou.html 
※KDDI auひかりの転送設定
https://www.au.com/internet/auhikari/ 
※Softbankおとくラインの転送設定
http://tm.softbank.jp/consumer/otoku/

＜ステップ４＞
マイページをスマホで表示させます。

・iphone/Safari→「アクション」「ホーム画面に追加」をタップ
http://minderu.com/wp-content/themes/minderu_theme/images/setting_iphone.pdf

・アンドロイド携帯/Chrome →右上「メニュー」「ホーム画面に追加」をタップ
http://minderu.com/wp-content/themes/minderu_theme/images/setting_Android.pdf

＜ステップ５＞
ホームページや名刺の電話番号に、<?php echo $user['tropo_telno']; ?>　を追加してください。
集客向けの電話番号は、　<?php echo $user['tropo_telno']; ?>　のみを、会社概要や名刺などは
既存の番号と併記すると便利です。


-----------------------------------------------------
　電話がかかってきたら
-----------------------------------------------------

お客様から電話がかかってくると、登録した電話機全てに着信します。
ナンバーディスプレイには、発行された　<?php echo $user['tropo_telno']; ?>　からの着信と表示されます。

1台の電話で通話が確立すると他の電話の呼び出しは終了します。
相手先の電話番号を確認したいときは、マイページの「着信履歴」でご確認ください。

話し中でも、他に電話がかかってくると空いている他の電話機が呼び出されます。

-----------------------------------------------------
　電話のかけ方
-----------------------------------------------------

登録された電話機から、発行された　<?php echo $user['tropo_telno']; ?>　へ電話をかけます。
音声ガイダンスが流れますので、それに従って発信したい電話番号を押してください。


１）<?php echo $user['tropo_telno']; ?>　へ発信します。
２）相手先電話番号を入力して＃を押してください。
３）相手の電話機に　<?php echo $user['tropo_telno']; ?>　が表示されて呼び出されます。
４）相手が電話に出たら通話が確立します。

-----------------------------------------------------
 スタッフ用アカウントの作成
-----------------------------------------------------
スタッフ用のアカウントを作成することができます。
スタッフアカウントでは着信履歴のみ閲覧することができます。
管理者の方が以下の方法でスタッフアカウントを追加してください。

1）スタッフアカウント追加ページ
https://minderu.com/mypage/staffs
にアクセスし、管理者ID・パスワードでログインしてください。

2.「追加」を押します。
3.表示名・ログインID・パスワードにそれぞれ任意の値を入力します。
4.「追加」を押します。

既に設定してある転送先をスタッフとして一括登録することも可能です。

作成したアカウントは
https://minderu.com/staff/staffs/login
からログインできます。
管理者以外の方へはこちらのアドレスとID・パスワードをお知らせください。

-----------------------------------------------------


よくある質問「使用方法について」もご覧ください
https://minderu.com/faq/#hou-to

ご不明な点、お気づきの点などがありましたら、当サポートまで
お気軽にお問い合わせください。

