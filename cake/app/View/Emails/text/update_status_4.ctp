<?php echo $shop_name ?> <?php echo $responsible_name ?> 様

みんなにでんわ転送のお申し込みをありがとうございました。
お支払い方法に「振込払い」を選択されましたので、以下の口座にご入金をお願いいたします。

ご入金が確認できましてから3営業日以内にサービスが開始されます。

++++++
振込銀行：楽天銀行　第一営業支店
口座番号：【普通 7193960】
口座名義：カ）ブルーホ°ンド
++++++

初月基本料　<?php echo $this->Number->currency(FIRST_COST * (1 + TAX),'JPY',array('places'=>0)); ?>

通信料チャージ　<?php echo $this->Number->currency(FIRST_CHARGE * (1 + TAX) ,'JPY',array('places'=>0)); ?>

合計　<?php echo $this->Number->currency(FIRST_COST * (1 + TAX) + FIRST_CHARGE * (1 + TAX) ,'JPY',array('places'=>0)); ?> 以上をお振込みください。

ポイント残高が <?php echo $this->Number->currency(2000 ,'JPY',array('places'=>0)); ?> を下回るとメールで通知が送信されます。 

ご不明な点、お気づきの点などがありましたら、当サポートまでお気軽にお問い合わせください。