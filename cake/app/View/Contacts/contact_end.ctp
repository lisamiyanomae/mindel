<?php 
$this->assign('title', 'お問い合わせ');
?>

<div id="main" class="mypage">
	
<div class="section">
	<div class="inner">

		<div class="box w400">
			<p class="mt40 mb20"> お問い合わせ を承りました。3営業日以内に <?php echo $user['email']?> 宛に回答差し上げます。少々お待ちください。</p>
			
			<p class="mb40"><a href="/mypage/">トップへ戻る ≫</a></p>
		</div>


	</div><!--/inner-->
</div><!--/-->


</div><!--/End main-->

