<?php 
$this->assign('title', 'お問合せ');
$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));
?>
<div id="main" class="mypage">

<script>
jQuery(function($){
    
    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });

});
</script>

<div class="section">
    <div class="inner">
        
        <h2>お問合せ</h2>
        
        <?php echo $this->Form->create('Contacts',array('id'=>'varidate_form','inputDefaults' => array('label' => false))); ?>
        <div class="w400 mb30">
            <p class="mb30">以下に記入の上、送信を押してください。システム不具合等に関するお問い合わせは、使用OS／ブラウザも併せてご記入ください。</p>
            <?php echo $this->Form->input('content' ,array('id' =>'content','type'=>'textarea' ,'placeholder' => '','escape'=>false,'class' => 'validate[required]' )); ?>
        </div>        
        <div class="registration mb20"><?php echo $this->Form->end('送信'); ?> </div>
        <?php echo $this->Flash->render(); ?>
            
    </div><!--/inner-->
</div><!--/-->


</div><!--/End main-->