<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>


<!doctype html>
<html lang="ja" class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">

<title>時間・応答 | みんなに電話転送</title>
<meta name="description" content="みんなに電話転送マイページ">


<link rel="apple-touch-icon" href="http://minderu.com/apple-touch-icon180.png">
<link rel="apple-touch-icon" sizes="57x57" href="http://minderu.com/wp-content/themes/minderu_theme/images/apple-touch-icon57.png">
<link rel="apple-touch-icon" sizes="72x72" href="http://minderu.com/wp-content/themes/minderu_theme/images/apple-touch-icon72.png">
<link rel="apple-touch-icon" sizes="76x76" href="http://minderu.com/wp-content/themes/minderu_theme/images/apple-touch-icon76.png">
<link rel="apple-touch-icon" sizes="114x114" href="http://minderu.com/wp-content/themes/minderu_theme/images/apple-touch-icon114.png">
<link rel="apple-touch-icon" sizes="120x120" href="http://minderu.com/wp-content/themes/minderu_theme/images/apple-touch-icon120.png">
<link rel="apple-touch-icon" sizes="144x144" href="http://minderu.com/wp-content/themes/minderu_theme/images/apple-touch-icon144.png">
<link rel="apple-touch-icon" sizes="152x152" href="http://minderu.com/wp-content/themes/minderu_theme/images/apple-touch-icon152.png">
<link rel="apple-touch-icon" sizes="180x180" href="http://minderu.com/apple-touch-icon180.png">


<script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('Android') > 0) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes,maximum-scale=1.0,minimum-scale=1.0">');
    }else{
        document.write('<meta name="viewport" content="width=1200">');
    }
</script>
<link rel="shortcut icon" href="http://minderu.com/wp-content/themes/minderu_theme/images/favicon.ico">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="http://minderu.com/wp-content/themes/minderu_theme/style.css">
<link rel="stylesheet" href="http://minderu.com/wp-content/themes/minderu_theme/contents.css">
<link rel="stylesheet" href="http://minderu.com/wp-content/themes/minderu_theme/mobile.css">
<link rel="stylesheet" href="http://minderu.com/wp-content/themes/minderu_theme/mypage.css">

<link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" >

<!--[if lt IE 9]>
<script src="js/html5shiv.min.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body id="bodytop">

<header>
<div id="mypageHeader">
	<div id="pg_ttl"><span>みんなに</span>でんわ転送《マイページ》</div>
</div>
</header>

<div id="gnav">
<nav>
	<ul id="nav-menu">
		<li><a href="#">着信<span>履歴</span></a></li>
		<li><a href="#">ご利用<span>明細</span></a></li>
		<li><a href="#">操作<span>Q&A</span></a></li>
		<li class="hover-click"><a href="mypage_2_0.html"><i class="blogicon-reorder"></i>設定<span>・変更</span></a>
		   <ul>
		   <li><a href="#">転送／発信</a></li>
		   <li><a href="#">時間・応答</a></li>
		   <li><a href="#">お支払い</a></li>
		   <li><a href="#">アカウント</a></li>
		   <li><a href="#">解約</a></li>
		   </ul>
		</li>
	</ul>
	<div id="btn_logout"><a href="">ログ<span>アウト</span></a></div>
</nav>
<div id="accountinfo">
	<p class="contractor"><span class="my_telnum">050-000-000</span><span class="company_name">みんなに商会</span><span class="contractor_name">田中 保志 様</span></p>
	<div class="info_alert"><a href="">お知らせがあります。<span>ご確認ください ≫</span><img src="http://minderu.com/wp-content/themes/minderu_theme/images/mypage/icon_alertmessage.png" alt="お知らせがあります"></a></div>	
</div>
</div><!--/gnav-->


<?php echo $this->Flash->render(); ?>

<?php echo $this->fetch('content'); ?>



<footer>
<div id="footer" class="mypagefooter">
	<div class="inner">
		<div class="customercenter">
			<p class="customer_time">カスタマーセンター（月～金曜日9:00-18:00）</p>
			<p class="customer_tel"><a href="tel:05031963580">050-3196-3580</a></p>
		</div>
		<div class="contactform"><a href="">お問い合わせフォームはこちら</a></div>
		<p class="copy">Copyright &copy みんなに電話転送 all right reserved.</p>
	</div>
</div>
</footer>


<div id="pagetop"><a href="#bodytop"><i class="fa fa-angle-up" aria-hidden="true"></i></a></div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="http://minderu.com/wp-content/themes/minderu_theme/js/main.js"></script>
<script>
  $(function() {
    var $win = $(window),
        $main = $('#main'),
        $nav = $('#gnav'),
        navHeight = $nav.outerHeight(),
        navPos = $nav.offset().top,
        fixedClass = 'is-fixed';

    $win.on('load scroll', function() {
      var value = $(this).scrollTop();
      if ( value > navPos ) {
        $nav.addClass(fixedClass);
        $main.css('margin-top', navHeight);
      } else {
        $nav.removeClass(fixedClass);
        $main.css('margin-top', '0');
      }
    });
  });


$(function() {
  var contents = $(".hover-click > ul");
  $(".hover-click > a").click(function(){
    $(contents).toggle();
    return false;
  });
  $(".hover-click")
  .mouseover(function(){
    $(contents).show();
  })
  .mouseout(function(){
    $(contents).hide();
  });
});


</script>

 
  
</body>
</html>



