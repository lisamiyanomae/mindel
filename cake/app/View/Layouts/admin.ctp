<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

<!doctype html>
<html lang="ja" class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">

<title><?php if($this->fetch('title') != 'Home'){echo $this->fetch('title').' | ';}  ?><?php echo SITE_NAME; ?> 運営センター</title>
<meta name="description" content="<?php echo SITE_NAME; ?> 運営センター">
<link rel="shortcut icon" href="/wp-content/themes/minderu_theme/images/favicon3.ico">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"-->
<link rel="stylesheet" href="<?php echo $this->Html->webroot; ?>css/admin_style.css">
<link rel="stylesheet" href="<?php echo $this->Html->webroot; ?>css/admin_contents.css">

<!--[if lt IE 9]>
<script src="js/html5shiv.min.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->


</head>

<body>

<header>
<div id="header">
    <h1 id="logo"><span><?php echo $this->Html->link(SITE_NAME, array('controller'=>'pages','action'=>'admin_home')); ?></span><?php echo $this->Html->link('運営センター', array('controller'=>'pages','action'=>'admin_home')); ?></h1>
    <div id="logout"><?php if($admin['id']){print('<span class="button text-white"><i class="fas fa-user"></i> '.$this->Html->link('ログアウト', array('controller'=>'administrators','action'=>'logout'))).'</span>';} ?></div>
</div>
    <?php
        //echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    
</header>
<main>


    <?php //echo $this->Flash->render(); ?>
    <?php echo $this->fetch('content'); ?>
    <?php //echo $this->element('sql_dump'); ?>

</main>



<footer>
<div id="footer">
        <p class="copy">Copyright &copy <?php echo SITE_NAME; ?> all right reserved.</p>
</div>
</footer>

<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script-->
<script src="<?php echo $this->Html->webroot; ?>js/main.js"></script>


</body>
</html>



