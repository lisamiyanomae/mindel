<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//$this->Html->script('jquery-2.1.0.min', array('inline' => false));

?>
<!doctype html>
<html lang="ja" class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php if($this->fetch('title') != 'Home'){echo $this->fetch('title').' | ';}  ?><?php echo SITE_NAME; ?>
    </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">



    <?php
        //echo $this->Html->meta('icon');
        //echo $this->Html->css('cake.generic');
        //echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>

<script>
if ((navigator.userAgent.indexOf('iPhone') > 0) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('Android') > 0) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes,maximum-scale=1.0,minimum-scale=1.0">');
    }else{
        document.write('<meta name="viewport" content="width=1200px">');
    }
</script>

<link rel="stylesheet" href="//fonts.googleapis.com/earlyaccess/notosansjapanese.css">

<link rel="icon" type="image/x-icon" href="/wp-content/themes/minderu_theme/images/favicon.ico"">
<link rel="icon" type="image/png" href="/wp-content/themes/minderu_theme/images/apple-touch-icon.png" sizes="180x180">
<link rel="apple-touch-icon" href="/wp-content/themes/minderu_theme/images/webclip.png">
<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/minderu_theme/images/apple-touch-icon57.png">
<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/minderu_theme/images/apple-touch-icon72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/minderu_theme/images/apple-touch-icon76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/minderu_theme/images/apple-touch-icon114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/minderu_theme/images/apple-touch-icon120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/minderu_theme/images/apple-touch-icon144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/minderu_theme/images/apple-touch-icon152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon180.png">

<link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" >

<!--[if lt IE 9]>
    <script type="text/javascript" src="/wp-content/themes/minderu_theme/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="/wp-content/themes/minderu_theme/js/respond.min.js"></script>
<![endif]-->


<!-- This site is optimized with the Yoast SEO plugin v6.3.1 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="/lioin/" />
<meta property="og:locale" content="ja_JP" />
<meta property="og:type" content="article" />
<meta property="og:title" content="<?php if($this->fetch('title') != 'Home'){echo $this->fetch('title').' | ';}  ?><?php echo SITE_NAME; ?>" />
<meta property="og:description" content="クラウド電話転送サービス お申込み" />
<meta property="og:url" content="/lioin/" />
<meta property="og:site_name" content="<?php echo SITE_NAME; ?>" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="クラウド電話転送サービス お申込み" />
<meta name="twitter:title" content="<?php if($this->fetch('title') != 'Home'){echo $this->fetch('title').' | ';}  ?><?php echo SITE_NAME; ?>" />
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","@id":"#website","url":"http:\/\/minderu.com\/","name":"\u307f\u3093\u306a\u306b\u3067\u3093\u308f\u8ee2\u9001","potentialAction":{"@type":"SearchAction","target":"http:\/\/minderu.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"Organization","url":"http:\/\/minderu.com\/lioin\/","sameAs":[],"@id":"#organization","name":"\u682a\u5f0f\u4f1a\u793e\u30d6\u30eb\u30fc\u30dd\u30f3\u30c9","logo":""}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//maxcdn.bootstrapcdn.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel='stylesheet' id='font-awesome.min-css'  href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='style-css'  href='/wp-content/themes/minderu_theme/style.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='content-css'  href='/wp-content/themes/minderu_theme/contents.css?ver=4.9.4' type='text/css' media='all' />
<link rel='stylesheet' id='mobile-css'  href='/wp-content/themes/minderu_theme/mobile.css?ver=4.9.4' type='text/css' media='all' />
</head>

<body id="bodytop">

<div id="sp-head">
	<span class="center_time">カスタマーセンター（9:00-18:00）</span>
	<span class="center_tel"><a href="tel:05031963580">050-3196-3580</a></span>
</div>
<header>
<div id="header">
    <h1 class="logo"><img src="/wp-content/themes/minderu_theme/images/logo.png" alt="<?php echo SITE_NAME; ?> | クラウド電話転送サービス" /></h1>
    <div id="headinfo">
        <ul class="inner">
            <li id="new_account">
                <div class="customercenter">
                    <p class="customer_time">カスタマーセンター（月～金曜日9:00-18:00）</p>
                    <p class="customer_tel"><a href="tel:05031963580">050-3196-3580</a></p>
                </div>
                <div class="entry"></div>
            </li>
            <?php if($this->action != "login" && $this->action != "add" && $this->action != "thanks" ){ ?>
            <li id="login"><?php echo $this->Html->link('ログアウト',  array('controller'=>'users','action'=>'logout')); ?></li>
            <?php } ?>
        </ul>
    </div>
</div>
</header>


<div id="pageVisual" style="background-image:url();">

</div>

<div id="global-nav">
<nav>
    <div class="inner">
            <ul>
                <li><a href="/">トップ</a></li>
                <li><a href="/service/">サービス内容</a></li>
                <li><a href="/price/">ご利用料金</a></li>
                <li><a href="/faq/">よくある質問</a></li>
                <li><a href="/contact/">お問い合わせ</a></li>
                <li><a href="/mypage/users/add/">サービス申込み</a></li>
            </ul>
            <div class="h_new_account"></div>
    </div>
</nav>
</div><!--/global-nav-->

<div id="topicpath" class="inner">
    <ul class="topic_path" itemprop="breadcrumb"><li itemscope="itemscope">
    <?php echo $this->Html->getCrumbs('&nbsp;&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;</li>&nbsp;&nbsp;<li class="current-crumb" itemscope="itemscope">', array(
        'text' => SITE_NAME.'HOME',
        'url' => empty($_SERVER["HTTPS"]) ? "http://" : "https://".$_SERVER["HTTP_HOST"],
        'escape' => false,
    )); ?>
    </li></ul>
</div>

<main>
<div id="main">

            <?php //echo $this->Flash->render(); ?>
            <?php echo $this->fetch('content'); ?>

</div><!--/End main-->
</main>


<footer>
<div id="footer">
	<div class="inner">
		<div class="f_left">
			<p class="f_logo"><a href="/"><span>みんなに</span>でんわ転送</a></p>
			<div class="row">
				<div class="cont1of3">
					<dl>
						<dt>ご利用について</dt>
						<dd>
							<ul>
								<li><a href="/service/">サービス内容</a></li>
                                <li><a href="/minderuimg/">クラウド転送電話の発着信イメージ</a></li>
								<li><a href="/flow/">導入までの流れ</a></li>
								<li><a href="/mypage/users/add/">お申し込み</a></li>
								<li><a href="/price/">料金／解約</a></li>
                                <li><a href="/calculation/">料金シミュレーター</a></li>
								<li><a href="/faq/">よくある質問</a></li>
								<li><a href="/confirmation/">確認資料について</a></li>
                                <li><a href="/usersupport/">ユーザーサポート</a></li>
							</ul>
						</dd>
					</dl>
				</div>
				<div class="cont1of3">
					<dl>
						<dt>広がる活用法</dt>
						<dd>
							<ul>
                                <li class="id-1661"><a href="/introduction/service/bcp_tereworktool/">BCP対策テレワーク支援ツール</a></li>  
                                <li class="id-1661"><a href="/introduction/service/visiting_care/">訪問介護、看護のICT電話システム</a></li>
                                <li class="id-1661"><a href="/introduction/service/cloud_pbx/">簡単クラウドPBX・クラウドビジネスフォン</a></li>
                                <li class="id-1661"><a href="/introduction/service/home_office/">簡単導入！テレワーク・在宅勤務のツール</a></li>
                                <li class="id-1568"><a href="/introduction/service/voicewarp/">ボイスワープを複数転送、同時転送に</a></li>
                                <li class="id-1661"><a href="/introduction/service/call_center/">格安クラウド型コールセンターシステムCTI</a></li>
                                <li class="id-1661"><a href="/introduction/service/school-dial/">学校情報配信ダイヤルの設置</a></li>
                                <li class="id-1661"><a href="/introduction/service/home_phone/">電話番号の取得が簡単なクラウド電話</a></li>
                                <li class="id-1578"><a href="/introduction/service/bousai/">災害情報ダイヤル/電話情報配信システム</a></li>
	  							<li class="id-1574"><a href="/introduction/service/auto-call/">電話自動応答システム/電話音声サービス</a></li>
	  							<li class="id-1571"><a href="/introduction/service/all-call/">一斉呼出し/緊急呼出システム</a></li>
	  							<li class="id-1568"><a href="/introduction/service/農家の直売と業務の効率化をサポート/">農家の直売と業務の効率化をサポート</a></li>
							</ul>
						</dd>
					</dl>
				</div>
				<div class="cont1of3">
					<dl>
						<dt>みんなにでんわ転送について</dt>
						<dd>
							<ul>
								<li><a href="/news/">お知らせ</a></li>
								<li><a href="/company/">会社概要</a></li>
								<li><a href="/kiyaku/">契約約款／免責事項</a></li>
								<li><a href="/privacy/">個人情報保護方針</a></li>
								<li><a href="/compliance/">コンプライアンス・ポリシー</a></li>
								<li><a href="/security/">情報セキュリティポリシー</a></li>
                                <li><a href="/low/">特定商取引に関する表記</a></li>
                                <li><a href="/policy/">反社会的勢力への方針</a></li>
								<li><a href="/sitemap/">サイトマップ</a></li>
							</ul>
						</dd>
					</dl>
				</div>
			</div>
		</div>
		<div class="f_right">
            <div class="free_account"><a href="/mypage/users/add/">サービスに申し込む</a></div>
			<div class="member_login"><a href="/mypage/users/login/">ユーザーログイン</a></div>
			<div class="customercenter">
				<p class="customer_time">カスタマーセンター（9:00-18:00）</p>
				<p class="customer_tel"><a href="tel:05031963580">050-3196-3580</a></p>
			</div>
			<div class="f_download"><a href="/introduction_download/">印刷用資料ダウンロード</a></div>
            <div class="f_demo"><a href="/demo_account/">デモアカウント申し込み</a></div>
			<div class="f_contact"><a href="/contact/">お問い合わせ</a></div>
		</div>
		<p class="copy">Copyright &copy <?php echo SITE_NAME; ?> all right reserved.</p>
	</div>
</div>
</footer>



<div id="pagetop"><a href="#bodytop"><i class="fa fa-angle-up" aria-hidden="true"></i></a></div>

<!--
-->
<script type="text/javascript" src="/wp-content/themes/minderu_theme/js/jquery.matchHeight.js"></script>
<script type="text/javascript" src="/wp-content/themes/minderu_theme/js/main.js"></script>


<script>

  $(function() {
    var $win = $(window),
        $main = $('#main'),
        $nav = $('#global-nav'),
        navHeight = $nav.outerHeight(),
        navPos = $nav.offset().top,
        fixedClass = 'is-fixed';

    $win.on('load scroll', function() {
      var value = $(this).scrollTop();
      if ( value > navPos ) {
        $nav.addClass(fixedClass);
        $main.css('margin-top', navHeight);
      } else {
        $nav.removeClass(fixedClass);
        $main.css('margin-top', '0');
      }
    });
  });

</script>



</body>
</html>
