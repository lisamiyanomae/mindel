<?php
$this->assign('title', $tstring);
$this->Html->addCrumb($tstring);

echo $this->Html->css('validationEngine.jquery', null, array('inline' => false));
echo $this->Html->script('jquery-2.1.0.min', array('inline' => false));
echo $this->Html->script('jquery.validationEngine', array('inline' => false));
echo $this->Html->script('jquery.validationEngine-ja', array('inline' => false));

?>
<div class="section">
    <div class="inner">
    
        <div class="center tyuui"><?php echo $this->Flash->render(); ?></div>
        <h2><?php echo $tstring; ?>(3/3)</h2>
        <p><?php echo $tstring2; ?></p>
        <div class="table2">
        <?php if($this->action=='start' && $user['status'] == 3 ){ ?>
            <h4 class=" mt40">お支払いについて</h4>
            <p>
            利用開始月の月額基本料のお振込みをもって利用開始となります。<br>
            以下の口座に、初月基本料と通信料をお振込みください。
            お振込み確認後電話番号が発行され、サービスが開始されます。</p>
            
            <p class="mt10">初月基本料　<?php echo $this->Number->currency(FIRST_COST * (1 + TAX),'JPY',array('places'=>0)); ?><br>
            通信料チャージ　<?php echo $this->Number->currency(FIRST_CHARGE * (1 + TAX) ,'JPY',array('places'=>0)); ?><br>
            合計　<?php echo $this->Number->currency(FIRST_COST * (1 + TAX) + FIRST_CHARGE * (1 + TAX) ,'JPY',array('places'=>0)); ?> 以上をお振込みください。</p>

            <p class="pay">
            振込銀行：楽天銀行　第一営業支店<br>
            口座番号：【普通 7193960】<br>
            口座名義：カ）ブルーホ°ンド<br>
            
            </p>
        
            
        <?php } ?>
        
		<?php echo $this->Form->create('Order',array('id'=>'OrderAddForm','inputDefaults' => array('label' => false),'novalidate' => true,'onsubmit' => 'send();return false;','class'=>'primary-form')); ?>
        <?php if($this->action=='start' && $user['status'] == 3){ ?>
            <h4 class="mt50">以下の内容をご確認ください</h4>
            <p>契約約款/免責事項<br>
            <textarea class="textarea"><?php echo $this->element('kiyaku');?></textarea>
            <p class="mt10">個人情報保護方針</p>
            <textarea class="textarea"><?php echo $this->element('privacy');?></textarea>
            <?php echo $this->Form->input( 'agree', array( 'type' => 'checkbox', 'checked' => false,'label' => '上記内容をすべて確認しました。', 'div' => false,'class'=>'validate[required] mt20 mb40')); ?>
        <?php } ?>
            <p>
            <?php echo $this->Form->end($bstring); ?>
            </p>
            
            
        </div>
        
        <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => $back_btn['controller'], 'action' =>  $back_btn['action'],'admin' => false),array('class'=>'button darkblue'))); ?></div>
        
    </div>

</div>

<style>
.textarea{
    height: 80px;
    
    padding: 15px;
    overflow: auto;
    text-align: left;
    border: 1px solid #B4A596;
    border-radius: 2px;
}

</style>

<script>
jQuery(function($){
    $("#OrderAddForm").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
    
});
</script>


