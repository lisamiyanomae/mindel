<?php
$this->assign('title', 'お手続き完了');
$this->Html->addCrumb(' サービス開始お手続き');
$this->Html->addCrumb('お手続き完了');
?>
<!-- Event snippet for みんなにでんわ転送 conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-993820658/JBb6CJqAsYMBEPL_8dkD'});
</script>
    <div class="section">
        <div class="inner">
            <div class="company_message">
            <p>みんなに電話転送のお申し込みをありがとうございました。<br>
            サービス開始のお手続きを承りました。<br>
            受付した内容で電話番号の発行と転送設定をいたします。<br>
            設定完了はメールにて<?php echo $email; ?>へご連絡いたします。<br>
            それまで少しお待ちください。</p>
            <br>
            <p><a href="/"><u>トップに戻る</u></a><p>

            </div>
        </div><!--/inner-->
    </div><!--/company-->
