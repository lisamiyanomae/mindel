<?php
$this->assign('title', $tstring);
$this->Html->addCrumb($tstring);

echo $this->Html->css('validationEngine.jquery', null, array('inline' => false));
echo $this->Html->script('jquery-2.1.0.min', array('inline' => false));
echo $this->Html->script('jquery.validationEngine', array('inline' => false));
echo $this->Html->script('jquery.validationEngine-ja', array('inline' => false));

?>
<div class="section">
    <div class="inner">
        <div class="center tyuui"><?php echo $this->Flash->render(); ?></div>
        <h2><?php echo $tstring; ?></h2>
        <p><?php echo $tstring2; ?></p>
        <div class="table2">
        <?php if($this->action=='first' && $user['status'] == 3 ){ ?>
            <h4 class=" mt40">お支払い設定</h4>
            <p>利用開始月の月額基本料金のお支払いをもって、利用開始となります。<br>クレジットカードでお支払いください。(お支払回数は１回のみとなります。)<br>
            以下に入力したカードより、毎週日曜に前週７日分の通信料をご請求させていただきます。</p>
            <p class="pay">基本使用料(<?php echo date('n');?>月分) <?php echo $this->Number->currency(FIRST_COST * (1 + TAX),'JPY',array('places'=>0)); ?> </p>
            <p><br>
        
        <?php }else{ ?>
            
            <?php if($this->action=='pay' && $amount > 0 ){ ?>
            <h3>お支払い金額</h3>    
            <div class="box w600 mb30">
                <p><?php echo $this->Number->format($amount); ?> 円</p>
            </div>
            <?php } ?>
            
            <h3>ご登録クレジットカード</h3>    
            <div class="box w600 mb30">
                <p><?php echo $card_info; ?></p>
            </div>
            <h3>クレジットカードを変更</h3>    
        <?php } ?>
        
            <p align="center">ご利用できるカードは以下の通りです。<br>デビットカード、プリペイド式クレジットカードもご利用いただけます。<br><img align="center" src="/wp-content/uploads/card.jpg" alt="VISA,master,amex,diners,jcb,discover" /></p>

            <?php echo $this->Form->create('Order',array('id'=>'OrderAddForm','inputDefaults' => array('label' => false),'novalidate' => true,'onsubmit' => 'send();return false;','class'=>'primary-form')); ?><!-- カード情報非保持化 onsubmit 追加-->
            <?php echo $this->Form->input('customer_card_id'    ,array('id' =>'customer_card_id'          ,'type'=>'hidden' ,'value'=>'' )); ?>
            <?php echo $this->Form->input('received_amount'     ,array('id' =>'received_amount'           ,'type'=>'hidden' ,'value'=> round($amount,0) )); ?>


            <table class="sp_border mb30">
                <tr>
                <th>クレジットカード番号 ※</th>
                <td><?php echo $this->Form->input('credit_card_no', array( 'size' => 25, 'style' => 'ime-mode:disabled','class' => 'validate[required,custom[number]]','placeholder'=>'1234567812345678', 'div' => false)) ?>（ハイフンなし）</td>
                </tr>
                <tr>
                <th>セキュリティコード※</span></th>
                <td>カード裏面署名欄に印字されている右端3けたの数字を入力してください。<br>
                
                <?php echo $this->Form->input('credit_security_code', array( 'size' => 5, 'style' => 'ime-mode:disabled','class' => 'validate[required,custom[number]] mb20')) ?>
                
                <a href="/wp-content/uploads/code.jpg" target="blank" ><img src="/wp-content/uploads/code.jpg" alt="セキュリティコード" width="75px" height="auto"align="left"/></a>
                ※ここに数字が印字されていないカードはご利用いただけません。<br>AMEXの場合はカード表面右上の4けたの数字になります。
                
                </td>
                </tr>
                <tr>
                <th>有効期限※</th>
                <td>
                <?php   echo $this->Form->input('credit_month', array('type'=>'select','options' => $list_month , 'div' => false,                   'style' => 'ime-mode:disabled','class' => 'validate[required]')) ?> 月 /&nbsp;
                20<?php echo $this->Form->input('credit_year',  array('type'=>'select','options' => $list_year  , 'div' => false, 'error' => false, 'style' => 'ime-mode:disabled','class' => 'validate[required]')) ?> 年<br/>
                <?php echo $this->Form->error('credit_year'); ?>
                <?php echo $this->Form->error('credit_month'); ?>

                </td>
                </tr>
            </table>
            
        <?php if($this->action=='first' && $user['status'] == 3){ ?>
            <h4 class="mt50">決済を完了する前に、以下の内容をご確認ください。</h4>
            <p>契約約款/免責事項<br>
            <textarea class="textarea"><?php echo $this->element('kiyaku');?></textarea>
            <p class="mt10">個人情報保護方針</p>
            <textarea class="textarea"><?php echo $this->element('privacy');?></textarea>
            <?php echo $this->Form->input( 'agree', array( 'type' => 'checkbox', 'checked' => false,'label' => '上記内容をすべて確認して決済します。', 'div' => false,'class'=>'validate[required] mt20 mb40')); ?>
        <?php } ?>
            <?php echo $this->Form->input('save_card_info', array('type' => 'hidden','value' => 1)) ?>
            <?php echo $this->Form->input('user_id', array('type' => 'hidden','value' => $user['id'])) ?>
            <?php echo $this->Form->input('token', array('type' => 'hidden')) ?><!-- カード情報非保持化 -->
            
            <p>
            
            <?php echo $this->Form->end($bstring); ?>
            </p>
            
            
        </div>
        
        <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => $back_btn['controller'], 'action' =>  $back_btn['action'],'admin' => false),array('class'=>'button darkblue'))); ?></div>
        
    </div>

</div>

<style>
.textarea{
    height: 80px;
    
    padding: 15px;
    overflow: auto;
    text-align: left;
    border: 1px solid #B4A596;
    border-radius: 2px;
}

</style>

<script>
jQuery(function($){
    $("#OrderAddForm").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
    
});
</script>

<!-- カード情報非保持化 -->
<script type="text/javascript" src="https://token.paygent.co.jp/js/PaygentToken.js" charset="UTF-8"></script>
<script type="text/javascript"> 
<!--
// send関数の定義。カード情報入力フォームの送信ボタン押下時の処理
function send() {

    // sumbit前にvalidate
    if (!$('#OrderAddForm').validationEngine( 'validate' )) {
            return false;
    }

    var form = document.getElementById("OrderAddForm");
<?php if($this->action=='first' && $user['status'] == 3){ ?>    
    if(!form.OrderAgree.checked){
        alert('契約約款/免責事項、個人情報保護方針 をご確認ください。')
        form.OrderCreditCardNo.focus();
        return false;
    }
<?php } ?>
    if(!form.OrderCreditCardNo.value){
        alert('クレジットカード番号は入力必須項目です。')
        form.OrderCreditCardNo.focus();
        return false;
    }
    if(!form.OrderCreditSecurityCode.value){
        alert('セキュリティーコードは入力必須項目です。')
        form.OrderCreditSecurityCode.focus();
        return false;
    }

    if(!form.OrderCreditMonth.value){
        alert('有効期限は入力必須項目です。')
        form.OrderCreditMonth.focus();
        return false;
    }

    if(!form.OrderCreditYear.value ){
        alert('有効期限は入力必須項目です。')
        form.OrderCreditYear.focus();
        return false;
    }
    
    
    var paygentToken = new PaygentToken(); //PaygentTokenオブジェクトの生成 
    paygentToken.createToken( 
        '38482',                                         // 第１引数：マーチャントID 本番時入れ替えが必要
        'live_cAfdQs7BhXnquOG7k64B4WJN',                 // 第２引数：トークン生成鍵 本番時入れ替えが必要
        {                                                // 第３引数：クレジットカード情報 
            card_number:form.OrderCreditCardNo.value,    // クレジットカード番号 
            expire_year:form.OrderCreditYear.value,      // 有効期限-YY 
            expire_month:form.OrderCreditMonth.value,    // 有効期限-MM 
            cvc:form.OrderCreditSecurityCode.value       // セキュリティーコード 
            //name:form.name.value                       // カード名義 
        },execPurchase                                   // 第４引数：コールバック関数(トークン取得後に実?) 
    );
    
    return false;
    

} 
// コールバック関数の定義。トークン取得後の処理
function execPurchase(response) { 

    var form = document.getElementById("OrderAddForm");
    
    if (response.result == '0000') { //トークン処理結果が正常の場合  カード情報入力フォームから、入力情報を削除。
        form.OrderCreditCardNo.removeAttribute('name'); 
        form.OrderCreditCardNo.value=response.tokenizedCardObject.masked_card_number;
        form.OrderCreditYear.removeAttribute('name'); 
        form.OrderCreditMonth.removeAttribute('name'); 
        form.OrderToken.value = response.tokenizedCardObject.token; //予め用意したhidden項目tokenにcreateToken()から応答されたトークンを設定。
        form.submit(); //カード情報入力フォームをsubmitしてtokenを送信する
    } else { //トークン処理結果が異常の場合 
        alert('入力されたカード情報では決済することができません。カード番号またはセキュリーティコード、有効期限をもう一度確認して入力してください。 ('+ response.result + ')');
        //alert('err get token response.result:' + response.result);
    }
    return false;
} 

-->
</script>
<!-- カード情報非保持化 終了 -->

