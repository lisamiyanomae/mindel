<?php
$this->assign('title', '売上管理'); 

$this->Html->script('jquery-2.1.0.min', array('inline' => false));

if(!$id){
    $this->Html->script('jquery-2.1.0.min', array('inline' => false));
    $this->Html->script('jquery.ui.datepicker-ja.min.js', array('inline' => false));
    $this->Html->script('jquery-ui.min.js', array('inline' => false));
    $this->Html->css('jquery-ui', null, array('inline' => false));
}
?>
<?php if(!$id){  ?>
<script>
  $(function() {
    $("#serch_start_date").datepicker({dateFormat: 'yy-mm-dd'});
    $("#serch_end_date").datepicker({dateFormat: 'yy-mm-dd'});
  });
</script>
<?php }?>

<div id="main">
<div class="section inner">

<?php if(!$id){  // ---------------------------------------------------------- ?>
    <h1>売上管理</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6">請求一覧・入金状況</div>
    
    <?php echo $this->Form->create('Order',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    
    
    <p class="mb10">請求自動作成日時を過ぎても、何らかの理由で請求データが作成されない場合は手動で作成できます。（すでに作成済みの場合は重複して作成されません。）</p>
    <p class="mb20"> <?php echo $this->Form->submit('週次データ作成',array('name' => 'do_week_data','div'=>false));?> <span>（各週日曜0:00）</span>
	<?php echo $this->Form->submit('月次データ作成',array('name' => 'do_month_data','div'=>false));?> <span>（各月1日0:00）</span>
	</p>
    
    
    <ul class="dataCreation">
	    <li>
	    
	    
	    </li>
	    <li></li>
    </ul>
    
    
    <div id="account_kensaku" class="table">
        <table>
            <tr>
            
                <th rowspan = 2>条件検索</th>
                <td class="accout_conditions">
                    <ul class="mb10">
                        <li><p>請求No</p>
                            <?php  echo $this->Form->input('serch_id'      ,array('id' =>'serch_id'        ,'type' => 'text','label' => false, 'div' => false)); ?>
                            
                        </li>
                        <li><p>ユーザーID</p>
                            <?php  echo $this->Form->input('serch_user_id'  ,array('id' =>'serch_user_id'        ,'type' => 'text','label' => false, 'div' => false)); ?>
                        </li>
                        <li><p>ユーザー名</p>
                            <?php  echo $this->Form->input('search_text'    ,array('id' =>'search_text'    ,'type' => 'text','label' => false, 'div' => false)); ?>
                        </li>
                        <li><p>請求日</p>
                            <?php echo $this->Form->input('serch_start_date',array('id'=>'serch_start_date','type' => 'text','label' => false, 'div' => false));?>　～　<?php echo $this->Form->input('serch_end_date',array('id'=>'serch_end_date','type'=>'text','label' => '','div'=>false,'label'=>false));?>
                        </li>
                        <li><p>入金日</p>
                            <?php echo $this->Form->input('serch_pay_start_date',array('id'=>'serch_pay_start_date','type' => 'text','label' => false, 'div' => false));?>　～　<?php echo $this->Form->input('serch_pay_end_date',array('id'=>'serch_pay_end_date','type'=>'text','label' => '','div'=>false,'label'=>false));?>
                        </li>
                           </ul> 
                   <?php echo $this->Form->submit('絞込',array('name' => 'submit','div'=>false));?>&ensp;
                   <?php echo $this->Form->submit('未入金で絞り込む',array('name' => 'no_pay','div'=>false));?>
                  
                </td>
                
                <td rowspan = 2 class="accout_search" style="border-left:1px solid #CCC;">
                    <?php echo $this->Form->submit('解除',array('name' => 'reset','div'=>false,'class'=>'button red')); ?>&ensp;
                    
                    <?php echo $this->Form->submit('CSV出力',array('name' => 'outputcsv','div'=>false));?>
                    
                </td>
            </tr>
            
        </table>
        
    
    
    </div>
    </form>
    
<?php }else{  // ---------------------------------------------------------- ?>

<h1>アカウント（明細）</h1>

    <div id="customerinfo" class="table">
        <table>
            <tr>
                <th>お客様No.</th>
                <th>店舗名または屋号</th>
                <th>発行番号</th>
                <th>メールアドレス</th>
                <th>ステイタス</th>
            </tr>
            <tr>
            <td><?php echo USER_ID_HEDDER.sprintf('%06d',$user['User']['id']);?></td>
                <td><?php echo $user['User']['shop_name']; ?></td>
                <td><a href="tel:<?php echo $user['User']['tropo_telno']; ?>"><?php echo $user['User']['tropo_telno']; ?></td>
                <td><a href="mailto:<?php echo $user['User']['email']; ?>"><?php echo $user['User']['email']; ?></td>
                <td><?php echo $user_status[$user['User']['status']]; ?></td>
            </tr>
        </table>
    </div>
<?php }  ?>

    <div id="claimList" class="table scroll">
        <table>
            <thead>
            <tr>
                <th>請求No.</th>
                <th>請求日</th>
                <th>期限</th>
                <th>期間</th>
                <?php if(!$id){ ?><th>ユーザー</th><?php } ?>
                <th>支払方法</th>
                <th>基本料</th>
                <th>通信料</th>
                <th>自動音声</th>
                <th>小計</th>
                <th>税額</th>
                <th>請求額</th>
                <th>入金日</th>
            </tr>
            </thead>
            
            <tbody>
            <?php foreach ($orders as $order): 
            if($order['Order']['hand_payment'] == 1){$str_no_pay_date = '未処理';}else{$str_no_pay_date = '決済エラー';};
            ?>
            <tr>
                <td><?php echo ORDER_ID_HEDDER .sprintf('%0'.ORDER_ID_DIGIT.'d',$order['Order']['id']); ?></td>
                <td><?php echo $order['Order']['invoice_date']; ?></td>
                <td><?php echo $order['Order']['limit_date']; ?></td>
                <td><?php echo $order['Order']['start_date']; ?>～<?php echo $order['Order']['end_date']; ?></td>
                <?php if(!$id){ ?><td><u><?php echo $this->Html->link(USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$order['Order']['user_id']), array('controller'=>'users','action'=>'view',$order['Order']['user_id'],'admin'=>true)) ?></u><br><?php echo $order['Order']['shop_name'];?></td><?php } ?>
                <td><?php if($order['Order']['hand_payment'] == 1){echo "振込";}else{echo "カード";}; ?></td>
                <td><?php if($order['Order']['kind'] == 1){echo $this->Number->currency($order['Order']['sum_price'],'JPY',array('wholeSymbol'=>"",'places'=>0));} ?></td>
                <td><?php if($order['Order']['kind'] == 2){echo $this->Number->currency($order['Order']['callomg_price'],'JPY',array('wholeSymbol'=>"",'places'=>0));} ?></td>
                <td><?php if($order['Order']['kind'] == 2){echo $this->Number->currency($order['Order']['message_price'],'JPY',array('wholeSymbol'=>"",'places'=>0));} ?></td>
                <td class="subtotal"><?php echo $this->Number->currency($order['Order']['sum_price'],'JPY',array('wholeSymbol'=>"",'places'=>0));?></td>
                <td class="tax"><?php echo $this->Number->currency($order['Order']['tax'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></td>
                <td class="total"><?php echo $this->Number->currency($order['Order']['amount'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></td>
                <td class="pay_day"><?php if($order['OrderErr']['story']){echo $order['OrderErr']['story'].'<br>';} if($order['Order']['pay_date']){echo date('Y-m-d',strtotime($order['Order']['pay_date']));}else{ echo "<span class='error-message'>". $this->Html->link($str_no_pay_date, array('controller'=>'orders','action'=>'err',$order['Order']['id'],'admin'=>true))."<span>";} ?></td>
            </tr>

            <?php endforeach; ?>
            <?php unset($orders); ?>

            </tbody>
        </table>
    </div>
    
    <div class="pagenate">
        <?php echo $this->Paginator->prev(  '< 前のページ',  null,  null,  array('class' => 'disabled')); ?>
        <?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(  '次のページ >',  null,  null,  array('class' => 'disabled'));?>
        <?php echo $this->Paginator->counter(); ?>
        /<?php echo $this->Paginator->counter(array('format' => __('  {:count}')));?>
    </div>
    
    <div class="backpage"><?php print($this->Html->link('戻 る', array('controller' => $back_controller, 'action' => $back_action,'admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->