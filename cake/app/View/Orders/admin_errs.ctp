<?php 
$this->assign('title', '未収処理' );
$this->Html->script('jquery-2.1.0.min', array('inline' => false));

?>
<div id="main">
    
<div class="section inner">
    <h1>未収処理</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>

    <div id="message" class="globalmenu2">決済エラーが<span><?php echo $err_cnt;  ?></span>件あります</div>

    <div id="to_peigent"><a href="#">PEIGENT 管理画面へ</a></div>
    
    <?php echo $this->Form->create('Order',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    <div id="account_kensaku" class="table">
        <table>
            <tr>
                <th>条件検索</th>
                <td class="accout_conditions">
                    <ul class="">
                        <li><p>請求No</p>
                            <?php  echo $this->Form->input('serch_id'      ,array('id' =>'serch_id'        ,'type' => 'text','label' => false, 'div' => false)); ?>
                        </li>
                        <li><p>店舗名または屋号</p>
                            <?php  echo $this->Form->input('search_text'    ,array('id' =>'search_text'    ,'type' => 'text','label' => false, 'div' => false)); ?>
                        </li>
                        </ul> 
                </td>
                <td class="accout_search" >
                   <?php echo $this->Form->submit('絞込',array('name' => 'submit','div'=>false));?>&ensp;
                   <?php echo $this->Form->submit('解除',array('name' => 'reset','div'=>false,'class'=>'button red')); ?>&ensp; 
                </td>
            </tr>
        </table>

    </div>
    </form>

    <div class="table scroll">
        <table>
            <thead>
            <tr>
                <th>発生日時</th>
                <th>請求No.</th>
                <th>お客様No.</th>
                <th>店舗名または屋号</th>
                <th>支払方法</th>
                <th>発行番号</th>
                <th>決済期間</th>
                <th>支払期限</th>
                <th>未決済金額</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($orders as $order): ?>
            <tr>
                <td><?php echo $order['OrderErr']['err_date']; ?></td>
                <td><?php echo ORDER_ID_HEDDER .sprintf('%0'.ORDER_ID_DIGIT.'d',$order['Order']['id']); ?></td>
                <td><?php echo USER_ID_HEDDER.sprintf('%06d',$order['Order']['user_id']);  ?></td>
                
                <td><?php echo $order['User']['shop_name']; ?></td>
                <td><?php if($order['Order']['hand_payment'] == 1){echo "振込";}else{echo "カード";}; ?></td>
                <td><?php echo $order['User']['tropo_telno']; ?></td>
                
                <td><?php echo $order['Order']['start_date']; ?>～<?php echo $order['Order']['end_date']; ?></td>
                <td><?php echo $order['Order']['limit_date']; ?></td>
                <td><?php echo $this->Number->currency($order['Order']['amount'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></td>
                <td>
                
                <?php if(!$order['Order']['pay_date']){print($this->Html->link('処理', 'err/'.$order['Order']['id'],array('class'=>'button blue')));}else{ echo date('m/d',strtotime($order['Order']['pay_date'])) . '入金 '.$order['OrderErr']['story'];} ?>
                <?php if($order['Order']['hand_payment'] == 0){print($this->Html->link('メール再送信', array('action'=>'send_err_mail/'.$order['Order']['id']),array('class'=>'button gray')));} ?>
                </td>

            </tr>
            <?php endforeach; ?>
            <?php unset($order); ?>

            </tbody>
        </table>
    </div>

    <div class="pagenate">
        <?php echo $this->Paginator->prev(  '< 前のページ',  null,  null,  array('class' => 'disabled')); ?>
        <?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(  '次のページ >',  null,  null,  array('class' => 'disabled'));?>
        <?php echo $this->Paginator->counter(); ?>
    </div>
    
    <div class="backpage"><?php print($this->Html->link('戻 る', array('controller' => 'pages', 'action' => 'home','admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->
