<?php 
$this->assign('title', '未収処理(詳細)' );

echo $this->Html->script('jquery-2.1.0.min', array('inline' => false));
echo $this->Html->script('jquery.ui.datepicker-ja.min.js');
echo $this->Html->script('jquery-ui.min.js');
echo $this->Html->css('jquery-ui');

echo $this->Html->css('validationEngine.jquery', null, array('inline' => false));
echo $this->Html->script('jquery.validationEngine', array('inline' => false));
echo $this->Html->script('jquery.validationEngine-ja', array('inline' => false));


?>
<script>
  $(function() {
    $("#pay_date").datepicker({dateFormat: 'yy-mm-dd'});
    $("#form_order").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
  });
</script>


<div id="main">
    
<div class="section inner">
    <h1>未収処理（詳細）</h1>

    <div id="customerinfo" class="table">
        <table>
            <tr>
                <th>お客様No.</th>
                <th>店舗名または屋号</th>
                <th>発行番号</th>
                <th>支払方法</th>
                <th>メールアドレス</th>
                <th>ステイタス</th>
            </tr>
            <tr>
                <td><u><?php echo $this->Html->link(USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$order['Order']['user_id']), array('controller'=>'users','action'=>'view',$order['Order']['user_id'],'admin'=>true)) ?></u></td>
                <td><?php echo $order['Order']['shop_name'];?></td>
                <td><?php echo $order['User']['tropo_telno'];?></td>
                <td><?php if($order['Order']['hand_payment'] == 1){echo "振込";}else{echo "カード";}; ?></td>
                <td><?php echo $order['User']['email'];?></td>
                <td><?php echo $user_status[$order['User']['status']];?></td>
            </tr>
        </table>
    </div>

    
    <div id="claimList" class="table">
        <?php echo $this->Form->create('Order',array('id'=>'form_order','inputDefaults' => array('label' => false, 'div' => false))); 
              echo $this->Form->input('OrderErr.id'  ,array('type'=>'hidden' ,'value'=>$order['OrderErr']['id'] )); 
        ?>
        <table>
            <thead>
            <tr>
                <th>請求No.</th>
                <th>請求日</th>
                <th>期限</th>
                <th>期間</th>
                <th>基本料</th>
                <th>通信料</th>
                <th>自動音声</th>
                <th>小計</th>
                <th>税額</th>
                <th>請求額</th>
                <th>入金日</th>
                <th>メモ</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><?php echo ORDER_ID_HEDDER .sprintf('%0'.ORDER_ID_DIGIT.'d',$order['Order']['id']); ?></td>
                <td><?php echo $order['Order']['invoice_date']; ?></td>
                
                <td><?php echo $order['Order']['limit_date']; ?></td>
                
                <td><?php echo $order['Order']['start_date']; ?><br>～<br><?php echo $order['Order']['end_date']; ?></td>
                <td><?php if($order['Order']['kind'] == 1){echo $this->Number->currency($order['Order']['sum_price'],'JPY',array('wholeSymbol'=>"",'places'=>0));} ?></td>
                <td><?php if($order['Order']['kind'] == 2){echo $this->Number->currency($order['Order']['callomg_price'],'JPY',array('wholeSymbol'=>"",'places'=>0));} ?></td>
                <td><?php if($order['Order']['kind'] == 2){echo $this->Number->currency($order['Order']['message_price'],'JPY',array('wholeSymbol'=>"",'places'=>0));} ?></td>
                <td class="subtotal"><?php echo $this->Number->currency($order['Order']['sum_price'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></td>
                <td class="tax"><?php echo $this->Number->currency($order['Order']['tax'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></td>
                <td class="total"><?php echo $this->Number->currency($order['Order']['amount'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></td>
                <td class="pay_day"><?php echo $this->Form->input('Order.pay_date' , array('id'=>'pay_date','type'=>'text','size' => 20, 'class' => 'validate[required]','placeholder'=>'')) ?><br>
                <td ><?php echo $this->Form->input('OrderErr.story' , array('id'=>'story','type'=>'textarea','size' => 20, 'class' => '','placeholder'=>'振込銀行名など')) ?><br>
                <td><?php echo $this->Form->submit('登録',array('name' => 'reset','div'=>false)); ?></td></td>
                
                
            </tr>
            </tbody>
        </table>
    </div>
    
    <div class="backpage"><?php print($this->Html->link('戻 る', '../orders/errs' ,array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->


