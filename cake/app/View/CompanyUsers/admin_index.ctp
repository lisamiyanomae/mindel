<?php
$this->assign('title', 'マルチアカウント');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
echo $this->Html->css('validationEngine.jquery', null, array('inline' => false));
echo $this->Html->script('jquery-2.1.0.min', array('inline' => false));
echo $this->Html->script('jquery.validationEngine', array('inline' => false));
echo $this->Html->script('jquery.validationEngine-ja', array('inline' => false));

?>
<div id="main">
<script>
jQuery(function($){
    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
    
});
</script>
<div class="section inner">

    <h1>マルチアカウント</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6">「<?php echo $company['showname']?> 」アカウント</div>
    <p></p>
    <div id="claimList" class="table">
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>ショップ名</th>
                <th>発行番号</th>
                <th>メールアドレス</th>
                <th></th>
            </tr>
            </thead>
            
            <tbody>
            <?php foreach ($datas as $member): ?>
            <tr>
                <td><?php echo USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$member['CompanyUser']['user_id']); ?></td>
                <td><?php echo $member['User']['shop_name']; ?></td>
                <td><?php echo $member['User']['tropo_telno']; ?></td>
                <td><?php echo $member['User']['email']; ?></td>

                
                <td><?php  print($this->Form->postLink('<i class="fas fa-trash-alt"></i>',array('action'=>'delete/'.$member['CompanyUser']['id'].'/'.$member['CompanyUser']['company_id']),array('escape' => false),'本当に削除しますか?'));  ?></td>
                </tr>
            <?php endforeach; ?>
            <?php unset($member); ?>
            </tbody>
        </table>
    </div>

    <div class="pagenate">
        <span><?php echo $this->Paginator->prev(  '< 前のページ',  null,  null,  array('class' => 'disabled')); ?></span>
        <span><?php echo $this->Paginator->numbers();?></span>
        <span><?php echo $this->Paginator->next(  '次のページ >',  null,  null,  array('class' => 'disabled'));?></span>
        <span><?php echo $this->Paginator->counter(); ?></span>
    </div>
	    
    <div class="mt30">
    <?php echo $this->Form->create('CompanyUser',array('id'=>'varidate_form','inputDefaults' => array('label' => false,'div'=>false),'novalidate' => true,'class'=>'primary-form')); ?>
    <?php echo $this->Form->input('company_id',array('type'=>'hidden','value' => $company['id'])); ?>
    <?php echo $this->Form->input('user_id',array('type'=>'select','options'=>$users,'empty'=>'選択してください','label' => 'アカウント追加 ','div'=>false,'class' => 'validate[required]')); ?>
    <?php echo $this->Form->submit('追加',array('div'=>false)); ?>
    </div>    

    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'companies', 'action' => 'index','admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->



