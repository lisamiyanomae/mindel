<?php 
$this->assign('title', 'お知らせ');

$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));

$this->Html->script('jquery-ui.min.js', array('inline' => false));
$this->Html->script('jquery.ui.datepicker-ja.min.js', array('inline' => false));

echo $this->Html->css('validationEngine.jquery', null, array('inline' => false));
echo $this->Html->css('jquery-ui', null, array('inline' => false));


?>
<script>
  $(function() {
    $("#start_date").datepicker({dateFormat: 'yy-mm-dd'});
    $("#end_date").datepicker({dateFormat: 'yy-mm-dd'});
  });
</script>
<div id="main">
    
<div class="section inner">
    <h1>お知らせ</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    
    <div id="message" class="globalmenu5">お知らせ投稿</div>

    <div id="new_post">
        <h2>新規お知らせを発行</h2>
        <?php echo $this->Form->create('Notice',array('id'=>'varidate_form','inputDefaults' => array('label' => false,'div'=>false),'novalidate' => true,'class'=>'primary-form')); ?>
        <dl>
            <dt><?php echo $this->Form->input('title',    array(  'class' => 'validate[required,maxSize[100]]' ,'placeholder'=>"タイトル")); ?></dt>
            <dd><?php echo $this->Form->input('content',  array(  'class' => 'validate[required]' ,'placeholder'=>"本文")); ?></dd>
            
            <dd class="mt20"><?php echo $this->Form->input('start_date',  array( 'id'=>'start_date', 'type'=>'text' , 'class' => 'validate[]' ,'placeholder'=>"注目期間")); ?> ～ <?php echo $this->Form->input('end_date',  array( 'id'=>'end_date', 'type'=>'text' , 'class' => 'validate[]' ,'placeholder'=>"")); ?></dd>

        </dl>
        <div class="cont_right">
            <ul>
                <li>特定ユーザー向けのお知らせの場合はお客様No.を入力 <?php echo $this->Form->input('user_id',  array('class' => 'validate[custom[onlyNumberSp]]' ,'div' => false , 'type' => 'text' )); ?></li>
            </ul>
        </div>
        <div class="clear cont_right"><?php echo $this->Form->end('投稿'); ?></div>
    </div>
    
    <br class="clear">

    <div id="infobox">
        <?php foreach ($notices as $notice): ?>
        <dl>
            <dt>
            <?php echo date('Y.m.d',strtotime($notice['Notice']['created'])); ?>
            <span><?php if($notice['User']['shop_name']){echo $this->Html->link($notice['User']['shop_name'], array('controller' => 'users', 'action' => 'view/'.$notice['Notice']['user_id'],'admin' => true));}else{echo '全てのユーザー';} ?></span>
            <?php print($this->Html->link('<i class="fas fa-edit"></i>',array( 'action'=>'edit',$notice['Notice']['id'],'admin'=>true),array('escape' => false))); ?>
            <?php print($this->Form->postLink('<i class="fas fa-trash-alt"></i>',array('action'=>'delete',$notice['Notice']['id'],'admin'=>true),array('escape' => false),'本当に削除しますか?'));  ?>
            </dt>
            <dd><p><strong><?php echo $notice['Notice']['title']; ?></strong></p>
            <?php echo $notice['Notice']['content']; ?>
            <p><?php if($notice['Notice']['start_date']){echo date('Y-m-d',strtotime($notice['Notice']['start_date']));} if($notice['Notice']['start_date'] || $notice['Notice']['end_date']){echo " ～ ";}  if($notice['Notice']['end_date']){echo date('Y-m-d',strtotime($notice['Notice']['end_date'])); }?></p>
            
            </dd>
        </dl>
        <?php endforeach; ?>
        <?php unset($notice); ?>

    </div>

    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'pages', 'action' => 'home','admin' => true),array('class'=>'button darkblue'))); ?></div>
    
</div>

</div><!--/End main-->

<script>
jQuery(function($){
    
    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
});
</script>

