<?php 
$this->assign('title', 'スタッフ追加');
?>
<div id="main" class="mypage">

    <div class="section">
        <div class="inner">
            <h2>スタッフ設定</h2>    
            <p class="mb30">スタッフアカウントを追加します。</p>
            <p class="history_lead"></p>
            <?php echo $this->Form->create('Staff',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    		<?php echo $this->Form->input('user_id',array('type' => 'hidden','value' => $user['id'])); ?>
    
            <div class="table w800">
                <table class="history">
                <thead>
	            <tr>
	                <th>表示名</th>
	                <th>ログインID</th>
	                <th>パスワード</th>
	                
	            </tr>
	            </thead>
	            <tbody>
	            <tr>
	            <td><?php echo $this->Form->input('showname',array('label' => false)); ?></td>
	            <td><?php echo $this->Form->input('username',array('label' => false)); ?></td>
	            <td><?php echo $this->Form->input('password',array('label' => false)); ?></td>
	            </tr>
	            </tbody>
                </table>
            </div>
            
            <?php echo $this->Form->end('追加'); ?>
    		<div class="backpage mt30"><?php print($this->Html->link('戻る', array('controller' => 'staffs', 'action' => 'index'),array('class'=>'button darkblue'))); ?></div>

            
        </div><!--/inner-->
    </div><!--/-->
</div><!--/-->
