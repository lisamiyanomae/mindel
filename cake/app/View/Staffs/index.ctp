<?php 
$this->assign('title', 'スタッフ設定');
?>
<div id="main" class="mypage">

    <div class="section">
        <div class="inner">
            <div class="flash"><?php echo $this->Flash->render(); ?></div>
            <h2>スタッフ設定</h2>    
            <p class="mb30">スタッフアカウントを設定します。</p>
            <p class="history_lead">ここで作成したアカウントは <a target="_blank" href="<?php echo dirname($this->Html->url('/', true)); ?>/staff/staffs/login"><?php echo dirname($this->Html->url('/', true)); ?>/staff/staffs/login</a> からログインできます。</p>
            
            <div class="table">
                <table class="history">
                    <thead>
                        <tr>
                        <th>表示名</th>
                        <th>ログインID</th>
                        <th>登録日時</th>
                        <th></th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($datas as $data): ?>
                        <tr>
                        <td><?php echo $data['Staff']['showname']; ?></td>
                        <td><?php echo $data['Staff']['username']; ?></td>
                
                        <td class="mobile_del"><?php echo $data['Staff']['created']; ?></td>
                        <td><?php print($this->Html->link('<i class="fas fa-edit"></i>',array( 'action'=>'edit',$data['Staff']['id']),array('escape' => false))); ?></td>
                        <td><?php print($this->Form->postLink('<i class="fas fa-trash-alt"></i>',array('action'=>'delete',$data['Staff']['id']),array('escape' => false),'本当に削除しますか?'));  ?></td>
                        </tr>
                        <?php endforeach; ?>
                        <?php unset($data); ?>
                    </tbody>
                </table>
            </div>
            
            
            
            <div id="pager">
            <span><?php echo $this->Paginator->prev(  '≪ PREV',  array('class' => 'nav'),  null,  array('class' => 'nav')); ?></span>
            <span class="num" ><?php echo $this->Paginator->numbers(array('separator'=>'</span><span class="num">'));?></span>
            <span><?php echo $this->Paginator->next(  'NEXT ≫',  array('class' => 'nav'),  null,  array('class' => 'nav'));?></span>
            <span><?php echo $this->Paginator->counter(); ?></span>
            </div>
            
            <div><?php print($this->Html->link('<i class="fas fa-user-plus fa-2x"></i>',array( 'action'=>'add'),array('escape' => false))); ?></div>
            
            <?php echo $this->Form->create('Staff',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
            <?php echo $this->Form->input('user_id',array('type' => 'hidden','value' => $user['id'])); ?>

            <p class="mt30"><?php echo $this->Form->submit('転送先をスタッフとして一括登録',array('name'=>'bulk_reg')); echo $this->Form->end();?></p>
            <p class="mt10">一括登録したスタッフのログインパスワードはすべて9999となります、適宜変更してご利用ください。</p>
            
            <div class="backpage mt30"><?php print($this->Html->link('戻る', array('controller' => 'telephones', 'action' => 'index'),array('class'=>'button darkblue'))); ?></div>

        </div><!--/inner-->
    </div><!--/-->
</div><!--/-->

