<?php
$this->assign('title', 'ポイントデータ');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
?>
<div id="main">
    
<div class="section inner">

    <h1>ポイントデータ</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6">ポイントユーザー一覧</div>
    
    <?php echo $this->Form->create('Point',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    <div id="account_kensaku" class="table">
        <table>
            <tr>
                <th>ユーザー検索</th>
                <td class="accout_conditions">
                    <ul class="">
                        <li><p>ID</p>
                            <?php  echo $this->Form->input('serch_id'       ,array('id' =>'serch_id'       ,'type' => 'text','label' => false, 'div' => false)); ?>
                        </li>
                        <li><p>店舗名または屋号</p>
                            <?php  echo $this->Form->input('search_text'    ,array('id' =>'search_text'    ,'type' => 'text','label' => false, 'div' => false)); ?>
                        </li>
                        <li><p>残高</p>
                            <?php  echo $this->Form->input('search_balance' ,array('id' =>'search_balance' ,'type' => 'text','label' => false, 'div' => false)); ?> 以下
                        </li>
                        </ul>
                </td>
                <td class="accout_search" >
                   <?php echo $this->Form->submit('絞込',array('name' => 'submit','div'=>false));?>&ensp;
                   <?php echo $this->Form->submit('解除',array('name' => 'reset','div'=>false,'class'=>'button red')); ?>&ensp; 
                </td>
            </tr>
        </table>

    </div>
    </form>
    
    <div id="claimList" class="table">
<?php if(count($add_user_ids) > 0){ ?>
        <table class="mb20">
            <thead>
            
            <tr><th>未登録のポイント払いユーザーがいます</th></tr>
            <tr><td>
                <?php foreach($add_user_ids AS $add_user_id){ ?>
                <p>
                <div class="new_user">
                <?php echo $add_user_id['User']['shop_name']." ".$this->Html->link(' ('.USER_ID_HEDDER.sprintf('%06d',$add_user_id['User']['id']).')',array( 'controller'=>'users','action'=>'view',$add_user_id['User']['id']),array('escape' => false,'class'=>'','title'=>'顧客詳細へ')); ?>
                </div>
                新規ポイントデータを <?php echo $this->Html->link('作成',array( 'action'=>'new/'.$add_user_id['User']['id']),array('escape' => false,'class'=>'button darkblue')); ?>
                または 既ユーザーに <u><?php echo $this->Html->link('紐づけ',array('controller'=>'users', 'action'=>'parent/'.$add_user_id['User']['id']),array('escape' => false,'class'=>'')); ?></u>
                </p>
                <?php } ?>
            </td></tr>
            
            </thead>
        </table>
    
<?php } ?>
    
        <table  class="mb20">
            <thead>
            <tr>
                <th></th>
                <th>お客様No.</th>
                <th>ポイント払い（親）ユーザー</th>
                <th>子ユーザー(追加は<u><?php echo $this->Html->link('アカウント管理',array( 'controller'=>'users','action'=>'index'),array('escape' => false)); ?></u>から)</th>
                <th>残高</th>
                <th>最終更新</th>
            </tr>
            </thead>
            
            <tbody>
            <?php foreach ($datas as $data): ?>
            <tr>
                <td><?php echo $this->Html->link('詳細',array( 'controller'=>'points','action'=>'detail',$data['User']['id']),array('escape' => false,'class'=>'button blue')); ?></td>
                <td><?php echo USER_ID_HEDDER.sprintf('%06d',$data['User']['id']); ?></td>
                <td>
                <?php echo $this->Html->link($data['User']['shop_name'],array( 'controller'=>'users','action'=>'view',$data['User']['id']),array('escape' => false,'class'=>'','title'=>'顧客詳細へ')); ?>
                </td>
                <td>
                <?php if(array_key_exists($data['User']['id'],$children)){ foreach($children[$data['User']['id']] AS $child_id => $child_name){ ?>
                    <p><?php echo $child_name; ?> (<?php echo USER_ID_HEDDER.sprintf('%06d',$child_id); ?>) <u><?php echo $this->Html->link('変更',array( 'controller'=>'users','action'=>'parent',$child_id),array('escape' => false,'class'=>'','title'=>'ポイント払い(親)ユーザー設定へ')); ?></u></p>
                <?php }} ?>
                </td>
                <td><?php echo number_format($data['Point']['balance'])." p"; ?></td>
                <td><?php echo $data['Point']['created']; ?></td>
                
                </tr>
            <?php endforeach; ?>
            <?php unset($data); ?>
            </tbody>
        </table>
    </div>
    
    <div class="pagenate">
        <?php echo $this->Paginator->prev(  '< 前のページ',  null,  null,  array('class' => 'disabled')); ?>
        <?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(  '次のページ >',  null,  null,  array('class' => 'disabled'));?>
        <?php echo $this->Paginator->counter(); ?>
    </div>

    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'pages', 'action' => 'home','admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->

<style>
.new_user{
    width:250px;
    white-space: nowrap;
    display: inline flow-root list-item;
    text-align:left;
    
}
.bold{
	font-weight:bold;
	font-size:1.2em;
}
</style>

