<?php
$this->assign('title', 'ポイントデータ');
$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.jpostal', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));

?>
<div id="main">
    
<div class="section inner">
    <h1>ポイントデータ (データ追加)</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6">ポイント<?php if($flg == "p"){ echo "加算"; }else{echo "減算"; } ?></div>
    
    <?php if($flg == "p"){ ?>
    <p>入金を確認して加算処理を行ってください。</p>
    <?php }else{ ?>
    <p>初回月額使用料の支払いなどを行います。対象アカウントを選択しポイント減算理由を明記してください。</p>
    <?php } ?>
    <?php echo $this->Form->create('Point',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    <td><?php echo $this->Form->input('done_datetime', array('type' => 'hidden','value' => date('Y-m-d H:i:s'))); ?></td>
    
    <div id="claimList" class="table">
        <table>
            <thead>
            <tr>
                <th>ポイント払い（親）ユーザー</th>
                <th>残高</th>
                <?php if($flg == "p"){ ?><th>加算ポイント</th><?php } ?>
                <?php if($flg == "m"){ ?><th>減算ポイント</th><?php } ?>
                <?php if($flg == "m"){ ?><th>対象アカウント</th><?php } ?>
                <th>メモ</th>
            </tr>
            </thead>
            <tbody>
             <tr>
                <td><?php echo $this->Form->input('user_id', array('type' => 'hidden','value'=>$user['User']['id'])); 
	                      echo $user['User']['shop_name']."<br>".USER_ID_HEDDER.sprintf('%06d',$user['User']['id']);  ?></td>
                <td><?php echo $this->Form->input('balance', array('type' => 'hidden','value'=>$user['Point']['balance'])); 
                          echo number_format($user['Point']['balance'])." p"; ?></td>
                <?php if($flg == "p"){ ?><td><?php echo $this->Form->input('add_points',   array('type' => 'num','class' => 'validate[required]')); ?></td><?php } ?>
                <?php if($flg == "m"){ ?><td><?php echo $this->Form->input('def_points',   array('type' => 'num','class' => 'validate[required]')); ?></td><?php } ?>
                <?php if($flg == "m"){ ?><td><?php echo $this->Form->input('do_user_id',   array('type' => 'select','options' => $children,'class' => 'validate[required]')); ?></td><?php } ?>
                <td><?php echo $this->Form->input('memo', array('type' => 'text','class' => 'validate[required]')); ?></td>
            </tr>
            </tbody>
        </table>
        
    </div>
    <?php echo $this->Form->end('実行'); ?>
    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'points', 'action' => 'detail/'.$user['User']['id'],'admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->
<script>
jQuery(function($){

    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
         
});
</script>
<style>
div.radio_nonfieldset fieldset{
width: auto;
border: none;
padding: 5px;
}
div.radio_nonfieldset legend{
width: auto;
border: none;
display:none;
}
</style>