<?php
$this->assign('title', 'ポイントデータ');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
?>
<div id="main">
    
<div class="section inner">

    <h1>ポイントデータ (詳細)</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6"><?php echo $this->Html->link($datas[0]['User']['shop_name'].' ('.USER_ID_HEDDER.sprintf('%06d',$datas[0]['User']['id']).')',array( 'controller'=>'users','action'=>'view',$datas[0]['User']['id']),array('escape' => false,'class'=>'','title'=>'顧客詳細へ')); ?> <br>ポイント詳細</div>
    
    
    <?php echo $this->Form->create('Point',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    
    
    
    <div id="claimList" class="table">
    
        <table  class="mb20 bold">
            <thead>
            <tr>
            <th>
            
            <?php echo '所有ポイント '.number_format($datas[0]['Point']['balance'])." p"; ?>
            <?php echo $this->Html->link('ポイント加算',array( 'action'=>'add/'.$datas[0]['Point']['user_id'].'/p'),array('escape' => false,'class'=>'button darkblue')); ?>
            <?php echo $this->Html->link('ポイント減算',array( 'action'=>'add/'.$datas[0]['Point']['user_id'].'/m'),array('escape' => false,'class'=>'button darkblue')); ?>
            <?php //echo $this->Html->link('メール送信',array( 'controller'=>'points','action'=>'sendmail',$datas[0]['Point']['user_id']),array('escape' => false,'class'=>'button blue')); ?>
            </th>
            </tr>
            </thead>
        </table>

        <table  class="mb20">
            <thead>
            <tr>
                <th>処理日時</th>
                <th>加算ポイント</th>
                <th>減算ポイント</th>
                <th>残高</th>
                <th>メモ</th>

            </tr>
            </thead>
            
            <tbody>
            <?php foreach ($datas as $data): ?>
            <tr>
                <td><?php echo $data['Point']['created']; ?></td>
                <td><?php if($data['Point']['add_points'] > 0){echo number_format($data['Point']['add_points'])." p";} ?></td>
                <td><?php if($data['Point']['def_points'] > 0){echo number_format($data['Point']['def_points'])." p";} ?></td>
                <td><?php echo number_format($data['Point']['balance'])." p"; ?></td>
                <td><?php echo $data['Point']['memo']; ?></td>
                
                </tr>
            <?php endforeach; ?>
            <?php unset($data); ?>
            </tbody>
        </table>
    </div>
    
    <div class="pagenate">
        <?php echo $this->Paginator->prev(  '< 前のページ',  null,  null,  array('class' => 'disabled')); ?>
        <?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(  '次のページ >',  null,  null,  array('class' => 'disabled'));?>
        <?php echo $this->Paginator->counter(); ?>
    </div>

    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'points', 'action' => 'index','admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->

<style>
.new_user{
    width:250px;
    white-space: nowrap;
    display: inline flow-root list-item;
    text-align:left;
    
}
.bold{
	font-weight:bold;
	font-size:1.2em;
}
</style>

