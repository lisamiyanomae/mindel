<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$this->assign('title', 'ページが見つかりません');
?>

<div id="main">

	<div class="section">
		<div id="searchlist" class="inner">
		
		<h2>ページが見つかりません</h2>
		<p class="404">誠に申し訳ございませんが、お探しのページは見つかりませんでした。</p>
		<p class="mb30">お探しのページは一時的に利用できない状況にあるか、移動もしくは削除された可能性がございます。<br>
		トップページからお探しください。</p>
		<div class="submitbtn registration"><a class="button darkblue" href="<?php echo $this->Html->url('/', true); ?>">みんなにでんわ転送マイページトップへ</a></div>
		</div>
	</div>
</div><!--/End main-->
