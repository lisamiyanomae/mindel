<?php 
$this->assign('title', '運営ポータル');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));

?>
<div id="main">
    
<div class="section inner">
    <h1>運営ポータル</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <ul id="flow">
        <li><div>申込み<br>WEB</div></li>
        <li><div>詳細入力<br>確認資料</div></li>
        <li><div>審査<br>（非承認）</div></li>
        <li><div>支払<br>転送設定</div></li>
        <li><div>電話番号<br>発行</div></li>
        <li><div>サービス<br>開始</div></li>
    </ul>
    
    <div id="globalmenu" class="row">
        <div class="cont1of3 globalmenu1">
            <?php  echo $this->Html->link('審 査', array('controller'=>'users','action'=>'admin_index/1/1')) ?>
            <p>審査待ちが<span><?php echo $status1_cnt; ?></span>件あります</p>    
        </div>
        <div class="cont1of3 globalmenu2">
            <?php  echo $this->Html->link('未収処理', array('controller'=>'orders','action'=>'admin_errs')) ?>
            <p>未収が<span><?php echo $order_err_cnt; ?></span>件あります</p>   
        </div>
        <div class="cont1of3 globalmenu3">
            <?php  echo $this->Html->link('番号発行', array('controller'=>'users','action'=>'admin_index/4/1')) ?>
            <p>発行待ちが<span><?php echo $status4_cnt; ?></span>件あります</p>        
        </div>
        <div class="cont1of3 globalmenu4">
            <?php  echo $this->Html->link('アカウント', array('controller'=>'users','action'=>'admin_index')) ?>
            <p>変更・返金・休止・印刷</p>        
        </div>
        <div class="cont1of3 globalmenu5">
            
            <?php  echo $this->Html->link('おしらせ', array('controller'=>'notices','action'=>'admin_add')) ?>
            <p>おしらせ投稿</p>            
        </div>
        <div class="cont1of3 globalmenu6">
            <?php  echo $this->Html->link('売上管理', array('controller'=>'orders','action'=>'admin_index')) ?>
            <p>帳簿を作成します</p>        
        </div>
        <div class="cont1of3 globalmenu6">
            <?php  echo $this->Html->link('ポイントデータ', array('controller'=>'points','action'=>'admin_index')) ?>
            <p>ポイントを管理します</p>            
        </div>
    </div>


    <div id="submenu" class="row">
        <div class="cont1of3 submenu1">
            <a href="https://jp.twilio.com/console/phone-numbers/incoming" target="_blank">TWILIO電話番号発行ページ</a>
        </div>
        <div class="cont1of3 submenu2">
            <a href="/faq/"  target="_blank">みんなにでんわ転送 Q＆A</a>
        </div>
        <div class="cont1of3 submenu3">
            <a href="https://www.paygent.co.jp/credit_card/" target="_blank">PEIGENT（カード決済）Q＆A</a>            
        </div>
        <div class="cont1of3 submenu4">
            <!--<a href="">送信メール文面一覧</a>--><?php  echo $this->Html->link('送信メール文面一覧', array('controller'=>'pages','action'=>'mails')) ?>     
        </div>
<?php if($admin['role'] == 1){ ?>
        <div class="cont1of3 submenu5">
            <?php  echo $this->Html->link('運営アカウント管理', array('controller'=>'administrators','action'=>'admin_index')) ?>
        </div>
<?php } ?>
        <div class="cont1of3 submenu5">
            <?php  echo $this->Html->link('マルチアカウント設定', array('controller'=>'companies','action'=>'admin_index')) ?>
        </div>

    </div>

</div>

</div><!--/End main-->