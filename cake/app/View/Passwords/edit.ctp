<?php 
$this->assign('title', 'パスワード再発行'); 
$this->Html->addCrumb('パスワード再発行');

?>

    <div class="section">
        <div class="inner">
            <div class="company_message">
                
                
                <?php echo $this->Form->create('User'); ?>
                <span class="center">新しいパスワードを入力してください。<br>(4文字以上20文字以内英数記号）</span>
                <p class="center"><?php echo $this->Form->input('plain_password',array('label' => 'パスワード '));  ?></p>
                <?php echo $this->Flash->render() ?>
                <p class="center"><?php echo $this->Form->end('更新する'); ?></p>
                

            </div>
        </div><!--/inner-->
    </div><!--/company-->



<style>
.company_message p,.company_message span{
    margin-bottom:20px;
}
</style>

