<?php 
$this->assign('title', 'パスワード再発行'); 
$this->Html->addCrumb('パスワード再発行');

?>

    <div class="section">
        <div class="inner">
            <div class="company_message">
                
                
                <?php echo $this->Form->create('User'); ?>
                <span class="center">登録されているメールアドレスを入力して下さい。</span>
                <div class="center tyuui mb20 mt10"><?php echo $this->Flash->render(); ?></div>
                <p class="center"><?php echo $this->Form->input('email',array('label' => 'メールアドレス　'));  ?></p>
                
                <p class="center"><?php echo $this->Form->end('再発行する'); ?></p>
                

            </div>
        </div><!--/inner-->
    </div><!--/company-->



<!--
<div class="users form">
    <?php echo $this->Flash->render() ?>

    <p>入力されたメールアドレス宛にパスワードを送信します。</p>
<fieldset>
    <?php //echo $this->Form->create('User'); ?>
    <?php //echo $this->Form->input('email',array('label' => 'メールアドレス'));  ?>
    <p><?php echo $this->Form->end('再発行する'); ?></p>
</fieldset>
</div>
-->
<style>
.company_message p,.company_message span{
    margin-bottom:20px;
}
</style>