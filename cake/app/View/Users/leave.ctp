<?php 
$this->assign('title', '解約');
echo $this->Html->css('validationEngine.jquery', null, array('inline' => false));
echo $this->Html->script('jquery-2.1.0.min', array('inline' => false));
echo $this->Html->script('jquery.validationEngine', array('inline' => false));
echo $this->Html->script('jquery.validationEngine-ja', array('inline' => false));
?>
<script>

jQuery(function($){
	$("#UserLeaveForm").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
	$('form').submit(function(){
		if (!$('#UserLeaveForm').validationEngine( 'validate' )) {
	            return false;
	    }
		if(!confirm('本当に解約しますか？')){
	        return false;
	    }else{
	    	return true;
	    }
	});
});


</script>
<div id="main" class="mypage">
    
    <div class="section">
        <div class="inner">
            
            <h2>解約</h2>
            <p class="mb30">解約処理まで数日いただくことがあります。<br>
                            解約後は、マイページのご利用ができなくなります。</p>
            <div class="box w400 mb40">
                <p><span class="tyuui">一度解約した電話番号の再発行はできません</span>ので慎重にご検討ください。</p>
            </div>
            <?php echo $this->Form->create('User',array('inputDefaults' => array('label' => false))); ?>
            <div class="box w400 mb40">
                <p class="txtleft mb10" style="font-size:100%;">今後のサービス向上のため、よろしければ解約の理由を具体的にお知らせください。</p>
                <?php echo $this->Form->input('content' ,array('id' =>'content','type'=>'textarea' ,'placeholder' => '使用状況など','escape'=>false , 'class'=>'validate[required]')); ?>
            </div>        
            <div class="registration"><?php echo $this->Form->end('解約を申し込む'); ?> </div>
            <div class="backpage mt20"><?php print($this->Html->link('戻る', array('controller' => 'telephones', 'action' => 'index','admin' => false),array('class'=>'button darkblue'))); ?></div>

        </div><!--/inner-->
    </div><!--/-->

</div><!--/End main-->

