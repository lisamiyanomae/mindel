<?php 
$this->assign('title', '基本情報の入力');
$this->Html->addCrumb('サービス申込み');
$this->Html->addCrumb('詳細入力');
$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.jpostal', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));
?>

<script>
function afile_changeHandler(evt){
    var files = evt.target.files;
    if(files[0].size > 6291456){
        alert('ファイルサイズが6Mを超えています、アップロードできません')
    }
    $("#confirmation_filename_1").alt = files[0].size;
    var target=evt.target;
    
    target.alt = files[0].size;
    
}
</script>

<div class="section">
    <div class="inner">
        <h2>基本情報の入力</h2>

<?php 
if($user['status'] == 0){
    if($basic == 0){  ?>
        <p><?php echo $user['shop_name']; ?> <?php echo $user['responsible_name']; ?> 様のアカウントが作成されました。<br>内容をご確認の上、以下の項目に入力し「サービス申し込み」にお進みください。<br>*は必須項目です</p>
<?php }else{?>
        <p><?php echo $user['shop_name']; ?> <?php echo $user['responsible_name']; ?> 様のアカウントの修正を行います。以下の項目に入力し「サービス申し込み」にお進みください。</p>
<?php 
    }    
}elseif($user['status'] == 2){
?>
        <p><?php echo $user['company_name']; ?> <?php echo $user['responsible_name']; ?> 様のアカウント修正を行います。以下の項目に入力し、もう一度「サービス申し込み」にお進みください。</p>
<?php
}
?>
        <div class="screen-reader-response mt20"></div>
        <?php echo $this->Form->create('User',array('id'=>'varidate_form','type'=>'file','inputDefaults' => array(),'novalidate' => true,'class'=>'primary-form')); 
        echo $this->Form->hidden('status');
        echo $this->Form->hidden('id');
        ?>
        <div class="table">
        
<?php if($basic == 0){  ?>
            <table class="sp_border">
                <tr>
                    <th>店舗名または屋号*</th>
                    <td><?php echo $user['shop_name'];  echo $this->Form->hidden('shop_name'); ?></td>
                </tr>
                <!--
                <tr>
                    <th>会社名<span class="fsmall">（契約者名）</span>*</th>
                    <td><?php //echo $user['company_name'];  echo $this->Form->hidden('company_name');  ?></td>
                </tr>
                <tr>
                    <th>カイシャメイ<span class="fsmall">（ケイヤクシャメイ）</span>*</th>
                    <td><?php //echo $user['company_kana'];   echo $this->Form->hidden('company_kana'); ?></td>
                </tr>
                -->
                <tr>
                    <th>ご担当者名*</th>
                    <td><?php echo $user['responsible_name']; echo $this->Form->hidden('responsible_name');  ?></td>
                </tr>
                <tr>
                    <th>ご担当者フリガナ*</th>
                    <td><?php echo $user['responsible_kana'];  echo $this->Form->hidden('responsible_kana'); ?></td>
                </tr>
                <tr>
                <th>メールアドレス*</th>
                <td><?php echo $user['email'];  echo $this->Form->hidden('email'); ?></td>
                </tr>
                <tr>
                <th>ログインID*</th>
                <td><?php echo $user['username'];  echo $this->Form->hidden('username');  ?></td>
                </tr>
                <tr>
                <th>パスワード* </th>
                <td>*************</td>
                </tr>
            </table>

            <p class="mt10 mb20 " align="right"><?php echo $this->Html->link('[ アカウント情報を編集する ]',  array('action'=>'edit/1')); ?></p>


<?php }else{ ?>

            <table class="sp_border" id="new_account">
                <tr>
                    <th>店舗名または屋号*</th>
                    <td><?php echo $this->Form->input('shop_name'       ,array('id' =>'shop_name'       ,'type'=>'text'     ,"size"=>"40"   ,'placeholder' => '〇〇株式会社△△支店'   ,'class' => 'validate[maxSize[100]]'                          , 'label'=>false, 'div'=>false )); ?></td>
                </tr>
                <!--
                <tr>
                    <th>会社名<span class="fsmall">（契約者名）</span>*</th>
                    <td><?php //echo $user['company_name'];  echo $this->Form->hidden('company_name');  ?></td>
                </tr>
                
                <tr>
                    <th>会社名<span class="fsmall">（契約者名）</span>フリガナ*</th>
                    <td><?php //echo $user['company_kana'];   echo $this->Form->hidden('company_kana'); ?></td>
                </tr>
                -->
                <tr>
                    <th>ご担当者名*</th>
                    <td><?php echo $this->Form->input('responsible_name',array('id' =>'responsible_name','type'=>'text'     ,"size"=>"40"  ,'placeholder' => '山田　太郎'          ,'class' => 'validate[required,maxSize[100]]'                 , 'label'=>false, 'div'=>false)); ?></td>
                </tr>
                <tr>
                    <th>ご担当者フリガナ*</th>
                    <td><?php echo $this->Form->input('responsible_kana',array('id' =>'responsible_kana','type'=>'text'     ,"size"=>"40"  ,'placeholder' => 'ヤマダ　タロウ'      ,'class' => 'validate[required,maxSize[100],custom[katakana]]', 'label'=>false, 'div'=>false)); ?></td>
                </tr>
                <tr>
                <th>メールアドレス*</th>
                    <td><?php echo $this->Form->input('email'           ,array('id' =>'email'           ,'type'=>'text'     ,"size"=>"40"   ,'placeholder' => 'example@mindel.com' ,'class' => 'validate[required,custom[email]]'                , 'label'=>false, 'div'=>false )); ?></td>
                </tr>
                <tr>
                <th>ログインID*</th>
                    <td><?php echo $this->Form->input('username'        ,array('id' =>'username'        ,'type'=>'text'     ,"size"=>"40"  ,'placeholder' => '6文字以上英数'       ,'class' => 'validate[required,custom[onlyLetterNumber]]'     , 'label'=>false, 'div'=>false  ,'maxlength' => '50', )); ?> (記号は使えません)</td>
                </tr>
                <tr>
                <th>パスワード*</th>
                <td>*************</td>
                </tr>
            </table>

<?php } ?>

            <table class="sp_border mt40" id="new_account">
                <tr>
                    <th>郵便番号</th>
                    <td class="postal"> <?php echo $this->Form->input('zip'             ,array('id' =>'zip'             ,'type'=>'text'  ,'placeholder' => '011-0102'                              ,'class' => 'validate[custom[zip]]'                    ,'label' => false,'maxlength' => '8','div'=>false)); ?> (ハイフンあり)</td>
                </tr>
                <tr>
                    <th>都道府県<span class="need">*</span></th>
                    <td><?php echo $this->Form->input('prefecture'      ,array('id' =>'prefecture'       ,'type'=>'select','options'     => $pref_list  ,'empty'=>'都道府県'       ,'class' => 'validate[required]'                        ,'label' => false)); ?></td>
                </tr>
                <tr>
                    <th>住所1<span class="need">*</span></th>
                    <td><?php echo $this->Form->input('address'         ,array('id' =>'address'          ,'type'=>'text'  ,"size"=>"40"  ,'placeholder' => '〇〇市△区'                        ,'class' => 'validate[required,maxSize[100]]' ,'label' => false)); ?></td>
                </tr>
                <tr>
                    <th>住所2</th>
                    <td><?php echo $this->Form->input('building'        ,array('id' =>'building'         ,'type'=>'text'  ,"size"=>"40" ,'placeholder' => '丁目番地、建物名・部屋番号まで'       ,'class' => ''                                  ,'label' => false )); ?></td>
                </tr>
                <?php if($user['status'] < 3){ ?>
                <tr>
                    <th>会社名<span class="fsmall">(契約者名）</span><span class="need">*</span></th>
                    <td><?php echo $this->Form->input('company_name'    ,array('id' =>'company_name'    ,'type'=>'text'   ,"size"=>"40"   ,'placeholder' => '〇〇株式会社' ,'class' => 'validate[required,maxSize[100]]'                  , 'label'=>false, 'div'=>false )); ?></td>
                </tr>
                <tr>
                    <th>会社名<span class="fsmall">(契約者名）</span>フリガナ<span class="need">*</span></th>
                    <td><?php echo $this->Form->input('company_kana'    ,array('id' =>'company_kana'    ,'type'=>'text'   ,"size"=>"40"  ,'placeholder' => '〇〇（カ','class' => 'validate[required,maxSize[100],custom[katakana]]' , 'label'=>false, 'div'=>false)); ?></td>
                </tr>
                <?php }else{ ?>
                <tr>
                    <th>会社名<span class="fsmall">（契約者名）</span><span class="need">*</span></th>
                    <td><?php echo $user['company_name'];  echo $this->Form->hidden('company_name');  ?></td>
                </tr>
                <tr>
                    <th>会社名<span class="fsmall">（契約者名）</span>フリガナ<span class="need">*</span></th>
                    <td><?php echo $user['company_kana'];   echo $this->Form->hidden('company_kana'); ?></td>
                </tr>

                
                
                
                <?php } ?>
                
                <tr>
                    <th>連絡用電話番号<span class="need">*</span></th>
                    <td><?php echo $this->Form->input('user_telno'      ,array('id' =>'user_telno'      ,'type'=>'text'  ,'placeholder' => '03-1234-5678'                         ,'class' => 'validate[required,custom[phone]]'                   ,'label' => false,'maxlength' => '13','div'=>false)); ?>（ハイフンあり）</td>
                </tr>
                <tr>
                    <th>業種<span class="need">*</span></th>
                    <td><?php echo $this->Form->input('industry'        ,array('id' =>'industry'        ,'type'=>'text'  ,'placeholder' => '通信業、建設業、小売りなど'             ,'class' => 'validate[required,maxlength[100]]'                  ,'label' => false)); ?></td>
                </tr>
                <tr>
                    <th>使用目的<span class="need">*</span></th>
                    <td><?php echo $this->Form->input('purpose'         ,array('id' =>'purpose'         ,'type'=>'text'  ,'placeholder' => '受付と清掃の兼務、保護者からの電話受付、配達先から代表電話発信など','class' => 'validate[required]','escape'=>false,'label' => false,'size'=>69 )); ?></td>
                </tr>               
<?php if($user['status'] < 3){ ?>
                <th>確認書類のアップロード<span class="need">*</span></th>
                <td>
                <p>いずれか1点をご提出ください。</p>
                <p>＜個人または、法人ご担当者様＞</p>
                <li>運転免許証(表面のみ）</li>
                <li>健康保険/国民健康保険 被保険者証</li>
                <li>個人番号カード 個人番号カード</li>
                <li>日本国パスポート 日本国パスポート</li>
                <p>※マイナンバー制度により送付される「通知カード」は本人確認書類としてはご利用いただけません。<br />
                その他の本人確認書類や、詳細については<u><a href="http://minderu.com/confirmation/" target="blank">こちら</a></u>をご覧ください。</p>
                <p>＜会社様は以下の書類もご利用できます＞</p>
                <li>履歴事項全部証明書</li>
                <li>登記簿謄(抄)本</li>
                <li>現在(履歴)事項証明書</li>
                <li>印鑑登録証明書</li>
                <p>※いずれも6ヶ月以内に発行されたもの</p>
                <p><br/></p>
                <p class="mb30">本人確認書類選択 (画像ファイル jpg,gif,bmp,png,pdf 各6MBまで)</p>
                <p>
                <?php 
                        if($user['confirmation_filename_1']){
                            echo "本人確認書類1:登録済み";
                            echo $this->Form->hidden('image_name1');
                        }else{
                            echo $this->Form->input('image_name1', array('id'=>'confirmation_filename_1','type' => 'file','title' => '画像選択', 'multiple','class' => 'validate[required,checkFileType[jpg|JPG|jpeg|gif|GIF|bmp|BMP|png|PNG|pdf|PDF],checkFileSize[2097152]]','label' => false)); 
                        } 
                        echo $this->Form->hidden('confirmation_filename_1');
                        if($user['confirmation_filename_2']){
                            echo "本人確認書類2:登録済み";
                            echo $this->Form->hidden('image_name2');
                        }else{
                            echo "<div >"; // 非表示の場合はclass='hide'を付加
                            echo $this->Form->input('image_name2', array('id'=>'confirmation_filename_2','type' => 'file','title' => '画像選択', 'multiple','class' => 'validate[checkFileType[jpg|JPG|jpeg|gif|bmp|png|pdf],checkFileSize[2097152]]','label' => false)); 
                            echo "</div>";
                        } 
                        echo $this->Form->hidden('confirmation_filename_2');
                ?>
                <br />
                </p>
                <p><!--※2枚目の画像添付が必要な場合は、送信用のURLをお送りします。--><br>※本人確認書類の偽造・加工修正は法令違反です。<br>偽造や加工修正が確認された場合、ご契約をお断りさせていただきます。</p>
                </td>
                </tr>
<?php } ?>
            </table>
        </div>
        
        <div class="submitbtn registration">
            <?php echo $this->Form->end($bstring); ?>
        </div>
        
        

    </div><!--/inner-->
</div><!--/company-->


<script>
jQuery(function($){
    
    $('#zipcode').jpostal({
             postcode : [
                 '#zip'
             ],
             address : {
                 '#prefecture' : '%3',
                 '#address'     : '%4%5',
             }
    });
    
    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
    
  // $("#UserImageName").on('change', afile_changeHandler);

         
});
</script>
<style>
#main.mypage table {
    text-align: left !important; 
}

</style>
