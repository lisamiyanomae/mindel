<?php 
$this->assign('title', preg_replace("/( |　)/", "", $title ));

echo $this->Html->script('jquery-2.1.0.min', array('inline' => false));
echo $this->Html->script('jquery.ui.datepicker-ja.min.js');
echo $this->Html->script('jquery-ui.min.js');
echo $this->Html->css('jquery-ui');
?>
<script>
  $(function() {
    $("#start_date").datepicker({dateFormat: 'yy-mm-dd'});
    $("#end_date").datepicker({dateFormat: 'yy-mm-dd'});
    
    $('.change_status').on('click', function() {
      var idx = $('.change_status').index(this);
      $('#status').val(idx);
      $('#submit').trigger('click');
      
    });
    
  });
</script>

<div id="main">
    
<div class="section inner">
    <h1><?php echo $title; ?></h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu<?php echo $icono ?>"><?php echo str_replace('_CNT_',count($members),$tstr) ?></div>
    
<?php if($status == 0){ ?>
    <div id="account_kensaku" class="table">
    <?php echo $this->Form->create('User',array('id'=>'varidate_form','type'=>'file','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
        <?php echo $this->Form->create('User'); ?>
        <table>
            <tr>
            
                <th>条件検索</th>
                <td class="accout_conditions">
                    <ul>
                        <li><p>ステイタス</p>
                            <?php echo $this->Form->input('status'         ,array('id' =>'status'         ,'type'=>'select'   ,'options'     => $user_status  ,'empty'=>'選択してください'    ,  'multiple'=> false    ,'class' => 'validate[required]','label' => false,'div'=>false)); ?>
                            <label for='didprint'>　　印刷　未 <?php echo $this->Form->input('didprint'    ,array('id' =>'didprint','type'=>'checkbox'  ,'label' => false,'div'=>false)); ?></label>
                            <label for='hand_payment'>　　振込払 <?php echo $this->Form->input('hand_payment'    ,array('id' =>'hand_payment','type'=>'checkbox'  ,'label' => false,'div'=>false)); ?></label>
                            
                            
                            <label for='no_payment'>　　非課金ユーザーを除く <?php echo $this->Form->input('no_payment'    ,array('id' =>'no_payment','type'=>'checkbox'  ,'label' => false,'div'=>false)); ?></label>
                            
                        </li>
                        <li><p>キーワード</p>
                            <?php  echo $this->Form->input('search_text'    ,array('id' =>'search_text'  ,'label' => false,'div'=>false)); ?>
                            
                        </li>
                        <li><p>登録日</p>
                            <?php echo $this->Form->input('start_date',array('id'=>'start_date','type'=>'text','div'=>false,'label'=>false));?>　～　<?php echo $this->Form->input('end_date',array('id'=>'end_date','type'=>'text','label' => '','div'=>false,'label'=>false));?>
                        </li>
                    </ul>
                </td>
                
                <td class="accout_search">
                    <?php echo $this->Form->submit('絞込',array('id' => 'submit','name' => 'submit','div'=>false));?>
                    <?php echo $this->Form->submit('解除',array('id' => 'reset' ,'name' => 'reset','div'=>false,'class'=>'button red')); ?>

                </td>
            </tr>
        </table>
    </div>
    
    <ul id="account_sortMenu">
        <li class="change_status">新規登録          </li>
        <li class="change_status">審査待ち          </li>
        <li class="change_status">再審査            </li>
        <li class="change_status">決済待ち          </li>
        <li class="change_status">Twilio番号発行待ち</li>
        <li class="change_status">サービス稼働中    </li><li class="change_status"></li>
        <li class="change_status">却下              </li>
        <li class="change_status">停止              </li>
        <li class="change_status">退会              </li>
    </ul>
<?php } ?>    
    
    <div class="table scroll">
        <table>
            <thead>
            
            <tr>
                <?php if($status == 0){ ?><th>ステイタス</th><?php } ?>
                <th>登録日時</th>
                <th>お客様No.</th>
                <th>店舗名または屋号</th>
                <th>都道府県</th>
                <th>発行番号</th>
                <th>支払方法</th>
                <th>印刷</th>
                
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($members as $member): ?>
            <tr>
                <?php if($status == 0){ ?><td><?php echo  $user_status[$member['User']['status']]; ?></td><?php } ?>
                <td><?php echo $member['User']['created']; ?></td>
                <td><?php echo USER_ID_HEDDER.sprintf('%06d',$member['User']['id']);?></td>
                <td><?php echo $member['User']['shop_name']; ?></td>
                <td><?php if($member['User']['prefecture']){echo $pref_list[$member['User']['prefecture']];} ?></td>
                <td><?php echo $member['User']['tropo_telno']; ?></td>
                <td><?php if($member['User']['status'] > 3){if($member['User']['hand_payment'] == 1){echo "振込";}else{echo "カード";}}  ?></td>
                <td><?php if($member['User']['didprint'] == 1){echo "済";}else{echo "未";}; ?></td>
                <td>
<?php 
if($member['User']['status'] == 1){
            print($this->Html->link('審査', 'examination/' . $member['User']['id'],array('class'=>'button blue')));
}elseif($member['User']['status'] == 4){
            print($this->Html->link('発行番号入力', 'tropo/' . $member['User']['id'],array('class'=>'button blue')));
}
            echo "&nbsp;";
if($status != 1 && $status != 4){
            print($this->Html->link('詳細', 'view/'.$member['User']['id'],array('class'=>'button blue'))); 
            echo "&nbsp;";
            print($this->Html->link('<i class="fas fa-yen-sign" title="請求履歴"></i>', '/admin/orders/index/'.$member['User']['id'],array('class'=>'button green','escape'=>false))); 
}


echo "&nbsp;";
            print($this->Html->link('<i class="fas fa-list-ul" title="利用明細"></i>', '/admin/telephones/detail/'.$member['User']['id'],array('class'=>'button gray','escape'=>false))); 

echo "&nbsp;";
            print($this->Html->link('<i class="fas fa-phone" title="通話履歴"></i>', '/admin/telephones/index/'.$member['User']['id'],array('class'=>'button gray','escape'=>false))); 

?>

            </td>
            </tr>
            <?php endforeach; ?>
            <?php unset($member); ?>

            </tbody>
        </table>
    </div>

    <div class="pagenate">
        <?php echo $this->Paginator->prev(  '< 前のページ',  null,  null,  array('class' => 'disabled')); ?>
        <?php echo $this->Paginator->numbers();?>
        <?php echo $this->Paginator->next(  '次のページ >',  null,  null,  array('class' => 'disabled'));?>
        <?php echo $this->Paginator->counter(); ?>
        /<?php echo $this->Paginator->counter(array('format' => __('  {:count}')));?>
    </div>

    <div class="backpage"><?php print($this->Html->link('戻 る', array('controller' => 'pages', 'action' => 'home','admin' => true),array('class'=>'button darkblue'))); ?></div>
    

</div>

</div><!--/End main-->
