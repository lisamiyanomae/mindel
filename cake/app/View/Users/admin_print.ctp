<?php 
$this->assign('title', '顧客詳細(印刷用)');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));

?>
<div id="main">
    
<div id="print" class="section inner">

    <div id="p_tbl1" class="table">
        <table>
            <tr>
                <th>お客様No.</th>
                <td><?php echo USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->data['User']['id']);?></td>
            </tr>
            <tr>
                <th>発行番号</th>
                <td><?php echo $this->data['User']['tropo_telno']; ?></td>
            </tr>
            <tr>
                <th>ステータス</th>
                <td><?php echo $user_status[$this->data['User']['status']]; ?></td>
            </tr>
        </table>
    </div>

    <div class="row">
        <div class="cont1of2">
            <h2>アカウント情報</h2>
                <div id="p_tbl2" class="table">
                    <table>
                        <table>
                        <tr>
                            <th>登録日時</th>
                            <td><?php echo date('Y-m-d',strtotime($this->data['User']['created'])); ?></td>
                        </tr>
                        <tr>
                            <th>承認日時</th>
                            <td><?php if($this->data['User']['status_3_date']){echo date('Y-m-d',strtotime($this->data['User']['status_3_date']));} ?><?php if($this->data['User']['status_3_admin_id']){echo '（'.$admin_list[$this->data['User']['status_3_admin_id']].')'; } ?></td>
                        </tr>
                        <tr>
                            <th>サービス開始</th>
                            <td><?php if($this->data['User']['status_5_date']){ echo date('Y-m-d',strtotime($this->data['User']['status_5_date']));} ?><?php if($this->data['User']['status_5_admin_id']){echo '（'. $admin_list[$this->data['User']['status_5_admin_id']] . ')'; } ?></td>
                        </tr>
                        <tr>
                            <th>サービス停止</th>
                            <td><?php if($this->data['User']['status_8_date']){echo date('Y-m-d',strtotime($this->data['User']['status_8_date']));} ?><?php if($this->data['User']['status_8_admin_id']){echo '（'.$admin_list[$this->data['User']['status_8_admin_id']].')'; } ?></td>
                        </tr>
                        <tr>
                            <th>退会日時</th>
                            <td><?php if($this->data['User']['status_9_date']){echo date('Y-m-d',strtotime($this->data['User']['status_9_date']));} ?><?php if($this->data['User']['status_9_admin_id']){echo '（'.$admin_list[$this->data['User']['status_9_admin_id']].')'; } ?></td>
                        </tr>
                        <tr>
                            <th>お客様No.</th>
                            <td><?php echo USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->data['User']['id']);?></td>
                        </tr>
                        <tr>
                            <th>店舗名または屋号</th>
                            <td><?php echo $this->data['User']['shop_name']; ?></td>
                        </tr>
                        <tr>
                            <th>担当者</th>
                            <td><?php echo $this->data['User']['responsible_name']; ?> <span class="furigana"><?php echo $this->data['User']['responsible_kana']; ?></span></td>
                        </tr>
                        <tr>
                            <th>メールアドレス</th>
                            <td><?php echo $this->data['User']['email']; ?></td>
                        </tr>
                    </table>
                </div>

            <!--/アカウント情報-->

            <h2>ユーザー情報</h2>
                <div id="p_tbl2" class="table">
                    <table>
                        <tr>
                            <th>会社名（契約者名）</th>
                            <td><?php echo $this->data['User']['company_name']; ?> <span class="furigana"><?php echo $this->data['User']['company_kana']; ?></span></td>
                        </tr>
                        <tr>
                            <th>郵便番号</th>
                            <td><?php echo $this->data['User']['zip']; ?></td>
                        </tr>
                        <tr>
                            <th>都道府県</th>
                            <td><?php if($this->data['User']['prefecture']){echo $pref_list[$this->data['User']['prefecture']];} ?></td>
                        </tr>
                        <tr>
                            <th>住所1</th>
                            <td><?php echo $this->data['User']['address']; ?></td>
                        </tr>
                        <tr>
                            <th>住所2</th>
                            <td><?php echo $this->data['User']['building']; ?></td>
                        </tr>
                        <tr>
                            <th>電話番号</th>
                            <td><?php echo $this->data['User']['user_telno']; ?></td>
                        </tr>
                        <tr>
                            <th>業種</th>
                            <td><?php echo $this->data['User']['industry']; ?></td>
                        </tr>
                        <tr>
                            <th>使用目的</th>
                            <td><?php echo $this->data['User']['purpose']; ?></td>
                        </tr>
                        <tr>
                            <th>本人確認資料</th>
                            <td>
                            <?php
                            if($this->data['User']['confirmation_filename_1']){
                                echo "<a href='".$this->Html->url("/images/") .CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_1']."' target='_blank'>";
                                echo "<img src='".$this->Html->url("/images/") .CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_1']."'>";
                                echo "</a>";
                            }
                            if($this->data['User']['confirmation_filename_2']){
                                echo "<a href='".$this->Html->url("/images/") .CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_2']."' target='_blank'>";
                                echo "<img src='".$this->Html->url("/images/") .CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_2']."'>";
                                echo "</a>";
                            }
                            if($this->data['User']['confirmation_filename_first_1']){
                                echo "(初回登録で再審査となった画像)<br>";
                                echo "<a href='".$this->Html->url("/images/") .CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_first_1']."' target='_blank'>";
                                echo "<img src='".$this->Html->url("/images/") .CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_first_1']."'>";
                                echo "</a>";
                            }

                            if($this->data['User']['confirmation_filename_first_2']){
                                echo "(初回登録で再審査となった画像)<br>";
                                echo "<a href='".$this->Html->url("/images/") .CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_first_2']."' target='_blank'>";
                                echo "<img src='".$this->Html->url("/images/") .CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_first_2']."'>";
                                echo "</a>";
                            }
                            ?>
                            </td>
                        </tr>
                    </table>
                </div>
            <!--/ユーザー情報-->

        </div>
        <div class="cont1of2">
            <h2>サービス内容</h2>
            <div id="serviceDetail">
                <div id="p_tbl2" class="table">
                    <table>
                        <tr>
                            <th>ご利用サービス名</th>
                            <td><?php echo SITE_NAME;?></td>
                        </tr>

                        <tr>
                            <th>ログインID</th>
                            <td><?php echo $this->data['User']['username']; ?></td>
                        </tr>
                        <tr>
                            <th>転送番号</th>
                            <td>
                                <ul>
                                <?php foreach($teltransfers AS $teltransfer){ ?>
                                    <li><?php echo $teltransfer['Teltransfer']['telno'];  ?><span><?php echo $teltransfer['Teltransfer']['memo'];  ?></span></li>
                                <?php } ?>
                                </ul>                                
                            </td>
                        </tr>
                        <tr>
                            <th>転送スケジュール</th>
                            <td>
                            <?php if($voicemail){ ?>
                                <ul>
                                    <li>月<span><?php if($voicemail['Voicemail']['teltransfer_day_01'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_01']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_01']));}else{echo "－";}  ?></span></li>
                                    <li>火<span><?php if($voicemail['Voicemail']['teltransfer_day_02'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_02']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_02']));}else{echo "－";}  ?></span></li>
                                    <li>水<span><?php if($voicemail['Voicemail']['teltransfer_day_03'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_03']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_03']));}else{echo "－";}  ?></span></li>
                                    <li>木<span><?php if($voicemail['Voicemail']['teltransfer_day_04'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_04']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_04']));}else{echo "－";}  ?></span></li>
                                    <li>金<span><?php if($voicemail['Voicemail']['teltransfer_day_05'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_05']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_05']));}else{echo "－";}  ?></span></li>
                                    <li>土<span><?php if($voicemail['Voicemail']['teltransfer_day_06'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_06']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_06']));}else{echo "－";}  ?></span></li>
                                    <li>日<span><?php if($voicemail['Voicemail']['teltransfer_day_00'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_00']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_00']));}else{echo "－";}  ?></span></li>
                                </ul>
                            <?php }else{ echo "<span class='notice'>設定されていません</span>";} ?>
                            </td>
                        </tr>
                        <tr>
                            <th>時間外メッセージ</th>
                            <td><?php if($voicemail){ ?>
                            <?php echo $voicemail['Voicemail']['message']; ?></td>
                            <?php }else{ echo "<span class='notice'>設定されていません</span>";} ?>
                        </tr>
                        <tr>
                            <th>発行番号</th>
                            <td><?php echo $this->data['User']['tropo_telno']; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--/サービス内容-->


            <h2>顧客メモ</h2>
            <div class="box border">
                <p><?php echo $this->data['User']['memo']; ?></p>
            </div>
            <!--/顧客メモ-->        
            
            <br class="clear">
            
            <div class="cont_right"><a class="button darkblue" href="" onclick="window.print(); return false;">印刷する</a></div>

        </div>
    </div>

    <div class="backpage"><?php print($this->Html->link('戻 る', array('controller' => 'users', 'action' => 'view/'.$this->data['User']['id'],'admin' => true),array('class'=>'button darkblue'))); ?></div>
    </div>

</div>

</div><!--/End main-->

