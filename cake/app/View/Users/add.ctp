<?php 
$this->assign('title', 'サービス申込み');
$this->Html->addCrumb('サービス申込み');

$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));
?>

<div class="section">
<div class="inner">
<h2>サービス申込み</h2>
<p class="lead">みんなにでんわ転送へのお申込みは、以下の項目に入力して「アカウント作成」ボタンを押してください。次のステップへのご案内メールが届きます。</p>
<p class="lead">※※「auひかり電話」からの転送は、au専用050番号を使用しますのでアカウント作成に合わせて　<a href="mailto:support@minderu.com?subject=auひかり転送から使用希望&amp;body=お名前または屋号をご記入ください：">support@minderu.com</a> まで「auひかりの転送を使用」とご連絡ください※※
</p>
<br>


<div role="form" class="wpcf7" id="wpcf7-f368-p366-o1" lang="ja" dir="ltr">
<div class="screen-reader-response"></div>

<div class="center"><?php echo $this->Flash->render(); ?></div>

<?php echo $this->Form->create('User',array('id'=>'varidate_form','type'=>'file','inputDefaults' => array(),'novalidate' => true,'class'=>'wpcf7-form primary-form')); ?>
<div id="form">

<div class="table">
<table class="sp_border">

<tr>
<th>アカウント名（店舗名）*<span></span></th>
<td><span class="wpcf7-form-control-wrap your-name"><?php echo $this->Form->input('shop_name'       ,array('id' =>'shop_name'       ,'type'=>'text'     ,"size"=>"40"   ,'placeholder' => '〇〇株式会社△△支店'   ,'class' => 'validate[required,maxSize[100]]'                                               , 'label'=>false , 'div'=>false )); ?></span></td>
</tr>                          
<!--                                                                                                                     
<tr>                                                                                                                                                  
<th>会社名</th>                                                                                                                                       
<td><span class="wpcf7-form-control-wrap your-name"><?php //echo $this->Form->input('company_name'    ,array('id' =>'company_name'    ,'type'=>'text'     ,"size"=>"40"   ,'placeholder' => '（株）モバイル通信' ,'class' => 'validate[maxSize[100]]'                                               , 'label'=>false , 'div'=>false )); ?></span></td>
</tr>                                                                                                                                                 
<tr>                                                                                                                                                  
<th>カイシャメイ</th>                                                                                                                                 
<td><span class="wpcf7-form-control-wrap your-name"><?php //echo $this->Form->input('company_kana'    ,array('id' =>'company_kana'    ,'type'=>'text'     ,"size"=>"40"  ,'placeholder' => 'カ）モバイルツウシン','class' => 'validate[maxSize[100],custom[katakana]]'                              , 'label'=>false , 'div'=>false)); ?></span></td>
</tr>
-->                                                                                                                                                 
<tr>                                                                                                                                                  
<th>ご担当者名（フルネーム）*</th>                                                                                                                    
<td><span class="wpcf7-form-control-wrap your-name"><?php echo $this->Form->input('responsible_name',array('id' =>'responsible_name','type'=>'text'     ,"size"=>"40"  ,'placeholder' => '山田　太郎'          ,'class' => 'validate[required,maxSize[100]]'                                      , 'label'=>false, 'div'=>false)); ?></span></td>
</tr>                                                                                                                                                 
<tr>                                                                                                                                                  
<th>ご担当者フリガナ*</th>                                                                                                                          
<td><span class="wpcf7-form-control-wrap your-name"><?php echo $this->Form->input('responsible_kana',array('id' =>'responsible_kana','type'=>'text'     ,"size"=>"40"  ,'placeholder' => 'ヤマダ　タロウ'      ,'class' => 'validate[required,maxSize[100],custom[katakana]]'                     , 'label'=>false, 'div'=>false)); ?></span></td>
</tr>                                                                                                                                                 
<tr>                                                                                                                                                  
<th>メールアドレス *</th>                                                                                                                             
<td><span class="wpcf7-form-control-wrap your-email"><?php echo $this->Form->input('email'          ,array('id' =>'email'           ,'type'=>'text'     ,"size"=>"40"   ,'placeholder' => 'example@minderu.com' ,'class' => 'validate[required,custom[email]]'                                     , 'label'=>false, 'div'=>false )); ?></span></td>
</tr>                                                                                                                                                 
<tr>                                                                                                                                                  
<th>ログインID * <span>(記号は使えません)</span></th>                                                                                                                                 
<td><span class="wpcf7-form-control-wrap your-name"><?php echo $this->Form->input('username'        ,array('id' =>'username'        ,'type'=>'text'     ,"size"=>"40"  ,'placeholder' => '6文字以上英数'       ,'class' => 'validate[required,custom[onlyLetterNumber]]'      ,'maxlength' => '50', 'label'=>false, 'div'=>false)); ?></span></td>
</tr>                                                                                                                                                 
<tr>
<th>パスワード * <span>(4文字以上20文字以内英数記号）</span></th>
<td><span class="wpcf7-form-control-wrap your-name"><?php echo $this->Form->input('password'        ,array('id' =>'password'        ,'type'=>'password' ,"size"=>"40"  ,'placeholder' => ''                    ,'class' => 'validate[required,custom[onlyLetterNumber],minSize[4],maxSize[20]]'      ,'maxlength' => '50', 'label'=>false, 'div'=>false)); ?></span></td>
</tr>
</table>
</div>
<p class="mb30">※サービス利用開始には、本人確認書類の複写が必要です。<br/></p>


<div class="submitbtn">
<?php echo $this->Form->end('アカウントを作成する',array("class"=>"wpcf7-form-control wpcf7-submit")); ?>

</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>

</div><!--/inner-->
</div><!--/company-->



<script>
jQuery(function($){
    
    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });

});
</script>




