<?php 
$this->assign('title', 'ご契約内容');
//$this->Html->addCrumb('お客様情報');
?>

<div id="main" class="mypage">
    
<div class="section">
    <div class="inner">
    
        <h2>ご契約内容</h2>
        <?php if($user['status'] > 4 ){  ?>
        <p class="mb30">お客様のアカウントは以下の通り登録されています。</p>
        <div class="center tyuui"><?php echo $this->Flash->render(); ?></div>
        <?php }else{ ?>
        <p class="mb30">登録情報が以下の通り登録されています。</p>
        <?php }?>
        
        <div id="account_detail" class="row">
            <div class="cont1of2">
                <h3>アカウント情報</h3>
                <div class="table">
                    <table class="sp_border mb30">
                        <tr>
                        <th>ご利用サービス名</th>
                        <td><?php echo SITE_NAME;?></td>
                        </tr>
                        <tr>
                        <th>お客様番号</th>
                        <td><?php echo USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$user['id']);?></td>
                        </tr>
                        <tr>
                        <th>店舗名または屋号<br /><span class="fsmall">（マイページに表示されます）</span></th>
                        <td><?php echo $user['shop_name'];?></td>
                        </tr>
                        <tr>
                        <th>会社名<span class="fsmall">（契約者名）</span></th>
                        <td><?php echo $user['company_name'];?></td>
                        </tr>
                        <tr>
                        <th>会社名<span class="fsmall">（契約者名）</span>フリガナ</th>
                        <td><?php echo $user['company_kana'];?></td>
                        </tr>
                        <tr>
                        <th>ご担当者名（フルネーム）</th>
                        <td><?php echo $user['responsible_name'];?></td>
                        </tr>
                        <tr>
                        <th>ご担当者名フリガナ</th>
                        <td><?php echo $user['responsible_kana'];?></td>
                        </tr>
                        <tr>
                        <th>メールアドレス</th>
                        <td><?php echo $user['email'];?></td>
                        </tr>
                    </table>
                </div>            
            </div>
            <div class="cont1of2">
                <h3>ユーザー情報</h3>
                <div class="table">
                    <table class="sp_border mb30">
                        <tr>
                        <th>発行番号</th>
                        <td><?php echo $user['tropo_telno'];?></td>
                        </tr>
                        <tr>
                        <th>ログインID</th>
                        <td><?php echo $user['username'];?></td>
                        </tr>
                        <tr>
                        <th>パスワード</th>
                        <td><u><?php echo $this->Html->link('(パスワードを忘れた場合)', '/forgot_password', array('class' => '')); ?></u></td>
                        </tr>
                        <tr>
                        <th>郵便番号</th>
                        <td><?php echo $user['zip'];?></td>
                        </tr>
                        <tr>
                        <th>住所1</th>
                        <td><?php echo $user['address'];?></td>
                        </tr>
                        <tr>
                        <th>住所2</th>
                        <td><?php echo $user['building'];?></td>
                        </tr>
                        <tr>
                        <th>ご連絡用電話番号</th>
                        <td><?php echo $user['user_telno'];?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        
        <?php if($user['status'] != 1 && $user['status'] != 3 && $user['status'] != 4 ){  ?>
        <div class="registration"><a class="button darkblue" href="edit">変更</a></div>
        <div class="backpage mt20"><?php print($this->Html->link('戻る', array('controller' => 'telephones', 'action' => 'index','admin' => false),array('class'=>'button darkblue'))); ?></div>
        
        <?php } ?>
        
		

    </div><!--/inner-->
</div><!--/-->


</div><!--/End main-->



