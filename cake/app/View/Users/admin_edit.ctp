<?php
$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.jpostal', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));
?>

<?php 
$this->Html->addCrumb('顧客詳細・編集');

?>
<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <?php 

?>
        <?php echo $this->Form->input('status'           ,array('id' =>'status'         ,'type'=>'select'   ,'options'     => $user_status  ,'empty'=>false          ,'class' => 'validate[required]','label' => 'ステイタス')); ?>
        
        <?php echo $this->Form->input('shop_name',       array('id' =>'shop_name'       ,'type'=>'text'     ,'placeholder' => 'みんでる通販'                         ,'class' => 'validate[required,maxSize[100]]','label' => '店舗名')); ?>
        <?php echo $this->Form->input('company_name',    array('id' =>'company_name'    ,'type'=>'text'     ,'placeholder' => '(株)転送紹介'                         ,'class' => 'validate[maxSize[100]]'         ,'label' => '会社名または屋号')); ?>
        <?php echo $this->Form->input('company_kana',    array('id' =>'company_kana'    ,'type'=>'text'     ,'placeholder' => 'カブシキガイシャ　テンソウショウカイ' ,'class' => 'validate[maxSize[100]]'         ,'label' => '会社名または屋号カナ')); ?>
        <?php echo $this->Form->input('responsible_name',array('id' =>'responsible_name','type'=>'text'     ,'placeholder' => '山田　太郎'                           ,'class' => 'validate[required,maxSize[100]]','label' => '代表者/担当者')); ?>
        <?php echo $this->Form->input('responsible_kana',array('id' =>'responsible_kana','type'=>'text'     ,'placeholder' => 'ヤマダ　タロウ'                       ,'class' => 'validate[required,maxSize[100]]','label' => '代表者/担当者名カナ')); ?>
        <?php echo $this->Form->input('email'           ,array('id' =>'email'           ,'type'=>'text'     ,'placeholder' => 'taro@mindel.com'                      ,'class' => 'validate[custom[email]]'          ,'label' => 'メールアドレス')); ?>
        <?php echo $this->Form->input('zip'             ,array('id' =>'zip'             ,'type'=>'text'     ,'placeholder' => '012-0102'                             ,'class' => 'validate[custom[zip]]'          ,'label' => '郵便番号','maxlength' => '8')); ?>
        <?php echo $this->Form->input('prefecture'      ,array('id' =>'prefecture'      ,'type'=>'select'   ,'options'     => $pref_list  ,'empty'=>'都道府県','class' => 'validate[required]','label' => '都道府県')); ?>
        <?php echo $this->Form->input('address'         ,array('id' =>'address'         ,'type'=>'text'     ,'placeholder' => '札幌市中央区'                         ,'class' => 'validate[required,maxSize[100]]','label' => '住所')); ?>
        <?php echo $this->Form->input('building'        ,array('id' =>'building'        ,'type'=>'text'     ,'placeholder' => '建物'                                 ,'class' => '','label' => '建物')); ?>
        <?php echo $this->Form->input('user_telno'      ,array('id' =>'user_telno'      ,'type'=>'text'     ,'placeholder' => '03-1234-5678'                         ,'class' => 'validate[required,custom[phone]]','label' => 'ご連絡先Tel','maxlength' => '13')); ?>
        <?php //echo $this->Form->input('purpose_id'      ,array('id' =>'purpose_id'      ,'type'=>'select'   ,'options'     => $purpose_list  ,'empty'=>'選択してください'       ,'class' => ''                                 ,'label' => '利用目的')); ?>
        <?php echo $this->Form->input('purpose'         ,array('id' =>'purpose'         ,'type'=>'textarea' ,'placeholder' => '（例）&#13;&#10;ああああ&#13;&#10;いいいい&#13;&#10;うううう&#13;&#10;えええ','class' => 'validate[required]','escape'=>false,'label' => '利用目的')); ?>


        <?php echo $this->Form->input('username'        ,array('id' =>'username'        ,'type'=>'text'     ,'placeholder' => 'mindel001'                              ,'class' => 'validate[required,custom[onlyLetterNumber]]','maxlength' => '50','label' => 'ユーザーID')); ?>
		<?php echo $this->Form->input('message'         ,array('id' =>'message'         ,'type'=>'textarea' ,'default' => DEFAULT_MESSAGE,'escape'=>false,'label' => '応答メッセージ')); ?>

            <?php 

echo $this->Form->input('message_from',array('type'=>'text' ,'label' => '留守電対応時間'));
echo $this->Form->input('message_to',array('type'=>'text' ,'label' => '～'));


echo $this->Form->input('tropo_apikey',array('label' => 'TROPOAPIキー'));
echo $this->Form->input('tropo_telno',array('label' => '代表番号'));


echo "<br><br>";


echo $this->Form->input('memo',array('label' => '顧客メモ'));


echo $this->Form->end($bstring);


     

echo "<br>本人確認書類:".$this->data['User']['confirmation_filename']."<br>";
        if($this->data['User']['confirmation_filename']){
echo $this->Html->image(CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename'],array('height'=>120));
}else{
echo "登録されていません";
}

if($this->data['User']['confirmation_filename_1']){
echo "<br>本人確認書類(再審査):".$this->data['User']['confirmation_filename_1']."<br>";
echo $this->Html->image(CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_1'],array('height'=>120));

}

echo "<br>サービス開始日時:".$this->data['User']['start_date'];
echo "<br>初回決済日時:"    .$this->data['User']['first_settlement_date'];
echo "<br>退会日時:"        .$this->data['User']['leaving_date'];

    ?>
    
    </fieldset>
    
<span class="button"><?php print($this->Html->link('戻る', array('controller' => 'users', 'action' => 'index','admin' => true))); ?></span>
</div>

<script>
jQuery(function($){
    
    $('#zipcode').jpostal({
             postcode : [
                 '#zip'
             ],
             address : {
                 '#prefecture' : '%3',
                 '#address'     : '%4%5',
             }
    });
    
    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
    
   $("#UserImageName").on('change', afile_changeHandler);

         
});
</script>
