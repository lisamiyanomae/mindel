<?php 
$this->assign('title', '顧客詳細');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));

?>
<div id="main">
    
<div class="section inner">


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


    <h1>アカウント（詳細）</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="customerinfo" class="table">
        <table>
            <tr>
                <th>お客様No.</th>
                <th>店舗名または屋号</th>
                <th>発行番号</th>
                <th>メールアドレス</th>
                <th>ステイタス</th>
            </tr>
            <tr>
                <td><?php echo USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->data['User']['id']);?></td>
                <td><?php echo $this->data['User']['shop_name']; ?></td>
                <td><?php echo $this->data['User']['tropo_telno']; if($admin['role'] == 1 && !$this->data['User']['tropo_telno']){print($this->Html->link('(番号発行する)', array('controller' => 'users', 'action' => 'tropo/'.$this->data['User']['id'],'admin' => true)));} ?></td>
                <td><?php echo $this->data['User']['email']; ?></td>
                <td><?php echo $user_status[$this->data['User']['status']]; ?></td>
            </tr>
        </table>
    </div>
    

    <div class="row">
        <div class="cont1of2">
            <h2>アカウント情報</h2>
            <div class="box border">
                <div class="table">
                    <table>
                        <tr>
                            <th>登録日時</th>
                            <td><?php echo date('Y-m-d',strtotime($this->data['User']['created'])); ?></td>
                        </tr>
                        <tr>
                            <th>承認日時</th>
                            <td><?php if($this->data['User']['status_3_date']){echo date('Y-m-d',strtotime($this->data['User']['status_3_date']));} ?><?php if($this->data['User']['status_3_admin_id']){echo '（'.$admin_list[$this->data['User']['status_3_admin_id']].')'; } ?></td>
                        </tr>
                        <tr>
                            <th>サービス開始</th>
                            <td><?php if($this->data['User']['status_5_date']){ echo date('Y-m-d',strtotime($this->data['User']['status_5_date']));} ?><?php if($this->data['User']['status_5_admin_id']){echo '（'. $admin_list[$this->data['User']['status_5_admin_id']] . ')'; } ?></td>
                        </tr>
                        <tr>
                            <th>サービス停止</th>
                            <td><?php if($this->data['User']['status_8_date']){echo date('Y-m-d',strtotime($this->data['User']['status_8_date']));} ?><?php if($this->data['User']['status_8_admin_id']){echo '（'.$admin_list[$this->data['User']['status_8_admin_id']].')'; } ?></td>
                        </tr>
                        <tr>
                            <th>退会日時</th>
                            <td><?php if($this->data['User']['status_9_date']){echo date('Y-m-d',strtotime($this->data['User']['status_9_date']));} ?><?php if($this->data['User']['status_9_admin_id']){echo '（'.$admin_list[$this->data['User']['status_9_admin_id']].')'; } ?></td>
                        </tr>
                        <tr>
                            <th>お客様No.</th>
                            <td><?php echo USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->data['User']['id']);?></td>
                        </tr>
                        <tr>
                            <th>店舗名または屋号</th>
                            <td><?php echo $this->data['User']['shop_name']; ?></td>
                        </tr>
                        <tr>
                            <th>担当者</th>
                            <td><?php echo $this->data['User']['responsible_name']; ?> <span class="furigana"><?php echo $this->data['User']['responsible_kana']; ?></span></td>
                        </tr>
                        <tr>
                            <th>メールアドレス</th>
                            <td><?php echo $this->data['User']['email']; ?></td>
                        </tr>
                        <tr>
                            <th>支払い方法</th>
                            <td><?php if($this->data['User']['status'] > 3){if($this->data['User']['hand_payment'] == 0){echo "クレジットカード"; }else{ echo "振込";}} ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--/アカウント情報-->
            <?php echo $this->Form->create('User',array('id'=>'varidate_form','type'=>'file','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
            
            <h2>ユーザー情報   <i class="fas fa-pen edit_icon"></i></h2>
            <div class="box border">
                <div class="table">
                    <table>
                        <tr>
                            <th>会社名（契約者名）<br>フリガナ</th>
                            <td>
                            
                            <?php echo $this->Form->input('User.company_name'  ,array('id' =>'company_name' ,'label' => false ,'class'=>'edit_area'));?>
                            <?php echo $this->Form->input('User.company_kana'  ,array('id' =>'company_kana' ,'label' => false ,'class'=>'edit_area'));?>
                            <p class="default_area">
                            <?php echo $this->data['User']['company_name']; ?> <span class="furigana"><?php echo $this->data['User']['company_kana']; ?></span>
                            </p>
                            </td>
                        </tr>
                        <tr>
                            <th>郵便番号</th>
                            <td><?php echo $this->data['User']['zip']; ?></td>
                        </tr>
                        
                        <tr>
                            <th>都道府県</th>
                            <td><?php if($this->data['User']['prefecture']){echo $pref_list[$this->data['User']['prefecture']];} ?></td>
                        </tr>
                        
                        
                        <tr>
                            <th>住所1</th>
                            <td><?php echo $this->data['User']['address']; ?></td>
                        </tr>
                        <tr>
                            <th>住所2</th>
                            <td><?php echo $this->data['User']['building']; ?></td>
                        </tr>
                        <tr>
                            <th>電話番号</th>
                            <td><?php echo $this->data['User']['user_telno']; ?></td>
                        </tr>
                        <tr>
                            <th>業種</th>
                            <td><?php echo $this->data['User']['industry']; ?></td>
                        </tr>
                        <tr>
                            <th>使用目的</th>
                            <td><?php echo $this->data['User']['purpose']; ?></td>
                        </tr>
                        <tr>
                            <th>本人確認資料</th>
                            <td>
                            <?php
                            if($this->data['User']['confirmation_filename_1']){
                                if(substr($this->data['User']['confirmation_filename_1'], strrpos($this->data['User']['confirmation_filename_1'], '.') + 1) != 'pdf'){
                                    $confirmation_filename_1 = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_1']);
                                    echo "<img src='data:image/jpg;base64,".base64_encode($confirmation_filename_1) . "' width='200'>";
                                    echo $this->Html->link("<i class='fas fa-search-plus fa-2x'></i>",array('action'=>'admin_imgdisp',$this->data['User']['id'],'confirmation_filename_1'),array('target' => '_blank','escape'=>false));
                                }else{
                                    echo $this->Html->link("<i class='fas fa-file-pdf fa-4x'></i>",array('action'=>'pdfdisp',$this->data['User']['id'],'confirmation_filename_1'),array('target' => '_blank','escape'=>false));
                                }
                            }
                            if($this->data['User']['confirmation_filename_2']){
                                if(substr($this->data['User']['confirmation_filename_2'], strrpos($this->data['User']['confirmation_filename_2'], '.') + 1) != 'pdf'){
                                    $confirmation_filename_2 = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_2']);
                                    echo "<img src='data:image/jpg;base64,".base64_encode($confirmation_filename_2) . "' width='200'>";
                                    echo $this->Html->link("<i class='fas fa-search-plus  fa-2x'></i>",array('action'=>'admin_imgdisp',$this->data['User']['id'],'confirmation_filename_2'),array('target' => '_blank','escape'=>false));
                                }else{
                                    echo $this->Html->link("<i class='fas fa-file-pdf fa-4x'></i>",array('action'=>'pdfdisp',$this->data['User']['id'],'confirmation_filename_2'),array('target' => '_blank','escape'=>false));
                                }
                            }
                            if($this->data['User']['confirmation_filename_first_1']){
                                echo "(初回登録で再審査となった画像)<br>";
                                if(substr($this->data['User']['confirmation_filename_first_1'], strrpos($this->data['User']['confirmation_filename_first_1'], '.') + 1) != 'pdf'){
                                    $confirmation_filename_first_1 = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_first_1']);
                                    echo "<img src='data:image/jpg;base64,".base64_encode($confirmation_filename_first_1) . "' width='200'>";
                                    echo $this->Html->link("<i class='fas fa-search-plus  fa-2x'></i>",array('action'=>'admin_imgdisp',$this->data['User']['id'],'confirmation_filename_first_1'),array('target' => '_blank','escape'=>false));
                                }else{
                                    echo $this->Html->link("<i class='fas fa-file-pdf fa-4x'></i>",array('action'=>'pdfdisp',$this->data['User']['id'],'confirmation_filename_first_1'),array('target' => '_blank','escape'=>false));
                                }
                            }

                            if($this->data['User']['confirmation_filename_first_2']){
                                echo "(初回登録で再審査となった画像)<br>";
                                if(substr($this->data['User']['confirmation_filename_first_2'], strrpos($this->data['User']['confirmation_filename_first_2'], '.') + 1) != 'pdf'){
                                    $confirmation_filename_first_2 = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_first_2']);
                                    echo "<img src='data:image/jpg;base64,".base64_encode($confirmation_filename_first_2) . "' width='200'>";
                                    echo $this->Html->link("<i class='fas fa-search-plus  fa-2x'></i>",array('action'=>'admin_imgdisp',$this->data['User']['id'],'confirmation_filename_first_2'),array('target' => '_blank','escape'=>false));
                                }else{
                                    echo $this->Html->link("<i class='fas fa-file-pdf fa-4x'></i>",array('action'=>'pdfdisp',$this->data['User']['id'],'confirmation_filename_first_2'),array('target' => '_blank','escape'=>false));
                                }
                            }
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <th>着信履歴の適用</th>
                            <td>
                            <?php echo $this->Form->text('User.formally_start_date'  ,array('id' =>'formally_start_date','type'=>'date','class'=>'edit_area'));?> 
                            <p class="default_area">
                            <?php if($this->data['User']['formally_start_date']){echo $this->data['User']['formally_start_date'];}else{echo "設定なし";} ?></span>
                           ～ </p>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
            <!--/ユーザー情報-->
            <!--div class="cont_right"><a class="button darkblue" href="">このアカウントのマイページへ</a></div-->
            
        </div>
        <div class="cont1of2">
            <h2>サービス内容</h2>
            <div id="serviceDetail" class="box border">
                <div class="table">
                    <table>
                        <tr>
                            <th>ご利用サービス名</th>
                            <td><?php echo SITE_NAME;?></td>
                        </tr>

                        <tr>
                            <th>ログインID</th>
                            <td><?php echo $this->data['User']['username']; ?></td>
                        </tr>
                        <tr>
                            <th>転送番号</th>
                            <td>
                                <ul>
                                <?php foreach($teltransfers AS $teltransfer){ ?>
                                    <li><?php echo $teltransfer['Teltransfer']['telno'];  ?><span><?php echo $teltransfer['Teltransfer']['memo'];  ?></span></li>
                                <?php } ?>
                                </ul>                                
                            </td>
                        </tr>
<?php if($voicemail){ ?>
                        <tr>
                            <th>転送スケジュール</th>
                            <td>
                                <ul>
                                    <li>月<span><?php if($voicemail['Voicemail']['teltransfer_day_01'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_01']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_01']));}else{echo "－";}  ?></span></li>
                                    <li>火<span><?php if($voicemail['Voicemail']['teltransfer_day_02'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_02']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_02']));}else{echo "－";}  ?></span></li>
                                    <li>水<span><?php if($voicemail['Voicemail']['teltransfer_day_03'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_03']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_03']));}else{echo "－";}  ?></span></li>
                                    <li>木<span><?php if($voicemail['Voicemail']['teltransfer_day_04'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_04']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_04']));}else{echo "－";}  ?></span></li>
                                    <li>金<span><?php if($voicemail['Voicemail']['teltransfer_day_05'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_05']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_05']));}else{echo "－";}  ?></span></li>
                                    <li>土<span><?php if($voicemail['Voicemail']['teltransfer_day_06'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_06']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_06']));}else{echo "－";}  ?></span></li>
                                    <li>日<span><?php if($voicemail['Voicemail']['teltransfer_day_00'] == 1){echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_from_00']));  ?> ～ <?php echo date('H : s',strtotime($voicemail['Voicemail']['teltransfer_to_00']));}else{echo "－";}  ?></span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th>時間外メッセージ <?php if($voicemail['Voicemail']['autovoice_mail_use1'] == 1){echo "[メール送信する]";}else{echo "[メール送信しない]";}?></th>
                            <td><?php echo $voicemail['Voicemail']['message']; ?></td>
                        </tr>
                        <tr>
                            <th>未応答メッセージ <?php if($voicemail['Voicemail']['timeout_seconds'] > 0){echo "(".$voicemail['Voicemail']['timeout_seconds']."秒後)";} if($voicemail['Voicemail']['autovoice_mail_use2'] == 1){echo "[メール送信する] ";}else{echo "[メール送信しない]";}  ?></th>
                            <td><?php if($voicemail['Voicemail']['timeout_seconds'] == 0){echo "OFF";}else{echo $voicemail['Voicemail']['timeout_message'];} ?></td>
                        </tr>
                        <tr>
                            <th>留守電使用</th>
                            <td><?php if($voicemail['Voicemail']['record_use'] == 1){echo "ON";}else{echo "OFF";} ?></td>
                        </tr>
                        <tr>
                            <th>自動応答通知メールアドレス</th>
                            <td><?php echo $voicemail['Voicemail']['autovoice_mail']; ?></td>
                        </tr>
<?php }else{ echo "<span class='notice'>* 転送スケジュールはまだ設定されていません</span>";} ?>

                        <tr>
                            <th>発行番号</th>
                            <td><?php echo $this->data['User']['tropo_telno']; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--/サービス内容-->


            <h2>顧客メモ</h2>
            <?php //echo $this->Form->create('User',array('id'=>'varidate_form','type'=>'file','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
            <div class="box border">
                <?php echo $this->Form->textarea('memo'  ,array('id' =>'memo' ,  'placeholder'=>'［前回不承認理由]' ));?>

            </div>
            <!--/顧客メモ-->
            
            
            
            <!-- div class="cont_right"><a class="button darkblue" href="">宛名印刷</a></div-->
            <div class="cont_right selectbox">
                <?php print($this->Html->link('帳票印刷', array('controller' => 'users', 'action' => 'print/'.$this->data['User']['id'],'admin' => true),array('class'=>'button darkblue'))); ?>
                <?php echo $this->Form->input('didprint'  ,array('id' =>'didprint'  ,'label' => '帳票印刷済','div'=>false)); ?> <?php echo $this->data['User']['print_datetime'] ?>
            </div>
            
            
            <br class="clear">
            
            <div class="cont_right"><?php echo $this->Form->submit('更　新', array('div'=>false,'class' => 'button darkblue','name' => 'button_update')); ?></div>
            <div class="cont_right selectbox">
                <?php echo $this->Form->input('status'  ,array('id' =>'status' ,'type'=>'select' ,'options' => $user_status  ,'empty'=>false  ,'class' => 'validate[required]','label' => false)); ?>
            </div>
            <div class="cont_right selectbox">
            <?php echo $this->Form->input('admin_id'  ,array('id' =>'admin_id'  ,'type'=>'select','options' => $admin_list  ,'empty'=>'担 当 者'  ,'default' => $admin['id'])); ?>
            </div>
            
            <h2>その他（通常は操作しません）</h2>
            <div class="box border">
            <p>
            <?php echo $this->Form->input('hand_payment'      ,array('id' =>'hand_payment'  ,'label' => '振込払&nbsp;','div'=>false)); ?> 
            
            <?php 
            if(count($children) > 0){
                echo "<p>以下のユーザーのポイント払い(親)ユーザーに設定されています</p>";
                foreach($children as $key => $val){
                    //echo "・".$val.' ('.USER_ID_HEDDER.sprintf('%06d',$key).')<br>';
                    echo "・".$val. ' ('. $this->Html->link(' '.USER_ID_HEDDER.sprintf('%06d',$key).' ',array( 'controller'=>'users','action'=>'view',$key),array('escape' => false,'class'=>'','title'=>'顧客詳細へ')).')<br>';
                }
            }else{
                echo "<br>ポイント払い(親)ユーザー : ";
                if($this->data['User']['parent_id']){echo $this->Html->link($this->data['Parent']['shop_name'].' ('.USER_ID_HEDDER.sprintf('%06d',$this->data['User']['parent_id']).') ',array( 'controller'=>'users','action'=>'view',$this->data['User']['parent_id']),array('escape' => false,'class'=>'','title'=>'顧客詳細へ')); }else{echo "-";};
                echo '<u>'.$this->Html->link('<br>設定または変更',array( 'controller'=>'users','action'=>'parent/'.$this->data['User']['id']),array('escape' => false,'class'=>'')).'</u>';
            }
            ?>
            </p>
            </div>
            
            <div class="cont_right selectbox">
            <u><?php print($this->Html->link('番号発行（詳細）へ', array('controller' => 'users', 'action' => 'tropo/'.$this->data['User']['id'],'admin' => true),array('class'=>'mt10'))); ?></u>
            </div>

            
            <?php if($admin['role'] == 1){ ?>
            <br class="clear">
            <hr>
            <p class="mb10">これ以降は管理アカウントのみ表示</p>
            <?php echo $this->Form->input('delflg'      ,array('id' =>'delflg'      ,'label' => '削除(非表示)&nbsp;','div'=>false)); ?> 
            <?php echo $this->Form->input('no_payment'  ,array('id' =>'no_payment'  ,'label' => '課金しない&nbsp;','div'=>false)); ?> 
            <?php echo $this->Form->submit('登録', array('div'=>false,'class' => 'button darkblue','name' => 'button_no_payment')); ?>
            
            <?php } ?>


            <br class="clear">
<!--
            <div class="cont_right"><input type="checkbox" name="" value="1" checked="checked"> ハガキ送付</div>
            <div class="cont_right selectbox"><input type="checkbox" name="" value="1" checked="checked"> ハガキ送付</div>
-->
            </form>
        </div>
    </div>

    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'users', 'action' => 'index','admin' => true),array('class'=>'button darkblue'))); ?></div>
    
    
    
    <script>
    <!--
    $(function(){
        $('.edit_area').css('display','none');
        $('.edit_icon').on('click',function(){
        
            $('.edit_area').css('display','block');
            $('.default_area').css('display','none');
        }); 
    });
    -->
    </script>


</div>

</div><!--/End main-->


<style>
i{
margin:5px;
}
</style>