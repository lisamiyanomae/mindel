<?php 
$this->assign('title', 'ログイン');
$this->Html->addCrumb('ログイン');

$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));
?>
<div class="section">
	<div class="inner">
		<h2>ログイン</h2>
		<p class="center">ログインIDとパスワードを入力してください</p>
		<div role="form" class="wpcf7" id="wpcf7-f433-p432-o1" lang="ja" dir="ltr">
			<div class="screen-reader-response"></div>
				
				<div id="form">
					<div class="table2">
						<?php echo $this->Form->create('User',array('id'=>'varidate_form','type'=>'file','inputDefaults' => array(),'novalidate' => true,'class'=>'primary-form')); ?>
						<div class="center tyuui mb20"><?php echo $this->Flash->render(); ?></div>
						<table class="sp_border">
							<tr>
							<th>ログインID</span></th>
							<td><span class="wpcf7-form-control-wrap your-name"><?php  echo $this->Form->input('username',array('class' => 'validate[required]','label' => false, 'size'=>"40" )); ?></span></td>
							</tr>
							<tr>
							<th>パスワード</th>
							<td><span class="wpcf7-form-control-wrap your-name"><?php echo $this->Form->input('password',array('class' => 'validate[required]','label' => false, 'size'=>"40" )); ?></span></td>
							</tr>
						</table>
					</div>
					
					<div class="center"><?php echo $this->Form->end('ログイン'); ?></div><br>
				</div>
			</form>
		</div>
		<p class="center"><u><?php echo $this->Html->link('パスワードを忘れた方はこちら', '/forgot_password', array('class' => '')); ?></u></p>
		<p class="center"><u><?php echo $this->Html->link('新規登録',  'add'); ?></u></p>
	</div><!--/inner-->
</div><!--/section-->


<!--
<div class="users form">
<?php //echo $this->Flash->render('auth'); ?>
<?php //echo $this->Form->create('User',array('id'=>'varidate_form','type'=>'file','inputDefaults' => array(),'novalidate' => true,'class'=>'primary-form')); ?>
    <fieldset>
        <legend>
            <?php echo 'ログインIDとパスワードを入力してください'; ?>
        </legend>
        <?php 
        //echo $this->Form->input('username',array('class' => 'validate[required]','label' => 'ログインID'));
        //echo $this->Form->input('password',array('class' => 'validate[required]','label' => 'パスワード'));
        ?>
    </fieldset>
<?php echo $this->Form->end('ログイン'); ?>


</div>
-->



<script>
jQuery(function($){
    
    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    
    
});
</script>
