<?php
$this->assign('title', 'ポイントデータ管理');
$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.jpostal', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));

?>
<div id="main">
    
<div class="section inner">
    <h1>ポイントデータ管理</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6">ポイント払い(親)ユーザー設定</div>
    
    <p>このユーザーのポイント払い(親)アカウントを設定します。ここで設定した内容はユーザー詳細画面から変更できます。</p>
    <?php echo $this->Form->create('User',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    <?php echo $this->Form->input('id', array('type' => 'hidden','value' => $user['User']['id'])); ?>
    
    <div id="claimList" class="table">
	    <table>
	        <thead>
	        <tr>
	        <th>
	        <?php echo $user['User']['shop_name']." ".$this->Html->link(' ('.USER_ID_HEDDER.sprintf('%06d',$user['User']['id']).')',array( 'controller'=>'users','action'=>'view',$user['User']['id']),array('escape' => false,'class'=>'','title'=>'顧客詳細へ')); ?>
	        </th>	
	        </tr>
	        <tr>
	        <td>
	        <p>
	        登録済みのポイント払いユーザー 
	        <?php echo $this->Form->input('parent_id', array('type' => 'select','options' => $reg_users,'class' => 'validate[required]','div'=>false,'empty'=>'選択してください','default' => $user['User']['parent_id'])); ?>
	        に 紐づけ
	        </p>
	        </td>
	        </tr>
	        </thead>
	    </table>
    </div>

    <?php echo $this->Form->end('実行'); ?>
    <div class="backpage"><?php print($this->Html->link('ポイントデータ一覧へ', array('controller' => 'points', 'action' => 'index','admin' => true),array('class'=>'button darkblue'))); ?>
    <?php print($this->Html->link('顧客詳細へ', array('controller' => 'users', 'action' => 'view/'.$user['User']['id'],'admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->
