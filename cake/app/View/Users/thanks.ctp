<?php 
$this->assign('title', 'アカウント作成');
$this->Html->addCrumb('アカウント作成');
$this->Html->addCrumb('完了');
?>
    <div class="section">
        <div class="inner">
            <div class="company_message">
            <p>アカウントの作成をありがとうございました。<br/>
            次のステップへのご案内を <?php echo $email; ?> に送信しました。<br/>
            メールをご確認の上、申込み詳細画面へお進みください。</p>
            <p class="tyuui">迷惑メールに振り分けられることがありますので合わせてご確認ください。</p>
            <br>
            <p><a href="https://minderu.com"><u>トップに戻る</u></a><p>
            </div>
        </div><!--/inner-->
    </div><!--/company-->



