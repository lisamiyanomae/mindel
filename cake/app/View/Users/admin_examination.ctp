<?php 
$this->assign('title', '審査');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
?>

<div id="main">
    
<div class="section inner">
    <h1>審　査（詳細）</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <?php echo $this->Form->create('User',array('id'=>'varidate_form','type'=>'file','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); 
            echo $this->Form->input('confirmation_filename'       ,array('id' =>'confirmation_filename'  ,'type'=>'hidden'  )); 
            echo $this->Form->input('confirmation_filename_1'     ,array('id' =>'confirmation_filename_1','type'=>'hidden'  )); 
            echo $this->Form->input('confirmation_filename_2'     ,array('id' =>'confirmation_filename_2','type'=>'hidden'  )); 
            echo $this->Form->input('shop_name'                   ,array('id' =>'shop_name'              ,'type'=>'hidden'  )); 
            echo $this->Form->input('company_name'                ,array('id' =>'company_name'           ,'type'=>'hidden'  )); 
            echo $this->Form->input('responsible_name'            ,array('id' =>'responsible_name'       ,'type'=>'hidden'  )); 
            echo $this->Form->input('username'                    ,array('id' =>'username'               ,'type'=>'hidden'  )); 
            echo $this->Form->input('email'                       ,array('id' =>'email'                  ,'type'=>'hidden'  )); 
    
    ?>
    <div class="row">
        <div class="cont1of2">
            <h2>アカウント情報</h2>
            <div class="box border">
                <div class="table">
                    <table>
                        <tr>
                            <th>登録日時</th>
                            <td><?php echo date('Y-m-d',strtotime($this->data['User']['created'])); ?></td>
                        </tr>
                        <tr>
                            <th>お客様No.</th>
                            <td><?php echo USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->data['User']['id']);?></td>
                        </tr>
                        <tr>
                            <th>店舗名または屋号</th>
                            <td><?php echo $this->data['User']['shop_name'];?></td>
                        </tr>
                        <tr>
                            <th>会社名（契約者名）</th>
                            <td><?php echo $this->data['User']['company_name'];?> <span class="furigana"><?php echo $this->data['User']['company_kana'];?></span></td>
                        </tr>
                        <tr>
                            <th>担当者</th>
                            <td><?php echo $this->data['User']['responsible_name'];?><span class="furigana"><?php echo $this->data['User']['responsible_kana'];?></span></td>
                        </tr>
                        <tr>
                            <th>メールアドレス</th>
                            <td><?php echo $this->data['User']['email'];?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--/アカウント情報-->

            <h2>ユーザー情報</h2>
            <div class="box border">
                <div class="table">
                    <table>
                        <tr>
                            <th>郵便番号</th>
                            <td><?php echo $this->data['User']['zip'];?></td>
                        </tr>
                        <tr>
                            <th>住所1</th>
                            <td><?php echo $this->data['User']['address'];?></td>
                        </tr>
                        <tr>
                            <th>住所2</th>
                            <td><?php echo $this->data['User']['building'];?></td>
                        </tr>
                        <tr>
                            <th>電話番号</th>
                            <td><?php echo $this->data['User']['user_telno'];?></td>
                        </tr>
                        <tr>
                            <th>業種</th>
                            <td><?php echo $this->data['User']['industry'];?></td>
                        </tr>
                        <tr>
                            <th>使用目的</th>
                            <td><?php echo $this->data['User']['purpose'];?></td>
                        </tr>
                        <tr>
                            <th>本人確認資料</th>
                            <td>
                            <?php
                            if($this->data['User']['confirmation_filename_1']){
                                echo "<br>[アップロードされたた画像1]<br>";
                                if(substr($this->data['User']['confirmation_filename_1'], strrpos($this->data['User']['confirmation_filename_1'], '.') + 1) != 'pdf'){
                                    $confirmation_filename_1 = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_1']);
                                    echo "<img src='data:image/jpg;base64,".base64_encode($confirmation_filename_1) . "'>";
                                    echo $this->Html->link("<i class='fas fa-search-plus'></i>",array('action'=>'admin_imgdisp',$this->data['User']['id'],'confirmation_filename_1'),array('target' => '_blank','escape'=>false));
                                }else{
                                    echo $this->Html->link("<i class='fas fa-file-pdf fa-4x'></i>",array('action'=>'pdfdisp',$this->data['User']['id'],'confirmation_filename_1'),array('target' => '_blank','escape'=>false));
                                }
                                
                            }
                            if($this->data['User']['confirmation_filename_2']){
                                
                                echo "<br>[アップロードされたた画像2]<br>";
                                if(substr($this->data['User']['confirmation_filename_2'], strrpos($this->data['User']['confirmation_filename_2'], '.') + 1) != 'pdf'){
                                    $confirmation_filename_2 = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_2']);
                                    echo "<img src='data:image/jpg;base64,".base64_encode($confirmation_filename_2) . "'>";
                                    echo $this->Html->link("<i class='fas fa-search-plus'></i>",array('action'=>'admin_imgdisp',$this->data['User']['id'],'confirmation_filename_2'),array('target' => '_blank','escape'=>false));
                                }else{
                                    echo $this->Html->link("<i class='fas fa-file-pdf fa-4x'></i>",array('action'=>'pdfdisp',$this->data['User']['id'],'confirmation_filename_2'),array('target' => '_blank','escape'=>false));
                                }
                            }
                            if($this->data['User']['confirmation_filename_first_1']){
                                echo "<br>[初回登録で再審査となった画像1]<br>";
                                if(substr($this->data['User']['confirmation_filename_first_1'], strrpos($this->data['User']['confirmation_filename_first_1'], '.') + 1) != 'pdf'){
                                    $confirmation_filename_first_1 = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_first_1']);
                                    echo "<img src='data:image/jpg;base64,".base64_encode($confirmation_filename_first_1) . "'>";
                                    echo $this->Html->link("<i class='fas fa-search-plus'></i>",array('action'=>'admin_imgdisp',$this->data['User']['id'],'confirmation_filename_first_1'),array('target' => '_blank','escape'=>false));
                                }else{
                                    echo $this->Html->link("<i class='fas fa-file-pdf fa-4x'></i>",array('action'=>'pdfdisp',$this->data['User']['id'],'confirmation_filename_first_1'),array('target' => '_blank','escape'=>false));
                                }

                            }
                            if($this->data['User']['confirmation_filename_first_2']){
                                echo "<br>[初回登録で再審査となった画像2]<br>";
                                if(substr($this->data['User']['confirmation_filename_first_2'], strrpos($this->data['User']['confirmation_filename_first_1'], '.') + 2) != 'pdf'){
                                    $confirmation_filename_first_2 = file_get_contents(CONFIRM_IMG_ROOT.CONFIRM_IMG_DIR.DIRECTORY_SEPARATOR.$this->data['User']['confirmation_filename_first_2']);
                                    echo "<img src='data:image/jpg;base64,".base64_encode($confirmation_filename_first_2) . "'>";
                                    echo $this->Html->link("<i class='fas fa-search-plus'></i>",array('action'=>'admin_imgdisp',$this->data['User']['id'],'confirmation_filename_first_2'),array('target' => '_blank','escape'=>false));
                                }else{
                                    echo $this->Html->link("<i class='fas fa-file-pdf fa-4x'></i>",array('action'=>'pdfdisp',$this->data['User']['id'],'confirmation_filename_first_2'),array('target' => '_blank','escape'=>false));
                                }
                            }
                            ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="cont_right"><!--a class="button darkblue" href="">追加資料アップロード</a--></div>
            </div>
            <!--/ユーザー情報-->


        </div>
        <div class="cont1of2">
            <h2>顧客メモ</h2>
            
            <div class="box border">
                <?php echo $this->Form->textarea('memo'  ,array('id' =>'memo' ,  'placeholder'=>'［前回不承認理由など]'         ));?>
            </div>
            <div class="cont_right mt10"><?php echo $this->Form->submit('更　新', array('div'=>false,'class' => 'button darkblue','name' => 'button_memo_update')); ?></div>
            <br class="clear">
            <br />
            <hr/>
            
            <p>情報を確認の上、[承認][再審査][却下]　を行ってください </p><br/>
            <h2>【 承認 】</h2>
            
            <div class="cont_right"><?php echo $this->Form->submit('承　認', array('div'=>false,'class' => 'button darkblue','name' => 'button_status_to_3')); ?></div>
            <div class="cont_right selectbox">
                <?php echo $this->Form->input('status_3_admin_id'  ,array('id' =>'status_3_admin_id'  ,'type'=>'select','options' => $admin_list  ,'empty'=>'担 当 者'  ,'default' => $admin['id'])); ?>
            </div>
            
            <!--/顧客メモ-->        

            <h2>【 再審査 】</h2>
            <div class="box border">
                <?php echo $this->Form->input('status_2_reason'  ,array('id' =>'status_2_reason'  ,'type'=>'select','options' => $status_2_reason_list  ,'empty'=>'再審査理由'  ,'class' => '')); ?>
                <h3 class="mt20">その他の場合のコメント（メール送信文面）</h3>
                <?php echo $this->Form->textarea('status_2_mailadd'  ,array('id' =>'status_2_mailadd' ,  'placeholder'=>'［その他の場合のコメント（メール送信文面）を入力してください］' ));?>
            </div>
            
            
            
            <div class="cont_right"><?php echo $this->Form->submit('再 審 査', array('div'=>false,'class' => 'button darkblue','name' => 'button_status_to_2')); ?></div>
            <div class="cont_right selectbox">
                <?php echo $this->Form->input('status_2_admin_id'  ,array('id' =>'status_2_admin_id'  ,'type'=>'select','options' => $admin_list  ,'empty'=>'担 当 者'  ,'default' => $admin['id'])); ?>
            </div>
            <!--/その他、コメント（メール送信文面）-->    

            <h2>【 却下 】</h2>
            <div class="box border">
                <?php echo $this->Form->textarea('status_7_reason'  ,array('id' =>'status_7_reason' ,  'placeholder'=>'[却下理由]' ));?>
            </div>
            <div class="cont_right"><?php echo $this->Form->submit('却 下', array('div'=>false,'name' => 'button_status_to_7','class' => 'button red')); ?></div>
            <div class="cont_right selectbox">
                <?php echo $this->Form->input('status_7_admin_id'  ,array('id' =>'status_7_admin_id'  ,'type'=>'select','options' => $admin_list  ,'empty'=>'担 当 者'  ,'default' => $admin['id'])); ?>

            </div>
            <!--/理由-->
            
            <?php if($admin['role'] == 1){ ?>
            <br class="clear">
            <div class="cont_right"><?php echo $this->Form->input('no_payment'  ,array('id' =>'no_payment'  ,'label' => '無課金','div'=>false)); ?> <?php echo $this->Form->submit('更　新', array('div'=>false,'class' => 'button darkblue','name' => 'button_no_payment')); ?></div>
            <?php } ?>

            
        </div>
        </form>
    </div>
    
    <div class="backpage button darkblue"><?php print($this->Html->link('戻 る', array('controller' => 'users', 'action' => 'index/1/1','admin' => true),array('class'=>'button darkblue'))); ?></div>


</div>

</div><!--/End main-->

<style>
i{
margin:10px;
}
</style>