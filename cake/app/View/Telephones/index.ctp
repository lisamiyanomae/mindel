<?php 
$this->assign('title', '着信履歴');
$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));
?>
<div id="main" class="mypage">

    <div class="section">
        <div class="inner">
            <h2>着信履歴</h2>    
            <p class="mb30">この電話番号に発着信した番号と時間を表示します。<br>録音メッセージがある場合は <i class='fas fa-volume-up'></i> をクリックして音声を確認することが出来ます。</p>
            <p class="history_lead"><img src="<?php echo $this->Html->webroot; ?>images/icon_incom.png" alt="着信" />着信　<img src="<?php echo $this->Html->webroot; ?>images/icon_outcom.png" alt="発信" />発信</p>
            <?php echo $this->Form->create('Telephones',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
            <?php echo $this->Form->hidden('Telephones.id',array('id'=>'telephone_id')); ?> 
            <?php echo $this->Form->hidden('Telephones.memo',array('id'=>'telephone_memo')); ?> 
            <div class="table">
                <table class="history">
                    <thead>
                        <tr>
                        
                        <th class="history_icon"></th>
                        <th>発信元</th>
                        <th>着信先</th>
                        <th>日時・時間</th>
                        <th>メモ <i class="fas fa-sync-alt" title="再読み込み"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($telephones as $telephone): ?>
                        <?php if($telephone['Telephones']['kind'] < 5 || $telephone['Telephones']['kind'] == 10){$str = "着信";$img = "icon_incom.png";}else{$str = "発信";$img = "icon_outcom.png";}?>
                        <tr>
                        <td><img src="<?php echo $this->Html->webroot; ?>images/<?php echo $img; ?>" alt="<?php echo $str; ?>" /></td>
                        <td><a href="tel:<?php echo str_replace("-","", $user['tropo_telno']) . "," . str_replace("+81","0", $telephone['Telephones']['telno']);?>#"><?php echo str_replace("+81","0", $telephone['Telephones']['telno']);?></a><?php echo ' '.$telephone['Teltransfer1']['memo'];  ?></td>
                        <td><?php 
                        if($telephone['Telephones']['kind'] == 0){
                            echo "[自動応答]";
                            if($telephone['Telephones']['recording_url']){ ?> <a href='<?php echo $telephone['Telephones']['recording_url'];?>' target='_blank' alt='録音メッセージ'><i class="fas fa-volume-up" title="録音再生"></i></a><?php }
                        } 
                        if($telephone['Telephones']['kind'] == 4 || $telephone['Telephones']['kind'] == 8 ){
                            echo "[対応中/未応答]";
                        }elseif($telephone['Telephones']['kind'] == 10 ){
                            echo "（着信前離脱）";
                        }else{
                            echo "<a href='tel:" . str_replace("-","", $user['tropo_telno']) . "," . str_replace("+81","0", $telephone['Telephones']['teltransferno']) ."#'>".str_replace("+81","0", $telephone['Telephones']['teltransferno'])."</a>"; 
                            echo ' '.$telephone['Teltransfer']['memo'];   
                        }
                        ?></td>
                        <td><?php echo $telephone['Telephones']['start_time']; ?></td>
                        <td class="fix-width"><span class="memo_val" id="memo_<?php echo $telephone['Telephones']['id']; ?>"><?php echo ' '.$telephone['Telephones']['memo'];   ?> </span>
                            <input type="text" maxlength = '50' class="input_memo validate[maxSize[25]]" id="input_<?php echo $telephone['Telephones']['id']; ?>" value="<?php echo $telephone['Telephones']['memo']; ?>"  >
                            <i class="fas fa-edit"      id="ico_edit_<?php echo $telephone['Telephones']['id'];   ?>" title="編集"></i> 
                            <i class="far fa-save"      id  ="ico_save_<?php echo $telephone['Telephones']['id']; ?>" title="保存"></i>
                            <i class="fas fa-undo-alt"  id="ico_undo_<?php echo $telephone['Telephones']['id'];   ?>" title="編集中止"></i>
                        </td>
                        </tr>
                        <?php endforeach; ?>
                        <?php unset($telephone); ?>
                    </tbody>
                </table>
            </div>
            </form>
            
            <div id="pager">
            <span><?php echo $this->Paginator->prev(  '≪ PREV',  array('class' => 'nav'),  null,  array('class' => 'nav')); ?></span>
            <span class="num" ><?php echo $this->Paginator->numbers(array('separator'=>'</span><span class="num">'));?></span>
            <span><?php echo $this->Paginator->next(  'NEXT ≫',  array('class' => 'nav'),  null,  array('class' => 'nav'));?></span>
            <span><?php echo $this->Paginator->counter(); ?></span>
            </div>
            <form><input type="button" value="印刷" onclick="window.print();"class="print_no"/></form>

        </div><!--/inner-->
    </div><!--/-->
</div><!--/-->

<style>
.fix-width{
    max-width: 200px;
}
.memo_val{
    word-wrap: anywhere;
}
.fa-undo-alt,
.fa-save,
.input_memo{
  display:none;
}
i{
  cursor:pointer;
}
</style>

<script>
$(function(){

  $('.fa-edit').click(function() {
    var tgt = $(this).attr('id').split('_');

    $('.fa-undo-alt').css("display","none");
    $('.fa-save').css("display","none");
    $('.input_memo').css("display","none");
    $('.fa-edit').css("display","unset");
    $('.memo_val').css("display","unset");
    
    $(this).css('display','none');
    $('#memo_' + tgt[2]).css('display','none');
    $('#input_' + tgt[2]).css('display','unset');
    $('#ico_undo_' + tgt[2]).css('display','unset');
    $('#ico_save_' + tgt[2]).css('display','unset');
    $('#ico_undo_' + tgt[2]).css('display','unset');
    $('#input_' + tgt[2]).val($('#memo_' + tgt[2]).text().trim());
    
  });
  
  $('.fa-undo-alt').click(function() {
    var tgt = $(this).attr('id').split('_');
    
    $('#memo_' + tgt[2]).css('display','unset');
    $(this).css('display','unset');
    $('#ico_edit_' + tgt[2]).css('display','unset');
    $('#input_' + tgt[2]).css('display','none');
    $('#ico_undo_' + tgt[2]).css('display','none');
    $('#ico_save_' + tgt[2]).css('display','none');
  
  });
  
  $('.fa-save').click(function() {
    if(!$("#telephone_id").val()){
      var tgt = $(this).attr('id').split('_');
      $("#telephone_id").val(tgt[2]);
      $("#telephone_memo").val($('#input_' + tgt[2]).val());
    }
    $("#varidate_form").submit();
  });

  $('.fa-sync-alt').click(function() {
    $("#varidate_form").submit();
  });

  $(".input_memo").on('keydown', function(e) {
    if(e.keyCode === 27) {
      $('.fa-undo-alt').trigger('click');
    }
    if(e.keyCode === 13) {
      var tgt = $(this).attr('id').split('_');
      $("#telephone_id").val(tgt[1]);
      $("#telephone_memo").val($('#input_' + tgt[1]).val());
      $('.fa-save').trigger('click');
    }
  });

  $("#varidate_form").validationEngine('attach', {
      promptPosition:"bottomLeft"
  });

});

</script>
