<?php 
$this->assign('title', '着信履歴');
?>
<style>
table td img{
    width:20px;
}
</style>
<div id="main" class="mypage">

    <div class="section">
        <div class="inner">


            <h2>着信履歴</h2>   
            <p class="mb30">店舗名：<?php echo $shop_name; ?><br>着信履歴はマイページで3ヶ月まで、運営センターで1年分までを確認できます。それ以前はシステム担当に依頼してください。<br> </p>

            <div class="table w900">
                <table class="history">
                    <thead>
                        <tr>
                        <th>通話種類</th>
                        <th>発信元</th>
                        <th>着信先</th>
                        <th>通話開始</th>
                        <th>通話終了</th>
                        <th>通話時間</th>
                        <th>料金(概算)</th>
                        <th>メモ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($telephones as $telephone): ?>
                        
                        <tr>
                        <td><?php echo $call_kind[$telephone['Telephones']['kind']]; ?></td>
                        <td><a href="tel:<?php echo str_replace("-","", $user['tropo_telno']) . "," . str_replace("+81","0", $telephone['Telephones']['telno']);?>#"><?php echo str_replace("+81","0", $telephone['Telephones']['telno']);?></a><?php echo ' '.$telephone['Teltransfer1']['memo'];  ?></td>
                        <td><?php 
                        if($telephone['Telephones']['kind'] == 0){
                            echo "[自動応答]";
                            if($telephone['Telephones']['recording_url']){ ?> <a href='<?php echo $telephone['Telephones']['recording_url'];?>' target='_blank' alt='録音メッセージ'><i class="fas fa-volume-up" title="録音再生"></i></a><?php }
                        } 
                        if($telephone['Telephones']['kind'] == 4 || $telephone['Telephones']['kind'] == 8 ){
                            echo "[対応中/未応答]";
                        }elseif($telephone['Telephones']['kind'] == 10 ){
                            echo "（着信前離脱）";
                        }else{
                            echo "<a href='tel:" . str_replace("-","", $user['tropo_telno']) . "," . str_replace("+81","0", $telephone['Telephones']['teltransferno']) ."#'>".str_replace("+81","0", $telephone['Telephones']['teltransferno'])."</a>"; 
                            echo ' '.$telephone['Teltransfer']['memo'];   
                        }
                        ?></td>
                        <td><?php echo $telephone['Telephones']['start_time']; ?></td>
                        <td><?php echo date("H:i:s",strtotime($telephone['Telephones']['end_time'])); ?></td>
                        <td><?php if($telephone['Telephones']['end_time']){echo gmdate("H:i:s", (strtotime($telephone['Telephones']['end_time']) - strtotime($telephone['Telephones']['start_time'])));} ?></td>
                        <td><?php 
                            if(in_array($telephone['Telephones']['kind'],array(1,2,3,5,6,7))){echo round(((strtotime($telephone['Telephones']['end_time']) - strtotime($telephone['Telephones']['start_time'])) / 60 * $call_fees[$telephone['Telephones']['kind'] % 2 ]),2)."円";}  
                            if($telephone['Telephones']['kind'] == 0)                        {echo AUTO_RESPONCE_FEE."円";}  
                        
                        ?></td>
                        
                        <td><?php echo $telephone['Telephones']['memo']; ?></td>
                        </tr>
                        <?php endforeach; ?>
                        <?php unset($telephone); ?>
                    </tbody>
                </table>
            </div>
            
            <div id="pager">
            <span><?php echo $this->Paginator->prev(  '≪ PREV',  array('class' => 'nav'),  null,  array('class' => 'nav')); ?></span>
            <span class="num" ><?php echo $this->Paginator->numbers(array('separator'=>'</span><span class="num">'));?></span>
            <span><?php echo $this->Paginator->next(  'NEXT ≫',  array('class' => 'nav'),  null,  array('class' => 'nav'));?></span>
            <span><?php echo $this->Paginator->counter(); ?></span>
            </div>
		    
		    <?php echo $this->Form->create(); ?>
		    <?php echo $this->Form->submit('CSV出力',array('name' => 'outputcsv','div'=>false));?>
    		</form>
            
        </div><!--/inner-->
    </div><!--/-->
    <div class="backpage"><?php print($this->Html->link('戻 る', array('controller' => 'users', 'action' => 'index','admin' => true),array('class'=>'button darkblue'))); ?></div>
</div><!--/-->
