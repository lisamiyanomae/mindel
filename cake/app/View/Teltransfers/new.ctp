<?php
$this->assign('title', '転送設定');
$this->Html->addCrumb($tstring);

$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));
?>

<div class="section">
    <div class="inner">

        <h2><?php echo $tstring; ?></h2>
        <p><?php echo $tstring2; ?></p>
        <?php echo $this->Form->create('Teltransfer',array('id'=>'form_teltransfer','inputDefaults' => array('label' => false, 'div' => false)));?>
        <div id="form">
            <?php if($this->action=='new'){ ?>
           <h4 class="mt40">転送先登録</h4> 
            <p class="mb10">
           転送先の電話番号を設定します。着信は以下の電話番号へ同時転送されます。<br />
この電話番号から発信すると相手に050番号が表示されます。転送先はマイページでいつでも編集できます。
            </p>
            <?php } ?>

            <div class="table2">
                <table class="sp_border" id="data_table">
                <thead>
                <tr>
                <th> </th>
                <td>登録電話番号</td>
                <td>名称</td>
                <td>転送</td>
                
                </tr>
                </thead>
                <tbody>
                <tr class="data_row">
                <th class="row_title">転送先1</th>
                <?php echo $this->Form->input('num', array('type' => 'hidden','value' => 1,'class' => 'num')) ?>
                <td><?php echo $this->Form->input('telno' , array( 'size' => 20, 'class' => 'validate[required,custom[phone]] telno','placeholder'=>'000-0000-0000')) ?></td>
                <td><?php echo $this->Form->input('memo'  , array( 'size' => 20, 'class' => 'validate[required] memo','placeholder'=>'わかりやすい名称　')) ?></td>
                <td><?php echo $this->Form->input('active', array( 'class' => 'active','default'=>1)); echo $this->Form->input('active_value', array('type' => 'hidden','class' => 'active_value')) ?></td>
                
                </tr>
                
                <th class="row_title">転送先2</th>
                <?php echo $this->Form->input('num', array('type' => 'hidden','value' => 2,'class' => 'num')) ?>
                <td><?php echo $this->Form->input('telno' , array( 'size' => 20, 'class' => 'validate[required,custom[phone]] telno','placeholder'=>'000-0000-0000')) ?></td>
                <td><?php echo $this->Form->input('memo'  , array( 'size' => 20, 'class' => 'validate[required] memo','placeholder'=>'事務所')) ?></td>
                <td><?php echo $this->Form->input('active', array( 'class' => 'active','default'=>1)); echo $this->Form->input('active_value', array('type' => 'hidden','class' => 'active_value')) ?></td>
                
                </tr>
                <th class="row_title">転送先3</th>
                <?php echo $this->Form->input('num', array('type' => 'hidden','value' => 3,'class' => 'num')) ?>
                <td><?php echo $this->Form->input('telno' , array( 'size' => 20, 'class' => 'validate[required,custom[phone]] telno','placeholder'=>'000-0000-0000')) ?></td>
                <td><?php echo $this->Form->input('memo'  , array( 'size' => 20, 'class' => 'validate[required] memo','placeholder'=>'自宅など')) ?></td>
                <td><?php echo $this->Form->input('active', array( 'class' => 'active','default'=>1)); echo $this->Form->input('active_value', array('type' => 'hidden','class' => 'active_value')) ?></td>

                </tr>
                </tbody>
                </table>
                
                <div class="add_telnum float_r mt20"><a class="button gray addRow" alt="行を追加します">＋</a> <a class="button gray delRow" alt="入力のない最終行を消去します">－</a></div>

            </div> 
            <?php if($this->action=='new'){ ?>
            <p class="mb40">
            <span class="tyuui">入力した電話番号に間違いがないか、必ずお確かめください。</span><br>
            ※同時転送は10台まで、登録は10台以上でもできます。<br>
            ※発信のみ行いたい電話は電話番号を登録して転送チェックを外して下さい。<br>
            ※自動応答のみで使用する場合は任意の電話番号を入力の上、転送チェックを外して下さい。
            </p>
            <?php } ?>
        </div> 
        
        <div class="submitbtn registration">
        <?php echo $this->Form->input('teltransfer_data',   array('id'=>'teltransfer_data'   ,'value'=>$teltransfer_data  ,'type'=>'hidden')); ?>
        <?php echo $this->Form->input('user_id', array('type' => 'hidden','value' => $user['id'])) ?>
        <?php echo $this->Form->end($bstring); ?>
        <?php if($this->action=='edit'){ ?>
        </div>
        <div class="mt20"><a class="button gray" href="index">更新せず戻る</a></div>
        <?php } ?>
    </div><!--/inner-->
</div><!--/-->


<script>
jQuery(function($){

    $("#form_teltransfer").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });

    // 編集時データ入力を復元
    var ret = $('#teltransfer_data').val();
    if(ret != '[]'){
        var obj = $.parseJSON(ret);
        
        for(var i = 0; i < (obj['num'].length) ; i++ ){
            if(i > 2){
                add_row();
            }
            $('.num:eq('+ i + ')').val(obj['num'][i]);
            $('.telno:eq('+ i + ')').val(obj['telno'][i]);
            $('.memo:eq('+ i + ')').val(obj['memo'][i]);
            if(obj['active'][i]){
                $('.active:eq('+ i + ')').prop("checked",true); 
            }else{
                $('.active:eq('+ i + ')').prop("checked",false); 
            }
        }
        for(var i = (obj['num'].length) ; i < 3; i++ ){
            del_row();
        }
    }else{
        del_row();
        del_row();
    }
    
    // 行を削除する
    $(document).on("click", ".delRow", function () {
        del_row();
    });    
    
    function del_row() {
        var cnt = $('.num').length;
        var last_tel = $(".telno:eq(-1)").val();
        if(!last_tel && cnt > 1){
            $("form_teltransfer").validationEngine('attach',{binded:false});
            $(".num:eq(-1)").parent().remove();
        }
    }
    
    // 行を追加する
    $(document).on("click", ".addRow", function () {
        var last_tel = $(".telno:eq(-1)").val();
        if(last_tel){
            add_row();
        }
    });
    function add_row() {
    
        var max_num = 0;
        var cnt = $('.num').length;
        for(var i = 0; cnt > i ; i++ ){
            if( $('.num:eq('+ i + ')').val() > max_num){
                max_num = $('.num:eq('+ i + ')').val();
            }
        }
        max_num = parseInt(max_num) + 1;
        
        $('#data_table tbody > tr:last').after('<tr>'+$(".data_row:eq(-1)").html()+'<tr>')
        $(".row_title:eq(-1)").html("転送先"+ $('.row_title').length);
        $(".memo:eq(-1)").attr('placeholder',' ');
        $(".num:eq(-1)").val(max_num); // $(".num:eq(-1)").val($('.row_title').length);
        $(".active:eq(-1)").prop('checked',true);
        
    }
    // sumbit前にデータを加工
    $('#form_teltransfer').submit(function(){
        if ( ! $('#form_teltransfer').validationEngine( 'validate' )) {
            return false;
        } else {
            var cnt = $('.num').length;
            for(var i = 0; cnt > i ; i++ ){
                
                var new_name1 = $('.num:eq('+ i + ')').attr('name')+ '['+ i +']';
                $('.num:eq('+ i + ')').attr('name', new_name1);
                var new_name2 = $('.telno:eq('+ i + ')').attr('name')+ '['+ i +']';
                $('.telno:eq('+ i + ')').attr('name', new_name2);
                var new_name3 = $('.memo:eq('+ i + ')').attr('name')+ '['+ i +']';
                $('.memo:eq('+ i + ')').attr('name', new_name3);
                
                
                if($('.active:eq('+ i + ')').prop('checked')){
                    $('.active_value:eq('+ i + ')').val(1);
                }else{
                    $('.active_value:eq('+ i + ')').val(0);
                }
                var new_name4 = $('.active_value:eq('+ i + ')').attr('name')+ '['+ i +']';
                $('.active_value:eq('+ i + ')').attr('name', new_name4);
            
            }
        }
    })

});

</script>



