<?php
$this->assign('title', 'スケジュール・自動応答'); 
$this->Html->addCrumb($tstring);

$this->Html->css('validationEngine.jquery', null, array('inline' => false));
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('jquery.validationEngine', array('inline' => false));
$this->Html->script('jquery.validationEngine-ja', array('inline' => false));
$this->Html->script('jquery.timepicker', array('inline' => false));
$this->Html->css('jquery.timepicker', null, array('inline' => false));
?>

<main>
<div id="main" class="mypage">
    
<div class="section">
    <div class="inner">
        
        <h2><?php echo $tstring; ?></h2>
        <?php if($this->action=='index'){ ?>
        <p class="mb40">転送スケジュールと、自動応答メッセージを設定します。</p>
        
        
        <div class="center tyuui mt40"><?php echo $this->Flash->render(); ?></div>
        
        <h3 class="">転送時間・曜日設定</h3><p class="mb20">電話転送を行う曜日と時間を設定してください。</p>
        <?php } ?>
        
        <?php echo $this->Form->create('Voicemails',array('id'=>'varidate_form','inputDefaults' => array('label' => false, 'div' => false),'novalidate' => true,'class'=>'primary-form')); ?>
        <p class="center mb20"><?php echo $this->Form->input('switch' ,array('id'=>'switch','type'=>'select' ,'options'=>array(0=>'転送スケジュール',1=>'自動応答スケジュール'),'label' => '' ,'empty'=>false  ,'default' => 0   ,'class' => 'validate[required]')); ?></p>
        
        
        <div class="box w400">
        (<label>全ての曜日にチェックを入れる<input type="checkbox" id="all" /></label>)    
            <?php 
            echo $this->Form->input('user_id'  ,array('type'=>'hidden' ,'value'=>$user['id']));
            echo $this->Form->input('id'  ,array('type'=>'hidden' ));
            ?>
            <ul class="transfer_time">
                <li>
                <?php
                echo $this->Form->input('teltransfer_day_01'  ,array('id'=>'teltransfer_day_01' ,'type'=>'checkbox' ,'label' => '月'  ,'class' => 'day_check' , 'default'=>1));
                echo $this->Form->input('teltransfer_from_01' ,array('id'=>'teltransfer_from_01','type'=>'text' ,'label' => ''       ,'default' => DEFAULT_TIME_FROM   ,'class' => 'time_select validate[required,custom[time]                         ] voicemails_time'));
                echo $this->Form->input('teltransfer_to_01'   ,array('id'=>'teltransfer_to_01'  ,'type'=>'text' ,'label' => ' ～ '   ,'default' => DEFAULT_TIME_TO     ,'class' => 'time_select validate[required,custom[time],funcCall[CheckTimeOrder]] voicemails_time'));
                ?>
                <i class="fas fa-angle-double-right" title="終日" id="ico_all_day_01"></i>
                </li>
                <li>
                <?php
                echo $this->Form->input('teltransfer_day_02'  ,array('id'=>'teltransfer_day_02' ,'type'=>'checkbox' ,'label' => '火'  ,'class' => 'day_check', 'default'=>1));
                echo $this->Form->input('teltransfer_from_02' ,array('id'=>'teltransfer_from_02','type'=>'text' ,'label' => ''       ,'default' => DEFAULT_TIME_FROM   ,'class' => 'time_select validate[required,custom[time]                         ] voicemails_time'));
                echo $this->Form->input('teltransfer_to_02'   ,array('id'=>'teltransfer_to_02'  ,'type'=>'text' ,'label' => ' ～ '   ,'default' => DEFAULT_TIME_TO     ,'class' => 'time_select validate[required,custom[time],funcCall[CheckTimeOrder]] voicemails_time'));
                ?>
                <i class="fas fa-angle-double-right" title="終日" id="ico_all_day_02"></i>
                </li>
                <li>
                <?php
                echo $this->Form->input('teltransfer_day_03'  ,array('id'=>'teltransfer_day_03' ,'type'=>'checkbox' ,'label' => '水'  ,'class' => 'day_check', 'default'=>1));
                echo $this->Form->input('teltransfer_from_03' ,array('id'=>'teltransfer_from_03','type'=>'text' ,'label' => ''       ,'default' => DEFAULT_TIME_FROM   ,'class' => 'time_select validate[required,custom[time]                         ] voicemails_time'));
                echo $this->Form->input('teltransfer_to_03'   ,array('id'=>'teltransfer_to_03'  ,'type'=>'text' ,'label' => ' ～ '   ,'default' => DEFAULT_TIME_TO     ,'class' => 'time_select validate[required,custom[time],funcCall[CheckTimeOrder]] voicemails_time'));
                ?>
                <i class="fas fa-angle-double-right" title="終日" id="ico_all_day_03"></i>
                </li>
                <li>
                <?php
                echo $this->Form->input('teltransfer_day_04'  ,array('id'=>'teltransfer_day_04' ,'type'=>'checkbox' ,'label' => '木'  ,'class' => 'day_check', 'default'=>1));
                echo $this->Form->input('teltransfer_from_04' ,array('id'=>'teltransfer_from_04','type'=>'text' ,'label' => ''       ,'default' => DEFAULT_TIME_FROM   ,'class' => 'time_select validate[required,custom[time]                         ] voicemails_time'));
                echo $this->Form->input('teltransfer_to_04'   ,array('id'=>'teltransfer_to_04'  ,'type'=>'text' ,'label' => ' ～ '   ,'default' => DEFAULT_TIME_TO     ,'class' => 'time_select validate[required,custom[time],funcCall[CheckTimeOrder]] voicemails_time'));
                ?>
                <i class="fas fa-angle-double-right" title="終日" id="ico_all_day_04"></i>
                </li>
                <li>
                <?php
                echo $this->Form->input('teltransfer_day_05'  ,array('id'=>'teltransfer_day_05' ,'type'=>'checkbox' ,'label' => '金'  ,'class' => 'day_check', 'default'=>1));
                echo $this->Form->input('teltransfer_from_05' ,array('id'=>'teltransfer_from_05','type'=>'text' ,'label' => ''       ,'default' => DEFAULT_TIME_FROM   ,'class' => 'time_select validate[required,custom[time]                         ] voicemails_time'));
                echo $this->Form->input('teltransfer_to_05'   ,array('id'=>'teltransfer_to_05'  ,'type'=>'text' ,'label' => ' ～ '   ,'default' => DEFAULT_TIME_TO     ,'class' => 'time_select validate[required,custom[time],funcCall[CheckTimeOrder]] voicemails_time'));
                ?>
                <i class="fas fa-angle-double-right" title="終日" id="ico_all_day_05"></i>
                </li>
                <li>
                <?php
                echo $this->Form->input('teltransfer_day_06'  ,array('id'=>'teltransfer_day_06' ,'type'=>'checkbox' ,'label' => '土'  ,'class' => 'day_check', 'default'=>0));
                echo $this->Form->input('teltransfer_from_06' ,array('id'=>'teltransfer_from_06','type'=>'text' ,'label' => ''       ,'default' => DEFAULT_TIME_FROM   ,'class' => 'time_select validate[required,custom[time]                         ] voicemails_time'));
                echo $this->Form->input('teltransfer_to_06'   ,array('id'=>'teltransfer_to_06'  ,'type'=>'text' ,'label' => ' ～ '   ,'default' => DEFAULT_TIME_TO     ,'class' => 'time_select validate[required,custom[time],funcCall[CheckTimeOrder]] voicemails_time'));
                ?>
                <i class="fas fa-angle-double-right" title="終日" id="ico_all_day_06"></i>
                </li>
                <li>
                <?php
                echo $this->Form->input('teltransfer_day_00'  ,array('id'=>'teltransfer_day_07' ,'type'=>'checkbox' ,'label' => '日'  ,'class' => 'day_check', 'default'=>0));
                echo $this->Form->input('teltransfer_from_00' ,array('id'=>'teltransfer_from_07','type'=>'text' ,'label' => ''       ,'default' => DEFAULT_TIME_FROM   ,'class' => 'time_select validate[required,custom[time]                         ] voicemails_time'));
                echo $this->Form->input('teltransfer_to_00'   ,array('id'=>'teltransfer_to_07'  ,'type'=>'text' ,'label' => ' ～ '   ,'default' => DEFAULT_TIME_TO     ,'class' => 'time_select validate[required,custom[time],funcCall[CheckTimeOrder]] voicemails_time'));
                ?>
                <i class="fas fa-angle-double-right" title="終日" id="ico_all_day_07"></i>
                </li>
            </ul>
        
        </div>
         
        <p class="mb30"></p>
        <?php if($this->action=='index'){?>
        <p class="mt10 align_left">
        ◆転送スケジュール：転送時間を曜日ごとに設定<br>
        ◆時間外応答スケジュール：転送時間が夜0:00をまたぐ時<br>
        ※ 終日の場合は<i class="fas fa-angle-double-right" title="終日" id="ico_all_day_00"></i>をクリックして 00:00 ～ 23:59 に設定
        </p>
        <?php ;} ?>
        
        <?php if($user['status'] != 3){ ?>
            <div class="registration">
                <?php echo $this->Form->submit($bstring); ?>
            </div>
        <?php } ?>
        
        <p class="mb50" id="messeage"></p>
        
        <h3>自動応答メッセージ</h3>
        <p class="mb30">自動応答の設定をします。編集はリアルタイムで反映されます。</p>
        <?php if($this->action=='edit'){echo '<p class="mb40">転送時間外、転送先がない場合の自動応答メッセージを編集します。</p>';} ?>
        <div class="box w400 response_message">
            <?php echo $this->Form->input('message'      ,array('type'=>'textarea'  ,'label' => '<span class="label">時間外応答</span>' ,'default' => str_replace('_SHOP_NAME_',$user['shop_name'],DEFAULT_MESSAGE)    ,'escape'=>false,'class' => 'validate[required] mt20' )); ?>
           <p class="mt10">
               <span class="label">メール通知</span><label><?php echo $this->Form->input('autovoice_mail_use1' ,array('type'=>'radio' ,'legend' => false ,'default' => 1  ,'escape'=>false,'class' => 'mt20','options'=>array(  1 =>'ON ',0 =>'OFF ') , 'separator'=>'</label><label>'  )); ?></label>
           </p>

        </div>

        <div class="box w400 response_message mt20" id="VMessage">
            <label for="VoicemailsMessage"><span class="label">未対応<br>(着信に出られなかった時)</span></label>
            
            <p class="mt10">
                <input type="radio" class="timeout_radio" name="timeout_radio" id="timeout_radio_1" value="1">
                <label for="timeout_radio_1">ON 呼出しから<?php echo $this->Form->input('timeout_seconds'  ,array('id'=>'timeout_seconds','type'=>'text', 'label' => false ,'default' => 10 ,'size'=>2  ,'escape'=>false,'class' => 'validate[min[5],max[60]] ' )); ?>秒後　　　</label>
                <p>※5～60秒まで指定できます。</p>
                <p>※10秒でおよそ6コールです。</p>
                
            </p>
            
            <?php echo $this->Form->input('timeout_message' ,array('type'=>'textarea'  ,'label' => false ,'default' => str_replace('_SHOP_NAME_',$user['shop_name'],DEFAULT_MESSAGE_TIMEOUT)    ,'escape'=>false,'class' => 'validate[required] mt20' )); ?>
            
           <p class="mb10">
               <span class="label">メール通知</span><label><?php echo $this->Form->input('autovoice_mail_use2' ,array('type'=>'radio' ,'legend' => false ,'default' => 1  ,'escape'=>false,'class' => 'mt20','options'=>array(  1 =>'ON ',0 =>'OFF ') , 'separator'=>'</label><label>'  )); ?></label>
           </p>

           <p class="mt40">
                <input type="radio" class="timeout_radio" name="timeout_radio" id="timeout_radio_0" value="0">
                <label for="timeout_radio_0">OFF (未対応メッセージを使わない)</label>
                <p>※30秒で呼出しが切断されます。</p>
                
            </p>
            
        </div>
<p id="mail"></p>
        <div class="box w400 response_message mt20" id="VMessage">
           <span class="label">通知用メールアドレス</span>
           <?php echo $this->Form->input('autovoice_mail' ,array('type'=>'email'  ,'legend' => false ,'default' => $user['email']  ,'escape'=>false,'class' => 'validate[required,custom[email]] mt20' )); ?>
        </div>
        
<p id="recording"></p>        
        <div class="box w400 response_message mt20" id="VMessage">
           <span class="label">留守番電話機能</span>
           <div>
           <label><?php echo $this->Form->input('record_use' ,array('type'=>'radio' ,'legend' => false ,'default' => 0  ,'escape'=>false,'class' => 'mt20','options'=>array( 1 =>'ON ',0 =>'OFF ') , 'separator'=>'</label><label>'  )); ?></label>
           </div>
        
        </div>
        

        <?php if($user['status'] == 3){ ?>
        <p class="mb50"></p>
        <h3>お支払方法</h3>
        <?php echo $this->Form->radio( 'User.hand_payment', array(0=>'クレジットカード決済（最短当日に番号発行）',1=>'事前振込（ポイントチャージ制）'),array( 'label' =>'以下から選択してください','value'=>0,'class'=>'validate[required] mt20','separator' => '  ','legend' => "以下から選択してください。")); ?>
        
        <br><br>
        <?php }else{ ?>
        <?php echo $this->Form->input('User.hand_paymen'    ,array('type'=>'hidden')); ?>
        <?php } ?>

        <div class="registration">
            <?php echo $this->Form->end($bstring); ?>
        </div>
        
        <div class="backpage mt20"><?php print($this->Html->link('戻る', array('controller' => $back_btn['controller'], 'action' =>  $back_btn['action'],'admin' => false),array('class'=>'button darkblue'))); ?></div>
        

    </div><!--/inner-->


</div><!--/-->


</div><!--/End main-->
</main>


<style>
#switch{
  font-size: 20px!important;
}
.voicemails_time{
  width:75px;
}
.transfer_time label{
  padding:0 3px 0 7px;
}
.registration{
  margin-top:30px;
}

@media screen and (max-width: 896px) { 
  #timeout_seconds{
      width:50px;
  }
}

#varidate_form label{
  margin: 0 10px;

}
#VoicemailsAutovoiceMail{
  width:100%;    
}
.align_left{
  width: 400px;
  text-align:left;
  margin: auto;
}
.fa-angle-double-right{
  cursor:pointer;
  padding:0 10px;
}
</style>
<script>
 jQuery(function($){
    $('.voicemails_time').timepicker({ 'timeFormat': 'H:i' });
    $("#varidate_form").validationEngine('attach', {
        promptPosition:"bottomLeft"
    });
    $('#all').on('change', function() {
        $('.day_check').prop('checked', this.checked);
    });
    var tss = $('#timeout_seconds').val();
    if(tss == 0){
        $("#timeout_radio_0").prop("checked", true);
        $('#timeout_seconds').val('');
    }else{
        $("#timeout_radio_1").prop("checked", true);
    }
    
    $('#timeout_seconds').on('focus', function() {
        $('#timeout_radio_1').prop("checked",true);
        $('#timeout_seconds').val(10);
    });
    
    $('.timeout_radio').on('change', function() {
       var val = $(this).val();
       if(val==0){
           $('#timeout_seconds').val('');
       }else{
           $('#timeout_seconds').val(10);
       }
       
    });
    $('.fa-angle-double-right').on('click', function() {
        var ret = $(this).attr('id').split("_");
        $('#teltransfer_from_'+ret[3]).val("00:00");
        $('#teltransfer_to_'+ret[3]).val("23:59");
        
    });
});

function CheckTimeOrder(field, rules, i, options){

    var tgt_id = $(field).attr('id');
    
    var seps = tgt_id.split('_')
    if(seps[1] == "from"){
        var par_id =  tgt_id.replace('from','to');
        var from_val = $('#'+ tgt_id).val();
        var to_val = $('#'+ par_id).val()
    }else{
        var par_id =  tgt_id.replace('to','from');
        var from_val = $('#'+ par_id).val();
        var to_val = $('#'+ tgt_id).val()
    }
    if(from_val > to_val){
        return options.allrules.CheckTimeOrder.alertText;
    }
}
</script>
