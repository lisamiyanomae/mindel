<?php
$this->assign('title', '登録完了');
?>
    <div class="section">
        <div class="inner">
            <div class="company_message">
            <p>
            社内用未決済ユーザーが登録されました。運営センターにて番号発行を行ってください。
            </p>
            </div>
        </div><!--/inner-->
    </div><!--/company-->
