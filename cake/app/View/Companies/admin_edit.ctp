<?php
$this->assign('title', 'マルチアカウント');
?>
<div id="main">
    
<div class="section inner">
    <h1>マルチアカウント</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6">マルチアカウント更新</div>
    
    <?php echo $this->Form->create('Company',array('id'=>'varidate_form','inputDefaults' => array('label' => false),'novalidate' => true,'class'=>'primary-form')); ?>
    <div id="claimList" class="table">
        <table>
            <thead>
            <tr>
                <th>表示名</th>
                <th>ログインID</th>
                <th>パスワード</th>
                <th>メールアドレス</th>
            </tr>
            </thead>
            <tbody>
             <tr>
            <td><?php echo $this->Form->input('showname',array('label' => false)); ?></td>
            <td><?php echo $this->Form->input('username',array('label' => false)); ?></td>
            <td><?php echo $this->Form->input('password',array('label' => false)); ?></td>
            <td><?php echo $this->Form->input('email'   ,array('label' => false)); ?></td>
            </tr>
            </tbody>
        </table>
        
    </div>
    <?php echo $this->Form->end('更新'); ?>
    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'companies', 'action' => 'index','admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->
