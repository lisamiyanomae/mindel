<?php
$this->assign('title', 'マルチアカウント');
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
?>

<div id="main">
<div class="section inner">

    <h1>マルチアカウント</h1>
    <div class="flash"><?php echo $this->Flash->render(); ?></div>
    <div id="message" class="globalmenu6">マルチアカウント一覧</div>
    <p class="mb10">アカウント数をクリックしてマルチアカウントに紐づくアカウントを編集できます。<br>作成したマルチアカウントは <a href="<?php echo dirname($this->Html->url('/', true)) ?>/comp/" target="_blank"><?php echo dirname($this->Html->url('/', true)) ?>/comp/</a> からログインできます。</p>
    <div id="claimList" class="table">
        <table>
            <thead>
            <tr>
                <th>表示名</th>
                <th>ログインID</th>
                <th>メールアドレス</th>
                <th>アカウント数</th>
                <th>登録日時</th>
                <th></th>
                <th></th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($datas as $member): ?>
            <tr>
                <td><?php echo $member['Company']['showname']; ?></td>
                <td><?php echo $member['Company']['username']; ?></td>
                <td><?php echo $member['Company']['email']; ?></td>
                <td><u><?php print($this->Html->link($member[0]['CNT'], array('controller' => 'company_users', 'action' => 'index/'.$member['Company']['id'],'admin' => true),array('class'=>''))); ?></u></td>
                
                <td class="mobile_del"><?php echo $member['Company']['created']; ?></td>
                <td><?php print($this->Html->link('<i class="fas fa-edit"></i>',array( 'action'=>'edit',$member['Company']['id']),array('escape' => false))); ?></td>
                <td><?php  print($this->Form->postLink('<i class="fas fa-trash-alt"></i>',array('action'=>'delete',$member['Company']['id']),array('escape' => false),'本当に削除しますか?'));  ?></td>
                </tr>
            <?php endforeach; ?>
            <?php unset($member); ?>
            </tbody>
        </table>
    </div>

    <div class="pagenate">
        <span><?php echo $this->Paginator->prev(  '< 前のページ',  null,  null,  array('class' => 'disabled')); ?></span>
        <span><?php echo $this->Paginator->numbers();?></span>
        <span><?php echo $this->Paginator->next(  '次のページ >',  null,  null,  array('class' => 'disabled'));?></span>
        <span><?php echo $this->Paginator->counter(); ?></span>
    </div>

	<div class="mt30"><?php print($this->Html->link('<i class="fas fa-user-plus fa-2x"></i>',array( 'action'=>'add'),array('escape' => false))); ?></div>

    <div class="backpage"><?php print($this->Html->link('戻る', array('controller' => 'pages', 'action' => 'home','admin' => true),array('class'=>'button darkblue'))); ?></div>

</div>

</div><!--/End main-->



