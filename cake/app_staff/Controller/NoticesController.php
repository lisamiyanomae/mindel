<?php
//App::uses('CakeEmail', 'Network/Email');

class NoticesController extends AppController {
 
    public function beforeFilter() {
        parent::beforeFilter();
    }
    
    public function index() {
        
        $this->Paginator->settings = array(
                'conditions' => array(
                    array('OR' => array('user_id' => $this->auth['id'] , 'user_id IS NULL')),
                    array('Notice.created >' => date('Y-m-d H:i:s',strtotime('-6 month')))),
                'order' => array('Notice.created' => 'desc')
            );


        $data = $this->Paginator->paginate('Notice');
        $this->set('notices', $data);
        $this->set('exflg', $this->notice_flash);
    
    }
    
    
    public function admin_delete($id = null) {
    
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');
        $this->autoRender = false;
        
        $this->Notice->id = $id;
        if (!$this->Notice->exists()) {
            throw new NotFoundException(__('Invalid Notice'));
        }
        if ($this->Notice->delete()) {
            $this->Flash->success('お知らせが削除されました');
            return $this->redirect(array('action' => 'add'));
            
        }
        $this->Flash->error('お知らせ削除に失敗しました');
        return $this->redirect(array('action' => 'add'));
    }
    
    public function admin_add() {
    
        $this->admin_edit();
        $this->render('admin_edit');
        
    }

    public function admin_edit($id = null) {
    
         // 表示文言の設定
        if($this->action == 'admin_edit'){
            $this->Notice->id = $id;
            if (!$this->Notice->exists()) {
                throw new NotFoundException(__('Invalid Notice'));
            }
        }
        
        // 一覧を表示
        $data = $this->Notice->find('all',array(
                'fields' => 'Notice.id,Notice.title,Notice.content,Notice.created,Notice.user_id,Notice.start_date,Notice.end_date,User.shop_name',
                'conditions' => array('Notice.created >' => date('Y-m-d H:i:s',strtotime('-6 month'))),
                'joins' => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'users', 
                            'alias' => 'User',  
                            'conditions' => array('Notice.user_id = User.id'),
                        ),
                   ),
                'order' => array('Notice.created' => 'desc'),
            ));
            
        $this->set('notices', $data);
        
        // 登録
        if ($this->request->is('post') || $this->request->is('put')) {
        
            $this->loadModel('User');
            if ($this->request->data['Notice']['user_id']) {
             
                // 入力されたお客様noをuser_idに加工して検索
                $this->request->data['Notice']['user_id'] = ltrim($this->request->data['Notice']['user_id'], USER_ID_HEDDER);
                $tgt_user = $this->User->findById($this->request->data['Notice']['user_id']);
                unset($tgt_user['User']['password']);
            
                if(empty($tgt_user)){
                    $this->Flash->error('存在しないお客様No.です。');
                    return;
                }
            
            }
            $this->Notice->begin();
            try{
            
                if ($this->Notice->save($this->request->data)) {
                    
                    //メール送信
                    
                    $email_str = "";
                    /*
                    if(!empty($tgt_user)){
                     
                        $email = new CakeEmail('smtp');
                        $email->to($tgt_user['User']['email']);
                        $email->emailFormat('text');
                        $email->template('notice');
                        
                        //テンプレートへ渡す変数。[変数名=>値]
                        $email->viewVars(array(
                            'user_name'  => $tgt_user['User']['shop_name'].' '.$tgt_user['User']['responsible_name'].' 様',
                            'content'    => $this->request->data['Notice']['content'],
                        ));
                        
                        $email->subject(SITE_NAME.'からのお知らせ | '.$this->request->data['Notice']['title']);
                        $email->send();
                        
                        $email_str = " (メールが送信されました。)";
                    }
                    */
                    
                    $this->Notice->commit();
                    
                    $this->Flash->success('お知らせが更新されました。'.$email_str);
                    return $this->redirect(array('action' => 'add'));
                }
                
                
                
                $this->Flash->error('お知らせの更新に失敗しました。');
                return;
            
        
            } catch(Exception $e) {
            }
            
            $this->Notice->rollback();
            $this->Flash->error('お知らせ更新に失敗しました。エラー内容をご確認ください。');
            return;
        
        
        } else{
            $this->request->data = $this->Notice->findById($id);
        
        }

    
    }
}