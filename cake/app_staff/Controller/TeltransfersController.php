<?php

class TeltransfersController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
                
        $this->Auth->allow('add','tropo_call','tropo_call_transfer','tropo_call_transfer_err','tropo_call_transfer_hangup','tropo_recdata');
    }

    public function index() {
    
        if($this->auth['status'] == 0){
        	return $this->redirect(array('controller'=>'users','action' => 'edit'));
        }elseif($this->auth['status'] == 1){
            return $this->redirect(array('controller'=>'pages','action' => 'status_1'));
        }elseif($this->auth['status'] == 2){
            return $this->redirect(array('controller'=>'users','action' => 'edit'));
        }elseif($this->auth['status'] == 3){
            return $this->redirect(array('controller'=>'teltransfers','action' => 'new'));
        }elseif($this->auth['status'] == 4){
            return $this->redirect(array('controller'=>'pages','action' => 'status_4'));
        }
        
        $data = $this->Teltransfer->find('all',array(
                'conditions' => array('user_id' => $this->auth['id']),
                'limit' => 100,
                'order' => array('num' => 'asc')
            ));
        $this->set('teltransfers', $data);
        $this->set('back_btn', array('controller'=>'telephones','action'=>'index'));
        
    }
    
    public function new() {
    
    	$auth = $this->Auth->user();
    	$err_message = '';
    
        // 表示文言の設定
        if($this->action == 'edit'){
            //$this->Teltransfer->id = $id;
            //if (!$this->Teltransfer->exists()) {
            //    throw new NotFoundException(__('Invalid teltransfer'));
            //}
            $this->set('tstring', '転送設定');
            $this->set('bstring', '更新');
            $this->set('tstring2', '転送先の電話番号を設定します。');
            
        }else{
        
        	$this->layout = 'user_add';
            $this->set('tstring', 'サービス開始お手続き(1/3)');
            $this->set('bstring', 'この内容で転送先を登録して転送時間の設定へ進む');
            $this->set('tstring2', $auth['shop_name'].' '.$auth['responsible_name'].' 様　サービスをカスタマイズするための設定を行ってください。');
            
        }
        
        $teltransfer_data = array();
        if ($this->request->is('post') || $this->request->is('put')) {

            $row_cnt = count($this->request->data['Teltransfer']['num']);
            $date['TeltransferSave'] = array();
            
            $dataSource = $this->Teltransfer->getDataSource();
                
            try{
                if(is_array($this->request->data['Teltransfer']['num'][0])){
                     throw new Exception('make Teltransfer date fail');
                }
                
                $dataSource->begin();
                
                for($i=0; $i<$row_cnt; $i++){
                    
                    if(array_key_exists($i,$this->request->data['Teltransfer']['num'])){
                    
                        if($this->request->data['Teltransfer']['num'   ][$i] > 0
                        || $this->request->data['Teltransfer']['telno' ][$i] > 0
                        || $this->request->data['Teltransfer']['memo'  ][$i] > 0
                        || $this->request->data['Teltransfer']['active_value'][$i] > 0
                        ){
                            $date['TeltransferSave'][] = array(
                                'user_id'   => $this->request->data['Teltransfer']['user_id'],
                                'num'       => $i + 1,
                                'telno'     => $this->request->data['Teltransfer']['telno'  ][$i],
                                'memo'      => $this->request->data['Teltransfer']['memo'   ][$i],
                                'active'    => $this->request->data['Teltransfer']['active_value' ][$i],
                            );
                        }
                    }
                }
                
                // Teltransferの対象データをいったんクリア
                if (!$this->Teltransfer->deleteAll(array('user_id' =>  $this->request->data['Teltransfer']['user_id'] ))) {
                    throw new Exception('Teltransfer delete all fail');
                }
                
                if ($this->Teltransfer->saveAll($date['TeltransferSave'])) {
                    $dataSource->commit();
                    if($this->action == 'new'){
                        return $this->redirect(array('controller'=>'voicemails','action' => 'index'));
                    }else{
                    	return $this->redirect(array('action' => 'index'));
                    }
                }
                
            } catch(Exception $e) {
                //print_r($e);
                $err_message = $e->getMessage();
            }
            
            $dataSource->rollback();
            
            $this->Flash->error('転送番号の登録に失敗しました。'. $err_message);
            return $this->redirect(array('action' => 'index'));
            
            
        }else{
        
            // 編集画面に渡す売りデータ
            $rets = $this->Teltransfer->find('all',array('fields'=>array('num','telno','memo','active'),'conditions' =>array('user_id' => $auth['id'] ),'order' => array('num' => 'asc')));
            $i = 0;
            foreach($rets AS $ret){
                $teltransfer_data['num'][$i]      = $ret['Teltransfer']['num'];
                $teltransfer_data['telno'][$i]    = $ret['Teltransfer']['telno'];
                $teltransfer_data['memo'][$i]     = $ret['Teltransfer']['memo'];
                $teltransfer_data['active'][$i]   = $ret['Teltransfer']['active'];
                $i++;
            }

        }

        $this->set('teltransfer_data', json_encode($teltransfer_data));
    
    }
    
    
    public function view($id = null) {
        $this->Teltransfer->id = $id;
        if (!$this->Teltransfers->exists()) {
            throw new NotFoundException(__('Invalid Teltransfers'));
        }
        $this->set('teltransfers', $this->Teltransfer->findById($id));
    }

    public function add() {
    
        $this->edit();
        $this->render('edit');
        
    }

    public function delete($id = null) {
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');
        $this->autoRender = false;
        
        $this->Teltransfer->id = $id;
        if (!$this->Teltransfer->exists()) {
            throw new NotFoundException(__('Invalid Teltransfers'));
        }
        if ($this->Teltransfer->delete()) {
            //$this->Flash->success('転送先が削除されました');
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error('転送先削除に失敗しました');
        return $this->redirect(array('action' => 'index'));
    }

    public function active($id = null,$onoff = null) {
        
        // Prior to 2.5 use
        $this->request->onlyAllow('post');
        
        $this->autoRender = false;
        $ret = $this->Teltransfer->findById($id);
        $data['Teltransfer']['id'] = $id;
        if($onoff){
        	$data['Teltransfer']['active'] = $onoff;
        }else{
        	$data['Teltransfer']['active'] = abs(($ret['Teltransfer']['active']) - 1);
        }
        if ($this->Teltransfer->save($data['Teltransfer'],false)) {
        }
    }

    public function edit() {
        $this->new();
        $this->render('new');
        
    }
    


}