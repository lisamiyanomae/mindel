<?php
App::uses('CakeEmail', 'Network/Email');

class ContactsController extends AppController {
    
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    public function contact() {
    
        // ボタン押下
        if ($this->request->is('post') || $this->request->is('put')) {
                 
            //読み込む設定ファイルの変数名を指定
            $email = new CakeEmail('smtp');
            $email->to(ADMIN_MAIL);

            //HTMLorテキストメール
            $email->emailFormat('text');
            $email->template('admin_contact');
            $email->viewVars(array(
                'admin_url'              => Router::url('/admin/', true),
                'user_id'                => USER_ID_HEDDER .sprintf('%0'.USER_ID_DIGIT.'d',$this->auth['id']), 
                'responsible_name'       => $this->auth_staff['showname'],
                'shop_name'              => $this->auth['shop_name'],
                'company_name'           => $this->auth['company_name'],
                'email'                  => $this->auth['email'],
                'tropo_telno'            => $this->auth['tropo_telno'],
                'content'                => $this->request->data['Contacts']['content'],
            ));
            $email->subject('スタッフアカウントよりお問い合わせ');
            $email->send();

            return $this->redirect(array('action' => 'contact_end'));
            
        }
    
    }
    
    public function contact_end() {
    
    }
    

}