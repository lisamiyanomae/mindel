<?php
class StaffsController extends AppController {
    
    public $uses = array('Staff');
    
    public function beforeFilter() {
        parent::beforeFilter();
        
        Security::setHash('blowfish');
        
        $this->Auth->allow('add','login','logout','thanks');
        
    }

    public function login() {
    
        if ($this->request->is('post')) {
            
            if ($this->Auth->login()) {
            
                $auth = $this->Auth->user();
                
                $this->log("ログイン:".$auth['username'],'app');
                $this->redirect($this->Auth->redirect());
            } else {
            
                $this->Flash->error('スタッフIDとパスワードを正しく入力してください。');
            }
        }
    }
    
    public function logout() {
        $this->redirect($this->Auth->logout());
    }
    


}