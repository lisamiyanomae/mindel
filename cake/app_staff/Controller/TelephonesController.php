<?php
class TelephonesController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
    }
    

    // 着信履歴
    public function index() {
        
        // 本運用年月日を加味
        $conditions = array('Telephones.user_id' => $this->auth['id']);
        if($this->auth['formally_start_date']){
            $conditions = array_merge($conditions,array('Telephones.created >=' => $this->auth['formally_start_date'].' 00:00:00'));
        }

        $this->loadModel('Telephones'); 
        $this->Paginator->settings = array(
            'fields' => 'Telephones.id,Telephones.recording_url,Telephones.telno,Telephones.teltransferno,Telephones.start_time,Telephones.end_time,Telephones.kind,Telephones.memo,Teltransfer.memo,Teltransfer1.memo',
            'conditions' => $conditions,
            'order' => array('Telephones.created' => 'desc'),
            'limit' => 30, 
            'joins' => array(
                        array (
                        'type' => 'LEFT',    
                        'table' => 'teltransfers', 
                        'alias' => 'Teltransfer',  
                        'conditions' => "replace(Teltransfer.telno,'-','') = replace(Telephones.teltransferno,'+81','0') AND Teltransfer.user_id =".$this->auth['id'],
                        ),
                        array (
                        'type' => 'LEFT',    
                        'table' => 'teltransfers', 
                        'alias' => 'Teltransfer1',  
                        'conditions' => "replace(Teltransfer1.telno,'-','') = replace(Telephones.telno,'+81','0') AND Teltransfer1.user_id =".$this->auth['id'],
                        ),
                   ),
        );
        $data = $this->Paginator->paginate('Telephones');
        $this->set('telephones', $data);

        // メモ更新
        if ($this->request->is('post') || $this->request->is('put')) {
            if($this->request->data['Telephones']['id'] > 0){
              if ($this->Telephones->save($this->request->data)) {
                  //$this->Flash->success('メモを更新しました');
              }
            }
            return $this->redirect(array('action' => 'index/page:'.$this->request->params['named']['page']));
            //$this->Flash->error('メモの更新に失敗しました。');
        }

    }

}