<?php 
$this->assign('title', 'お問合せ');
?>
<div id="main" class="mypage">
    
<div class="section">
    <div class="inner">
        
        <h2>お問合せ</h2>
        
        <?php echo $this->Form->create('Contacts',array('inputDefaults' => array('label' => false))); ?>
        <div class="w400 mb30">
            <p class="mb30">以下に記入の上、送信を押してください。システム不具合等に関するお問い合わせは、使用OS／ブラウザも併せてご記入ください。</p>
            <?php echo $this->Form->input('content' ,array('id' =>'content','type'=>'textarea' ,'placeholder' => '','escape'=>false)); ?>
        </div>        
        <div class="registration"><?php echo $this->Form->end('送信'); ?> </div>
        <p class="mt30">回答は店舗に登録されたメールアドレス <?php echo $user['email']?> に送信されます。</p>
    </div><!--/inner-->
</div><!--/-->


</div><!--/End main-->