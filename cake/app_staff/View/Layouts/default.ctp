<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$this->Html->script('jquery-2.1.0.min', array('inline' => false));
$this->Html->script('main', array('inline' => false)); 

?>

<!doctype html>
<html lang="ja" class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">


<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">

    <?php echo $this->Html->charset(); ?>
    <title>
        <?php if($this->fetch('title') != 'Home'){echo $this->fetch('title').' | ';}  ?><?php echo SITE_NAME; ?>
    </title>


<meta name="description" content="<?php echo SITE_NAME; ?>スタッフページ">

    <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    
<link rel="apple-touch-icon" href="/apple-touch-icon180.png">
<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/minderu_theme/images/apple-touch-icon57.png">
<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/minderu_theme/images/apple-touch-icon72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/minderu_theme/images/apple-touch-icon76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/minderu_theme/images/apple-touch-icon114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/minderu_theme/images/apple-touch-icon120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/minderu_theme/images/apple-touch-icon144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/minderu_theme/images/apple-touch-icon152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon180.png">

<script type="text/javascript">
if ((navigator.userAgent.indexOf('iPhone') > 0) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('Android') > 0) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes,maximum-scale=1.0,minimum-scale=1.0">');
    }else{
        document.write('<meta name="viewport" content="width=1200">');
    }
</script>
<link rel="shortcut icon" href="/wp-content/themes/minderu_theme/images/favicon5.ico">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"-->
<link rel="stylesheet" href="/wp-content/themes/minderu_theme/style.css">
<link rel="stylesheet" href="/wp-content/themes/minderu_theme/contents.css">
<link rel="stylesheet" href="/wp-content/themes/minderu_theme/mobile.css">
<link rel="stylesheet" href="/wp-content/themes/minderu_theme/mypage.css">

<!--[if lt IE 9]>
<script src="js/html5shiv.min.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body id="bodytop">
<header>
<div id="mypageHeader">
    <div id="pg_ttl"><a href="<?php echo $this->Html->url('/', true); ?>telephones"><img src="/wp-content/themes/minderu_theme/images/staff/staffpageLogo.svg"></a></div>
</div>


<?php if(isset($user)){ ?>

<div id="gnav">
<nav>

    <ul id="nav-menu">
        <li><?php echo $this->Html->link('着信履歴',    array('controller'=>'telephones','action'=>'index')); ?></li>
        <li class="hover-click"><?php echo $this->Html->link('設定・変更',  array('controller'=>'pages' ,'action'=>'index')); ?>
           <ul>
           <li><?php echo $this->Html->link('転送・発信',  array('controller'=>'teltransfers' ,'action'=>'index')); ?></li>
           </ul>
        </li>
    </ul>
    <div id="btn_logout"><?php echo $this->Html->link('ログアウト',  array('controller'=>'staffs','action'=>'logout')); ?></div>
</nav>
</div><!--/gnav-->

<div id="accountinfo">

    <p class="contractor"><span class="my_telnum"><?php echo $user['tropo_telno']; ?></span><span class="company_name"><?php echo $user['shop_name']; ?></span> 様</span></p>
    <div class="info_alert">
    <span><?php 
    if($notice_cnt>0){
    	echo $this->Html->link("<i class='fas fa-exclamation-circle fa-yellow fa-1x'></i> お知らせがあります ≫",  array('controller'=>'notices','action'=>'index'),array('escape'=>false)); 
    }else{
    	echo $this->Html->link("お知らせはこちら ≫",  array('controller'=>'notices','action'=>'index'),array('escape'=>false)); 
    }
    
    ?></span>
    <?php echo $this->Html->link($this->Html->image('icon_alertmessage.png'),array('controller'=>'notices','action'=>'index'),array('escape'=>false));?>
    </div>
</div>


</header>
<?php } ?>


<main>
<div id="main" class="mypage">
    <?php if($acc_stop_msg){echo "<p class='tyuui mt20'>".$acc_stop_msg."</p>";} ?>
    
<?php //echo $this->Flash->render(); ?>
<?php echo $this->fetch('content'); ?>
<?php //echo $this->element('sql_dump'); ?>
</div><!--/End main-->
</main>

<footer>
<div id="footer" class="mypagefooter">
    <div class="inner">
        <div class="customercenter">
            <p class="customer_qa"><?php echo $this->Html->link('≫ 操作Q＆Aはこちら', empty($_SERVER["HTTPS"]) ? "http://" : "https://".$_SERVER["HTTP_HOST"].'/qa/#q03',array( 'target' => '_blank' )); ?></p>
            <p class="customer_time">カスタマーセンター（月～金曜日9:00-18:00）</p>
            <p class="customer_tel"><a href="tel:05031963580">050-3196-3580</a></p>
        </div>
<?php if(isset($user)){ ?>
        <div class="contactform"><?php echo $this->Html->link('お問い合わせフォームはこちら',    array('controller'=>'contacts','action'=>'contact')); ?></div>
<?php } ?>
        <p class="copy">Copyright &copy <?php echo SITE_NAME; ?> all right reserved.</p>
    </div>
</div>

</footer>


<div id="pagetop"><a href="#bodytop"><i class="fa fa-angle-up" aria-hidden="true"></i></a></div>


<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script-->
<!--script type="text/javascript" src="/wp-content/themes/minderu_theme/js/main.js"></script-->




</body>
</html>