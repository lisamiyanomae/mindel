<?php
$this->Html->addCrumb('転送・発信設定');
$this->assign('title', '転送・発信設定');
?>
<div class="section">
    <div class="inner">
        
        <h2>転送・発信設定</h2>    
        
        <p class="mb30">転送先の電話番号を設定します。着信は以下の電話番号へ同時転送されます。<br>この電話番号から発信すると相手に050番号が表示されます。</p>
        
        <div class="center tyuui"><?php echo $this->Flash->render(); ?></div>
        
        <div class="table transfer">
            <table>
                <thead>
                    <tr>
                        <th>転送 (<label>全選択<input type="checkbox" id="all" /></label>)</th>
                        <th>登録<br class="sp_only">電話番号</th>
                        <th>名称</th>
                        
                        <!--th></th-->
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($teltransfers as $teltransfer): ?>
                    <tr>
                        <td class="checkbox"><label for="active_check<?php echo $teltransfer['Teltransfer']['id']; ?>" ><input class="active_check" id="active_check<?php echo $teltransfer['Teltransfer']['id']; ?>" type="checkbox" <?php if($teltransfer['Teltransfer']['active']){echo "checked=checked";} ?> value="<?php echo $teltransfer['Teltransfer']['id']; ?>"><span class="onoff" id="onoff<?php echo $teltransfer['Teltransfer']['id']; ?>"><?php if($teltransfer['Teltransfer']['active']){echo '<i class="fas fa-bell fa-1x my-skyblue"></i>'; }else{ echo '<i class="far fa-bell-slash fa-1x my-skyblue"></i>';} ?></span></label></td>
                        
                        <td><?php echo $teltransfer['Teltransfer']['telno']; ?></td>
                        <td><?php echo $teltransfer['Teltransfer']['memo']; ?></td>
                        
                        <!--td><span ><?php //echo $this->Form->postLink('削除',array('action'=>'delete',$teltransfer['Teltransfer']['id']),array('class'=>'button blue'),'本当に '.$teltransfer['Teltransfer']['memo'].' を削除しますか?');?></span></td-->
                    </tr>
                    <?php endforeach; ?>
                    <?php unset($teltransfer); ?>
                </tbody>
            </table>
            
            <!--div class="mt20 mb20"><?php echo $this->html->link('追加・編集', array('action'=>'edit'), array('class'=>'button gray','target'=>'_self')); ?></div-->
            
        </div>
        
        <p class="mt30 mb30 txtleft w600">
            ※アイコンのクリックで転送の有無を設定できます。<i class="fas fa-bell fa-1x my-skyblue"></i> 転送する / <i class="far fa-bell-slash fa-1x my-skyblue"></i> 転送しない<br>
            ※推奨10台<br>
            ※一時的に転送しない時、発信のみ行いたいときは転送チェックを外して下さい。<br>
            ※転送先が1台も無い場合は、転送時間に関わらず自動応答メッセージが流れます。<br>
        </p>
        <div class="backpage mt20"><?php print($this->Html->link('戻る', array('controller' => $back_btn['controller'], 'action' =>  $back_btn['action'],'admin' => false),array('class'=>'button darkblue'))); ?></div>
    </div><!--/inner-->
</div><!--/-->
<style>
.active_check{
  display:none;
}
.onoff{
  cursor:pointer;
}
.my-skyblue {
  color: #666;
}

</style>
<script>
jQuery(function($){
    $(document).on("click", ".active_check", function () {
        var url = "<?php echo $this->Html->webroot ?>/teltransfers/active/" + $(this).val();
        $.ajax({
            type: "post",
            url:  url,
            async: true
        });

        if($(this).prop("checked")){
            $("#onoff"+$(this).val()).html('<i class="fas fa-bell fa-1x my-skyblue"></i>');
        }else{
            $("#onoff"+$(this).val()).html('<i class="far fa-bell-slash fa-1x my-skyblue"></i>');
        }

    });
    $('#all').on('change', function() {
        $('.active_check').prop('checked', this.checked);

        if($(this).prop("checked")){
            $(".onoff").html('<i class="fas fa-bell fa-1x my-skyblue"></i>');
        }else{
            $(".onoff").html('<i class="far fa-bell-slash fa-1x my-skyblue"></i>');
        }
        $(".active_check").each(function() {
        
            if($("#all").prop("checked")){
                var url = "<?php echo $this->Html->webroot ?>/teltransfers/active/" + $(this).val()+"/1";
            }else{
                var url = "<?php echo $this->Html->webroot ?>/teltransfers/active/" + $(this).val()+"/0";
            }
            $.ajax({
                type: "post",
                url:  url,
                async: true
            });
        });
    });
});
</script>