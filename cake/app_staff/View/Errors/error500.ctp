<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$this->assign('title', 'エラーが発生しました');
?>

<div id="main">

	<div class="section">
		<div id="searchlist" class="inner">
		
		<h2>エラーが発生しました</h2>
		<p class="404">誠に申し訳ございませんが、システムエラーが発生したため、このページは閲覧できません。</p>
		<p class="mb30">時間を空けて、もう一度お試しください。<br>
		トップページからお探しください。</p>
		<div class="submitbtn registration"><a class="button darkblue" href="<?php echo $this->Html->url('/', true); ?>">みんなにでんわ転送マイページトップへ</a></div>
		</div>
	</div>
</div><!--/End main-->
