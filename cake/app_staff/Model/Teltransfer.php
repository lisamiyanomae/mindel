<?php
class Teltransfer extends AppModel {

    public $validate = array(
       'telno' => array(
            'default' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^[0-9-]{7,14}$/i',
                'message' => 'ご転送先電話番号は半角数字で必ず入力して下さい。',
            ),
        ),
        
      
    );

}
