<?php
App::uses('CakeEmail', 'Network/Email');

class ContactsController extends AppController {
    
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();

    }
    
    public function contact() {
    
        // ボタン押下
        if ($this->request->is('post') || $this->request->is('put')) {
            
            if($this->request->data['Contacts']['content']){
            
                //読み込む設定ファイルの変数名を指定
                $email = new CakeEmail('smtp');
                $email->to(ADMIN_MAIL);

                //HTMLorテキストメール
                $email->emailFormat('text');
                $email->template('admin_contact');
                $email->viewVars(array(
                
                    'admin_url'  => dirname(Router::url('/', true)).'/mypage/admin/',
                    'id'         => $this->auth_comp['id'],
                    'showname'   => $this->auth_comp['showname'],
                    'email'      => $this->auth_comp['email'],
                    'content'    => $this->request->data['Contacts']['content'],
                    
                ));
                $email->subject('マルチアカウントよりお問い合わせがありました');
                $email->send();

                return $this->redirect(array('action' => 'contact_end'));
            
            }else{
            
                $this->Flash->error('お問い合わせ内容を入力して下さい。');
                return $this->redirect(array('action' => 'contact'));
            }
            
        }
    
    }
    
    public function contact_end() {
    
    }
    

}