<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller','Telephone');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
 App::uses('Sanitize', 'Utility');
class AppController extends Controller {

    public $helpers = array('Html','Form');
    public $components = array(
        'Paginator',
        'Session',
        'Flash',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
        )
    );
    
    
    public $auth;
    //public $auth_admin;
    public $notice_flash;
    public $users;
    
    public function beforeFilter() {
    
        parent :: beforeFilter();
        
        $acc_stop_msg = "";

        $this->Auth->authenticate = array(
        'Form' => array(
                'passwordHasher' => 'Blowfish',
                    'userModel' => 'Company'
                //'fields' => array('username' => 'username','password'=>'password')
            )
        );
        
        
        $this->Auth->loginAction    = array('controller' => 'companies',  'action' => 'login',  );
        $this->Auth->loginRedirect  = array('controller' => 'telephones', 'action' => 'index',  );
        $this->Auth->logoutRedirect = array('controller' => 'companies',  'action' => 'login',  );

        Router::connect('/', array('controller' => 'telephones', 'action' => 'index', )); 
        
        AuthComponent::$sessionKey  = "Auth.Company";

        $this->auth_comp = $this->Auth->user();
        $this->set('comp', $this->auth_comp);

        
        // 子アカウントを取得
        $this->loadModel('CompanyUser');
        $users = $this->CompanyUser->find('list',array(
            'fields' => 'User.id,User.shop_name',
            'joins' => array(
                        array (
                            'type' => 'LEFT',    
                            'table' => 'users', 
                            'alias' => 'User',  
                            'conditions' => 'CompanyUser.user_id = User.id'
                            ),
            ),

            'conditions' => array('company_id'=>$this->auth_comp['id'])
        
        ));
        
        
        $this->set('users', $users);
        $this->users = $users;
        
        // ユーザーをカンマ区切りで取得
        $usrlist = array();
        foreach($users AS $id => $data){
            $usrlist[] = $id;
        }
        
        if(count($usrlist) > 0){
            $str_usrlist = implode(',',$usrlist);
        }else{
            $str_usrlist = "";
        }
        
        // お知らせ注目数カウント
        if($this->auth_comp['id'] ){
        
            $this->loadModel('Notice');
            if($this->Notice->find('count') > 0){
            
                $conditions  = " created > '".date('Y-m-d H:i:s',strtotime('-6 month')) ."'" ;
                $conditions .= " AND ( ";
                $conditions .= "  (start_date <= '" .date('Y-m-d'). "' AND end_date >='" .date('Y-m-d'). "') OR ";
                $conditions .= "  (start_date <= '" .date('Y-m-d'). "' AND end_date IS NULL) OR ";
                $conditions .= "  (start_date IS NULL AND end_date >='" .date('Y-m-d'). "')";
                $conditions .= "  ) ";
                
                if($str_usrlist != ""){
                
                $conditions .= " AND (user_id IN (".$str_usrlist .")  OR (user_id IS NULL))";
                }
                
                $ret = $this->Notice->find('list',array(
                    'fields' => 'id',
                    'conditions' => $conditions
                    
                ));
                $this->notice_flash = $ret;
                $this->set('notice_cnt', count($ret));
                
            }else{
                $this->set('notice_cnt', -1);
            
            }
            
            //if($this->auth['status'] != 5){ //アカウント停止中
            //    $acc_stop_msg = "お客様のアカウントは現在停止中です。カスタマーセンターにご連絡ください。";
            //}
            
        }
        
        $this->set('acc_stop_msg', $acc_stop_msg);
        
    }

    public function __sanitize() {
        $this->data = Sanitize::clean($this->data, array('remove_html' => true, 'encode' => true, 'escape' => false));
    }

}
