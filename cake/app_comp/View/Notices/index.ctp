<?php 
$this->assign('title', 'お知らせ');
?>

<div id="main" class="mypage">
    
<div class="section">
    <div class="inner">
        
        <h2>お知らせ</h2>
        <p class="mb30">カスタマーセンターからのお知らせ</p>
        <div id="infobox">
        	<!-- 注目マーク付き -->
            <?php foreach ($notices as $notice): 
            if(array_key_exists($notice['Notice']['id'],$exflg)){   ?>
            <dl>
                <dt><span><?php echo date('Y.m.d',strtotime($notice['Notice']['created'])); ?></span>
                <?php  echo '<i class="fas fa-exclamation-circle fa-yellow fa-1x"></i> '; echo $notice['Notice']['title'];  ?>
                </dt>
                <dd>
                <p><?php echo $notice['Notice']['content']; ?></p>
                </dd>
            </dl>
            <?php }endforeach; ?>
            
            <!-- 注目マークなし -->
            <?php foreach ($notices as $notice):
            if(!array_key_exists($notice['Notice']['id'],$exflg)){   ?>
            <dl>
                <dt><span><?php echo date('Y.m.d',strtotime($notice['Notice']['created'])); ?></span>
                <?php echo $notice['Notice']['title'];  ?>
                </dt>
                <dd>
                <p><?php echo $notice['Notice']['content']; ?></p>
                </dd>
            </dl>
            <?php }endforeach; ?>
            
            <?php unset($notice); ?>
            <span><?php echo $this->Paginator->prev(  '< 前のページ',  null,  null,  array('class' => 'disabled')); ?></span>
            <span><?php echo $this->Paginator->numbers();?></span>
            <span><?php echo $this->Paginator->next(  '次のページ >',  null,  null,  array('class' => 'disabled'));?></span>
            <span><?php echo $this->Paginator->counter(); ?></span>

        </div>

    </div><!--/inner-->
</div><!--/-->


</div><!--/End main-->



