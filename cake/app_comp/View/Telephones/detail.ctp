<?php 
$this->assign('title', 'ご利用明細');
if(empty($user)){ 
	$user['User']['shop_name']='未設定';
	$user['User']['tropo_telno']='未設定';
	$user['User']['id'] = null;
}
?>
<div id="main" class="mypage">
    
<div class="section">
    <div class="inner">
        
        
        <h2><?php echo $user['User']['shop_name']; ?>(<?php echo $user['User']['tropo_telno']; ?>) ご利用明細 (<?php echo substr($yyyymm,0,4).'年';echo substr($yyyymm,5,2).'月'; ?>)</h2>
        
        
        
        <p class="mb30">ご利用料金のお引落日は、カード会社によって異なります。</p>
        
        <div class="w600 mb20">
        <?php echo $this->Form->create('Telephone',array('id'=>'form_telephone','inputDefaults' => array('label' => false, 'div' => false)));?>
        <?php echo $this->Form->input('Telephone.yyyymm', array( 'options' => $yyyymm_list ,'value'=>$yyyymm,'empty' => false ,'onChange'=>'this.form.submit()'));  ?>
        </form>
        </div>
        <div class="w600 mb40">
        
            <?php 
                foreach ($tgt_w as $week): 
                // 料金の集計
                if(array_key_exists($week,$telephones)){
                    $call_fee = $telephones[$week];
                }else{
                    $call_fee = 0;
                }
                $auto_fee = 0;
                if(array_key_exists($week,$auto_responces)){
                    $auto_fee  = $auto_responces[$week] * AUTO_RESPONCE_FEE;
                }
                $total     = $this->Number->currency($call_fee + $auto_fee,'JPY',array('wholeSymbol'=>"",'places'=>0));
                $tax       = $this->Number->currency(($call_fee + $auto_fee) * TAX,'JPY',array('wholeSymbol'=>"",'places'=>0));
                $total_all = ($call_fee + $auto_fee) * (1 + TAX);
                
                if($total_all > 0){
                
            ?>
            <p class="meisai_data"><?php echo date('Y.m.d', strtotime($week));  ?> ～ <?php echo date('Y.m.d', strtotime($week. "+6 day"));  ?></p>
            <div class="table meisai">
            <table class="sp_border">
                <tbody>
                    <tr>
                        <th>
                            <p><?php if(array_key_exists($week,$orders)){echo 'no.'.ORDER_ID_HEDDER .sprintf('%0'.ORDER_ID_DIGIT.'d',$orders[$week]);}else{echo "(請求未確定)";} ?></p>
                            <dl>
                                <dt>請求日</dt>
                                <dd><?php echo date('m/d', strtotime($week. "+7 day"));  ?></dd>
                                <dt>支払期限</dt>
                                <dd><?php echo date('m/d', strtotime($week. "+14 day"));  ?></dd>
                            </dl>
                        </th>
                        <td>
                            <ul class="kamokulist">
                                <li><span class="kamoku">通信料</span><span class="price"><?php echo $this->Number->currency($call_fee,'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></span></li>
                                <li><span class="kamoku">自動応答</span><span class="price"><?php echo $auto_fee; ?></span></li>
                                <!--li><span class="kamoku">通話記録</span><span class="price">次バージョン</span></li-->
                            </ul>
                            <ul class="total">
                                <li><span class="kamoku">小計</span><span class="price"><?php echo $total; ?></span></li>
                                <li><span class="kamoku">税額</span><span class="price"><?php echo $tax; ?></span></li>
                                <li><span class="kamoku">合計</span><span class="price"><?php echo  $this->Number->currency($total_all,'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></span></li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            </div>

            <?php } endforeach; ?>
            <?php unset($week); ?>


            <?php if(!empty($order_monthly)){ ?>
            <p class="meisai_data"><?php echo date('Y.m.d', strtotime($order_monthly['start_date']));  ?> ～ <?php echo date('Y.m.d', strtotime($order_monthly['end_date']));  ?></p>
            <div class="table meisai">
            <table class="sp_border">
                <tbody>
                    <tr>
                        <th>
                            <p><?php echo 'no.'.ORDER_ID_HEDDER .sprintf('%0'.ORDER_ID_DIGIT.'d',$order_monthly['id']); ?></p>
                            <dl>
                                <dt>請求日</dt>
                                <dd><?php echo date('m/d', strtotime($order_monthly['invoice_date']));  ?></dd>
                                <dt>支払期限</dt>
                                <dd><?php if($order_monthly['limit_date']){echo date('m/d', strtotime($order_monthly['limit_date']));}else{echo date('m/d', strtotime($order_monthly['invoice_date']));}  ?></dd>
                            </dl>
                        </th>
                        <td>
                            <ul class="kamokulist">
                                <li><span class="kamoku">基本料</span><span class="price"><?php echo $this->Number->currency($order_monthly['sum_price'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></span></li>
                            </ul>
                            <ul class="total">
                                <li><span class="kamoku">小計</span><span class="price"><?php echo $this->Number->currency($order_monthly['sum_price'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></span></li>
                                <li><span class="kamoku">税額</span><span class="price"><?php echo $this->Number->currency($order_monthly['tax'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></span></li>
                                <li><span class="kamoku">合計</span><span class="price"><?php echo $this->Number->currency($order_monthly['amount'],'JPY',array('wholeSymbol'=>"",'places'=>0)); ?></span></li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            </div>
            <?php } ?>


        </div>
        
            <div class="mt30">
            <?php echo $this->Form->create('CompanyUser',array('id'=>'varidate_form','inputDefaults' => array('label' => false,'div'=>false),'novalidate' => true,'class'=>'primary-form')); ?>
            <?php echo $this->Form->input('user_id',array('type'=>'select','options'=>$users,'default'=>$user['User']['id'],'label' => 'ユーザー切替 ','div'=>false,'class' => 'validate[required]')); ?>
            <?php echo $this->Form->submit('切替',array('div'=>false)); ?>
            </div>    

    </div><!--/inner-->
</div><!--/-->


</div><!--/End main-->