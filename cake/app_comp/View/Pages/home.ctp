<?php 
$this->assign('title', '設定変更');
?>

<div id="main" class="mypage">
	
<div class="section">
	<div class="inner">
		
		<h2>設定変更</h2>	
		<p class="mb30">各種設定ができます</p>
		<ul class="contentMenu row menu1of5">
			<li><?php print($this->Html->link('転送／発信'    , array('controller' => 'teltransfers','action' => 'index' ))); ?></li>
			<li><?php print($this->Html->link('時間・応答'    , array('controller' => 'voicemails'  ,'action' => 'index' ))); ?></li>
			<li><?php print($this->Html->link('お支払い'      , array('controller' => 'orders'      ,'action' => 'change'))); ?></li>
			<li><?php print($this->Html->link('アカウント'    , array('controller' => 'users'       ,'action' => 'edit'  ))); ?></li>
			<li><?php print($this->Html->link('解約'          , array('controller' => 'users'       ,'action' => 'leave' ))); ?></li>
			
		</ul>
		
	</div><!--/inner-->
</div><!--/-->


</div><!--/End main-->





