﻿/* move default 10pg */
$(function() {
/*
  var $win = $(window),
      $main = $('#main'),
      $nav = $('header'),
      navHeight = $nav.outerHeight(),
      navPos = $nav.offset().top,
      fixedClass = 'is-fixed';

  $win.on('load scroll', function() {
    var value = $(this).scrollTop();
    if ( value > navPos ) {
      $nav.addClass(fixedClass);
      $main.css('margin-top', navHeight);
    } else {
      $nav.removeClass(fixedClass);
      $main.css('margin-top', '0');
    }
  });
*/

});


$(function() {
  var contents = $(".hover-click > ul");
  $(".hover-click > a").click(function(){
    $(contents).toggle();
    return false;
  });
  $(".hover-click")
  .mouseover(function(){


    $(contents).show();
  })
  .mouseout(function(){
    $(contents).hide();
  });
});





/* =========================================================

Fix menu

=========================================================== */

$(function() {
    var topBtn = $('#fixmenu');    
    topBtn.hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 380) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
});


/* =========================================================

pagetop

=========================================================== */

$(function() {
    var topBtn = $('#pagetop');    
    topBtn.hide();
    //スクロールが100に達したらボタン表示
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            topBtn.fadeIn();
        } else {
            topBtn.fadeOut();
        }
    });
    //スクロールしてトップ
    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
});

//スムーススクロール
$(function(){
 var headerHight = 60;
   $('a[href^="#"]').on('click', function(){
      var speed = 400;
      var href= $(this).attr('href');
      var target = $(href == '#' || href == '' ? 'html' : href);
      var position = target.offset().top-headerHight;;
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});





/* =========================================================

slideopen

=========================================================== */


$(function(){
     $(".open").click(function(){
      $(".slideBox").slideToggle("slow");
     });
     
     $(".open02").click(function(){
      $(".slideBox02").slideToggle("slow");
     });     

     $(".open03").click(function(){
      $(".slideBox03").slideToggle("slow");
     });

     $(".open04").click(function(){
      $(".slideBox04").slideToggle("slow");
     });

     $(".open05").click(function(){
      $(".slideBox05").slideToggle("slow");
     });

     $(".open06").click(function(){
      $(".slideBox06").slideToggle("slow");
     });

     $(".open07").click(function(){
      $(".slideBox07").slideToggle("slow");
     });

     $(".open08").click(function(){
      $(".slideBox08").slideToggle("slow");
     });

     $(".open09").click(function(){
      $(".slideBox09").slideToggle("slow");
     });

     $(".open10").click(function(){
      $(".slideBox10").slideToggle("slow");
     });
     
     $(".open11").click(function(){
      $(".slideBox11").slideToggle("slow");
     });     
     
     $(".open12").click(function(){
      $(".slideBox12").slideToggle("slow");
     });     
     
     $(".open13").click(function(){
      $(".slideBox12").slideToggle("slow");
     });   
     
     $(".open14").click(function(){
      $(".slideBox12").slideToggle("slow");
     });        
     
     $(".open15").click(function(){
      $(".slideBox12").slideToggle("slow");
     });        
     
     $(".open16").click(function(){
      $(".slideBox12").slideToggle("slow");
     });   
     
     $(".open17").click(function(){
      $(".slideBox12").slideToggle("slow");
     });   
     
     $(".open18").click(function(){
      $(".slideBox12").slideToggle("slow");
     });   
     
     $(".open19").click(function(){
      $(".slideBox12").slideToggle("slow");
     });   
     
     $(".open20").click(function(){
      $(".slideBox12").slideToggle("slow");
     });   
});



/* =========================================================

高さ揃え

=========================================================== */
$(function(){
    //$('#qaList div').matchHeight();
    //$('.it_can').matchHeight();
});


/* =========================================================

機能比較 price

=========================================================== */

/*
    For functions getElementsByClassName, addClassName, and removeClassName
    Copyright Robert Nyman, http://www.robertnyman.com
    Free to use if this text is included
*/
function getElementsByClassName(className, tag, elm){
    var testClass = new RegExp("(^|\\s)" + className + "(\\s|$)");
    var tag = tag || "*";
    var elm = elm || document;
    var elements = (tag == "*" && elm.all)? elm.all :elm.getElementsByTagName(tag);
    var returnElements = [];
    var current;
    var length = elements.length;
    for(var i=0; i<length; i++){
        current = elements[i];
        if(testClass.test(current.className)){
            returnElements.push(current);
        }
    }
    return returnElements;
}
function addClassName(elm, className){
    var currentClass = elm.className;
    if(!new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i").test(currentClass)){
        elm.className = currentClass + ((currentClass.length > 0)? " " :"") + className;
    }
    return elm.className;
}
function removeClassName(elm, className){
    var classToRemove = new RegExp(("(^|\\s)" + className + "(\\s|$)"), "i");
    elm.className = elm.className.replace(classToRemove, "").replace(/^\s+|\s+$/g, "");
    return elm.className;
}
function hasClass(el, c) {
  if (!el || !el.className.length) return;
  var bits = el.className.split(' '), has = false;
  for (var j = 0; j < bits.length; j++) if (bits[j] === c) has = true;
  return has;
}
function activateThisColumn(column) {
  var table = document.getElementById('pricetable');
  var form = document.getElementById('formcontainer');
  /* まず、すべての他のth要素からonクラスを削除 */
  var ths = table.getElementsByTagName('th');
  for (var g=0; g<ths.length; g++) {
    removeClassName(ths[g], 'on');
    if (!hasClass(ths[g],'side')) {
      ths[g].style.display = 'none';
    }
  }
  /* 次に、すべての他のtd要素からonクラスを削除 */
  var tds = table.getElementsByTagName('td');
  for (var m=0; m<tds.length; m++) {
    removeClassName(tds[m], 'on');
    if (!hasClass(tds[m],'side')) {
      tds[m].style.display = 'none';
    }
  }
  /* 選択したth要素にonクラスを追加 */
  var newths = getElementsByClassName(column, 'th', table);
  for (var h=0; h<newths.length; h++) {
    addClassName(newths[h], 'on');
    newths[h].style.display = '';
    /* not all browsers like display = 'block' for cells */
  }
  /* 選択したtd要素にonクラスを追加 */
  var newtds = getElementsByClassName(column, 'td', table);
  for (var i=0; i<newtds.length; i++) {
    addClassName(newtds[i], 'on');
    newtds[i].style.display = '';
    /* not all browsers like display = 'block' for cells */
  }
  /* フォームを表示 */
  form.style.display = 'block';
}





/* =========================================================

Footer Fix

=========================================================== */

/*--------------------------------------------------------------------------*
 *  
 *  footerFixed.js
 *  
 *  MIT-style license. 
 *  
 *  2007 Kazuma Nishihata [to-R]
 *  http://blog.webcreativepark.net
 *  
 *--------------------------------------------------------------------------*/

new function(){
	
	var footerId = "footer";
	//メイン
	function footerFixed(){
		//ドキュメントの高さ
		var dh = document.getElementsByTagName("body")[0].clientHeight;
		//フッターのtopからの位置
		document.getElementById(footerId).style.top = "0px";
		var ft = document.getElementById(footerId).offsetTop;
		//フッターの高さ
		var fh = document.getElementById(footerId).offsetHeight;
		//ウィンドウの高さ
		if (window.innerHeight){
			var wh = window.innerHeight;
		}else if(document.documentElement && document.documentElement.clientHeight != 0){
			var wh = document.documentElement.clientHeight;
		}
		if(ft+fh<wh){
			document.getElementById(footerId).style.position = "relative";
			document.getElementById(footerId).style.top = (wh-fh-ft-1)+"px";
		}
	}
	
	//文字サイズ
	function checkFontSize(func){
	
		//判定要素の追加	
		var e = document.createElement("div");
		var s = document.createTextNode("S");
		e.appendChild(s);
		e.style.visibility="hidden"
		e.style.position="absolute"
		e.style.top="0"
		document.body.appendChild(e);
		var defHeight = e.offsetHeight;
		
		//判定関数
		function checkBoxSize(){
			if(defHeight != e.offsetHeight){
				func();
				defHeight= e.offsetHeight;
			}
		}
		setInterval(checkBoxSize,1000)
	}
	
	//イベントリスナー
	function addEvent(elm,listener,fn){
		try{
			elm.addEventListener(listener,fn,false);
		}catch(e){
			elm.attachEvent("on"+listener,fn);
		}
	}

	addEvent(window,"load",footerFixed);
	addEvent(window,"load",function(){
		checkFontSize(footerFixed);
	});
	addEvent(window,"resize",footerFixed);
	
}

