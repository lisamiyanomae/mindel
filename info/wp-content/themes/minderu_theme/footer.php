<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to finish
	/* rendering the page and display the footer area/content
	/*-----------------------------------------------------------------------------------*/
?>


<footer>
<div id="footer">
	<div class="inner">
		<div class="row">
			<div class="cont1of3">
				<p class="f_logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><span>みんなに</span>でんわ転送</a></p>
			</div>
			<div class="cont1of3">
				<dl>
					<dt>ご利用について</dt>
					<dd>
						<ul>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>service/">サービス内容</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>minderuimg/">利用イメージ</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>flow/">導入までの流れ</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>new_account/">お申し込み</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>price/">料金／解約</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>faq/">よくある質問</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>confirmation/">確認資料について</a></li>
						</ul>
					</dd>
				</dl>
				<dl>
					<dt>みんなにでんわ転送について</dt>
					<dd>
						<ul>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>news/">お知らせ</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>company/">会社概要</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>kiyaku/">契約約款／免責事項</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>privacy/">個人情報保護方針</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>low/">特定商取引に関する表記</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>policy/">反社会勢力への方針</a></li>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>sitemap/">サイトマップ</a></li>
						</ul>
					</dd>
				</dl>
			</div>
			<div class="cont1of3">
				<div class="free_account"><a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage/users/add/">サービスに申し込む</a></div>
				<div class="member_login"><a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage/users/login/">ユーザーログイン</a></div>
				<div class="customercenter">
					<p class="customer_time">カスタマーセンター（月?金曜日9:00-18:00）</p>
					<p class="customer_tel"><a href="tel:05031963580">050-3196-3580</a></p>
				</div>
				<div class="f_download"><a href="<?php echo esc_url( home_url( '/' ) ); ?>download/">印刷用資料ダウンロード</a></div>
				<div class="f_contact"><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact/">お問い合わせ</a></div>
			</div>
		</div>
		<p class="copy">Copyright &copy みんなにでんわ転送 all right reserved.</p>
	</div>
</div>
</footer>


<div id="pagetop"><a href="#bodytop"><i class="fa fa-angle-up" aria-hidden="true"></i></a></div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.matchHeight.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<script>
  $(function() {
    var $win = $(window),
        $main = $('#main'),
        $nav = $('#global-nav'),
        navHeight = $nav.outerHeight(),
        navPos = $nav.offset().top,
        fixedClass = 'is-fixed';

    $win.on('load scroll', function() {
      var value = $(this).scrollTop();
      if ( value > navPos ) {
        $nav.addClass(fixedClass);
        $main.css('margin-top', navHeight);
      } else {
        $nav.removeClass(fixedClass);
        $main.css('margin-top', '0');
      }
    });
  });

</script>




<?php wp_footer(); ?>
</body>
</html>
