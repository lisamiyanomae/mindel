<?php 
/**
 * 	Template Name: お知らせ一覧ページテンプレート
 *
 */

get_header(); ?>


<main>
<div id="main">
	
<div id="topicpath" class="inner"><ul class="topic_path" itemprop="breadcrumb"><li itemscope="itemscope"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url"><span  itemprop="title">みんなにでんわ転送 TOPへ</span></a></li><li>&nbsp;&gt;&nbsp;</li><li class="current-crumb" itemscope="itemscope"><span itemprop="title">お知らせ一覧</span></li></ul></div>



<section id="oshirase">
<div class="inner">

<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>

<div class="post">
<ul class="postlist">
	<?php query_posts("post_type=post"); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<li><span><?php the_time('Y.m.d'); ?></span><p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
	<div class="column_txt">
		<?php
		if(mb_strlen($post->post_content, 'UTF-8')>150){
			$content= mb_substr(strip_tags($post->post_content), 0, 150, 'UTF-8');
			echo $content.'……';
		}else{
			echo strip_tags($post->post_content);
		}
		?>	
	</div>
	</li>
	<?php endwhile; endif; ?>
	<?php wp_reset_query(); ?>	
</ul>
</div>


<div id="pagenum">
<?php
if ($the_query->max_num_pages > 1) {
	echo paginate_links(array(
		'base' => get_pagenum_link(1) . '%_%',
		'format' => 'page/%#%/',
		'current' => max(1, $paged),
		'prev_text' => '«',
		'next_text' => '»',
		'total' => $the_query->max_num_pages
	));
}
?>
</div>


 
<?php wp_reset_postdata(); ?>
<?php the_content(); ?>

<?php wp_link_pages(); ?>
<?php endwhile; ?>
<?php else : ?>
	
<div class="section">
<p class="404">誠に申し訳ございませんが、お探しのページは見つかりませんでした。</p>
<p>お探しのページは一時的に利用できない状況にあるか、移動もしくは削除された可能性がございます。<br>
トップページからお探しください。</p>
</div>

<?php endif; ?>

</div>
</section>


</div><!--/End main-->
</main>




<?php get_footer(); ?>