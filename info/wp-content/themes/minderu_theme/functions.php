<?php
	/*-----------------------------------------------------------------------------------*/
	/* This file will be referenced every time a template/page loads on your Wordpress site
	/* This is the place to define custom fxns and specialty code
	/*-----------------------------------------------------------------------------------*/

define( 'NAKED_VERSION', 1.0 );

/*-----------------------------------------------------------------------------------*/
/*  Set the maximum allowed width for any content in the theme
/*-----------------------------------------------------------------------------------*/
if ( ! isset( $content_width ) ) $content_width = 900;

/*-----------------------------------------------------------------------------------*/
/* Add Rss feed support to Head section
/*-----------------------------------------------------------------------------------*/
add_theme_support( 'automatic-feed-links' );


/*-----------------------------------------------------------------------------------*/
/* Register main menu for Wordpress use
/*-----------------------------------------------------------------------------------*/
register_nav_menus( 
	array(
		'primary'	=>	__( 'Primary Menu', 'minderu' ),
	)
);

/*-----------------------------------------------------------------------------------*/
/* Activate sidebar for Wordpress use
/*-----------------------------------------------------------------------------------*/
function naked_register_sidebars() {
	register_sidebar(array(	
		'id' => 'sidebar', 
		'name' => 'Sidebar',
		'description' => 'Take it on the side...', 
		'before_widget' => '<div>',	
		'after_widget' => '</div>',	
		'before_title' => '<h3 class="side-title">',
		'after_title' => '</h3>',
		'empty_title'=> '',
	));
} 
// adding sidebars to Wordpress (these are created in functions.php)
add_action( 'widgets_init', 'naked_register_sidebars' );

/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function naked_scripts()  { 

	// get the theme directory style.css and link to it in the header
	wp_enqueue_style('font-awesome.min', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css');
	wp_enqueue_style('content', get_stylesheet_directory_uri() . '/contents.css');
	wp_enqueue_style('mobile', get_stylesheet_directory_uri() . '/mobile.css');	


}
add_action( 'wp_enqueue_scripts', 'naked_scripts' ); 



/*-------------------------------------------*/
/*  jquery
/*-------------------------------------------*/

/*
add_action('init', 'my_init');
function my_init() {
    if ( !is_admin() ) {
        wp_deregister_script('jquery');
    }
}
*/




/*-----------------------------------------------------------------------------------*/
/* head 非表示項目
/*-----------------------------------------------------------------------------------*/


//　head内（ヘッダー）から不要なコード削除
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);

//head内（ヘッダー）絵文字削除 
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles' );
remove_action('admin_print_styles', 'print_emoji_styles');

//head内（ヘッダー）Embed系の記述削除 
remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');
remove_action('template_redirect', 'rest_output_link_header', 11 );


function remove_recent_comments_style() {
 global $wp_widget_factory;
 remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'remove_recent_comments_style' );


/*-----------------------------------------------------------------------------------*/
/*<p>　<br>　が入らないようにする
/*-----------------------------------------------------------------------------------*/
function disable_page_wpautop() {
	if ( is_page() ) remove_filter( 'the_content', 'wpautop' );
}
add_action( 'wp', 'disable_page_wpautop' );


/*-----------------------------------------------------------------------------------*/
/*PHPファイルをインクルード
/*-----------------------------------------------------------------------------------*/

function Include_my_php($params = array()) {
    extract(shortcode_atts(array(
        'file' => 'default'
    ), $params));
    ob_start();
    include(get_theme_root() . '/' . get_template() . "/inc/$file.php");
    return ob_get_clean();
}  

add_shortcode('myphp', 'Include_my_php'); 






/*-----------------------------------------------------------------------------------*/
/* 画像ファイルのリンクパス
/*-----------------------------------------------------------------------------------*/

function replaceImagePath($arg) {
	$content = str_replace('"images/', '"' . get_bloginfo('template_directory') . '/images/', $arg);
	return $content;
}  
add_action('the_content', 'replaceImagePath');




/*-------------------------------------------*/
/*  パンくずリスト
/*-------------------------------------------*/


/**
 * パンくずナビゲーションを出力します。
 *
 * @param array $args {
 *      オプションです。パンくずリストの出力を配列型の引数で変更できます。
 *
 *      @type string    $container          パンくずリスト囲むタグを指定。デフォルトは'div'。
 *      @type string    $container_class    パンくずリストを囲むタグのClassを指定。デフォルトは'breadcrumb-section'。
 *      @type string    $container_id       パンくずリストを囲むタグのIDを指定。デフォルトは無し。
 *      @type string    $crumb_tag          パンくずリスト自体のタグを指定。デフォルトは'ul'で、他に指定できるのは'ol'のみ。
 *      @type string    $crumb_class        パンくずリスト自体のタグにClassを指定。デフォルトは'crumb-lists'。
 *      @type string    $crumb_id           パンくずリスト自体のタグにIDを指定。デフォルトは無し。
 *      @type bool      $echo               パンくずリストのHTMLを変数に格納する場合は'false'を指定。デフォルトは'true'なので直接出力する。
 *      @type string    $home_class         パンくずリストのホームの階層を示すタグにClassを指定。デフォルトは'crumb-home'。
 *      @type string    $home_text          パンくずリストのホームの階層を示すタグのテキストを指定。デフォルトは'ホーム'。
 *      @type string    $delimiter          パンくずリストの区切り文字を指定。デフォルトは'<li>&nbsp;&gt;&nbsp;</li>'。
 *      @type string    $crumb_microdata    パンくずリストタグ['ul' または 'ol']に指定するリッチスニペット。デフォルトは' itemprop="breadcrumb"'。
 *      @type string    $li_microdata       パンくずリストのliタグに指定するリッチスニペット。デフォルトは' itemscope itemtype="http://data-vocabulary.org/Breadcrumb"'。
 *      @type string    $url_microdata      パンくずリストのaタグに指定するリッチスニペット。デフォルトは' itemprop="url"'。
 *      @type string    $title_microdata    パンくずリストのspanタグに指定するリッチスニペット。デフォルトは' itemprop="title"'。
 * }
 *
 * @return string 各ページに合致するパンくずナビゲーションのHTMLを吐き出します。
 *
 * @version 1.3
 */
function breadcrumb( $args = array() ) {
	$defaults = array(
		'container'         => 'div',
		'container_class'   => 'inner',
		'container_id'      => 'topicpath',
		'crumb_tag'         => 'ul',
		'crumb_class'       => 'topic_path',
		'crumb_id'          => '',
		'echo'              => true,
		'home_class'        => '',
		'home_text'         => 'みんなにでんわ転送HOME',
		'delimiter'         => '<li>&nbsp;&gt;&nbsp;</li>',
		'crumb_microdata'   => ' itemprop="breadcrumb"',
		'li_microdata'      => ' itemscope="itemscope"',
		'url_microdata'     => ' itemprop="url"',
		'title_microdata'   => ' itemprop="title"'
	);

	$args = wp_parse_args( $args, $defaults );
	$args = (object) $args;

	$breadcrumb_html      = '';

	//region Rich Snippets (microdata) Setting
	$crumb_microdata    = $args->crumb_microdata ? $args->crumb_microdata : '';
	$li_microdata       = $args->li_microdata ? $args->li_microdata : '';
	$url_microdata      = $args->url_microdata ? $args->url_microdata : '';
	$title_microdata    = $args->title_microdata ? $args->title_microdata : '';
	//endregion

	//region Nested Function
	/**
	 * 現在のページのパンくずリスト用タグを作成します。
	 *
	 * @param $current_permalink : current crumb permalink
	 * @param string $current_text : current crumb text
	 * @param string $current_class : class name
	 * @param array $args : microdata settings
	 *
	 * @return string
	 */
	/*
	 * Nest Function [current_crumb_tag()] Argument
	 */
	$current_microdata = array(
		'li_microdata'      => $li_microdata,
		'url_microdata'     => $url_microdata,
		'title_microdata'   => $title_microdata
	);
	function current_crumb_tag( $current_permalink, $current_text = '', $args = array(), $current_class = 'current-crumb' ) {
		$defaults = array(
			'li_microdata'      => ' itemscope="itemscope"',
			'url_microdata'     => ' itemprop="url"',
			'title_microdata'   => ' itemprop="title"'
		);

		$args = wp_parse_args( $args, $defaults );
		$args = (object) $args;

		$current_class      = $current_class ? ' class="' . esc_attr( $current_class ) . '"' : '';
		$start_anchor_tag   = $current_permalink ? '<a href="' . $current_permalink . '"' . $args->url_microdata . '>' : '<span class="crumb-no-link">';
		$end_anchor_tag     = $current_permalink ? '</a>' : '</span>';

		$current_before     = '<li' . $current_class . $args->li_microdata . '>' . $start_anchor_tag . '<span' . $args->title_microdata . '>';
		$current_crumb_tag  = $current_text;
		$current_after      = '</span>' . $end_anchor_tag . '</li>';

		if ( get_query_var( 'paged' ) ) {
			if ( is_paged() || is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) {
				$current_after = ' (ページ' . get_query_var( 'paged' ) . ')' . $current_after;
			}
		} elseif ( ( is_page() || is_single() ) && get_query_var( 'page' ) ) {
			$current_after = ' (ページ' . get_query_var( 'page' ) . ')' . $current_after;
		}

		return $current_before . $current_crumb_tag . $current_after;
	}
	//endregion

	if (
		( ! is_home() && ! is_front_page() )
		|| ( is_home() && ! is_front_page() )
		|| is_paged()
	) {

		// Breadcrumb Container Start Tag
		if ( $args->container ) {
			$class = $args->container_class ? ' class="' . esc_attr( $args->container_class ) . '"' : ' class="' . $defaults['container_class'] . '"';
			$id = $args->container_id ? ' id="' . esc_attr( $args->container_id ) . '"' : '';
			$breadcrumb_html .= '<'. $args->container . $id . $class . '>';
		}

		// Breadcrumb Start Tag
		if ( $args->crumb_tag ) {
			$crumb_tag_allowed_tags = apply_filters( 'crumb_tag_allowed_tags', array( 'ul', 'ol' ) );
			if ( in_array( $args->crumb_tag, $crumb_tag_allowed_tags ) ) {
				$id = $args->crumb_id ? ' id="' . esc_attr( $args->crumb_id ) . '"' : '';
				$class = $args->crumb_class ? ' class="' . esc_attr( $args->crumb_class ) . '"' : '';
				$breadcrumb_html .= '<' . $args->crumb_tag . $id . $class . $crumb_microdata . '>';
			}
		} else {
			$breadcrumb_html .= '<' . $defaults['crumb_tag'] .  $crumb_microdata . '>';
		}

		global $post;

		// Home Crumb Item
		$home_class = $args->home_class ? ' class="'. esc_attr( $args->home_class ) . '"' : '';
		$breadcrumb_html .= '<li'. $home_class . $li_microdata . '><a href="' . home_url() . '"' . $url_microdata . '><span ' . $title_microdata . '>' . $args->home_text . '</span></a></li>' . $args->delimiter;

		if ( is_home() && ! is_front_page() ) {
			$home_ID = get_option('page_for_posts');
			$breadcrumb_html .= current_crumb_tag( get_the_permalink( $home_ID ), get_the_title( $home_ID ), $current_microdata );

		} else if ( is_paged() ) {
			if ( 'post' == get_post_type() ) {
				$breadcrumb_html .= current_crumb_tag( get_pagenum_link( get_query_var( 'paged' ) ), '投稿一覧', $current_microdata );

			}
			elseif ( 'page' == get_post_type() ) {
				$breadcrumb_html .= current_crumb_tag( get_pagenum_link( get_query_var( 'paged' ) ), get_the_title(), $current_microdata );
			}
//			else {
//				$breadcrumb_html .= $current_before . get_post_type_object( get_post_type() )->label . $current_after;
//			}


		} elseif ( is_category() ) {
			$cat = get_queried_object();
			if ( $cat->parent != 0 ) {
				$ancestors = array_reverse( get_ancestors( $cat->cat_ID, 'category' ) );
				foreach ( $ancestors as $ancestor ) {
					$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_category_link( $ancestor ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_cat_name( $ancestor ) . '</span></a></li>' . $args->delimiter;
				}
			}
			$breadcrumb_html .= current_crumb_tag( get_category_link( $cat->term_id ), single_cat_title( '', false ), $current_microdata );

		} elseif ( is_day() ) {
			$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_year_link( get_the_time( 'Y' ) ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_time( 'Y' ) . '年</span></a></li>' . $args->delimiter;
			$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_time( 'F' ) . '</span></a></li>' . $args->delimiter;
			$breadcrumb_html .= current_crumb_tag( get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ), get_the_time( 'd' ) . '日', $current_microdata );

		} elseif ( is_month() ) {
			$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_year_link( get_the_time( 'Y' ) ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_time( 'Y' ) . '年</span></a></li>' . $args->delimiter;
			$breadcrumb_html .= current_crumb_tag( get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), get_the_time( 'F' ), $current_microdata );

		} elseif ( is_year() ) {
			$breadcrumb_html .= current_crumb_tag( get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) . '年', $current_microdata );

		} elseif ( is_single() && ! is_attachment() ) {
			$single = get_queried_object();

			if ( get_post_type() == 'post' ) {
				if ( get_option( 'page_for_posts' ) ) {
					$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_page_link( get_option( 'page_for_posts' ) ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_title( get_option( 'page_for_posts' ) ) . '</span></a></li>' . $args->delimiter;
				}

				$categories = get_the_category( $post->ID );
				$cat        = $categories[0];

				if ( $cat->parent != 0 ) {
					$ancestors = array_reverse( get_ancestors( $cat->cat_ID, 'category' ) );

					foreach ( $ancestors as $ancestor ) {
						$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_category_link( $ancestor ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_cat_name( $ancestor ) . '</span></a></li>' . $args->delimiter;
					}
				}

				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_category_link( $cat->cat_ID ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_cat_name( $cat->cat_ID ) . '</span></a></li>' . $args->delimiter;

				$breadcrumb_html .= current_crumb_tag( get_the_permalink( $single->ID ), get_the_title( $single->ID ), $current_microdata );

			} else {
				$post_type_object = get_post_type_object( get_post_type() );
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( get_post_type() ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $post_type_object->label . '</span></a></li>' . $args->delimiter;

				$taxonomies =  get_object_taxonomies( get_post_type() );
				$category_term = '';
				foreach ( $taxonomies as $taxonomy ) {
					$taxonomy_obj = get_taxonomy( $taxonomy );
					if ( true == $taxonomy_obj->hierarchical ) {
						$category_term = $taxonomy_obj;
						break;
					}
				}

                if ( $category_term ) {
                    $terms = get_the_terms( $post->ID, $category_term->name );
                    if ( $terms ) {
                        if ( ! $terms || is_wp_error( $terms ) )
                            $terms = array();

                        $terms = array_values( $terms );

                        $term = $terms[0];
                        if ( $term->parent != 0 ) {
                            $ancestors = array_reverse( get_ancestors( $term->term_id, $term->taxonomy ) );
                            foreach ( $ancestors as $ancestor ) {
                                $breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_term_link( $ancestor, $term->taxonomy ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_term( $ancestor, $term->taxonomy )->name . '</span></a></li>' . $args->delimiter;
                            }
                        }

                        $breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_term_link( $term, $term->taxonomy ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . $term->name . '</span></a></li>' . $args->delimiter;
                    }
                }

				$breadcrumb_html .= current_crumb_tag( get_the_permalink( $single->ID ), get_the_title( $single->ID ), $current_microdata );

			}

		} elseif ( is_attachment() ) {
			$attachment = get_queried_object();

			if ( ! empty( $post->post_parent ) ) {
				$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_permalink( $post->post_parent ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_title( $post->post_parent ) . '</span></a></li>' . $args->delimiter;
			}
			$breadcrumb_html .= current_crumb_tag( get_the_permalink( $attachment->ID ), get_the_title( $attachment->ID ), $current_microdata );

		} elseif ( is_page() ) {
			$page = get_queried_object();
			if ( $post->post_parent ) {
				$ancestors = array_reverse( get_post_ancestors( $post->ID ) );
				foreach ( $ancestors as $ancestor ) {
					$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_permalink( $ancestor ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_the_title( $ancestor ) . '</span></a></li>' . $args->delimiter;
				}
			}
			$breadcrumb_html .= current_crumb_tag( get_the_permalink( $page->ID ), get_the_title( $page->ID ), $current_microdata );

		} elseif ( is_search() ) {
			$breadcrumb_html .= current_crumb_tag( get_search_link(), get_search_query() . '" の検索結果', $current_microdata );

		} elseif ( is_tag() ) {
			$tag = get_queried_object();
			$breadcrumb_html .= current_crumb_tag( get_term_link( $tag->term_id, $tag->taxonomy ), single_tag_title( '', false ), $current_microdata );

		} elseif ( is_tax() ) {
			$taxonomy_name  = get_query_var( 'taxonomy' );
			$post_types = get_taxonomy( $taxonomy_name )->object_type;
			$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_post_type_archive_link( $post_types[0] ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_post_type_object( $post_types[0] )->label . '</span></a></li>' . $args->delimiter;

			$tax = get_queried_object();
			if ( $tax->parent != 0 ) {
				$ancestors = array_reverse( get_ancestors( $tax->term_id, $tax->taxonomy ) );
				foreach ( $ancestors as $ancestor ) {
					$breadcrumb_html .= '<li' . $li_microdata . '><a href="' . get_term_link( $ancestor, $tax->taxonomy ) . '"' . $url_microdata . '><span' . $title_microdata . '>' . get_term( $ancestor, $tax->taxonomy )->name . '</span></a></li>' . $args->delimiter;
				}
			}

			$breadcrumb_html .= current_crumb_tag( get_term_link( $tax->term_id, $tax->taxonomy ), single_tag_title( '', false ), $current_microdata );

		} elseif ( is_author() ) {
			$author = get_queried_object();
			$breadcrumb_html .= current_crumb_tag( get_author_posts_url( $author->ID ), get_the_author_meta( 'display_name' ), $current_microdata );

		} elseif ( is_404() ) {
			$breadcrumb_html .= current_crumb_tag( null, '404 Not found' );

		} elseif ( is_post_type_archive( get_post_type() ) ) {
		    // elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' && ! is_search() )
			if ( false == get_post_type() ) {
				$post_type_obj = get_queried_object();
				$breadcrumb_html .= current_crumb_tag( $post_type_obj->name, $post_type_obj->label, $current_microdata );

			} else {
				$post_type_obj = get_post_type_object( get_post_type() );
				$breadcrumb_html .= current_crumb_tag( get_post_type_archive_link( get_post_type() ), $post_type_obj->label, $current_microdata );
			}

		} else {
			$breadcrumb_html .= current_crumb_tag( site_url(), wp_title( '', true ), $current_microdata );
		}

	}

	// Breadcrumb End Tag
	if ( $args->crumb_tag ) {
		$crumb_tag_allowed_tags = apply_filters( 'crumb_tag_allowed_tags', array( 'ul', 'ol' ) );
		if ( in_array( $args->crumb_tag, $crumb_tag_allowed_tags ) ) {
			$breadcrumb_html .= '</' . $args->crumb_tag . '>';
		}
	} else {
		$breadcrumb_html .= '</' . $defaults['crumb_tag'] . '>';
	}

	// Breadcrumb Container End Tag
	if ( $args->container ) {
		$breadcrumb_html .= '</' . $args->container . '>';
	}

	if ( $args->echo )
		echo $breadcrumb_html;
	else
		return $breadcrumb_html;
}



/*-------------------------------------------*/
/*  アイキャッチ画像を登録
/*-------------------------------------------*/
add_theme_support('post-thumbnails'); 


/*-------------------------------------------*/
/*  カスタム投稿 アイキャッチ画像を登録
/*-------------------------------------------*/

add_theme_support( 'post-thumbnails', array( 'post','partyreport' ));
add_theme_support( 'post-thumbnails', array( 'post','plan' ));



/*-----------------------------------------------------------------------------------*/
/*管理画面使わないメニューを非表示
/*-----------------------------------------------------------------------------------*/
function remove_menus () {
    global $menu;
    unset($menu[25]); // コメント
}
add_action('admin_menu', 'remove_menus');



/*-----------------------------------------------------------------------------------*/
/*メディア挿入時　不要な属性を削除
/*-----------------------------------------------------------------------------------*/
add_filter( 'image_send_to_editor', 'remove_image_attribute', 10 );
add_filter( 'post_thumbnail_html', 'remove_image_attribute', 10 );

function remove_image_attribute( $html ){
	$html = preg_replace( '/(width|height)="\d*"\s/', '', $html );
	$html = preg_replace( '/class=[\'"]([^\'"]+)[\'"]/i', '', $html );
	$html = preg_replace( '/title=[\'"]([^\'"]+)[\'"]/i', '', $html );
//	$html = preg_replace( '/<a href=".+">/', '', $html );
//	$html = preg_replace( '/<\/a>/', '', $html );
	return $html;
}



/*-----------------------------------------------------------------------------------*/
/*メディア挿入時　<div>で囲う
/*-----------------------------------------------------------------------------------*/
function my_image_send_to_editor( $html, $id, $caption, $title, $align, $url, $size ) { 
    $html = '<div class="align_center">' .$html .'</div>'; 
    return $html;
}
 
add_action( 'image_send_to_editor', 'my_image_send_to_editor', 10 ,7);



/*-------------------------------------------*/
/* 編集画面 [ビジュアル]?［テキスト］入れ替えると p div が消えるのを制御
/*-------------------------------------------*/

function custom_editor_settings( $initArray ){
	$initArray['body_id'] = 'primary';	// id の場合はこれ
	$initArray['body_class'] = 'post';	// class の場合はこれ
	// styleや、divの中のdiv,span、spanの中のspanを消させない
	$initArray['valid_children'] = '+body[style],+div[div|span],+span[span]';
	// 空タグや、属性なしのタグとか消そうとしたりするのを停止。余計なことすんな！
	$initArray['verify_html'] = false;
	// []←ショートコードを&#91;とかのままにしたかったけど出来なかった…。
	//$initArray['entity_encoding'] = 'raw';
	//$initArray['entities'] = '91,93';
	return $initArray;
}
add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );



/*-------------------------------------------*/
/*  ビジュアルエディタ非表示
/*-------------------------------------------*/
function disable_visual_editor_in_page(){
	global $typenow;
	if( $typenow == 'page' ){
		add_filter('user_can_richedit', 'disable_visual_editor_filter');
	}
}
function disable_visual_editor_filter(){
	return false;
}
add_action( 'load-post.php', 'disable_visual_editor_in_page' );
add_action( 'load-post-new.php', 'disable_visual_editor_in_page' );




/*-------------------------------------------*/
/*　ページネーション
/*-------------------------------------------*/


//Pagenation
function pagination($pages = '', $range = 2)
{
     $showitems = ($range * 2)+1;//表示するページ数（５ページを表示）

     global $paged;//現在のページ値
     if(empty($paged)) $paged = 1;//デフォルトのページ

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;//全ページ数を取得
         if(!$pages)//全ページ数が空の場合は、１とする
         {
             $pages = 1;
         }
     }

     if(1 != $pages)//全ページが１でない場合はページネーションを表示する
     {
		 echo "<div class=\"pagenation\">\n";
		 echo "<ul>\n";
		 //Prev：現在のページ値が１より大きい場合は表示
         if($paged > 1) echo "<li class=\"prev\"><a href='".get_pagenum_link($paged - 1)."'>Prev</a></li>\n";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                //三項演算子での条件分岐
                echo ($paged == $i)? "<li class=\"active\">".$i."</li>\n":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>\n";
             }
         }
		//Next：総ページ数より現在のページ値が小さい場合は表示
		if ($paged < $pages) echo "<li class=\"next\"><a href=\"".get_pagenum_link($paged + 1)."\">Next</a></li>\n";
		echo "</ul>\n";
		echo "</div>\n";
     }
}



/*-------------------------------------------*/
/* 管理画面メニュー名変更
/*-------------------------------------------*/


function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'お知らせを投稿';
    $submenu['edit.php'][5][0] = 'お知らせ一覧';
    $submenu['edit.php'][10][0] = '新規追加';
    $submenu['edit.php'][16][0] = 'タグ';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'お知らせ';
    $labels->singular_name = 'お知らせ';
    $labels->add_new = '新規追加';
    $labels->add_new_item = 'お知らせを追加';
    $labels->edit_item = 'お知らせの編集';
    $labels->new_item = 'お知らせ';
    $labels->view_item = 'お知らせの表示';
    $labels->search_items = 'お知らせを検索';
    $labels->not_found = 'お知らせが見つかりませんでした。';
    $labels->not_found_in_trash = 'ゴミ箱内にお知らせが見つかりませんでした。';
    $labels->all_items = '全てのお知らせ';
    $labels->menu_name = 'お知らせ';
    $labels->name_admin_bar = 'お知らせ';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );






/*-------------------------------------------*/
/* 固定ページの親ページと子ページをスラッグで判定する
/*-------------------------------------------*/

function is_parent_slug() {
  global $post;
  if ($post->post_parent) {
    $post_data = get_post($post->post_parent);
    return $post_data->post_name;
  }
}


/*-------------------------------------------*/
/*　ショートコードを使えるようにする
/*-------------------------------------------*/
add_filter('widget_text', 'do_shortcode');

function getheadvisual() {
	$areaname = get_post_meta(get_the_ID(), 'headvisual', true);
	return $areaname;
}
add_shortcode('viewAheadvisual', 'getheadvisual');



/*-------------------------------------------*/
/*　レンダリングブロックしているJavascriptの読み込みを遅らせる
/*-------------------------------------------*/

function move_scripts_head_to_footer_ex(){
  //ヘッダーのスクリプトを取り除く
  remove_action('wp_head', 'wp_print_scripts');
  remove_action('wp_head', 'wp_print_head_scripts', 9);
  remove_action('wp_head', 'wp_enqueue_scripts', 1);

  //フッターにスクリプトを移動する
  add_action('wp_footer', 'wp_print_scripts', 5);
  add_action('wp_footer', 'wp_print_head_scripts', 5);
  add_action('wp_footer', 'wp_enqueue_scripts', 5);
}
add_action( 'wp_enqueue_scripts', 'move_scripts_head_to_footer_ex' );


/*-------------------------------------------*/
/*　検索窓 ショートコード
/*-------------------------------------------*/

add_shortcode('search_form', function() {
	return get_search_form(false);
});



/*-------------------------------------------*/
/*   カスタム投稿を設定
/*-------------------------------------------*/

add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts() {

	$args = array(
		"label" => __( 'よくある質問', '' ),
		"labels" => 'Q＆A一覧',
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"all_items" => 'Q＆A一覧',
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "faq", "with_front" => false ),
		"query_var" => false,
		'menu_position' => 5,
				
		"supports" => array( "title", "editor", "thumbnail" ),				
	);
//	flush_rewrite_rules(false);
	register_post_type( "faq", $args );
	
// End of cptui_register_my_cpts()
}


add_action( 'init', 'cptui_register_my_taxes' );
function cptui_register_my_taxes() {
	$labels = array(
		"name" => __( 'Q＆Aカテゴリー', '' ),
		"singular_name" => __( 'カテゴリー', '' ),
		);

	$args = array(
		"label" => __( 'Q＆Aカテゴリー', '' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Q＆Aカテゴリー",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'faq_cate', 'with_front' => true ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "faq_cate", array( "faq" ), $args );
	
// End cptui_register_my_taxes()
}





//
//カスタム分類アーカイブ用のリライトルールを追加
add_rewrite_rule('faq/([^/]+)/?$', 'index.php?faq_cate=$matches[1]', 'top');
add_rewrite_rule('faq/([^/]+)/page/([0-9]+)/?$', 'index.php?faq_cate=$matches[1]&paged=$matches[2]', 'top');



