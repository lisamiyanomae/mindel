<?php
$rss = fetch_feed('http://www.sapporo-u.ac.jp/news/graduate/rss.xml'); // RSSのURLを指定
if (!is_wp_error( $rss ) ) :
	$maxitems = $rss->get_item_quantity(3); // 表示する記事の最大件数
	$rss_items = $rss->get_items(0, $maxitems); 
endif;
?>

<?php
if ($maxitems == 0): echo '<li>表示するものががありません</li>';
else :
date_default_timezone_set('Asia/Tokyo');
foreach ( $rss_items as $item ) : ?>
<li>
<a href="<?php echo $item->get_permalink(); ?>" rel="bookmark">
<span class="hentry-date"><?php echo $item->get_date('Y.n.j'); ?></span>
<span class="entry-title"><?php echo $item->get_title(); ?></span>　<i class="fa fa-external-link" aria-hidden="true"></i>
</a>
</li>
<?php endforeach; ?>
<?php endif; ?>

