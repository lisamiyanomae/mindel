<?php
/**
 * The template for displaying any single page.
 *
 */

get_header(); ?>

<?php breadcrumb(); ?>

<main>
<div id="main">


<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>

<?php endwhile; ?>
<?php else : ?>

<section>
	<div class="inner">
		<p class="404">誠に申し訳ございませんが、お探しのページは見つかりませんでした。</p>
		<p>お探しのページは一時的に利用できない状況にあるか、移動もしくは削除された可能性がございます。<br>
		トップページからお探しください。</p>
		<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>">みんなにでんわ転送トップへ</a></p>
	</div>
</section>

<?php endif; ?>



</div><!--/End main-->
</main>



<?php get_footer(); ?>
