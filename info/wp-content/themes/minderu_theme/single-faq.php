<?php
/**
 * The template for displaying any single post.
 *
 */

get_header(); ?>

<main>
<div id="main">
<?php breadcrumb(); ?>


<div class="section">
<div id="qa" class="inner">
<h2>よくある質問</h2>

<div class="answer">

<h3>
		<?php
		//記事IDとタクソノミーを指定してタームを取得
		$product_terms = wp_get_object_terms($post->ID, 'faq_cate');
		
		//タームを出力
		if(!empty($product_terms)){
		  if(!is_wp_error( $product_terms )){
		    foreach($product_terms as $term){
		      echo '<span>'.$term->name.'</span>'; 
		    }
		  }
		}
		
		?>	
</h3>


<?php
	$cat = get_the_category();
	$cat = $cat[0];
?>



		
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>


<dl class="yokuaru_qa">	
<dt><p><?php the_title(); ?></p></dt>
<dd><?php the_content(); ?></dd>
</dl>

			
<?php wp_link_pages(); ?>

</div><!--/answer-->



<div id="qa_taxoList">					
<p class="ttl_qa_taxoList">よくある質問 - 		<?php
		//記事IDとタクソノミーを指定してタームを取得
		$product_terms = wp_get_object_terms($post->ID, 'faq_cate');
		
		//タームを出力
		if(!empty($product_terms)){
		  if(!is_wp_error( $product_terms )){
		    foreach($product_terms as $term){
		      echo '<span>'.$term->name.'</span>'; 
		    }
		  }
		}
		
		?> - 目次
</p>
<ul>

<?php
 global $post;
 $term = array_shift(get_the_terms($post->ID, 'faq_cate')); //←ここが追加
 $args = array(
  'numberposts' => 30, //30件表示(デフォルトは５件)
  'post_type' => 'faq', //カスタム投稿タイプ名
  'taxonomy' => 'faq_cate', //タクソノミー名
  'term' => $term->slug, //ターム名 ←ここが追加
  'orderby' => 'rand', //ランダム表示
  'post__not_in' => array($post->ID) //表示中の記事を除外
 );
?>
<?php $myPosts = get_posts($args); if($myPosts) : ?>
<?php foreach($myPosts as $post) : setup_postdata($post); ?>
<li id="post-<?php the_ID(); ?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>　<a href="<?php the_permalink(); ?>"><?php
		 if(mb_strlen($post->post_title)>35) { $title= mb_substr($post->post_title,0,35) ; echo $title. ･･･ ;
		} else {echo $post->post_title;}?></a></a>
		    </li>
<?php endforeach; ?>
<?php else : ?>
 <p>関連項目はまだありません。</p>
<?php endif; wp_reset_postdata(); ?>


</ul>
</div>					




<?php endwhile; ?>
<?php endif; ?>



</div><!--/inner-->
</div><!--/qa-->
</section>


</div><!--/End main-->
</main>




<?php get_footer(); ?>
