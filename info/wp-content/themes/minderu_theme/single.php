<?php
/**
 * The template for displaying any single post.
 *
 */
get_header(); ?>


<main>
<div id="main">
	
<div id="topicpath" class="inner"><ul class="topic_path" itemprop="breadcrumb"><li itemscope="itemscope"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url"><span  itemprop="title">みんなにでんわ転送 TOPへ</span></a></li><li>&nbsp;&gt;&nbsp;</li><li class="current-crumb" itemscope="itemscope"><a href="<?php echo esc_url( home_url( '/' ) ); ?>news/" itemprop="url"><span itemprop="title">お知らせ一覧</span></a></li><li>&nbsp;&gt;&nbsp;</li><li class="current-crumb" itemscope="itemscope"><span itemprop="title"><?php the_title(); ?></span></li></ul></div>


	
<section id="oshirase">
	<div class="inner">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		       
		<div class="post">
			<h1 class="h1ttl"><?php the_title(); ?></h1> 		
			<div class="entry">
				<p class="postdata"><?php the_time('Y.m.d'); ?></p>
				<?php the_content(); ?>

			</div>
		</div><!--post-->
		
		<div class="pagerBox">  
		<span class="prev"><?php previous_post_link('%link','前へ ') ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class="next"><?php next_post_link('%link',' 次へ') ?></span>
		</div>
		
		
		<?php endwhile; else: ?> 
		<div class="section">
			<div class="inner">
				<p class="404">誠に申し訳ございませんが、お探しのページは見つかりませんでした。</p>
				<p>お探しのページは一時的に利用できない状況にあるか、移動もしくは削除された可能性がございます。<br>
				トップページからお探しください。</p>
				<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>">みんなにでんわ転送トップへ</a></p>
			</div>
		</div>
		<?php endif; ?>
	</div>
</section>


</div><!--/End main-->
</main>




<?php get_footer(); ?>
