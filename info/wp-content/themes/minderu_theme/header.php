<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to begin 
	/* rendering the page and display the header/nav
	/*-----------------------------------------------------------------------------------*/
?>

<!doctype html>
<html lang="ja" class="no-js">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118320121-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118320121-1');
</script>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="format-detection" content="telephone=no">

<script>
if ((navigator.userAgent.indexOf('iPhone') > 0) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('Android') > 0) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes,maximum-scale=1.0,minimum-scale=1.0">');
    }else{
        document.write('<meta name="viewport" content="width=1200px">');
    }
</script>


<link rel="stylesheet" href="//fonts.googleapis.com/earlyaccess/notosansjapanese.css">


<link rel="icon" type="image/x-icon" href="<?php echo esc_url( home_url( '/' ) ); ?>favicon.ico">
<link rel="icon" type="image/png" href="<?php echo esc_url( home_url( '/' ) ); ?>apple-touch-icon.png" sizes="180x180">
<link rel="apple-touch-icon" href="<?php echo esc_url( home_url( '/' ) ); ?>webclip.png">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon57.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url( home_url( '/' ) ); ?>apple-touch-icon180.png">



<!--[if lt IE 9]>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
<![endif]-->


<?php wp_head(); ?>
</head>



<body id="bodytop">


<header>
<div id="header">
	<h1 class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="みんなにでんわ転送 | クラウド電話転送サービス" /></a></h1>
	<div id="headinfo">
		<ul class="inner">
			<li id="new_account">
				<div class="customercenter">
					<p class="customer_time">カスタマーセンター（月～金曜日9:00-18:00）</p>
					<p class="customer_tel"><a href="tel:05031963580">050-3196-3580</a></p>
				</div>
				<div class="contact_mail"><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact/">メールでお問合せ</a></div>
				<div class="entry"><a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage/users/add/">サービスに申し込む</a></div>
			</li>
			<li id="login"><a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage/users/login/">ユーザーログイン</a></li>
		</ul>
	</div>
</div>
</header>


<?php 
// アイキャッチ画像のIDを取得
$thumbnail_id = get_post_thumbnail_id(); 
 
// mediumサイズの画像内容を取得（引数にmediumをセット）
$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
 
// 取得した画像URLにてイメージタグを出力
// 更にdata-aliasというHTML5のカスタムデータ属性を追加
echo '<div id="pageVisual" style="background-image:url('.$eye_img[0].');">';　
?>

</div><!--/pageVisual-->


<div id="global-nav">
<nav>
	<div class="inner">
			<ul>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">トップ</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>service/">サービス内容</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>price/">ご利用料金</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>faq/">よくある質問</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact/">お問い合わせ</a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage/users/add/">サービス申込み</a></li>
			</ul>
			<div class="h_new_account"><a href="<?php echo esc_url( home_url( '/' ) ); ?>mypage/users/add/">サービスに申し込む</a></div>
	</div>
</nav>
</div><!--/global-nav-->





