<?php
/**
 * The template for displaying the home/index page.
 * This template will also be called in any case where the Wordpress engine 
 * doesn't know which template to use (e.g. 404 error)
 */

get_header(); ?>

<main>
<div id="main">
<?php breadcrumb(); ?>


<div class="section">
<div id="qa" class="inner">
<h2>よくある質問</h2>

<?php
	$cat = get_the_category();
	$cat = $cat[0];
?>

<div id="qaList">
<h3><?php
$taxonomy = $wp_query->get_queried_object();
echo esc_html($taxonomy->name);
			?></h3>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
	<ul>
		<?php if ($posts) : foreach ($posts as $post) : start_wp(); ?>
		    <li id="post-<?php the_ID(); ?>"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>　<a href="<?php the_permalink(); ?>"><?php
		 if(mb_strlen($post->post_title)>35) { $title= mb_substr($post->post_title,0,35) ; echo $title. ･･･ ;
		} else {echo $post->post_title;}?></a></a>
		    </li>
		    
		    <?php endforeach; else: ?>
		    <li class="post span9">
		    <div><h5>お探しの記事は見つかりませんでした</h5></div>
		    </li>
		    <?php endif; ?>
	</ul>
</div>



<?php endwhile; ?>
<?php endif; ?>



</div><!--/inner-->
</div><!--/qa-->
</section>


</div><!--/End main-->
</main>




<?php get_footer(); ?>