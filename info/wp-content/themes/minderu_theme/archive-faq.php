<?php
/**
 * The template for displaying any single page.
 *
 */

get_header(); ?>

<main>
<div id="main">
<?php breadcrumb(); ?>


<div class="section">
<div id="qa" class="inner">
<h2>よくある質問</h2>
<div class="searchform">
<form>
<div>
	<label class="screen-reader-text" for="s">質問内容を入力してください:</label>
	<input type="search" name="s">
	<input type="hidden" name="cat" value="カテゴリーID">
	<input type="submit" value="検索">
</div>
</form>
</div>


<div id="qaList" class="row">
<div>
<h3>機能について</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_function',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_function/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「機能について」一覧を見る</a></p>-->
</div><!--/機能について-->

<div>
<h3>接続先について</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_accesspoint',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_accesspoint/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「接続先について」一覧を見る</a></p>-->
</div><!--/接続先について-->

<div>
<h3>申込について</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_apply',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_apply/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「申込について」一覧を見る</a></p>-->
</div><!--/申込について-->

<div>
<h3>審査について</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_exam',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_exam/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「審査について」一覧を見る</a></p>-->
</div><!--/審査について-->

<div>
<h3>料金について</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_fee',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_fee/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「料金について」一覧を見る</a></p>-->
</div><!--/料金について-->

<div>
<h3>使用方法について</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_usage',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_usage/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「使用方法・初期設定について」一覧を見る</a></p>-->
</div><!--/使用方法・初期設定について-->


<div>
<h3>初期設定について</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_initialsetting',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_usage/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「使用方法・初期設定について」一覧を見る</a></p>-->
</div><!--/使用方法・初期設定について-->



<div>
<h3>マイページでできる設定</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_mypage',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_mypage/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「使用方法・マイページでできる設定」一覧を見る</a></p>-->
</div><!--/使用方法・マイページでできる設定-->

<div>
<h3>変更手続きについて</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_procedure',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_procedure/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「変更手続きについて」一覧を見る</a></p>-->
</div><!--/変更手続きについて-->

<div>
<h3>お支払いについて</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_payment',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_payment/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「お支払いについて」一覧を見る</a></p>-->
</div><!--/お支払いについて-->

<div>
<h3>解約・休止</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_cancel',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_cancel/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「解約・休止」一覧を見る</a></p>-->
</div><!--/解約・休止-->

<div>
<h3>トラブルシューティング</h3>
	<ul>
		<?php
		$args = array(
		'post_type' => 'faq',
		'taxonomy' => 'faq_cate',
		'term' => 'qa_trouble',
		'posts_per_page' => 30,
		'numberposts' => '-1',
		);
		$my_posts = get_posts($args);
		foreach ( $my_posts as $post ) {
		setup_postdata($post); ?>
		<li class="icon"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
		<?php
		}
		?>
	</ul>
<!--<p class="btn_qaList"><a href="<?php echo home_url(); ?>/faq/qa_trouble/"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 「トラブルシューティング」一覧を見る</a></p>-->
</div><!--/トラブルシューティング-->


</div><!--/qaList-->
		

</div><!--/inner-->
</div><!--/qa-->
</section>


</div><!--/End main-->
</main>




<?php get_footer(); ?>