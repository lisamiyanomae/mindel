<?php
/**
 * The template for displaying any single page.
 *
 */

get_header(); ?>

<?php breadcrumb(); ?>

<main>
<div id="main">


<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>

<?php endwhile; ?>
<?php else : ?>

<div class="section">
<div id="searchlist" class="inner">
<h2>ページが見つかりません</h2>
<p class="404">誠に申し訳ございませんが、お探しのページは見つかりませんでした。</p>
<p class="mb30">お探しのページは一時的に利用できない状況にあるか、移動もしくは削除された可能性がございます。<br>
トップページからお探しください。</p>
<div class="submitbtn registration"><a class="button darkblue" href="<?php echo esc_url( home_url( '/' ) ); ?>">みんなにでんわ転送トップへ</a></div>

</div>
</div>

<?php endif; ?>



</div><!--/End main-->
</main>



<?php get_footer(); ?>
