<?php get_header(); ?>


<div id="topicpath" class="inner"><ul class="topic_path" itemprop="breadcrumb"><li itemscope="itemscope"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url"><span  itemprop="title">みんなにでんわ転送 TOPへ</span></a></li><li>&nbsp;&gt;&nbsp;</li><li class="current-crumb" itemscope="itemscope"><span itemprop="title">検索結果一覧</span></li></ul></div>

<main>
<div id="main">
	

<div class="section">
<div id="searchlist" class="inner">

<h2>検索結果一覧</h2>
<h3>『<?php echo get_search_query(); ?>』での検索結果一覧</h3>
<?php if(have_posts()): ?>
<?php while(have_posts()): the_post(); ?>
<dl>
<dt><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dt>
<dd><?php the_excerpt(); ?></dd>
</dl>

<?php endwhile; ?>
<?php else : ?>
<p>検索条件にヒットした記事がありませんでした。</p>
<?php endif; ?>

</div><!--/searchlist-->
</div><!--/section-->


</div><!--/End main-->
</main>




<?php get_footer(); ?>